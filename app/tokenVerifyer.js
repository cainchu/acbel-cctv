const jwt = require('jsonwebtoken');
const TOKEN_KEY = 'My-cctv';

exports.generateTokenSync = generateTokenSync;
exports.decodTokenSync = decodTokenSync;

/**
 * Token生成
 * @param {object} permission - 權限 A~E
 * @param {string} expiresIn - 有效時間 def 1D
 * @returns
 */
function generateTokenSync(meta, expiresIn = '1d') {
  return new Promise((resolve, reject) => {
    jwt.sign(
      meta,
      TOKEN_KEY,
      {
        expiresIn,
      },
      (err, token) => {
        if (err) {
          reject(err);
          return;
        }
        resolve(token);
      },
    );
  });
}

/**
 * 解碼
 * @param {string} token
 */
function decodTokenSync(token) {
  return new Promise((resolve, reject) => {
    jwt.verify(token, TOKEN_KEY, (err, decoded) => {
      if (err) {
        reject(err);
        //jwt expired
        //invalid signature
        //jwt must be provided
        //jwt malformed
        return;
      }
      resolve(decoded);
    });
  });
}
