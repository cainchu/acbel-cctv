if (process.env.NODE_ENV !== 'production') {
  require('dotenv').config()
}
const PORT = process.env.PORT || 4000
const DOMAIN = process.env.DOMAIN || '127.0.0.1'
const WEB_RTC_SERVER = process.env.WEB_RTC_SERVER || 'http://192.168.1.20:8083'
const IVAP_CONTROL = process.env.IVAP_CONTROL || 'http://192.168.1.20:7777'
// const IVAP_CONTROL = process.env.IVAP_CONTROL || 'https://fd98-220-128-216-143.jp.ngrok.io'
const NX_SERVER = process.env.NX_SERVER || 'https://192.168.1.155:7001'
const NX_SERVER_2 = process.env.NX_SERVER_2 || 'https://192.168.1.155:7001'
const NX_AUTH = process.env.NX_AUTH || 'admin:Nadi1234;'
const SSH_KEY_NAME = process.env.SSH_KEY_NAME || '192.168.1.135'
const SMART_FACTORY_SERVICE =
  process.env.SMART_FACTORY_SERVICE || 'https://www-smart-factory.apitech.com.tw'
const { version } = require('../package.json')

console.info('version:', version)
console.info('WEB_RTC_SERVER:', WEB_RTC_SERVER)
console.info('IVAP_CONTROL:', IVAP_CONTROL, process.env.IVAP_CONTROL)
console.info('NX_SERVER:', NX_SERVER)
console.info('NX_SERVER_2:', NX_SERVER_2)
console.info('NX_AUTH:', NX_AUTH)
console.info('SSH_KEY_NAME:', SSH_KEY_NAME)

const Lazy = require('lazy.js')
const express = require('express')
const cors = require('cors')
const path = require('path')
const { createProxyMiddleware } = require('http-proxy-middleware')
const Md5 = require('crypto-js/md5')
const cookieParser = require('cookie-parser')

const app = express().use('*', cors())

const { generateTokenSync, decodTokenSync } = require('./tokenVerifyer')

require('dotenv').config()

app.use(cookieParser())

/* 路由認證 token */
const onProxyReq = (proxyReq, req, res) => {
  decodTokenSync(req.cookies?.token)
    .then(() => {})
    .catch((err) => {
      res.status(401).json(err.message || err)
    })
}

/* webRTC串流。 */
const exampleProxy = createProxyMiddleware('/stream/**', {
  target: WEB_RTC_SERVER, // target host
  changeOrigin: true, // needed for virtual hosted sites
  cookieDomainRewrite: DOMAIN,
  secure: false,
  //pathRewrite: { '^/stream': '' },
  onProxyReq,
})
app.use(exampleProxy)

/* AI API 反向代理 */
const aiApiProxy = createProxyMiddleware('/ivapControl/**', {
  target: IVAP_CONTROL, // target host
  changeOrigin: true, // needed for virtual hosted sites
  cookieDomainRewrite: DOMAIN,
  secure: false,
  ws: true,
  onProxyReq,
})
app.use(aiApiProxy)
/* NX路由 */
app.use(
  createProxyMiddleware('/nx/**', {
    target: NX_SERVER, // target host
    auth: NX_AUTH,
    changeOrigin: true, // needed for virtual hosted sites
    cookieDomainRewrite: DOMAIN,
    secure: false,
    ws: true,
    pathRewrite: { '^/nx': '' },
    onProxyReq,
  }),
)

app.use(
  createProxyMiddleware('/nx2/**', {
    target: NX_SERVER_2, // target host
    auth: NX_AUTH,
    changeOrigin: true, // needed for virtual hosted sites
    cookieDomainRewrite: DOMAIN,
    secure: false,
    ws: true,
    pathRewrite: { '^/nx2': '' },
    onProxyReq,
  }),
)

app.use(
  createProxyMiddleware('/smartFactory/**', {
    target: SMART_FACTORY_SERVICE, // target host
    changeOrigin: true, // needed for virtual hosted sites
    cookieDomainRewrite: DOMAIN,
    secure: false,
    ws: true,
    onProxyReq,
  }),
)

const compression = require('compression')
app.use(compression({ filter: shouldCompress })) //壓縮加速

function shouldCompress(req, res) {
  if (req.headers['x-no-compression']) {
    // don't compress responses with this request header
    return false
  }
  return compression.filter(req, res)
}
const history = require('connect-history-api-fallback')
app.use(history())
app.use(express.static(path.join(__dirname, '..', 'dist')))

/* 帳號 */
const Account = require(path.join(__dirname, '..', 'cctv-config', 'pass.json'))
Object.entries(Account).forEach(([key, user]) => {
  Account[key].permission = key
  Account[key].verify = Md5(`${user.name}-${user.password}-cctv`).toString()
})

/* API */
app.use(express.json())

app.get('/api/config', (req, res) => {
  res.status(200).json({
    version,
    IVAP_CONTROL,
    NX_SERVER,
    NX_SERVER_2,
    WEB_RTC_SERVER,
  })
})

app.post('/api/verify', (req, res) => {
  decodTokenSync(req.cookies.token)
    .then((meta) => {
      res.status(200).json(meta)
    })
    .catch((err) => {
      res.status(401).json(err.message || err)
    })
})
app.post('/api/login', (req, res) => {
  const { name, pass } = req.body
  const userOb = Lazy(Account).find((user) => {
    return user.name === name
  })
  if (userOb && pass === userOb.verify) {
    const { permission } = userOb
    generateTokenSync({ permission })
      .then((token) => {
        res.cookie('token', token, {
          maxAge: 24 * 60 * 60 * 1000,
          httpOnly: true,
        })
        res.status(200).json('')
      })
      .catch((err) => {
        res.cookie('token', '', {
          httpOnly: true,
        })
        res.status(500).json(err)
      })
  } else {
    res.status(401).json('帳號密碼錯誤')
  }
})
app.use('/api/logout', (req, res) => {
  res.cookie('token', '', {
    httpOnly: true,
  })
  res.status(200).json('')
})

const ioTree = require(path.join(__dirname, '..', 'cctv-config', 'structure', 'ioTree.json'))
const menuTree = require(path.join(__dirname, '..', 'cctv-config', 'structure', 'menuTree.json'))
/* IO數據初始化 */
app.get('/api/builging', (req, res) => {
  decodTokenSync(req.cookies?.token)
    .then(() => {
      res.status(200).json(ioTree)
    })
    .catch((err) => {
      res.status(401).json(err.message || err)
    })
})
/* 選單數據初始化 */
app.get('/api/menuTree', (req, res) => {
  res.status(200).json(menuTree)
})
/* 服務器時間 */
app.get('/api/time', (req, res) => {
  decodTokenSync(req.cookies?.token)
    .then(() => {
      res.status(200).json(new Date().getTime())
    })
    .catch((err) => {
      res.status(401).json(err.message || err)
    })
})

if (process.env.NODE_ENV !== 'production') {
  //開發模式下才會有的路由
  const fakeCameraList = require('../fake/camera.json')
  const fakeIoList = require('../fake/ioList.json')
  app.get('/fakeApi/cameraList', (req, res) => {
    res.json(fakeCameraList)
  })
  app.get('/fakeApi/ioist', (req, res) => {
    res.json(fakeIoList)
  })
}

const https = require('https')
const fs = require('fs')
const serverhttps = https.createServer(
  {
    cert: fs.readFileSync(path.resolve(process.env.SSL_CERT_FILE || `/ssh/${SSH_KEY_NAME}.pem`)),
    key: fs.readFileSync(path.resolve(process.env.SSL_KEY_FILE || `/ssh/${SSH_KEY_NAME}-key.pem`)),
  },
  app,
)
serverhttps.listen(PORT, () => {
  console.log(`Server Listening on port ${PORT}`)
})
