import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'
import { VitePWA } from 'vite-plugin-pwa'
import compress from 'vite-plugin-compress'
// https://vitejs.dev/config/
// import { viteCommonjs } from '@originjs/vite-plugin-commonjs';

export default defineConfig({
  plugins: [
    react({
      babel: {
        plugins: [
          [
            'babel-plugin-styled-components',
            {
              displayName: true,
              fileName: false,
            },
          ],
        ],
      },
    }),
    compress({
      brotli: false,
    }),
    // viteCommonjs()
    VitePWA({
      registerType: 'autoUpdate',
      injectRegister: 'auto',
      workbox: {
        globPatterns: ['**/*.{js,css,html,ico,png,svg,jpg}'],
      },
      manifest: {
        name: '康舒CCTV',
        short_name: '康舒CCTV',
        description: '康舒AI-CCTV',
        theme_color: '#76a6c4',
        icons: [
          {
            src: 'images/building_med.png',
            sizes: '219x219',
            type: 'image/png',
          },
        ],
        display: 'fullscreen',
        start_url: '/',
      },
    }),
  ],
  base: '/',
  server: {
    host: '0.0.0.0',
    proxy: {
      '/api': {
        target: 'https://127.0.0.1:4000', // target host
        // target: 'https://10.10.177.51:4000', // target host
        changeOrigin: true, // needed for virtual hosted sites
        cookieDomainRewrite: '127.0.0.1',
        secure: false,
        // rewrite: (path) => path.replace(/^\/api/, '/api'),
      },
      '/stream': {
        target: 'https://10.10.177.51:4000', // target host
        changeOrigin: true, // needed for virtual hosted sites
        cookieDomainRewrite: '127.0.0.1',
        secure: false,
        proxyTimeout: 2000,
      },
      '/ivapControl': {
        // target: 'https://10.10.177.51:4000', // target host
        target: 'https://f0eb-220-128-216-143.jp.ngrok.io', // target host
        // target: 'http://localhost:4000', // target host
        changeOrigin: true, // needed for virtual hosted sites
        cookieDomainRewrite: '127.0.0.1',
        secure: false,
        ws: true,
        proxyTimeout: 2000,
      },
      '/nx': {
        target: 'https://10.10.177.51:4000', // target host
        auth: 'admin:Nadi1234;',
        changeOrigin: true, // needed for virtual hosted sites
        cookieDomainRewrite: '127.0.0.1',
        secure: false,
        ws: true,
        // rewrite: (path) => path.replace(/^\/nx/, ''),
      },
      '/nx2': {
        target: 'https://10.10.177.51:4000', // target host
        auth: 'admin:Nadi1234;',
        changeOrigin: true, // needed for virtual hosted sites
        cookieDomainRewrite: '127.0.0.1',
        secure: false,
        ws: true,
        // rewrite: (path) => path.replace(/^\/nx/, ''),
      },
      '/smartFactory': {
        target: 'https://10.10.177.51:4000', // target host
        changeOrigin: true, // needed for virtual hosted sites
        cookieDomainRewrite: '127.0.0.1',
        secure: false,
        ws: true,
      },
      '/fakeApi': {
        target: 'https://127.0.0.1:4000', // target host
        changeOrigin: true, // needed for virtual hosted sites
        cookieDomainRewrite: '127.0.0.1',
        secure: false,
      },
    },
  },
  css: {
    postcss: {
      plugins: [require('autoprefixer'), require('postcss-nested')],
    },
  },
})
