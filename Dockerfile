FROM node:16.15.0-alpine3.14
ENV NODE_ENV=production
RUN yarn add express cors compression lazy.js crypto-js cookie-parser jsonwebtoken http-proxy-middleware dotenv connect-history-api-fallback cross-env
COPY app app
COPY dist dist
COPY package.json package.json
EXPOSE 4000
ENTRYPOINT [ "yarn","start" ]