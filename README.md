# 康舒 CCTV

項目 Layout
<https://app.zeplin.io/project/628341aa9fa295179b59675e>
觀看協作請聯繫 iverson.yeh@nadisystem.com 開啟權限

## How to start dev

api 路由開發

```bash
yarn app
```

開發模式

```bash
yarn dev
```

## Dokcer build

```bash
yarn build
docker build . -t cainmaila/cctv-web
docker run -d -p 4000:4000 -v ~/ssh:/ssh -v ~/cctv-config:/cctv-config --name cctv-web cainmaila/cctv-web
```

> 請加掛 ssh key /ssh/[ SSH_KEY_NAME ].pem 與 /ssh/[ SSH_KEY_NAME ]-key.pem
> cctv-config 資料夾為動態載入資料

### 技術框架

- vite react ts
- react-router-dom
- redux tools kit
- redux-saga

### 帳號

帳號設定檔 `app/pass.json`

![permission](info/金蛋康舒CCTV_開發階段5帳密與權限_220627.png)

### 環境變數

| 變數                  | 預設值                                                    | 用途                   |
| --------------------- | --------------------------------------------------------- | ---------------------- |
| PORT                  | 4000                                                      | web export             |
| DOMAIN                | 120.0.0.1                                                 | cookies domain         |
| WEB_RTC_SERVER        | <http://192.168.1.20:8083>                                | webRtc 服務器          |
| IVAP_CONTROL          | <http://192.168.1.20:7777>                                | 中控 服務器            |
| NX_SERVER             | [<http://192.168.1.135:8083>](https://192.168.1.155:7001) | NX 服務器              |
| NX_SERVER_2           | [<http://192.168.1.135:8083>](https://192.168.1.155:7001) | NX 服務器 2 號 (無 AI) |
| NX_AUTH               | --                                                        | NX auth                |
| SSH_KEY_NAME          | 192.168.1.135                                             | SSH Key Name           |
| SMART_FACTORY_SERVICE | <https://www-smart-factory.apitech.com.tw>                | 設備點位服務           |
