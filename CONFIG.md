# 康舒 AI CCTV Web 服務設定說明

康舒 AI CCTV Web，內容設定說明，可針對以下內容進行設置，放置於中控服務器根目錄

- 登入的帳號密碼設置
- 攝影機的點位名稱設置
- 功能選單的名稱與排順序

> 修改文檔後，服務器需要重新啟動後，才能套用新的設定。

## Web 服務設定檔結構

```bash
cctv-config
│   pass.json            密碼權限設置
│
└───structure
    │   ioTree.json      設備點位樹設置檔
    │   menuTree.json    選單內容設置
```

## 密碼權限設置 (pass.json)

權限管理提供了五組等級權限，請依序設置帳號密碼
![permission](info/金蛋康舒CCTV_開發階段5帳密與權限_220627.png)

```json
{
  /* 戰情中心 */
  "A": {
    "name": "a" /* 帳號 */,
    "password": "0000" /* 密碼 */
  },
  /* 遠端戰情 */
  "B": {
    "name": "b",
    "password": "0000"
  },
  /* 監視系統 */
  "C": {
    "name": "c",
    "password": "0000"
  },
  /* 生產安全 */
  "D": {
    "name": "d",
    "password": "0000"
  },
  /* 碼頭 */
  "E": {
    "name": "e",
    "password": "0000"
  }
}
```

## 點位設置 (ioTree.json )

針對整個系統的點位與名稱設置

> 目前系統並無預期會新增點位，但可以修改 title 名稱

```json
{
  /* 建築物 */
  "builhing": [
    {
      "buildKey": "a",   /* 關聯的key請勿修改 */
      "title": "A棟"     /* title顯示名稱，可依需求調整 */
    },
    {
      "buildKey": "b",
      "title": "B棟"
    }
  ],
  /* 樓層 */
  "floors": [
    {
      "floorKey": "a1",               /* 關聯的key請勿修改 */
      "building": "a",                /* 關聯的樓層key請勿修改 */
      "title": "BUILDING_A FLOOR_1"   /* title顯示名稱，可依需求調整 */
    },
    {
      "floorKey": "a2",
      "building": "a",
      "title": "BUILDING_A FLOOR_2"
    },
    {
        ...
    }
  ],
  /* 攝影機點位 */
  "camera": [
    {
      "floor": "a1",                  /* 關聯的樓層key請勿修改 */
      "cameraKey": "a1-1",            /* 關聯的key請勿修改 */
      "id": "",                       /* 強制對應到webRTC服務某個視訊，預設請留空 */
      "title": "HASS ROOM CCTV 1",    /* title顯示名稱，可依需求調整 */
      "permission": "ABC"             /* 告警關聯的權限等級 */
    },
    {
      "floor": "a1",
      "cameraKey": "a1-2",
      "id": "",
      "title": "HASS ROOM CCTV 2",
      "permission": "ABC"
    },
    {
        ....
    }
  ],
  /* IO告警點位 */
  "io": [
    {
      "ioKey": "A-1F-co2sensor",   /* HassRoom告警key請勿修改 */
      "cameraKey": "a1-1",         /* 關聯的攝影機key */
      "type": "hass",              /* 關聯的告警type類型請勿修改 */
      "permission": "ABC"          /* 告警關聯的權限等級 */
    },
    {
      "ioKey": "A-2F-laneleft",  /* 碼頭左車道告警key請勿修改 */
      "cameraKey": "a2-1",
      "type": "pair",
      "permission": "ABCE"
    },
    {
      "ioKey": "A-2F-laneright",  /* 碼頭右車道告警key請勿修改 */
      "cameraKey": "a2-2",
      "type": "pair",
      "permission": "ABCE"
    },
    {
      "ioKey": "A-3F-hangdoor",   /* A棟3f吊掛門告警key請勿修改 */
      "cameraKey": "a3-1",
      "type": "door",
      "permission": "ABC"
    },
    {
      "ioKey": "A-4F-hangdoor",  /* A棟4f吊掛門告警key請勿修改 */
      "cameraKey": "a4-3",
      "type": "door",
      "permission": "ABC"
    },
    {
      "ioKey": "A-4F-co2sensor",  /* A棟4f氣體消防IO告警key請勿修改 */
      "cameraKey": "a4-1",
      "type": "burn",
      "permission": "ABC"
    },
    {
      "ioKey": "A-5F-hangdoor",  /* A棟5f吊掛門告警key請勿修改 */
      "cameraKey": "a5-7",
      "type": "door",
      "permission": "ABC"
    },
    {
      "ioKey": "A-6F-hangdoor",  /* A棟6f吊掛門告警key請勿修改 */
      "cameraKey": "a6-7",
      "type": "door",
      "permission": "ABC"
    },
    {
      "ioKey": "B-2F-hangdoor",  /* B棟2f吊掛門告警key請勿修改 */
      "cameraKey": "b2-1",
      "type": "door",
      "permission": "ABC"
    },
    {
      "ioKey": "A-1F-cameraSos",  /* A棟1f SOS通話事件key請勿修改 */
      "cameraKey": "a1-5",
      "type": "sos",
      "permission": "ABE"
    },
    {
      "ioKey": "A-3F-cameraSos",  /* A棟3f SOS通話事件key請勿修改 */
      "cameraKey": "a3-2",
      "type": "sos",
      "permission": "ABC"
    },
    {
      "ioKey": "A-4F-cameraSos",  /* A棟4f SOS通話事件key請勿修改 */
      "cameraKey": "a4-4",
      "type": "sos",
      "permission": "ABC"
    },
    {
      "ioKey": "A-5F-cameraSos",  /* A棟5f SOS通話事件key請勿修改 */
      "cameraKey": "a5-8",
      "type": "sos",
      "permission": "ABC"
    },
    {
      "ioKey": "A-6F-cameraSos",  /* A棟6f SOS通話事件key請勿修改 */
      "cameraKey": "a6-8",
      "type": "sos",
      "permission": "ABC"
    },
    {
      "ioKey": "B-2F-cameraSos",  /* B棟2f SOS通話事件key請勿修改 */
      "cameraKey": "b2-3",
      "type": "sos",
      "permission": "ABC"
    },
    {
      "ioKey": "A-1F-firealarm",  /* A棟1f 火災告警key請勿修改 */
      "cameraKey": "a1-1",
      "type": "fire",
      "permission": "ABC"
    },
    {
      "ioKey": "A-2F-firealarm",  /* A棟2f 火災告警key請勿修改 */
      "cameraKey": "a2-1",
      "type": "fire",
      "permission": "ABC"
    },
    {
      "ioKey": "A-3F-firealarm",  /* A棟3f 火災告警key請勿修改 */
      "cameraKey": "a3-1",
      "type": "fire",
      "permission": "ABC"
    },
    {
      "ioKey": "A-4F-firealarm",  /* A棟4f 火災告警key請勿修改 */
      "cameraKey": "a4-1",
      "type": "fire",
      "permission": "ABC"
    },
    {
      "ioKey": "A-5F-firealarm",  /* A棟5f 火災告警key請勿修改 */
      "cameraKey": "a5-1",
      "type": "fire",
      "permission": "ABC"
    },
    {
      "ioKey": "A-6F-firealarm",  /* A棟6f 火災告警key請勿修改 */
      "cameraKey": "a6-1",
      "type": "fire",
      "permission": "ABC"
    },
    {
      "ioKey": "B-1F-firealarm",  /* B棟1f 火災告警key請勿修改 */
      "cameraKey": "b1-1",
      "type": "fire",
      "permission": "ABC"
    },
    {
      "ioKey": "B-2F-firealarm",  /* B棟2f 火災告警key請勿修改 */
      "cameraKey": "b2-1",
      "type": "fire",
      "permission": "ABC"
    },
    {
      "ioKey": "B-3F-firealarm",  /* B棟3f 火災告警key請勿修改 */
      "cameraKey": "b3-1",
      "type": "fire",
      "permission": "ABC"
    }
  ]
}
```

## 功能選單設置 (ioTree.json )

針對功能選單的內容作設置

```json
{
  /* 設備操作監看系統 */
  "equipment": {
    "title": "設備操作監看系統" /* title可依需求修改內容 */,
    "system": [
      {
        "title": "吊掛門安全監視" /* title可依需求修改內容 */,
        "path": "hanging-door" /* 對應的頁面路由，請勿調整 */,
        "items": [
          {
            "title": "A棟3F 吊掛平台" /* title可依需求修改內容 */,
            "cameraKeys": ["a3-1", "a3-2"] /* 圖面上的顯示位置順序 */
          },
          {
            "title": "A棟4F 吊掛平台",
            "cameraKeys": ["a4-3", "a4-4"]
          },
          {
            "title": "A棟5F 吊掛平台",
            "cameraKeys": ["a5-7", "a5-8"]
          },
          {
            "title": "A棟6F 吊掛平台",
            "cameraKeys": ["a6-7", "a6-8"]
          },
          {
            "title": "B棟2F 吊掛平台",
            "cameraKeys": ["b2-1", "b2-3"]
          }
        ]
      },
      {
        "title": "HASS ROOM",
        "path": "hass-room",
        "items": [
          {
            "title": "A棟1F HASS ROOM",
            "cameraKeys": ["a1-4", "a1-3", "a1-2", "a1-1"]
          }
        ]
      },
      {
        "title": "貨車卸貨區",
        "path": "unloading-zone",
        "items": [
          {
            "title": "A棟1F卸貨區",
            "cameraKeys": ["a1-5"]
          },
          {
            "title": "A棟2F卸貨區",
            "cameraKeys": ["a2-1", "a2-2", "a2-3"]
          }
        ]
      },
      {
        "title": "燒機測試區",
        "path": "burnIn-test",
        "items": [
          {
            "title": "A棟4F 組裝+燒機區",
            "cameraKeys": ["a4-1", "a4-2"]
          }
        ]
      },
      {
        "title": "重要區域人員入侵",
        "path": "personnel",
        "items": [
          {
            "title": "B棟1F DQ-Lab",
            "cameraKeys": ["b1-1", "b1-2"]
          },
          {
            "title": "B棟2F DQ-Lab",
            "cameraKeys": ["b2-2"]
          },
          {
            "title": "B棟3F DQ-Lab",
            "cameraKeys": ["b3-1", "b3-2"]
          }
        ]
      },
      {
        "title": "火警逃生路徑",
        "path": "fire",
        "items": [
          {
            "title": "A棟1F 逃生路線圖",
            "path": "/a/1",
            "cameraKeys": ["a1-4", "a1-3", "a1-2", "a1-1", "a1-5"]
          },
          {
            "title": "A棟2F 逃生路線圖",
            "path": "/a/2",
            "cameraKeys": ["a2-3", "a2-1", "a2-2"]
          },
          {
            "title": "A棟3F 逃生路線圖",
            "path": "/a/3",
            "cameraKeys": ["a3-2", "a3-1", "a3-3"]
          },
          {
            "title": "A棟4F 逃生路線圖",
            "path": "/a/4",
            "cameraKeys": ["a4-1", "a4-4", "a4-3", "a4-2"]
          },
          {
            "title": "A棟5F 逃生路線圖",
            "path": "/a/5",
            "cameraKeys": ["a5-6", "a5-2", "a5-1", "a5-3", "a5-5", "a5-4", "a5-7", "a5-8"]
          },
          {
            "title": "A棟6F 逃生路線圖",
            "path": "/a/6",
            "cameraKeys": ["a6-6", "a6-3", "a6-2", "a6-1", "a6-5", "a6-4", "a6-7", "a6-8"]
          },
          {
            "title": "B棟1F 逃生路線圖",
            "path": "/b/1",
            "cameraKeys": ["b1-2", "b1-1"]
          },
          {
            "title": "B棟2F 逃生路線圖",
            "path": "/b/2",
            "cameraKeys": ["b2-2", "b2-3", "b2-1"]
          },
          {
            "title": "B棟3F 逃生路線圖",
            "path": "/b/3",
            "cameraKeys": ["b3-2", "b3-1"]
          }
        ]
      }
    ]
  },
  /* 生產安全監視系統 */
  "production": {
    "title": "生產安全監視系統",
    "system": [
      {
        "title": "馬車組裝區",
        "path": "carriage",
        "items": [
          {
            "title": "A棟3F 馬車組裝區",
            "cameraKeys": ["a3-3"]
          }
        ]
      },
      {
        "title": "組裝燒機區",
        "path": "assemble",
        "items": [
          {
            "title": "A棟4F 組裝+燒機區",
            "cameraKeys": ["a4-1", "a4-2"]
          }
        ]
      },
      {
        "title": "SMT/PCBA",
        "path": "smtpcba",
        "items": [
          {
            "title": "A棟5F 生產區",
            "cameraKeys": ["a5-5", "a5-6", "a5-4"]
          },
          {
            "title": "A棟6F 生產區",
            "cameraKeys": ["a6-6", "a6-5", "a6-4"]
          }
        ]
      },
      {
        "title": "波鋒迴焊爐",
        "path": "wave-soldering",
        "items": [
          {
            "title": "A棟5F 波鋒迴焊爐區",
            "cameraKeys": ["a5-1", "a5-3", "a5-2"]
          },
          {
            "title": "A棟6F 波鋒迴焊爐區",
            "cameraKeys": ["a6-3", "a6-2", "a6-1"]
          }
        ]
      }
    ]
  },
  /* 碼頭靠站監控系統 */
  "pier": {
    "title": "碼頭靠站監控系統",
    "cameraKeys": ["a1-5", "a2-1", "a2-2", "a2-3"]
  }
}
```
