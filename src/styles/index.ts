import { css } from 'styled-components';

/* 固定size */
export const pageSize: React.CSSProperties = {
  position: 'relative',
  width: '1920px',
  height: '1080px',
};

export const fullScreen: React.CSSProperties = {
  ...pageSize,
  position: 'absolute',
  top: 0,
  left: 0,
  width: '100%',
  height: '100vh',
};

export const transformCenter: React.CSSProperties = {
  position: 'absolute',
  left: '50%',
  transform: 'translate3D(-50%,0,0)',
};

export const cssFlex = {
  verticalCenter: css`
    display: flex;
    align-items: center;
  `,
  center: css`
    display: flex;
    align-items: center;
    justify-content: center;
  `,
};

/**
 * 顏色
 */
export const colorValue = {
  background: `#0c1a2a`,
  border: `#345676`,
  dashedBorder: `#163e52`,
  main: `#76a6c4`,
  second: `#2a8ab6`,
  bkTex: '#4ed2ee',
  title: `#34a8f2`,
  notice: `#ff5163`,
  light: `#42c6fc`,
  brightBackground: `#34a8f215`,
  warn: `#e42626`,
  warning: `#ff5656`,
  special: `#00ffba`,
};

export const fontColorSty = {
  main: css`
    color: #76a6c4;
  `,
  second: css`
    color: #2a8ab6;
  `,
};
