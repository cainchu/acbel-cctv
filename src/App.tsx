import { Provider } from 'react-redux'
import Lazy from 'lazy.js'
import { BrowserRouter, useRoutes } from 'react-router-dom'
import { store, useAppDispatch, useAppSelector } from './store'
import './App.css'
import Login from './pages/Login'
import Loading from './components/Loading'
import Main from './pages/Main'
import type {} from 'styled-components/cssprop'
import { useEffect, useMemo } from 'react'
import { initConfigAction, verifyAction } from './store/saga'
import useWsScoket from './hooks/useWsScoket'
import { useRootSelector, useUserSelector } from './hooks/useSelect'
import usePushPath from './hooks/usePushPath'
import FullMessage from './components/FullMessage'
import { useIoAlarmListSelector } from './hooks/useSelectIoAlarmList'
import { IO_KEY } from './store/root'

const RootRouter = () => {
  const dispatch = useAppDispatch()
  const { uid, permission } = useUserSelector()
  const { subscriptionAlarm } = useRootSelector()
  usePushPath()
  useEffect(() => {
    dispatch(verifyAction())
  }, [])

  // useEffect(() => {
  //   if (permission) dispatch(initConfigAction())
  // }, [permission])

  const { ioAlarmList } = useIoAlarmListSelector()

  const watchAlarmObs = useMemo(() => {
    const mapOff = ioOffMappingToType(ioAlarmList)
    const { camera, io } = subscriptionAlarm

    const ioOff = Lazy(io)
      .filter((_io) => (_io === IO_KEY.door_3f ? !!mapOff.get('hanging_platform_A3') : true))
      .filter((_io) => (_io === IO_KEY.door_4f ? !!mapOff.get('hanging_platform_A4') : true))
      .filter((_io) => (_io === IO_KEY.door_5f ? !!mapOff.get('hanging_platform_A5') : true))
      .filter((_io) => (_io === IO_KEY.door_6f ? !!mapOff.get('hanging_platform_A6') : true))
      .filter((_io) => (_io === IO_KEY.door_b2 ? !!mapOff.get('hanging_platform_B2') : true))
      .filter((_io) =>
        _io === IO_KEY.piar1 || _io === IO_KEY.piar2 ? !!mapOff.get('truck_40m_A2') : true,
      )
      .filter((_io) => (_io === IO_KEY.fire_B2F ? !!mapOff.get('fire_alarm') : true))
      .filter((_io) => (_io === IO_KEY.fire_B1F ? !!mapOff.get('fire_alarm') : true))
      .filter((_io) => (_io === IO_KEY.fire_1F ? !!mapOff.get('fire_alarm') : true))
      .filter((_io) => (_io === IO_KEY.fire_2F ? !!mapOff.get('fire_alarm') : true))
      .filter((_io) => (_io === IO_KEY.fire_3F ? !!mapOff.get('fire_alarm') : true))
      .filter((_io) => (_io === IO_KEY.fire_4F ? !!mapOff.get('fire_alarm') : true))
      .filter((_io) => (_io === IO_KEY.fire_5F ? !!mapOff.get('fire_alarm') : true))
      .filter((_io) => (_io === IO_KEY.fire_6F ? !!mapOff.get('fire_alarm') : true))
      .filter((_io) => (_io === IO_KEY.fire_7F ? !!mapOff.get('fire_alarm') : true))
      .filter((_io) => (_io === IO_KEY.fire_8F ? !!mapOff.get('fire_alarm') : true))
      .filter((_io) => (_io === IO_KEY.fire_9F ? !!mapOff.get('fire_alarm') : true))
      .filter((_io) => (_io === IO_KEY.fire_B1 ? !!mapOff.get('fire_alarm') : true))
      .filter((_io) => (_io === IO_KEY.fire_B2 ? !!mapOff.get('fire_alarm') : true))
      .filter((_io) => (_io === IO_KEY.fire_B3 ? !!mapOff.get('fire_alarm') : true))
      .filter((_io) => (_io === IO_KEY.hass1 ? !!mapOff.get('lack_oxygen') : true))
      .filter((_io) => (_io === IO_KEY.hass2 ? !!mapOff.get('lack_oxygen') : true))
      .filter((_io) => (_io === IO_KEY.gas_fire_1 ? !!mapOff.get('gas_fire') : true))
      .filter((_io) => (_io === IO_KEY.gas_fire_2 ? !!mapOff.get('gas_fire') : true))
      .filter((_io) => (_io === IO_KEY.gas_fire_3 ? !!mapOff.get('gas_fire') : true))
      .filter((_io) => (_io === IO_KEY.gas_fire_4 ? !!mapOff.get('gas_fire') : true))
      .filter((_io) => (_io === IO_KEY.gas_fire_5 ? !!mapOff.get('gas_fire') : true))
      .filter((_io) => (_io === IO_KEY.gas_fire_6 ? !!mapOff.get('gas_fire') : true))
      .toArray()
    return { camera, io: ioOff }
  }, [ioAlarmList, subscriptionAlarm])

  useWsScoket('/ivapControl/analysisResult/alarm', watchAlarmObs)

  return useRoutes([
    {
      path: '*',
      element: uid ? <Main /> : <Login />,
    },
  ])
}

const RootView = () => {
  const loading = useAppSelector((state) => state.root.loading)
  const { message } = useRootSelector()
  return (
    <>
      <RootRouter />
      <Loading loading={loading > 0} />
      {message && <FullMessage>{message}</FullMessage>}
    </>
  )
}

function App() {
  return (
    <BrowserRouter>
      <Provider store={store}>
        <RootView />
      </Provider>
    </BrowserRouter>
  )
}

export default App

/* 把 io off store 轉成 ioKey 的 off布林 */
function ioOffMappingToType(offs: ioAlarmSwitchItem[]) {
  const map = new Map<string, boolean>()
  offs.forEach((_off) => {
    map.set(_off.id, _off.status === 1)
  })
  return map
}
