import { createSlice, PayloadAction } from '@reduxjs/toolkit';
export interface historyState {
  historyList: historyResult[];
  historyTotalLength:number;
  historyStatus:number;
}

const initialState: historyState = {
  historyList: [],
  historyTotalLength:0,
  historyStatus:0
};

export const historySlice = createSlice({
  name: 'history',
  initialState,
  reducers: {
    setHistory(state, action: PayloadAction<historyState>) {
      const historyObject = action.payload;
      state.historyList = historyObject.historyList;
      state.historyTotalLength = historyObject.historyTotalLength;
      state.historyStatus = historyObject.historyStatus;
    },
  },
  extraReducers: {},
});

export const { setHistory } = historySlice.actions;

export default historySlice.reducer;
