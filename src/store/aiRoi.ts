import { createSlice, PayloadAction } from '@reduxjs/toolkit';
export interface AiRoiState {
  aiRoiList: aiRoiResponse[];
  aiMenuList: { id: string; title: string; ai: string }[];
}

const initialState: AiRoiState = {
  aiRoiList: [],
  aiMenuList: [],
};

export const aiRoiSlice = createSlice({
  name: 'aiRoi',
  initialState,
  reducers: {
    setAiRoiInfo(state, action: PayloadAction<aiRoiResponse[]>) {
      const list = action.payload;
      state.aiRoiList = list;
    },
    setAiMenuList(
      state,
      action: PayloadAction<{ id: string; title: string; ai: string }[]>,
    ) {
      state.aiMenuList = action.payload;
    },
  },
  extraReducers: {},
});

export const { setAiRoiInfo, setAiMenuList } = aiRoiSlice.actions;

export default aiRoiSlice.reducer;
