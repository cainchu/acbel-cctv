import { put, call, all, delay, cancel } from 'redux-saga/effects';
import { setHistory } from '../history';
import { historySelect, eventSnapshotSelect } from '../../utils/sdk';
import { PayloadAction } from '@reduxjs/toolkit';
import { store } from './../index';
import * as R from 'ramda';
import {roundTo} from './../../utils/tools';
import { setMessage } from '../root'
interface BlobData extends Blob {
  data: Blob;
}

export function* getHistoryList(
  action: PayloadAction<historyFrontParams>,
) {
  const {
    offset,
    data_length_per_page,
    id,
    analysis_type,
    start_alarm_timestamp,
    end_alarm_timestamp,
    round,
    device_type,
    matched_camera
  } = action.payload;

  const end = offset + data_length_per_page;
  try {
    const { data: response }: { data: historyResponse } = yield call(
      historySelect,
      {
        id,
        analysis_type,
        start_alarm_timestamp,
        end_alarm_timestamp,
        offset,
        end,
        device_type
      },
    );
    // console.log('response', response);
    const result = response.result;
    // console.log('result', result)

    function isHistoryType(item: historyResult): item is historyResult {
      return item && (item as historyResult).id !== undefined
    }

    if(result.length === 0 || (result.length && isHistoryType(result[0]))) {
      let prevList = store.getState().history.historyList;
      // console.log('prevList', prevList);
      let mergedList: historyResult[] = [];
      let temp : historyResult[][] = [];
        try {
          function* getImageUrl(item: historyResult) {
            try {
              if(device_type === 'cctv') {
                const imageResponse: BlobData = yield call(eventSnapshotSelect, {
                  event_id: item.event_id,
                });
                // console.log('imageResponse', imageResponse)
                let urlCreator = window.URL || window.webkitURL;
                let image_url = urlCreator.createObjectURL(imageResponse.data);
                return { ...item, image_url: image_url };
              } else {
                return { ...item, image_url: '' };
              }
  
            } catch (err){
              return { ...item, image_url: '' };
            }
          }
          for (let i = 0; i < round; i++) {
            if(round > 1) console.log('export', roundTo((i+1)/round*100, 2), '%');
            let fragment = result.slice(i * 30, 30 * (i + 1));
            // console.log('fragment', fragment);
            mergedList = yield all(fragment.map((item) => call(getImageUrl, item)));
            // console.log('mergedList', mergedList);
            // mergedList = fragment;
            temp.push(mergedList);
          }
          const eachResultList = prevList.concat(R.flatten(temp));
          
          // console.log('eachResultList', eachResultList);
          yield put(
            setHistory({
              historyList: eachResultList,
              historyTotalLength: response.total,
              historyStatus: 200
            }),
          );
        } catch (err:any) {
          // console.error(err);
          // console.log('err', err)
        }
      yield delay(200);
    } else {
      yield put(setMessage('中控API服務失效!'))
    }


  } catch (err:any) {
    // console.error(err);
    // console.log('err', err)
    yield put(
      setHistory({
        historyList: [],
        historyTotalLength: 0,
        historyStatus: err.response && err.response.status ? err.response.status : 408
      }),
    );
  }
}
