import { put, select, call } from 'redux-saga/effects'
import { dataReady, rootLoading, setBuildingTree, setMenuTree, setSpeakerMap } from '../root'
import { setTreeMap } from '../vdos'
import Lazy from 'lazy.js'
import { cameraListWithStorage, getBuilging, getMenuTree } from '../../utils/sdk'
import aiMenuListSaga from './aiMenuListSaga'

import { getIoList } from './ioListSaga'
import { getIoAlarmListAction } from '.'

/**
 * 初始化資料左側選單資料
 */
export function* initTreeMapSync(_treeMap: I_TreeMap) {
  yield put(setTreeMap(_treeMap))
}

// 偷懶備忘
// type getBuilgingReturnType = SagaReturnType<typeof getBuilging>;

/**
 * 載入初始化資訊
 */
export function* initConfigSync() {
  yield put(rootLoading(true))
  const permission: string = yield select((state) => state.user.permission) //取得目前權限
  //取回IO列表
  yield getIoList()
  try {
    //取回攝影機列表
    const cameraListData: I_CameraInfo[] = yield call(cameraListWithStorage)
    const cameraListDataLazy = Lazy(cameraListData)
    const { data }: { data: treeBuildingMetaType } = yield call(getBuilging)

    data.camera = Lazy(data.camera)
      .map((vdo) => {
        const _camera = cameraListDataLazy.find(({ keyword }) => vdo.cameraKey === keyword)
        if (_camera) {
          vdo = {
            ...vdo,
            id: _camera.uuid,
            info: _camera,
          }
        }
        return vdo
      })
      .toArray()
    yield put(setBuildingTree(data))
    /* 生成喇叭 */
    const speakers = cameraListDataLazy
      .filter((_camera) => {
        return _camera.keyword === 'hass-speaker' || _camera.keyword === 'pair-speaker'
      })
      .toArray()
    yield put(setSpeakerMap(speakers))
    /* 生成左方選單 */
    const lazyCamera = Lazy(data.camera)
    const floors = data.floors
    const floorMenu = Lazy(floors)
      .map((floor) => ({
        uuid: floor.floorKey,
        title: floor.title,
        vdos: lazyCamera
          .filter((vdo) => vdo.floor === floor.floorKey)
          .map((vdo) => ({
            uuid: vdo.cameraKey,
            cameraId: vdo.id,
            title: vdo.title,
            isPlaying: false,
            ptz: vdo.info?.ptz === 1,
            sos: vdo.info?.sos === 1,
          }))
          .toArray(),
      }))
      .toArray()
    yield initTreeMapSync({ floors: floorMenu as I_Floor[] })
    //取回menuTree
    const { data: menuTree }: { data: menuTreeType } = yield call(getMenuTree)
    yield put(setMenuTree(menuTree))
    yield aiMenuListSaga() //設定ai選單
    yield put(getIoAlarmListAction()) //取回io報警開關
    yield put(dataReady())
  } catch (err) {
    console.error(err)
  }
  yield put(rootLoading(false))
}
