import { put, call } from 'redux-saga/effects';
import { setAiStatus } from '../aiStatus';
import { aiStatusSelect, aiStatusUpdate } from '../../utils/sdk';
import { PayloadAction } from '@reduxjs/toolkit';

export function* getAiStatus(action: PayloadAction) {
  try {
    const { data: response }: { data: aiStatusParam[] } = yield call(aiStatusSelect);
    // console.log('getAiStatus_response', response)

    yield put(setAiStatus(response));
  } catch (err) {
    console.error(err);
  }
}

export function* updateAiStatus(action: PayloadAction<aiStatusParam>) {
  const { id, ai_switch, human_detection, ai_fences, ai_retentate, ai_schedule } = action.payload;
  try {
    const { data: response }: { data: string } = yield call(aiStatusUpdate, {
      id, ai_switch, human_detection, ai_fences, ai_retentate, ai_schedule
    });
    // console.log('updateAiStatus_response', response)
  } catch (err) {
    console.error(err);
  }
}

