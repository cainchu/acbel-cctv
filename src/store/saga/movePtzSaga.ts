import { PayloadAction } from '@reduxjs/toolkit'
import { call } from 'redux-saga/effects'
import { movePtz } from '../../utils/sdk'

/* 對應字典檔 */
const PTZ_MAPER = new Map<string, string>()
PTZ_MAPER.set('BURN_IN_ROOM1', 'room1')
PTZ_MAPER.set('BURN_IN_ROOM2', 'room2')
PTZ_MAPER.set('BURN_IN_ROOM3', 'room3')
PTZ_MAPER.set('BURN_IN_ROOM4', 'room4')
PTZ_MAPER.set('BURN_IN_ROOM5', 'room5')
PTZ_MAPER.set('BURN_IN_ROOM6', 'room6')

export function* movePtzSynz(action: PayloadAction<string>) {
  const { payload: ptz } = action
  try {
    yield call(movePtz, PTZ_MAPER.get(ptz) || ptz)
  } catch (e) {
    window.alert(`${ptz} ptz定位失效!`)
  }
}
