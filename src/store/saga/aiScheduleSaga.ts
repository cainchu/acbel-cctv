import { put, call } from 'redux-saga/effects'
import { setSelectAiSchedule, setUpdateAiSchedule } from '../aiSchedule'
import { scheduleSelect, scheduleUpdate } from '../../utils/sdk'
import { PayloadAction } from '@reduxjs/toolkit'
import { createInitialSchedule } from './../../utils/tools'

export function* getAiSchedule(action: PayloadAction<scheduleSelectParams>) {
  const { id, ai_type } = action.payload
  try {
    const { data: schedule_table }: { data: scheduleTable } = yield call(scheduleSelect, {
      id,
      ai_type,
    })
    if (Object.keys(schedule_table).length) {
      yield put(setSelectAiSchedule({ data: schedule_table, status: 200 }))
    } else {
      yield put(setSelectAiSchedule({ data: createInitialSchedule(), status: 200 }))
    }
  } catch (err: any) {
    yield put(setSelectAiSchedule({ data: undefined, status: err.response && err.response.status ? err.response.status : 408 }))
  }
}

export function* updateAiSchedule(action: PayloadAction<scheduleUpdateParams>) {
  const { id, ai_type, schedule_table } = action.payload
  try {
    const { data: response }: { data: string } = yield call(scheduleUpdate, {
      id,
      ai_type,
      schedule_table,
    })
    console.log('updateAiSchedule_response', response)
    yield put(setUpdateAiSchedule({ data: response, status: 200 }))
  } catch (err: any) {
    // console.error(err);
    yield put(setUpdateAiSchedule({ data: '', status: err.response && err.response.status ? err.response.status : 408 }))
  }
}
