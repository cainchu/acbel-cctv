import { put, call } from 'redux-saga/effects';
import { setIoAlarmList } from '../ioAlarmList';
import { ioAlarmSelect , ioAlarmUpdate } from '../../utils/sdk';
import { PayloadAction } from '@reduxjs/toolkit';
import { store } from './../index';

export function* getIoAlarmList(action: PayloadAction) {
  try {
    const { data: ioAlarmList }: { data: ioAlarmSwitchItem[] } = yield call(ioAlarmSelect);
    // console.log('ioAlarmList', ioAlarmList)
    
    yield put(setIoAlarmList(ioAlarmList));
  } catch (err) {
    console.error(err);
  }
}


export function* updateIoAlarm(action: PayloadAction<ioAlarmUpdateParams>) {
  const { id, status } = action.payload;
  try {
    const { data: response }: { data: string } = yield call(ioAlarmUpdate, {
      id, status
    });
    console.log('response', response)

  } catch (err) {
    console.error(err);
  }
}