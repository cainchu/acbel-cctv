import { createAction, PayloadAction } from '@reduxjs/toolkit'
import { delay, put, takeEvery, call } from 'redux-saga/effects'
import { syncLogin, syncLogout, syncVerify } from './loginSaga'
import {
  fullAnVdoSync,
  monitorCustomSync,
  monitorFloorSync,
  switchVdoSync,
} from './monitorFloorSaga'
import { getAiRoiInfo, getThumbnail, createAiRoi, deleteAiRoi, switchAiRoi } from './aiRoiSaga'
import { getAiStatus, updateAiStatus } from './aiStatusSaga'
import { getAiRetentate, updateAiRetentate } from './aiRetentateSaga'
import { getAiSchedule, updateAiSchedule } from './aiScheduleSaga'
import { getBroadcast, updateBroadcast } from './broadcastSaga'
import { getHistoryList } from './historySaga'
import { getEventSnapshot } from './eventSnapshotSaga'
import { getIoAlarmList, updateIoAlarm } from './ioAlarmListSaga'
import { setPushPath } from '../root'
import { VDO_PAGE_TYPE } from '../vdos'
import pageReadySaga from './pageReadySaga'
import { getConfig } from '../../utils/sdk'
import loadConfigSaga from './loadConfigSaga'
import { movePtzSynz } from './movePtzSaga'

/**
 * 認證
 */
export const verifyAction = createAction('VERIFY_ACT')
/**
 * 登入
 */
export const loginAction = createAction<I_LoginParam>('LOGIN_ACT')
/**
 * 登出
 */
export const logoutAction = createAction('LOGOUT_ACT')
/**
 * push路由
 */
export const pushPathAction = createAction<string>('pushPathAction')
/**
 * 交換影片位置
 */
export const switchVdoAction = createAction<{ uuid: string; key: number }>('SWITCH_VDO')

/**
 * 切換監視樓層
 */
export const monitorFloorAction = createAction<string | { floor: string; pageType: VDO_PAGE_TYPE }>(
  'MONITOR_FLOOR',
)

/**
 * 設定特定監視畫面
 */
export const monitorCustomAction = createAction<string[]>('monitorCustom')

/**
 * Full影像
 */
export const fullAnVdoAction = createAction<number>('fullAnVdoAction')

/**
 * 告警觸發
 */
export const alertNotificationAction = createAction<I_NotificationMeta>('alertNotificationAction')
/**
 * 移除告警
 */
export const removeAlertNotificationAction = createAction<I_NotificationMeta>(
  'removeAlertNotificationAction',
)

/**
 * 觀看失效告警
 */
export const viewNotificationAction = createAction<I_NotificationMeta>('viewNotificationAction')

/**
 * 取得AiRoi
 */
export const getTargetAiRoiAction = createAction<aiRoiParam>('getTargetAiRoiAction')

/**
 * 取得camera縮圖
 */
export const getCameraThumbnailAction = createAction<thumbnailParam>('getCameraThumbnailAction')

/**
 * 新增AiRoi
 */
export const createTargetAiRoiAction = createAction<aiRoiResponse>('createTargetAiRoiAction')

/**
 * 更新AiRoi
 */
export const switchDeviceAiRoiAction = createAction<aiRoiResponse>('switchDeviceAiRoiAction')

/**
 * 刪除AiRoi
 */
export const deleteTargetAiRoiAction = createAction<aiRoiResponse>('deleteTargetAiRoiAction')

/**
 * 查詢Ai功能狀態
 */
export const getAiStatusAction = createAction('getAiStatusAction')

/**
 * 更新Ai功能狀態
 */
export const updateCameraAiStatusAction = createAction<aiStatusParam>('updateCameraAiStatusAction')

/**
 * 查詢Ai滯留物背景圖
 */
export const getAiRetentateAction = createAction<thumbnailParam>('getAiRetentateAction')

/**
 * 更新Ai滯留物背景圖
 */
export const updateAiRetentateAction = createAction<thumbnailParam>('updateAiRetentateAction')

/**
 * 取得歷史記錄
 */
export const getHistoryAction = createAction<historyFrontParams>('getHistoryAction')

/**
 * 取得單一歷史事件截圖
 */
export const getEventSnapshotAction = createAction<eventSnapshotParams>('getEventSnapshotAction')

/**
 * 取得排程
 */
export const getAiScheduleAction = createAction<scheduleSelectParams>('getAiSchedule')
/**
 * 更新排程
 */
export const updateAiScheduleAction = createAction<scheduleUpdateParams>('updateAiSchedule')

/**
 * 取得廣播設定列表
 */
export const getBroadcastAction = createAction('getBroadcast')
/**
 * 更新特定項目廣播對象
 */
export const updateBroadcastAction = createAction<broadcastUpdateParams>('updateBroadcast')

/**
 * 取得I/O告警開關列表
 */
export const getIoAlarmListAction = createAction('getIoAlarmList')
/**
 * 更新I/O告警開關列表
 */
export const updateIoAlarmAction = createAction<ioAlarmUpdateParams>('updateIoAlarm')
/**
 * 數據初始化
 */
export const initConfigAction = createAction('initConfigAction')

/**
 * Page Ready事件
 */
export const pathReadyAction = createAction<string>('pathReadyAction')

/**
 * 移動ptz
 */
export const movePtzAction = createAction<string>('movePtzAction')

export default function* () {
  yield call(loadConfigSaga)
  yield takeEvery(verifyAction, syncVerify)
  yield takeEvery(loginAction, syncLogin)
  yield takeEvery(logoutAction, syncLogout)
  yield takeEvery(switchVdoAction, switchVdoSync)
  yield takeEvery(monitorFloorAction, monitorFloorSync)
  yield takeEvery(monitorCustomAction, monitorCustomSync)
  yield takeEvery(fullAnVdoAction, fullAnVdoSync)
  yield takeEvery(getTargetAiRoiAction, getAiRoiInfo)
  yield takeEvery(getCameraThumbnailAction, getThumbnail)
  yield takeEvery(createTargetAiRoiAction, createAiRoi)
  yield takeEvery(switchDeviceAiRoiAction, switchAiRoi)
  yield takeEvery(deleteTargetAiRoiAction, deleteAiRoi)
  yield takeEvery(getAiStatusAction, getAiStatus)
  yield takeEvery(updateCameraAiStatusAction, updateAiStatus)
  yield takeEvery(getAiRetentateAction, getAiRetentate)
  yield takeEvery(getAiScheduleAction, getAiSchedule)
  yield takeEvery(updateAiScheduleAction, updateAiSchedule)
  yield takeEvery(getBroadcastAction, getBroadcast)
  yield takeEvery(updateBroadcastAction, updateBroadcast)
  yield takeEvery(updateAiRetentateAction, updateAiRetentate)
  yield takeEvery(getHistoryAction, getHistoryList)
  yield takeEvery(getEventSnapshotAction, getEventSnapshot)
  yield takeEvery(getIoAlarmListAction, getIoAlarmList)
  yield takeEvery(updateIoAlarmAction, updateIoAlarm)
  yield takeEvery(pushPathAction, pushPathSync)
  yield takeEvery(pathReadyAction, pageReadySaga)
  yield takeEvery(movePtzAction, movePtzSynz)
  // } catch (error) {
  // console.error(error);
  // }
}

function* pushPathSync(action: PayloadAction<string>) {
  yield put(setPushPath(action.payload))
  yield delay(0)
  yield put(setPushPath(null))
}
