import { put, call } from 'redux-saga/effects';
import { setAiRetentateUrl } from '../aiRetentate';
import { aiRetentateBackgroundSelect, aiRetentateBackgroundUpdate } from '../../utils/sdk';
import { PayloadAction } from '@reduxjs/toolkit';

export function* getAiRetentate(action: PayloadAction<thumbnailParam>) {
  const { cameraID } = action.payload;
  try {
    const { data: thumbnail }: { data: Blob } = yield call(aiRetentateBackgroundSelect, {
      cameraID: cameraID,
    });
    // console.log('getAiRetentate_thumbnail', thumbnail)
    let urlCreator = window.URL || window.webkitURL;
    let imageUrl = urlCreator.createObjectURL(thumbnail);
    yield put(setAiRetentateUrl(imageUrl));
  } catch (err) {
    console.error(err);
  }
}

export function* updateAiRetentate(action: PayloadAction<thumbnailParam>) {
  const { cameraID } = action.payload;
  try {
    const { data: response }: { data: string } = yield call(aiRetentateBackgroundUpdate, {
      cameraID
    });
    // console.log('updateAiRetentate_response', response)

  } catch (err) {
    console.error(err);
  }
}