import { PayloadAction } from '@reduxjs/toolkit';
import { put, call } from 'redux-saga/effects';
import { setReadyPagePath } from '../root';
import { setFloorUUid, setFullKey, setRealVdoNum, setVods } from '../vdos';

export default function* (action: PayloadAction<string>) {
  yield call(pagePathHendler, action.payload);
}

export function* pagePathHendler(path: string) {
  if (path) {
    // yield call(setVdoByPath, path);
  } else {
    yield call(claerVdos);
  }
  yield put(setReadyPagePath(path));
}

/**
 * 依據path設定vods
 * @param path
 */
// function* setVdoByPath(path: string) {
//   console.log('#依據path設定vods', path);
// }

/**
 * 清空vdos
 */
function* claerVdos() {
  yield put(setVods([]));
  yield put(setFloorUUid(''));
  yield put(setFullKey(-1));
  yield put(setRealVdoNum(0));
}
