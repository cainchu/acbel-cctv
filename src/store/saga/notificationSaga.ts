import { PayloadAction } from '@reduxjs/toolkit'
import { put, select, takeEvery, delay, takeLatest } from 'redux-saga/effects'
import {
  alertNotificationAction,
  fullAnVdoAction,
  pushPathAction,
  removeAlertNotificationAction,
  switchVdoAction,
  viewNotificationAction,
} from '.'
import { AlarmType } from '../../utils/AlarmSystem'
import { pushNotification, removeNotification, setAlarmSoundOff } from '../notification'
import { camerasGetterById } from '../root'
import { alarmSync } from './monitorFloorSaga'

export default function* () {
  //開始監控報警================================================================
  yield takeEvery(alertNotificationAction, alertNotificationSync)
  yield takeEvery(removeAlertNotificationAction, removeAlertNotificationSync)
  yield takeLatest(viewNotificationAction, viewNotificationSync)
}

/**
 * 告警訊號
 * @param action
 */
export function* alertNotificationSync(action: PayloadAction<I_NotificationMeta>) {
  yield put(pushNotification(action.payload))
  yield put(viewNotificationAction(action.payload))
  yield alarmSync()
}

/**
 * 移除告警
 */
export function* removeAlertNotificationSync(action: PayloadAction<I_NotificationMeta>) {
  yield put(removeNotification(action.payload))
  yield alarmSync()
  const validNotifications: I_NotificationMeta[] = yield select(
    (state) => state.notification.validNotifications,
  )
  if (validNotifications.length === 0) yield put(setAlarmSoundOff(false))
}

/**
 * 檢視告警
 */
export function* viewNotificationSync(action: PayloadAction<I_NotificationMeta>) {
  const { type, id, disable } = action.payload
  switch (type) {
    case AlarmType.FIRE: //TODO: 火災邏輯
      yield fireProcess(action.payload)
      break
    case AlarmType.HASS: //Hass報警
      yield hassProcess()
      break
    case AlarmType.BURN_1: //燒機室氣體消防報警
    case AlarmType.BURN_2: //燒機室氣體消防報警
    case AlarmType.BURN_3: //燒機室氣體消防報警
    case AlarmType.BURN_4: //燒機室氣體消防報警
    case AlarmType.BURN_5: //燒機室氣體消防報警
    case AlarmType.BURN_6: //燒機室氣體消防報警
    case AlarmType.SOS:
    default:
      yield vodsProcess(action.payload)
  }
  if (!disable) yield put(setAlarmSoundOff(true)) //如果是正式告警就撥放音效
}

function* hassProcess() {
  const path = '/hass-room/0' //寫死 hass 目前也沒別的了...
  yield put(pushPathAction(path))
  yield waitPageing(path)
}

/* 火警的報警流程 */
function* fireProcess({ type, id }: I_NotificationMeta) {
  const path = `/fire/${id.charAt(0)}/${id.charAt(1)}`
  yield put(pushPathAction(path))
  yield waitPageing(path)
}

/* 分割畫面的報警流程 */
function* vodsProcess({ type, id }: I_NotificationMeta) {
  const permission: string = yield select((state) => state.user.permission)
  const cameras: treeDataCameraType[] = yield select((state) => state.root.cameras)
  const camera = camerasGetterById(cameras, id)
  const path = swatchPathByKeyAndType(camera.cameraKey, type, permission)
  if (!isSetting()) {
    //設定頁不告警
    yield put(pushPathAction(path))
    yield waitPageing(path)
  }
  const fullKey: number = yield select((state) => state.vdos.fullKey)
  if (fullKey !== -1) yield put(fullAnVdoAction(fullKey)) //如果有放大的畫面先正常
  yield delay(10) //UI顯示時間
  yield put(switchVdoAction({ uuid: camera.cameraKey, key: 0 })) //換成主畫面
  yield put(fullAnVdoAction(0))
}

/* 請等我換頁 */
function* waitPageing(path: string) {
  let readyPagePath = ''
  let vdoMetas: I_CtvMeta[] = []
  while (readyPagePath !== path || vdoMetas.length === 0) {
    //真正換頁，且vdoMetas ready
    yield delay(10)
    readyPagePath = yield select((state) => state.root.readyPagePath)
    vdoMetas = yield select((state) => state.vdos.vdoMetas)
  }
}

/* Hot Fix 可優化 */
function isSetting() {
  switch (window.location.pathname) {
    case '/setting-ai':
    case '/setting-alert':
    case '/setting-log':
    case '/setting-push':
    case '/setting-schedule':
      return true
    default:
      return false
  }
}

/**
 * 根據type返回報警畫面型式，window(播放框) fire hass
 * @param type
 * @returns
 */
function layoutByType(type: string) {
  switch (type) {
    case AlarmType.FENCES:
    case AlarmType.RETENTETE:
    case AlarmType.PAIR:
    case AlarmType.DOOR:
    case AlarmType.BURN_1:
    case AlarmType.BURN_2:
    case AlarmType.BURN_3:
    case AlarmType.BURN_4:
    case AlarmType.BURN_5:
    case AlarmType.BURN_6:
      return 'window'
    case AlarmType.FIRE:
      return 'fire'
    case AlarmType.HASS:
      return 'hass'
    default:
      return 'window'
  }
}

function isBurnType(type: string): boolean {
  switch (type) {
    case AlarmType.BURN_1:
    case AlarmType.BURN_2:
    case AlarmType.BURN_3:
    case AlarmType.BURN_4:
    case AlarmType.BURN_5:
    case AlarmType.BURN_6:
      return true
    default:
      return false
  }
}

function swatchPathByKeyAndType(key: string, type?: string, permission?: string): string {
  if (isBurnType(type || '')) return permission === 'D' ? `/assemble/0` : `/burnIn-test/0` //氣體消防
  switch (key) {
    //以下Hass room================================
    case 'a1-1':
    case 'a1-2':
    case 'a1-3':
    case 'a1-4':
      return '/hass-room/0'
    //以下人員入侵================================
    case 'b1-1':
    case 'b1-2':
      return '/personnel/0'
    case 'b2-2':
      return '/personnel/1'
    case 'b3-1':
    case 'b3-2':
      return '/personnel/2'
    //以下碼頭================================
    case 'a1-5':
    case 'a2-1':
    case 'a2-2':
    case 'a2-3':
      return '/piar'
    //以下吊掛門================================
    case 'a3-1':
    case 'a3-2':
      return '/hanging-door/0'
    case 'a4-3':
    case 'a4-4':
      return '/hanging-door/1'
    case 'a5-7':
    case 'a5-8':
      return '/hanging-door/2'
    case 'a6-7':
    case 'a6-8':
      return '/hanging-door/3'
    case 'b2-1':
    case 'b2-3':
      return '/hanging-door/4'
    //以下圍焊爐遺留物================================
    case 'a5-1':
    case 'a5-2':
    case 'a5-3':
      return '/wave-soldering/0'
    case 'a6-1':
    case 'a6-2':
    case 'a6-3':
      return '/wave-soldering/1'
    default:
      return ''
  }
}
