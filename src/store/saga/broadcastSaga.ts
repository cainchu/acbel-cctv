import { put, call } from 'redux-saga/effects';
import { setBroadcast } from '../broadcast';
import { broadcastSelect, broadcastUpdate } from '../../utils/sdk';
import { PayloadAction } from '@reduxjs/toolkit';

export function* getBroadcast(action: PayloadAction) {
  try {
    const { data: broadcast_table }: { data: broadcastTable } = yield call(broadcastSelect);
    // console.log('broadcast_table', broadcast_table)
    
    yield put(setBroadcast(broadcast_table));
  } catch (err) {
    console.error(err);
  }
}

export function* updateBroadcast(action: PayloadAction<broadcastUpdateParams>) {
  const { eventId, targetUrl } = action.payload;
  try {
    const { data: response }: { data: string } = yield call(broadcastUpdate, {
      eventId, targetUrl
    });
    console.log('response', response)

  } catch (err) {
    console.error(err);
  }
}