import { call } from 'redux-saga/effects'
import { getConfig } from '../../utils/sdk'

export default function* () {
  const { data } = yield call(getConfig)
  logVar({ name: '康舒CCTV', version: data.version })
  // console.table(data)
}

/**
 * 書出版號
 * @param {*} param0 版號資訊
 * @param {*} _color 背影色
 * @param {*} _txt_color 文字色
 */
const logVar = (
  { name, version }: { name: string; version: string },
  _color = '#5b60c8',
  _txtColor = '#fff',
) => {
  console.info(`%c ${name} v${version} `, `background: ${_color}; color: ${_txtColor}`)
}
