import { PayloadAction } from '@reduxjs/toolkit'
import { put, select, fork } from 'redux-saga/effects'
import Md5 from 'crypto-js/md5'
import axios from 'axios'
import { rootLoading, setSubscriptionAlarmByPermission } from '../root'
import { loginMessage, setUid } from '../user'
import { verifyAction } from '.'
import { setFullKey, VodState } from '../vdos'
import { initConfigSync } from './initTreeMap'
import notificationEvent from './notificationSaga'
import { restNotification } from '../notification'

/**
 * 認證流程
 */
export function* syncVerify(action: PayloadAction) {
  yield put(rootLoading(true))
  try {
    const { data } = yield axios.post(`/api/verify`)
    yield put(setUid(data.permission))
    const stateData: VodState = yield select((state) => state.vdos)
    if (!stateData.treeMap) {
      yield initConfigSync()
    }
    yield put(setSubscriptionAlarmByPermission(data.permission)) //依據權限設定告警監視列表
    //開始監控報警================================================================
    yield fork(notificationEvent)
  } catch (err) {
    yield put(setUid(''))
    yield put(setSubscriptionAlarmByPermission('')) //清空警監視列表
    yield put(setFullKey(-1)) //初始化 FullKey
    yield put(restNotification()) //初始化報警列表
  } finally {
    yield put(rootLoading(false))
  }
}

/**
 * 登入流程
 */
export function* syncLogin(action: PayloadAction<I_LoginParam>) {
  const { name, pass } = action.payload
  yield put(loginMessage(''))
  if (!name) {
    yield put(loginMessage('請輸入帳號!'))
    return
  }
  if (!pass) {
    yield put(loginMessage('請輸入密碼!'))
    return
  }
  yield put(loginMessage(''))
  yield put(rootLoading(true))
  try {
    yield axios.post(`/api/login`, {
      name,
      pass: Md5(`${name}-${pass}-cctv`).toString(),
    })
    yield put(verifyAction())
  } catch (err) {
    yield put(setUid(''))
    yield put(loginMessage('帳號/密碼錯誤!'))
  } finally {
    yield put(rootLoading(false))
  }
}

/**
 * 登出流程
 */
export function* syncLogout(action: PayloadAction) {
  yield put(rootLoading(true))
  try {
    yield axios.post(`/api/logout`)
    yield put(verifyAction())
  } catch (err) {
    yield put(setUid(''))
  } finally {
    yield put(rootLoading(false))
  }
}
