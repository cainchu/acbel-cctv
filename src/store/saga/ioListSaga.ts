import { put, call } from 'redux-saga/effects'
import { setIoList } from '../ioList'
import { ioListSelect } from '../../utils/sdk'
import { PayloadAction } from '@reduxjs/toolkit'
import { setMessage } from '../root'
import axios from 'axios'

export function* getIoList(action?: PayloadAction) {
  try {
    const { data: ioList }: { data: ioItemResult[] } = yield call(ioListSelect)
    yield put(setIoList(ioList))
  } catch (err) {
    if (import.meta.env.DEV) {
      const { data: ioList }: { data: ioItemResult[] } = yield call(() =>
        axios.get('/fakeApi/ioist'),
      )
      yield put(setIoList(ioList))
    } else {
      yield put(setMessage('權限驗證失效!'))
    }
    console.error(err)
  }
}
