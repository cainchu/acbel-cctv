import { put, select } from 'redux-saga/effects'
import Lazy from 'lazy.js'
import { camerasGetterByKey } from '../root'
import { setAiMenuList } from '../aiRoi'
import { hasPermission } from '../../utils/permission'

const AI_SETTING_MAP = [
  /* Hass room */
  { cameraKey: 'a1-1', ai: '10' },
  { cameraKey: 'a1-2', ai: '10' },
  { cameraKey: 'a1-3', ai: '10' },
  { cameraKey: 'a1-4', ai: '10' },
  /* 2F碼頭 */
  { cameraKey: 'a2-1', ai: '11' },
  { cameraKey: 'a2-2', ai: '11' },
  /* 56樓波風爐 */
  { cameraKey: 'a5-1', ai: '01' },
  { cameraKey: 'a5-2', ai: '01' },
  { cameraKey: 'a5-3', ai: '01' },
  { cameraKey: 'a6-1', ai: '01' },
  { cameraKey: 'a6-2', ai: '01' },
  { cameraKey: 'a6-3', ai: '01' },
  /* B棟重要區域 */
  { cameraKey: 'b1-1', ai: '10' },
  { cameraKey: 'b1-2', ai: '10' },
  { cameraKey: 'b2-1', ai: '10' },
  { cameraKey: 'b2-2', ai: '10' },
  { cameraKey: 'b3-1', ai: '10' },
  { cameraKey: 'b3-2', ai: '10' },
]

const aiSettingLazy = Lazy(AI_SETTING_MAP)

const AI_E_SETTING_MAP = [
  /* 2F碼頭 */
  { cameraKey: 'a2-1', ai: '11' },
  { cameraKey: 'a2-2', ai: '11' },
]

const ai_e_SettingLazy = Lazy(AI_E_SETTING_MAP)

/**
 * 設定 AI 選單
 */
export default function* () {
  const permission: string = yield select((state) => state.user.permission) //取得目前權限
  const cameras: treeDataCameraType[] = yield select((state) => state.root.cameras) //取得目前權限

  const aiMenuList = isPermissionAB(permission)
    ? aiSettingLazy
        .map((aiSetting) => {
          const camera = camerasGetterByKey(cameras, aiSetting.cameraKey)
          return camera ? { id: camera.id, title: camera.title, ai: aiSetting.ai } : null
        })
        .filter((camera) => !!camera)
        .toArray()
    : isPermissionE(permission)
    ? ai_e_SettingLazy
        .map((aiSetting) => {
          const camera = camerasGetterByKey(cameras, aiSetting.cameraKey)
          return camera ? { id: camera.id, title: camera.title, ai: aiSetting.ai } : null
        })
        .filter((camera) => !!camera)
        .toArray()
    : []

  yield put(setAiMenuList(aiMenuList as { id: string; title: string; ai: string }[]))
}

/* A B 權限 */
function isPermissionAB(permission: string): boolean {
  return hasPermission('AB')(permission)
}
function isPermissionE(permission: string): boolean {
  return hasPermission('E')(permission)
}
