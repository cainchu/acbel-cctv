import Lazy from 'lazy.js'
import { v4 as uuidv4 } from 'uuid'
import { PayloadAction } from '@reduxjs/toolkit'
import { put, select } from 'redux-saga/effects'
import { jsonClone } from '../../utils/tools'
import {
  setFloorUUid,
  setFullKey,
  setPageType,
  setRealVdoNum,
  setVodLayoutType,
  setVods,
  VDO_LAYOUT,
  VDO_PAGE_TYPE,
  VodState,
} from '../vdos'
import { fullAnVdoAction } from '.'
import { RootState } from '../root'

/**
 * floor顯示攝影機
 */
export function* monitorFloorSync(
  action: PayloadAction<string | { floor: string; pageType: VDO_PAGE_TYPE }>,
) {
  const floorKey = typeof action.payload === 'string' ? action.payload : action.payload.floor
  const pageType =
    typeof action.payload === 'string' ? VDO_PAGE_TYPE.WINDOW : action.payload.pageType
  const fullKey: number = yield getFullKey()
  if (fullKey !== -1) yield put(fullAnVdoAction(fullKey)) //如果滿版情況就關閉
  const stateData: VodState = yield select((state) => state.vdos)
  const { treeMap } = stateData
  const floor: I_Floor | null = Lazy(jsonClone(treeMap))
    .get('floors')
    .find((floor: I_Floor) => floor.uuid === floorKey)
  yield put(setFloorUUid(floorKey))
  if (floor) {
    const type = floor.vdos.length > 4 ? VDO_LAYOUT.V5add1 : VDO_LAYOUT.V4
    yield put(setRealVdoNum(floor.vdos.length))
    yield put(setVodLayoutType(type))
    /* 根據頁面類型決定順續 */
    switch (pageType) {
      case VDO_PAGE_TYPE.FIRE:
        const rootState: RootState = yield select((state) => state.root)
        const { menuTree } = rootState
        const fireItem = Lazy(menuTree?.equipment?.system || []).find(
          (item) => item.path === 'fire',
        )
        const fireFloorData = Lazy(fireItem?.items || []).find(
          (item) => item.path === `/${floorKey.charAt(0)}/${floorKey.charAt(1)}`,
        )
        const lazyVdos = Lazy(floor.vdos)
        const vdos = Lazy(fireFloorData.cameraKeys)
          .map((cameraKey) => {
            return lazyVdos.find((_vdo) => _vdo.uuid === cameraKey)
          })
          .toArray()
        yield put(setVods(vdosNumHeader(vdos, type)))
        break
      default:
        yield put(setVods(vdosNumHeader(floor.vdos, type)))
    }
    yield alarmSync()
  } else {
    yield put(setRealVdoNum(0))
    yield put(setVods([]))
  }
}

/* 設定火警情況的 vdos */
export function* monitorFireFloorSync(
  action: PayloadAction<string | { building: string; floor: string }>,
) {
  const fullKey: number = yield getFullKey()
  if (fullKey !== -1) yield put(fullAnVdoAction(fullKey)) //如果滿版情況就關閉
}

/**
 * 顯示特定監視
 */
export function* monitorCustomSync(action: PayloadAction<string[]>) {
  yield put(setFloorUUid('')) //樓層tag先取消
  const cameras: treeDataCameraType[] = yield select((state) => state.root.cameras)
  //從 cameras 列表取出要用的
  const selectKeyLazy = Lazy(action.payload)
  const vdos = selectKeyLazy
    .map((key) => {
      return Lazy(cameras).find((camera) => camera.cameraKey === key)
    })
    .map((vdo) => ({
      uuid: vdo.cameraKey,
      cameraId: vdo.id,
      title: vdo.title,
      isPlaying: false,
      ptz: vdo.info?.ptz === 1,
      sos: vdo.info?.sos === 1,
    }))
    .toArray()
  yield put(setRealVdoNum(vdos.length))
  yield put(setVods(vdosNumHeader(vdos as I_CtvMeta[], VDO_LAYOUT.V4)))
  yield put(setVodLayoutType(VDO_LAYOUT.V4)) //設定版面4格
  yield alarmSync()
}

/**
 * 交換顯示影片
 */
export function* switchVdoSync(action: PayloadAction<{ uuid: string; key: number }>) {
  const { uuid, key } = action.payload
  const _vdoMetas: I_CtvMeta[] = yield getVdoMetas()
  const _ind = Lazy(_vdoMetas)
    .map((ob) => ob.uuid)
    .indexOf(uuid)
  if (_ind >= 0) {
    const b = _vdoMetas[key]
    _vdoMetas[key] = _vdoMetas[_ind]
    _vdoMetas[_ind] = b
    yield put(setVods(_vdoMetas))
  } else {
    const treeMap: I_TreeMap = yield getTreeMap()
    const floors = Lazy(treeMap).get('floors')
    const vdo = Lazy(floors)
      .pluck('vdos')
      .flatten()
      .find((vdo) => uuid === vdo.uuid)
    _vdoMetas[key] = vdo
    yield put(setVods(_vdoMetas))
  }
  yield alarmSync()
}

/**
 * Full一個影像
 */
export function* fullAnVdoSync(action: PayloadAction<number>) {
  const _vdoMetas: I_CtvMeta[] = yield getVdoMetas()
  const _lastKey = _vdoMetas.length - 1
  const b = _vdoMetas[_lastKey]
  _vdoMetas[_lastKey] = _vdoMetas[action.payload]
  _vdoMetas[action.payload] = b
  yield put(setVods(_vdoMetas))
  const fullKey: number = yield getFullKey()
  yield put(setFullKey(fullKey === -1 ? action.payload : -1))
  yield alarmSync()
}

/**
 * 同步目前的報警訊號，vdos陣列變化與報警變動時都要處理
 */
export function* alarmSync() {
  const validNotifications: I_NotificationMeta[] = yield select(
    (state) => state.notification.validNotifications,
  )
  const validLazy = Lazy<I_NotificationMeta>(validNotifications)
  const _vdoMetas: I_CtvMeta[] = yield getVdoMetas()
  const list = Lazy(_vdoMetas)
    .map((vdo) => {
      vdo.warnTypes = []
      validLazy.each((notification) => {
        if (vdo?.cameraId === notification.id) {
          vdo.warnTypes.push(notification.type)
        }
      })
      return vdo
    })
    .toArray()
  yield put(setVods(list))
}

function* getVdos() {
  const stateData: VodState = yield select((state) => state.vdos)
  return stateData
}

function* getVdoMetas() {
  const stateData: VodState = yield select((state) => state.vdos)
  const { vdoMetas } = stateData
  return jsonClone(vdoMetas)
}

function* getTreeMap() {
  const stateData: VodState = yield select((state) => state.vdos)
  const { treeMap } = stateData
  return jsonClone(treeMap)
}

function* getFullKey() {
  const stateData: VodState = yield getVdos()
  return stateData.fullKey
}

/**
 * 滿版影片情況
 * @returns
 */
function* getIsFullMode() {
  const stateData: VodState = yield getVdos()
  return stateData.fullKey !== -1
}

/**
 * 把vods弄成適當顯示的陣列
 */
function vdosNumHeader(vods: I_CtvMeta[] = [], type: VDO_LAYOUT = VDO_LAYOUT.V5add1) {
  const _num = vods.length
  switch (type) {
    case VDO_LAYOUT.V5add1:
    case VDO_LAYOUT.V4:
      if (_num > type) {
        vods.length = type
      } else {
        while (vods.length < type) {
          vods.push(greaterNullCtvMeta())
        }
      }
      vods.push(greaterNullCtvMeta()) //最後推一個空的打算給Full用
      break
    default:
      console.warn('🥵未定義的類型', type)
  }
  return vods
}

/**
 * 生成空的 vdoMate
 */
export function greaterNullCtvMeta(): I_CtvMeta {
  return {
    uuid: uuidv4(),
    warnTypes: [],
  }
}
