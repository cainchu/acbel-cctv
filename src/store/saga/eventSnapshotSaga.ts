import { call } from 'redux-saga/effects';
import { eventSnapshotSelect } from '../../utils/sdk';
import { PayloadAction } from '@reduxjs/toolkit';

export function* getEventSnapshot(action: PayloadAction<eventSnapshotParams>) {
  const { event_id } = action.payload;
  try {
    const { data: thumbnail } : { data: Blob } = yield call(eventSnapshotSelect, {
      event_id: event_id,
    });
    let urlCreator = window.URL || window.webkitURL;
    let imageUrl = urlCreator.createObjectURL(thumbnail);
  } catch (err) {
    console.error(err);
  }
}