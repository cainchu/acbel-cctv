import { put, call } from 'redux-saga/effects';
import { setAiRoiInfo } from '../aiRoi';
import { setThumbnail } from '../thumbnail';
import { aiRoiSelect, thumbnailSelect, aiRoiCreate, aiRoiDelete, aiRoiShowSwitch } from '../../utils/sdk';
import { PayloadAction } from '@reduxjs/toolkit';

export function* getAiRoiInfo(action: PayloadAction<aiRoiParam>) {
  const { cameraID, ai_type } = action.payload;
  try {
    const { data: aiRoiInfo }: { data: aiRoiResponse[] } = yield call(
      aiRoiSelect,
      {
        cameraID: cameraID,
        ai_type: ai_type,
      },
      );
      // console.log('aiRoiInfo', aiRoiInfo)

    yield put(setAiRoiInfo(aiRoiInfo));
  } catch (err) {
    console.error(err);
  }
}

export function* createAiRoi(action: PayloadAction<aiRoiResponse>) {
  const {
    id,
    ai_type,
    roi_type,
    pos,
    sensitivity,
    notation,
    show_roi,
    roi_color,
  } = action.payload;

  try {
    const { data: response }: { data: string } = yield call(
      aiRoiCreate,
      {
        id,
        ai_type,
        roi_type,
        pos,
        sensitivity,
        notation,
        show_roi,
        roi_color,
      },
      );
      // console.log('create_response', response)
  } catch (err) {
    console.error(err);
  }
}

export function* switchAiRoi(action: PayloadAction<aiRoiResponse>) {
  const {
    id,
    ai_type,
    roi_type,
    pos,
    sensitivity,
    notation,
    show_roi,
    roi_color,
  } = action.payload;

  try {
    const { data: response }: { data: string } = yield call(
      aiRoiShowSwitch,
      {
        id,
        ai_type,
        roi_type,
        pos,
        sensitivity,
        notation,
        show_roi,
        roi_color,
      },
      );
      // console.log('switch_response', response)

  } catch (err) {
    console.error(err);
  }
}
export function* deleteAiRoi(action: PayloadAction<{id:string}>) {
  const {
    id
  } = action.payload;

  try {
    const { data: response }: { data: string } = yield call(
      aiRoiDelete,
      {
        id
      },
      );
      console.log('delete_response', response)

  } catch (err) {
    console.error(err);
  }
}

export function* getThumbnail(action: PayloadAction<thumbnailParam>) {
  const { cameraID } = action.payload;
  try {
    const { data: thumbnail }: { data: Blob } = yield call(thumbnailSelect, {
      cameraID: cameraID,
    });
    let urlCreator = window.URL || window.webkitURL;
    let imageUrl = urlCreator.createObjectURL(thumbnail);
    yield put(setThumbnail(imageUrl));
  } catch (err) {
    console.error(err);
  }
}
