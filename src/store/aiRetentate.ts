import { createSlice, PayloadAction } from '@reduxjs/toolkit';
export interface aiRetentateState {
  aiRetentateUrl: string;
}

const initialState: aiRetentateState = {
  aiRetentateUrl: '',
};

export const aiRetentateSlice = createSlice({
  name: 'aiRetentate',
  initialState,
  reducers: {
    setAiRetentateUrl(state, action: PayloadAction<string>) {
      const aiRetentateUrl = action.payload;
      state.aiRetentateUrl = aiRetentateUrl;
    },
  },
  extraReducers: {},
});

export const { setAiRetentateUrl } = aiRetentateSlice.actions;

export default aiRetentateSlice.reducer;
