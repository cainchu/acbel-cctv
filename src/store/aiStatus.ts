import { createSlice, PayloadAction } from '@reduxjs/toolkit';
export interface aiStatusState {
  aiStatusList: aiStatusParam[];
}

const initialState: aiStatusState = {
  aiStatusList: [],
};

export const aiStatusSlice = createSlice({
  name: 'aiStatus',
  initialState,
  reducers: {
    setAiStatus(state, action: PayloadAction<aiStatusParam[]>) {
      const statusList = action.payload;
      state.aiStatusList = statusList;
    },
  },
  extraReducers: {},
});

export const { setAiStatus } = aiStatusSlice.actions;

export default aiStatusSlice.reducer;
