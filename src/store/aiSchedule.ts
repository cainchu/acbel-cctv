import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { STATUS_CODES } from 'http';
import {createInitialSchedule} from './../utils/tools';
export interface aiScheduleState {
  aiScheduleSelect: {data: scheduleTable | undefined, status:number};
  aiScheduleUpdate: {data: string, status:number};
}

const initialState: aiScheduleState = {
  aiScheduleSelect: {data: undefined, status:0},
  aiScheduleUpdate: {data: '', status:0},
};

export const aiScheduleSlice = createSlice({
  name: 'aiSchedule',
  initialState,
  reducers: {
    setSelectAiSchedule(state, action: PayloadAction<{data: scheduleTable | undefined, status:number}>) {
      // console.log('action', action)
      state.aiScheduleSelect.data = action.payload.data;
      state.aiScheduleSelect.status = action.payload.status;
    },
    setUpdateAiSchedule(state, action: PayloadAction<{data: string , status:number}>) {
      // console.log('action', action)
      state.aiScheduleUpdate.data = action.payload.data;
      state.aiScheduleUpdate.status = action.payload.status;
    },
  },
  extraReducers: {},
});

export const { setSelectAiSchedule, setUpdateAiSchedule } = aiScheduleSlice.actions;

export default aiScheduleSlice.reducer;
