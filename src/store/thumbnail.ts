import { createSlice, PayloadAction } from '@reduxjs/toolkit';
export interface  thumbnailState {
  imageUrl:string;
}

const initialState:  thumbnailState = {
  imageUrl:''
};

export const thumbnailSlice = createSlice({
  name: 'aiRoi',
  initialState,
  reducers: {
    setThumbnail(state, action: PayloadAction<string>) {
      const imageUrl = action.payload;
      state.imageUrl = imageUrl;
    },
  },
  extraReducers: {},
});

export const { setThumbnail } = thumbnailSlice.actions;

export default thumbnailSlice.reducer;
