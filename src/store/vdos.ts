import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import Lazy from 'lazy.js'
import { greaterNullCtvMeta } from './saga/monitorFloorSaga'

export enum VDO_LAYOUT {
  V5add1 = 6,
  V4 = 4,
}

/**
 * 畫面的類型
 */
export enum VDO_PAGE_TYPE {
  WINDOW = 'window', //5+1或4分割
  FIRE = 'fire', //樓層火警
  HASS = 'hass', //hass
  EQUIPMENT = 'equipment', //設備
}

export interface VodState {
  treeMap: I_TreeMap | null
  vdoMetas: I_CtvMeta[]
  vodLayoutType: VDO_LAYOUT
  floorUUid: string
  fullKey: number
  pageType: VDO_PAGE_TYPE
  realVdoNum: number
}

const initialState: VodState = {
  treeMap: null,
  vdoMetas: [],
  vodLayoutType: VDO_LAYOUT.V5add1, //5+1版面 或 4分割
  floorUUid: '',
  fullKey: -1, //如果不是-1代表這個key目前滿版
  pageType: VDO_PAGE_TYPE.WINDOW, //頁面的類型
  realVdoNum: 0, //實際此顯示的攝影機數量
}

export const vdosSlice = createSlice({
  name: 'vdos',
  initialState,
  reducers: {
    /* 資料初始化 */
    setTreeMap(state, action: PayloadAction<I_TreeMap>) {
      state.treeMap = action.payload
    },
    /**
     * 設定播放頁面內容
     */
    setVods(state, action: PayloadAction<I_CtvMeta[]>) {
      state.vdoMetas = action.payload
    },
    /**
     * 改變監視畫面版面
     */
    setVodLayoutType(state, action: PayloadAction<VDO_LAYOUT>) {
      state.vodLayoutType = action.payload
    },
    /**
     * 顯示樓層的影片
     */
    setFloorUUid(state, action: PayloadAction<string>) {
      state.floorUUid = action.payload
    },
    /**
     * 更變isPlaying key
     */
    setVdoLifeCycle(state, action: PayloadAction<{ uuid: string; isPlaying: boolean }>) {
      const { uuid, isPlaying } = action.payload
      const vdo: I_CtvMeta | null = Lazy(state.treeMap?.floors)
        .pluck('vdos')
        .flatten()
        .find((vdo) => vdo.uuid === uuid)
      if (vdo) {
        vdo.isPlaying = isPlaying
      }
    },
    /**
     * 移除一個播放 by 位置 key
     */
    claerVdoGrupByKey(state, action: PayloadAction<number>) {
      if (action.payload <= state.vdoMetas.length - 1)
        state.vdoMetas[action.payload] = greaterNullCtvMeta()
    },
    /**
     * 滿版某個key的影片欄位
     */
    setFullKey(state, action: PayloadAction<number>) {
      state.fullKey = action.payload
    },
    /**
     * 設定頁面的類型
     */
    setPageType(state, action: PayloadAction<VDO_PAGE_TYPE>) {
      state.pageType = action.payload
    },
    setRealVdoNum(state, action: PayloadAction<number>) {
      state.realVdoNum = action.payload
    },
  },
  extraReducers: {},
})

export const {
  setTreeMap,
  setVods,
  setVodLayoutType,
  setFloorUUid,
  setVdoLifeCycle,
  claerVdoGrupByKey,
  setFullKey,
  setPageType,
  setRealVdoNum,
} = vdosSlice.actions

export default vdosSlice.reducer
