import { createSlice, PayloadAction } from '@reduxjs/toolkit';
export interface ioAlarmListState {
  ioAlarmList: ioAlarmSwitchItem[];
}

const initialState:ioAlarmListState = {
  ioAlarmList:[]
};

export const ioAlarmListSlice = createSlice({
  name: 'ioAlarmList',
  initialState,
  reducers: {
    setIoAlarmList(state, action: PayloadAction<ioAlarmSwitchItem[]>) {
      const ioAlarmList = action.payload;
      state.ioAlarmList = ioAlarmList;
    },
  },
  extraReducers: {},
});

export const { setIoAlarmList } = ioAlarmListSlice.actions;

export default ioAlarmListSlice.reducer;
