import { createSlice, PayloadAction } from '@reduxjs/toolkit';
export interface broadcastState {
  broadcast: broadcastTable;
}

const initialState: broadcastState = {
  broadcast: {
    teams_hook_hanging_platform: '',
    mail_address_hanging_platform: '',
    teams_hook_truck_40m: '',
    mail_address_truck_40m: '',
    teams_hook_person_invasion: '',
    mail_address_person_invasion: '',
    teams_hook_lack_oxygen: '',
    mail_address_lack_oxygen: '',
    teams_hook_fire_alarm: '',
    mail_address_fire_alarm: '',
    teams_hook_gas_fire: '',
    mail_address_gas_fire: '',
    teams_hook_clearance_error: '',
    mail_address_clearance_error: ''
  },
};

export const broadcastSlice = createSlice({
  name: 'broadcast',
  initialState,
  reducers: {
    setBroadcast(state, action: PayloadAction<broadcastTable>) {
      const broadcast = action.payload;
      state.broadcast = broadcast;
    },
  },
  extraReducers: {},
});

export const { setBroadcast } = broadcastSlice.actions;

export default broadcastSlice.reducer;
