import { createSlice, PayloadAction } from '@reduxjs/toolkit';
export interface ioListState {
  ioList: ioItemResult[];
}

const initialState:ioListState = {
  ioList:[]
};

export const ioListSlice = createSlice({
  name: 'ioList',
  initialState,
  reducers: {
    setIoList(state, action: PayloadAction<ioItemResult[]>) {
      const ioList = action.payload;
      state.ioList = ioList;
    },
  },
  extraReducers: {},
});

export const { setIoList } = ioListSlice.actions;

export default ioListSlice.reducer;
