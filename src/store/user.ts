import { createSlice, PayloadAction } from '@reduxjs/toolkit';

export interface UserState {
  uid: string;
  permission: string;
  loginMessage: string;
}

const initialState: UserState = {
  uid: '',
  permission: '',
  loginMessage: '',
};

export const userSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {
    /**
     * user id
     */
    setUid(state, action: PayloadAction<string>) {
      state.uid = action.payload;
      state.permission = action.payload;
    },
    /**
     * 登入訊息(error)
     */
    loginMessage(state, action: PayloadAction<string>) {
      state.loginMessage = action.payload;
    },
  },
  extraReducers: {},
});

export const { setUid, loginMessage } = userSlice.actions;

export default userSlice.reducer;
