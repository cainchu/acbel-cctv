import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import Lazy from 'lazy.js'
import { jsonClone } from '../utils/tools'

export enum IO_KEY {
  fire_B2F = 'A-B2-firealarm',
  fire_B1F = 'A-B1-firealarm',
  fire_1F = 'A-1F-firealarm',
  fire_2F = 'A-2F-firealarm',
  fire_3F = 'A-3F-firealarm',
  fire_4F = 'A-4F-firealarm',
  fire_5F = 'A-5F-firealarm',
  fire_6F = 'A-6F-firealarm',
  fire_7F = 'A-7F-firealarm',
  fire_8F = 'A-8F-firealarm',
  fire_9F = 'A-9F-firealarm',
  fire_B1 = 'B-1F-firealarm',
  fire_B2 = 'B-2F-firealarm',
  fire_B3 = 'B-3F-firealarm',
  door_3f = 'A-3F-hangdoor',
  door_4f = 'A-4F-hangdoor',
  door_5f = 'A-5F-hangdoor',
  door_6f = 'A-6F-hangdoor',
  door_b2 = 'B-2F-hangdoor',
  gas_fire_1 = 'A-4F-co2sensor1',
  gas_fire_2 = 'A-4F-co2sensor2',
  gas_fire_3 = 'A-4F-co2sensor3',
  gas_fire_4 = 'A-4F-co2sensor4',
  gas_fire_5 = 'A-4F-co2sensor5',
  gas_fire_6 = 'A-4F-co2sensor6',
  hass1 = 'A-1F-co2sensor1',
  hass2 = 'A-1F-co2sensor2',
  piar1 = 'A-2F-laneright',
  piar2 = 'A-2F-laneleft',
  sos_a1 = 'A-1F-cameraSos',
  sos_a3 = 'A-3F-cameraSos',
  sos_a4 = 'A-4F-cameraSos',
  sos_a5 = 'A-5F-cameraSos',
  sos_a6 = 'A-6F-cameraSos',
  sos_b2 = 'B-2F-cameraSos',
}

export interface RootState {
  loading: number
  message: string
  config: I_Config | {}
  pushPath: string | null
  builds: treeDataBuilhingType[]
  floors: treeDataFloorType[]
  cameras: treeDataCameraType[]
  ios: treeDataIoType[]
  menuTree: menuTreeType | null
  subscriptionAlarm: I_AlarmMeta
  readyPagePath: string
  initDataReady: boolean
  speakerMap: { [key: string]: string }
}

const initialState: RootState = {
  loading: 0,
  /* 没有在任何地方使用。 */
  message: '',
  pushPath: null, //給redux的路由資訊，如果有變動會導致換頁，會影響 hooks/usePushPath
  config: {},
  builds: [], //建築物
  floors: [], //樓層
  cameras: [], //攝影機
  ios: [], //IO對應表
  menuTree: null, //選單結構樹
  subscriptionAlarm: { camera: [], io: [] }, //需要監視的設備ID列表
  readyPagePath: '', //ready的 page path
  initDataReady: false,
  speakerMap: {}, //廠區擴音喇叭UUID對應
}

export const rootSlice = createSlice({
  name: 'root',
  initialState,
  reducers: {
    dataReady(state) {
      state.initDataReady = true
    },
    setConfig(state, action: PayloadAction<I_Config>) {
      state.config = action.payload
    },
    /* 路由換頁 */
    setPushPath(state, action: PayloadAction<string | null>) {
      state.pushPath = action.payload
    },
    /**
     * 全域的 loadomg
     */
    rootLoading(state, action: PayloadAction<boolean>) {
      state.loading += action.payload ? 1 : -1
    },
    setMessage(state, action: PayloadAction<string>) {
      state.message = action.payload
    },
    setBuildingTree(state, action: PayloadAction<treeBuildingMetaType>) {
      const { builhing, floors, camera, io } = action.payload
      state.builds = builhing
      state.floors = floors
      state.cameras = camera
      state.ios = io
    },
    setMenuTree(state, action: PayloadAction<menuTreeType>) {
      state.menuTree = action.payload
    },
    /**
     * 依據權限設定告警監視列表
     */
    setSubscriptionAlarmByPermission(state, action: PayloadAction<string>) {
      const camerasLazy = Lazy<treeDataCameraType>(jsonClone(state.cameras))
      const ioLazy = Lazy<treeDataCameraType>(jsonClone(state.ios))
      state.subscriptionAlarm = {
        camera:
          camerasLazy
            .filter((_camera) => !!_camera.id)
            .filter((_camera) => _camera.permission.indexOf(action.payload) >= 0)
            .map((_camera) => _camera.id)
            .toArray() || [],
        io:
          ioLazy
            .filter((_io) => _io.permission.indexOf(action.payload) >= 0)
            .map((_io) => _io.ioKey)
            .toArray() || [],
      }
    },
    setReadyPagePath(state, action: PayloadAction<string>) {
      state.readyPagePath = action.payload
    },
    setSpeakerMap(state, action: PayloadAction<I_CameraInfo[]>) {
      action.payload.forEach((speaker) => {
        state.speakerMap[speaker.keyword] = speaker.uuid
      })
    },
  },
  extraReducers: {},
})

export const {
  dataReady,
  setConfig,
  rootLoading,
  setBuildingTree,
  setMenuTree,
  setSubscriptionAlarmByPermission,
  setPushPath,
  setReadyPagePath,
  setSpeakerMap,
  setMessage,
} = rootSlice.actions

export default rootSlice.reducer

/**
 * 取得特定 camera
 * @param cameras
 * @param id
 * @returns
 */
export const camerasGetterById = (cameras: treeDataCameraType[], id: string) =>
  Lazy(cameras).find((camera) => camera.id === id)

export const camerasGetterByKey = (cameras: treeDataCameraType[], key: string) =>
  Lazy(cameras).find((camera) => camera.cameraKey === key)

/**
 * 找到指定的IO
 * @param ios
 * @param key
 * @returns
 */
export const iosGetterByKey = (ios: treeDataIoType[], key: string) =>
  Lazy<treeDataIoType>(ios).find((io) => io.ioKey === key)

/**
 * 取回system中的所有cameraKeys
 * @param system
 */
function getLazySystemAllCameraKeys(system: menuTreeItemListType[]) {
  return Lazy(system).pluck('items').flatten().pluck('cameraKeys').flatten()
}

/**
 * key置換成id，參數都是lazy物件
 */
function exchangeCameraKeyToId(
  lazyKeys: LazyJS.Sequence<string>,
  lazyCamera: LazyJS.Sequence<treeDataCameraType>,
) {
  return lazyKeys
    .uniq()
    .map((key) => {
      return lazyCamera.find((camera) => camera.cameraKey === key)
    })
    .filter((item) => !!item)
    .pluck('id')
    .toArray()
}
