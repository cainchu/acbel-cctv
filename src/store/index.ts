import { configureStore } from '@reduxjs/toolkit';
import { TypedUseSelectorHook, useDispatch, useSelector } from 'react-redux';
import createSagaMiddleware from 'redux-saga';
import rootReducer from './root';
import userReducer from './user';
import vdosReducer from './vdos';
import notificationReducer from './notification';
import aiRoiReducer from './aiRoi';
import aiStatusReducer from './aiStatus';
import thumbnailReducer from './thumbnail';
import aiRetentateReducer from './aiRetentate';
import historyReducer from './history';
import aiScheduleReducer from './aiSchedule';
import broadcastReducer from './broadcast';
import ioListReducer from './ioList';
import ioAlarmListReducer from './ioAlarmList';
import rootSaga from './saga';

const sagaMiddleware = createSagaMiddleware();
export const store = configureStore({
  reducer: {
    root: rootReducer /* 整個APP的主狀態機 */,
    user: userReducer /* User的資訊 */,
    vdos: vdosReducer /* 畫面視訊的資訊 */,
    notification: notificationReducer /* 及時告警訊號 */,
    aiRoiList: aiRoiReducer,
    thumbnail: thumbnailReducer,
    aiStatusList: aiStatusReducer,
    aiRetentate: aiRetentateReducer,
    history: historyReducer,
    aiSchedule: aiScheduleReducer,
    broadcast: broadcastReducer,
    ioList: ioListReducer,
    ioAlarmList: ioAlarmListReducer 
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware().concat(sagaMiddleware),
});

sagaMiddleware.run(rootSaga);

type RootState = ReturnType<typeof store.getState>;
type AppDispatch = typeof store.dispatch;
export const useAppDispatch = () => useDispatch<AppDispatch>();
export const useAppSelector: TypedUseSelectorHook<RootState> = useSelector;
