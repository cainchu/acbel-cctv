import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { jsonClone } from '../utils/tools'
import Lazy from 'lazy.js'

export interface NotificationState {
  notifications: I_NotificationMeta[]
  validNotifications: I_NotificationMeta[]
  alarmSoundOff: boolean
}

const initialState: NotificationState = {
  notifications: [] /* 即時告警訊號 */,
  validNotifications: [] /* 有效用的報警，為了效率特別整理出來 */,
  alarmSoundOff: false, //用來開關警報音訊是否撥音
}

export const notificationSlice = createSlice({
  name: 'notification',
  initialState,
  reducers: {
    /* 新增一個告警訊號(反向推送) */
    pushNotification(state, action: PayloadAction<I_NotificationMeta>) {
      const hasInNotification /* 已存在告警 */ =
        Lazy<I_NotificationMeta>(jsonClone(state.validNotifications))
          .filter(
            (notification) =>
              notification.id === action.payload.id && notification.type === action.payload.type,
          )
          .size() > 0
      hasInNotification || state.validNotifications.push(action.payload)
      /* 檢查是否已有重複的告警，後期hot fix請優化.. */
      Lazy<I_NotificationMeta>(jsonClone(state.notifications))
        .filter(
          (notification) =>
            notification.id === action.payload.id &&
            notification.time === action.payload.time &&
            notification.type === action.payload.type,
        )
        .size() > 0 || state.notifications.unshift(action.payload)
    },
    /* 移除一個 */
    removeNotification(state, action: PayloadAction<I_NotificationMeta>) {
      const list = jsonClone(state.notifications)
      const removeItem = action.payload
      state.notifications = Lazy<I_NotificationMeta>(list)
        .map((item: I_NotificationMeta) => {
          if (item.id === removeItem.id && item.type === removeItem.type) {
            item.disable = true
          }
          return item
        })
        .toArray()
      //在validNotifications移除他
      const validList = jsonClone(state.validNotifications)
      const index = Lazy<I_NotificationMeta>(validList).indexOf((item: I_NotificationMeta) => {
        return item.id === removeItem.id && item.type === removeItem.type
      })
      validList.splice(index, 1)
      state.validNotifications = validList
    },
    restNotification(state) {
      state.notifications = []
      state.validNotifications = []
    },
    setAlarmSoundOff(state, action: PayloadAction<boolean>) {
      state.alarmSoundOff = action.payload
    },
  },
  extraReducers: {},
})

export const { pushNotification, removeNotification, restNotification, setAlarmSoundOff } =
  notificationSlice.actions

export default notificationSlice.reducer
