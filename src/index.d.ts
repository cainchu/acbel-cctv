/**
 * 網站設定
 */
declare interface I_Config {}

declare interface I_LoginParam {
  name: string
  pass: string
}

/* 螢幕播放的CTV Meta */
declare interface I_CtvMeta {
  uuid: string
  cameraId?: string
  title?: string
  isPlaying?: boolean
  warnTypes: string[]
  ptz?: boolean
  sos?: boolean
}

declare interface I_Floor {
  uuid: string
  title: string
  vdos: I_CtvMeta[]
}

declare interface I_TreeMap {
  floors: I_Floor[]
}

declare interface I_NotificationMeta {
  id: string
  type: string
  time: number
  disable?: boolean
}

declare interface I_FloorMeta {
  title: string
}

declare interface I_BuildingMeta {
  floors: I_FloorMeta[]
}

/* 結構樹大樓資料 */
declare type treeDataBuilhingType = {
  buildKey: string
  title: string
}

/* 結構樹樓層資料 */
declare type treeDataFloorType = {
  floorKey: string
  building: string
  title: string
}

/* 攝影機IO結構 */
declare type I_CameraInfo = {
  analysis_rtsp: string
  area: string
  gearing: string
  io_device: string
  id: string
  ip: string
  keyword: string
  mac: string
  ptz: number
  sos: number
  source: string
  uuid: string
}

/* 結構樹攝影機資料 */
declare type treeDataCameraType = {
  ioKey: string
  floor: string
  cameraKey: string
  id: string
  title: string
  info?: I_CameraInfo
  type?: string
  permission: string
}

/* 結構樹 的 io 資料 */
declare type treeDataIoType = {
  ioKey: string
  cameraKey: string
  type: string
  permission: string
}

/* 結構樹規格。 */
declare type treeBuildingMetaType = {
  builhing: treeDataBuilhingType[]
  floors: treeDataFloorType[]
  camera: treeDataCameraType[]
  io: treeDataIoType[]
}

/* 支援Ai功能的攝影機格式 */
declare type aiCamera = {
  id: string
  title: string
  ai: string
}

/*  需要監視的設備ID列表 */
declare interface I_AlarmMeta {
  camera: string[]
  io: string[]
}

/* 攝影機告警狀態 */
declare interface I_CameraAlertState {
  id: string
  ai_fences: boolean
  ai_fences_alarm_time: number
  ai_retentate: boolean
  ai_retentate_alarm_time: number
  sos?: boolean
  sos_time?: number
}

/* IO告警狀態 */
declare interface I_IoAlertState {
  id: string
  alarm: boolean[]
  alarm_time: number
}

/* 監視的告警狀態列表 */
declare type AlarmMetaResType = {
  cmera: I_CameraAlertState[]
  io: I_IoAlertState[]
}

/* Ai圖形查詢回應；新增、刪除、修改參數。 */
declare type aiRoiResponse = {
  id: string
  ai_type: string
  roi_type: string
  pos: string
  sensitivity: number
  notation: string
  show_roi: number
  roi_color: string
  uuid?: string
}

/* 特定攝影機Ai圖形列表查詢參數。 */
declare type aiRoiParam = {
  cameraID: string
  ai_type: string
}

/* 特定攝影機查詢截圖參數。 */
declare type thumbnailParam = {
  cameraID: string
}

/* 特定攝影機Ai功能開啟狀態參數。 */
declare type aiStatusParam = {
  id: string
  ai_switch: number
  human_detection: number
  ai_fences: number
  ai_retentate: number
  ai_schedule: number
}

/* 攝影機歷史資料前端參數。 */
declare type historyFrontParams = {
  id: string
  analysis_type: string
  start_alarm_timestamp: number
  end_alarm_timestamp: number
  offset: number
  data_length_per_page: number
  round: number
  device_type: string
  matched_camera: string
}

/* 攝影機歷史資料後端參數。 */
declare type historyBackParams = {
  id: string
  analysis_type: string
  start_alarm_timestamp: number
  end_alarm_timestamp: number
  offset: number
  end: number
  device_type: string
}

/* 攝影機歷史資料結構。 */
declare type historyResult = {
  photo_path?: string
  event_id: string
  id: string
  device: string
  analysis_type: string
  start_alarm_timestamp: number
  end_alarm_timestamp: number
  notation: string
}

/* 歷史紀錄查詢結果。 */
declare type historyResponse = {
  total: number
  result: historyResult[]
}

/* I/O裝置 */
declare type ioItemResult = {
  io_type: string
  keyword: string
}

declare type ioItem = {
  id: string
  ioKey: string
  permission: string
  location: string
  type: string
}

/* 單一歷史事件截圖參數。 */
declare type eventSnapshotParams = {
  event_id: string
}

/* 排程查詢參數 */
declare type scheduleSelectParams = {
  id: string
  ai_type: string
}

/* 排程更新參數 */
declare type scheduleUpdateParams = {
  id: string
  ai_type: string
  schedule_table: scheduleTable | undefined
}

/* 單一裝置排程查詢結果 */
declare type scheduleTable = {
  [key: string]: string
  mon: string
  tue: string
  wed: string
  thu: string
  fri: string
  sat: string
  sun: string
}

/* 裝置分析或偵測類型 */
declare type analysisType = {
  title: string
  sysName: string
  enable?: boolean
}

/* 推播查詢結果 */
declare type broadcastTable = {
  [key: string]: string
  teams_hook_hanging_platform: string
  mail_address_hanging_platform: string
  teams_hook_truck_40m: string
  mail_address_truck_40m: string
  teams_hook_person_invasion: string
  mail_address_person_invasion: string
  teams_hook_lack_oxygen: string
  mail_address_lack_oxygen: string
  teams_hook_fire_alarm: string
  mail_address_fire_alarm: string
  teams_hook_gas_fire: string
  mail_address_gas_fire: string
  teams_hook_clearance_error: string
  mail_address_clearance_error: string
}

/* 推播更新Url結構 */
declare type broadcastUpdateUrl = {
  [key: string]: string
  url: string
}

/* 推播更新參數 */
declare type broadcastUpdateParams = {
  eventId: string
  targetUrl: broadcastUpdateUrl
}

/* I/O告警設定更新請求狀態 */
declare type ioAlarmUpdateStatus = {
  [key: string]: number
  alarm_status: number
}

/* I/O告警開關單一設定項目 */
declare type ioAlarmSwitchItem = {
  id: string
  status: number
}

/* I/O告警設定更新參數 */
declare type ioAlarmUpdateParams = {
  id: string
  status: ioAlarmUpdateStatus
}

/* 選單物件結構。 */
declare type menuTreeItemType = {
  title: string
  cameraKeys: string[]
  path?: string
}

/* 選單群組列表結構。 */
declare type menuTreeItemListType = {
  title: string
  path: string
  items: menuTreeItemType[]
}

/* 選單模塊結構。 */
declare type menuTreeModuleType = {
  title: string
  system: menuTreeItemListType[]
}

/* 選單樹結構 */
declare type menuTreeType = {
  equipment: menuTreeModuleType
  production: menuTreeModuleType
  pier: menuTreeItemType
}

/* Ai功能攝影機列表 */
declare type aiDeviceMenuType = {
  ai: string
  id: string
  title: string
}
