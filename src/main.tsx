import 'webrtc-adapter'
import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App'
import './index.css'
import axios from 'axios'

axios.defaults.timeout = 5000
// axios.defaults.timeout = 200

/* Web RTC 服務器 */
// import { rtspSetting } from './components/WTCVideo';
// import { rtspSetting } from 'cain-wtc-video'
// rtspSetting.server = 'http://192.168.1.135:8083/stream';
// rtspSetting.server = '/stream'

ReactDOM.createRoot(document.getElementById('root')!).render(
  // <React.StrictMode>
  <App />,
  // </React.StrictMode>,
)
