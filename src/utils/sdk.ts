import axios, { AxiosError, AxiosResponse } from 'axios'
const API_SCOPE = '/api'
const NX_SCOPE = '/nx'
enum API {
  CONFIG = `/config`,
  GET_BUILGING = `/builging`,
  GET_MENUTREE = `/menuTree`,
}
/* 取得網站設定。 */
export const getConfig = (): Promise<AxiosResponse<I_Config, AxiosError>> => {
  return axios.get(`${API_SCOPE}${API.CONFIG}`)
}
/* 取得建物資訊。 */
export const getBuilging = (): Promise<AxiosResponse<treeBuildingMetaType, AxiosError>> => {
  return axios.get(`${API_SCOPE}${API.GET_BUILGING}`)
}

/* 取得Menu資訊。 */
export const getMenuTree = (): Promise<AxiosResponse<menuTreeType, AxiosError>> => {
  return axios.get(`${API_SCOPE}${API.GET_MENUTREE}`)
}

const AI_API_SCOPE = '/ivapControl'
enum AI_API {
  cameraList = `/cameraList`,
  aiSetting = `/aiSetting`,
  aiRoiSetting = `/aiRoiSetting`,
  thumbnailSelect = `/cameraThumbnail`,
  aiDisplaySetting = `/aiDisplaySetting`,
  aiRetentate = `/aiRetentate`,
  analysisTmpResult = `/analysisTmpResult`,
  aiSchedule = `/aiSchedule`,
  aiEventPush = `/aiEventPush`,
  ioList = `/ioList`,
  ioAlarmSwitch = `/ioAlarmSwitch`,
  moveptz = `/moveptz` /* 移動ptz */,
}

/* 移動ptz API */
export const movePtz = (ptz: string) => {
  return axios.get(`${AI_API_SCOPE}${AI_API.moveptz}/${ptz}`)
}

/* 取得攝影機IOT列表。 */
export const cameraList = (): Promise<AxiosResponse<I_CameraInfo[], AxiosError>> => {
  return axios.get(`${AI_API_SCOPE}${AI_API.cameraList}`)
}

export const cameraListWithStorage = () =>
  new Promise<I_CameraInfo[]>(async (resolve, reject) => {
    try {
      const { data } = await cameraList()
      window.localStorage.setItem('cameraList', JSON.stringify(data))
      resolve(data)
    } catch (error) {
      if (import.meta.env.DEV) {
        const { data } = await axios.get('/fakeApi/cameraList')
        resolve(data)
      } else {
        const localData = window.localStorage.getItem('cameraList')
        localData ? resolve(JSON.parse(localData)) : resolve([])
      }
      // localData ? resolve(JSON.parse(localData)) : reject(error)
    }
  })

/* 每隻相機AI功能狀態查詢 */
export const aiSetting = (): Promise<AxiosResponse<unknown, AxiosError>> => {
  return axios.get(`${AI_API_SCOPE}${AI_API.aiSetting}`)
}

export const aiStatusSelect = (): Promise<AxiosResponse<unknown, AxiosError>> => {
  return axios.get(`${AI_API_SCOPE}${AI_API.aiSetting}`)
}
export const aiStatusUpdate = ({
  id,
  ai_switch,
  human_detection,
  ai_fences,
  ai_retentate,
  ai_schedule,
}: aiStatusParam): Promise<AxiosResponse<unknown, AxiosError>> => {
  const data = {
    id,
    ai_switch,
    human_detection,
    ai_fences,
    ai_retentate,
    ai_schedule,
  }
  return axios.put(`${AI_API_SCOPE}${AI_API.aiSetting}`, data)
}

export const aiRoiSelect = ({
  cameraID,
  ai_type,
}: aiRoiParam): Promise<AxiosResponse<unknown, AxiosError>> => {
  return axios.get(`${AI_API_SCOPE}${AI_API.aiRoiSetting}/${cameraID}/${ai_type}`)
}

export const aiRoiCreate = ({
  id,
  ai_type,
  roi_type,
  pos,
  sensitivity,
  notation,
  show_roi,
  roi_color,
}: aiRoiResponse): Promise<AxiosResponse<unknown, AxiosError>> => {
  const data = {
    id,
    ai_type,
    roi_type,
    pos,
    sensitivity,
    notation,
    show_roi,
    roi_color,
  }
  return axios.post(`${AI_API_SCOPE}${AI_API.aiRoiSetting}`, data)
}

export const aiRoiShowSwitch = ({
  id,
  ai_type,
  roi_type,
  pos,
  sensitivity,
  notation,
  show_roi,
  roi_color,
}: aiRoiResponse): Promise<AxiosResponse<unknown, AxiosError>> => {
  const data = {
    id,
    ai_type,
    roi_type,
    pos,
    sensitivity,
    notation,
    show_roi,
    roi_color,
  }
  return axios.put(`${AI_API_SCOPE}${AI_API.aiRoiSetting}/showRoi`, data)
}

export const aiRoiDelete = ({
  id,
}: {
  id: string
}): Promise<AxiosResponse<unknown, AxiosError>> => {
  const data = {
    id: id,
  }
  return axios.delete(`${AI_API_SCOPE}${AI_API.aiRoiSetting}`, { data })
}

export const thumbnailSelect = ({
  cameraID,
}: thumbnailParam): Promise<AxiosResponse<unknown, AxiosError>> => {
  let res = axios.get(`${AI_API_SCOPE}${AI_API.thumbnailSelect}/${cameraID}`, {
    responseType: 'blob',
  })
  let result
  res.then((innerRes: any) => {
    result = innerRes
  })
  return result ? result : res
}

export const aiRetentateBackgroundSelect = ({
  cameraID,
}: thumbnailParam): Promise<AxiosResponse<unknown, AxiosError>> => {
  let res = axios.get(`${AI_API_SCOPE}${AI_API.aiRetentate}/${cameraID}`, {
    responseType: 'blob',
  })
  let result
  res.then((innerRes: any) => {
    result = innerRes
  })
  return result ? result : res
}

export const aiRetentateBackgroundUpdate = ({
  cameraID,
}: thumbnailParam): Promise<AxiosResponse<unknown, AxiosError>> => {
  return axios.put(`${AI_API_SCOPE}${AI_API.aiRetentate}/${cameraID}`)
}

export const historySelect = ({
  id,
  analysis_type,
  start_alarm_timestamp,
  end_alarm_timestamp,
  offset,
  end,
  device_type,
}: historyBackParams): Promise<AxiosResponse<unknown, AxiosError>> => {
  return axios.get(
    `${AI_API_SCOPE}${AI_API.analysisTmpResult}/result/${device_type}/${start_alarm_timestamp}/${end_alarm_timestamp}?id=${id}&analysis_type=${analysis_type}&page_rang=${offset},${end}`,
  )
}

export const eventSnapshotSelect = ({
  event_id,
}: eventSnapshotParams): Promise<AxiosResponse<unknown, AxiosError>> => {
  let res = axios.get(`${AI_API_SCOPE}${AI_API.analysisTmpResult}/image/${event_id}`, {
    responseType: 'blob',
  })
  let result
  res.then((innerRes: any) => {
    result = innerRes
  })
  return result ? result : res
}

export const scheduleSelect = ({
  id,
  ai_type,
}: scheduleSelectParams): Promise<AxiosResponse<unknown, AxiosError>> => {
  return axios.get(`${AI_API_SCOPE}${AI_API.aiSchedule}/${id}/${ai_type}`)
}

export const scheduleUpdate = ({
  id,
  ai_type,
  schedule_table,
}: scheduleUpdateParams): Promise<AxiosResponse<unknown, AxiosError>> => {
  const data = schedule_table
  return axios.patch(`${AI_API_SCOPE}${AI_API.aiSchedule}/${id}/${ai_type}`, data)
}

export const broadcastSelect = (): Promise<AxiosResponse<unknown, AxiosError>> => {
  return axios.get(`${AI_API_SCOPE}${AI_API.aiEventPush}`)
}

export const broadcastUpdate = ({
  eventId,
  targetUrl,
}: broadcastUpdateParams): Promise<AxiosResponse<unknown, AxiosError>> => {
  const data = targetUrl
  return axios.patch(`${AI_API_SCOPE}${AI_API.aiEventPush}/${eventId}`, data)
}

export const ioAlarmSelect = (): Promise<AxiosResponse<unknown, AxiosError>> => {
  return axios.get(`${AI_API_SCOPE}${AI_API.ioAlarmSwitch}`)
}

export const ioAlarmUpdate = ({
  id,
  status,
}: ioAlarmUpdateParams): Promise<AxiosResponse<unknown, AxiosError>> => {
  const data = status
  return axios.patch(`${AI_API_SCOPE}${AI_API.ioAlarmSwitch}/${id}`, data)
}

export const ioListSelect = (): Promise<AxiosResponse<unknown, AxiosError>> => {
  return axios.get(`${AI_API_SCOPE}${AI_API.ioList}`)
}

/**
 * 取回NX系統時間
 * @returns 一个承诺。
 */
export const getNxTime = (): Promise<AxiosResponse<unknown, AxiosError>> => {
  return axios.get(`${NX_SCOPE}/api/getTime`)
}
