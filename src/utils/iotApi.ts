import axios from 'axios'
/**
 * SMT設備點位api
 * @param line - 那條線 A~F
 * @param startTime
 * @param endTime
 * @returns
 */
export const smpApi = (line: string, startTime: number, endTime: number) =>
  axios.get(
    `/smartFactory/apis/equips/alarm?type=Line&section=SMT&select=SMT-${line}&startTime=${startTime}&endTime=${endTime}`,
  )
