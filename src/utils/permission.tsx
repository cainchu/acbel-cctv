/**
 * 權限驗證器
 * @param {string} testPermissionStr - string - 这是您要测试的字符串。
 * @returns 一个接受字符串并返回数字的函数。
 */
export const hasPermission = (testPermissionStr: string) => {
  const _testPermissionStr = testPermissionStr.toLowerCase()
  return (permission: string): boolean =>
    _testPermissionStr.indexOf(permission.toLowerCase()) !== -1
}
