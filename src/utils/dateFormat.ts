import moment from 'moment'

/**
 * 返回格式为 `YYYY-MM-DDTHH:mm:ss.SSSZ` 的字符串
 * @param time - moment.MomentInput
 */
export const yyyy_mm_dd_h_m_s = (time: string | number) =>
  moment(new Date(time)).format('YYYY/MM/DD HH:mm:ss')
