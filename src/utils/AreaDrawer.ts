import { Layer } from 'konva/lib/Layer'
import { Circle } from 'konva/lib/shapes/Circle'
import { Path, PathConfig } from 'konva/lib/shapes/Path'
import { Stage, StageConfig } from 'konva/lib/Stage'
import { fromEvent, Subscription } from 'rxjs'

export interface I_xy {
  x: number
  y: number
}
export type xyPoints = I_xy[]

/**
 * 區域繪製Tools
 */
export default class AreaDrawer extends Stage {
  static selectColor: string = '#00088'
  private _layer = new Layer()
  private _path!: Path
  private _points: Circle[] = []
  private keyup$: Subscription | undefined
  private _onPoints: (points: xyPoints) => void | undefined
  private _circle: Circle | null = null
  private _pathSetting: PathConfig = {
    x: 0,
    y: 0,
    data: '',
    fill: '#ff000088',
    stroke: '#ff0000',
    strokeWidth: 1,
    scaleX: 1,
    scaleY: 1,
    max: 99, //數量限制
  }
  private _canEdit: boolean
  /**
   * 构造函数接受一个 StageConfig、一个 PathConfig 和一个接受 xyPoints 并且不返回任何内容的函数。
   * @param {StageConfig} setting - 阶段配置
   * @param {PathConfig | undefined} pathConfig - 路径配置 |不明确的，
   * @param onPoints - （点：xyPoints）=> 无效 |不明确的，
   */
  constructor(
    setting: StageConfig,
    pathConfig: PathConfig | undefined,
    canEdit: boolean = false,
    onPoints: (points: xyPoints) => void | undefined,
  ) {
    super(setting)
    this._canEdit = canEdit
    this._init()
    this._initPath(pathConfig || {})
    this._onPoints = onPoints
  }
  _init() {
    this.add(this._layer)
  }
  _initPath(config: PathConfig) {
    this._pathSetting = {
      ...this._pathSetting,
      ...config,
    }
    this._path = new Path(this._pathSetting)
    this._layer.add(this._path)

    if (!this._canEdit) return

    this.on('pointerdown', (e) => {
      if (this._points.length >= this._pathSetting.max) return //限制數量
      const { evt, target } = e
      this._circle && this._circle.fill(this._pathSetting.stroke as string)
      if (target.className === 'Circle') {
        this._circle = target as Circle
        this._circle.fill(AreaDrawer.selectColor)
        return
      }

      const { offsetX, offsetY } = evt
      this._addCircle({ x: offsetX, y: offsetY })
      this._draw(this._points, this._path)
    })

    this.on('dragmove', (e) => {
      this._draw(this._points, this._path)
    })

    this.keyup$ = fromEvent(window, 'keyup').subscribe((e) => {
      const { key } = e as KeyboardEvent
      if (key === 'Delete' && this._circle) {
        const _ind = this._points.findIndex((_c) => {
          return _c === this._circle
        })
        this._points.splice(_ind, 1)[0].destroy()
        this._circle = this._points[_ind - 1]
        this._circle = this._circle || this._points[this._points.length - 1]
        this._draw(this._points, this._path, this._circle)
      }
    })
  }
  _addCircle({ x, y }: I_xy, color: string = AreaDrawer.selectColor) {
    this._circle = new Circle({
      x,
      y,
      radius: 5,
      fill: color,
      draggable: this._canEdit,
    })
    this._circle.on('mouseover', () => {
      document.body.style.cursor = 'move'
    })
    this._circle.on('mouseout', () => {
      document.body.style.cursor = 'default'
    })

    this._layer.add(this._circle)
    this._points.push(this._circle)
  }
  /**
   * 对于数组中的每个点，在该点上添加一个圆圈并绘制路径。
   * @param {xyPoints} _points - xyPoints - 这是一个 xyPoints 数组，它是一个包含 x 和 y 值的类。
   */
  drawPath(_points: xyPoints) {
    while (this._points.length) {
      this._points.pop()?.destroy()
    }
    _points.forEach((point) => {
      this._addCircle(point, this._pathSetting.stroke as string)
    })
    this._draw(this._points, this._path, this._circle)
  }
  _draw(_points: Circle[], path: Path, circle: Circle | null = null) {
    let _svg = ''
    const xy: xyPoints = []
    _points.forEach((pos, key) => {
      const { x, y } = pos.attrs
      xy.push({ x, y })
      if (key === 0) {
        _svg = `M${x},${y}`
      } else {
        _svg += ` L${x},${y}`
      }
    })
    _svg += 'Z'
    path.data(_svg)
    circle && circle.fill(AreaDrawer.selectColor)
    this._onPoints && this._onPoints(xy)
  }
  /**
   *  destroy() 函数。
   */
  unload() {
    this.keyup$ && this.keyup$.unsubscribe()
    super.destroy()
  }
}
