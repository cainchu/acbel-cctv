import moment from 'moment'
export function nxTimeFormat(time: number) {
  const dateTimeValue = moment(Math.floor(Number(time) / 1000))
    .locale('zh-tw')
    .format('YYYY-MM-DDTHH:mm:ss.SSS')
  return dateTimeValue
}
