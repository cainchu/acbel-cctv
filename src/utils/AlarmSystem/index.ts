import EventEmitter from './EventEmitter'

export const AlarmType = {
  FENCES: 'fences', //電子圍籬
  NO_CLEAR: 'no_clear', //電子圍籬人員未淨空
  RETENTETE: 'retentete', //遺留物
  /* 以下可能棄用 統一改用 'io' */
  FIRE: 'fire', //火災
  HASS: 'hass', //氣體消防
  DOOR: 'door', //吊掛門
  PAIR: 'pair', //碼頭
  SOS: 'sos', //sos攝影機要求對話
  BURN_1: 'burn_1', //燒機室1
  BURN_2: 'burn_2', //燒機室2
  BURN_3: 'burn_3', //燒機室3
  BURN_4: 'burn_4', //燒機室4
  BURN_5: 'burn_5', //燒機室5
  BURN_6: 'burn_6', //燒機室6
}

/**
 * 報警觀察者
 */
export default class extends EventEmitter {
  private cameraMap = new Map<string, CameraAlertWatcher>()
  private ioMap = new Map<string, IoAlertWatcher>()
  /**
   * 報警觀察者
   * @param camera 攝影機ID列表
   * @param io IOID列表
   */
  constructor(camera: string[], io: string[]) {
    super()
    this.initCameraAlertWatcher(camera)
    this.initIoAlertWatcher(io)
  }
  initCameraAlertWatcher(camera: string[]) {
    this.cameraMap = new Map<string, CameraAlertWatcher>()
    camera.forEach((id) => {
      const _watcher = new CameraAlertWatcher(id)
      this.cameraMap.set(id, _watcher)
      _watcher.on(AlarmType.FENCES, (off: boolean) => {
        this.emit(AlarmType.FENCES, {
          id: _watcher.id,
          off,
          time: _watcher.ai_fences_alarm_time,
        })
      })
      _watcher.on(AlarmType.RETENTETE, (off: boolean) => {
        this.emit(AlarmType.RETENTETE, {
          id: _watcher.id,
          off,
          time: _watcher.ai_retentate_alarm_time,
        })
      })
      _watcher.on(AlarmType.SOS, (off: boolean) => {
        this.emit(AlarmType.SOS, {
          id: _watcher.id,
          off,
          time: _watcher.sos_time,
        })
      })
    })
  }
  initIoAlertWatcher(io: string[]) {
    this.ioMap = new Map<string, IoAlertWatcher>()
    io.forEach((id) => {
      const _watcher = new IoAlertWatcher(id)
      this.ioMap.set(id, _watcher)
      _watcher.on('io', (off: boolean) => {
        this.emit('io', {
          id: _watcher.id,
          off,
          time: _watcher.alarm_time,
        })
      })
    })
  }
  /**
   * 更新狀態
   * @param cameraStatus
   * @param ioStatus
   */
  setStatus(cameraStatus: I_CameraAlertState[], ioStatus: I_IoAlertState[]) {
    cameraStatus.forEach((status) => {
      const _watcher = this.cameraMap.get(status.id)
      _watcher?.setStatus(status)
    })
    ioStatus.forEach((status) => {
      const _watcher = this.ioMap.get(status.id)
      _watcher?.setStatus(status)
    })
  }
}

/* 它是一个在类的状态发生变化时发出事件的类 */
class CameraAlertWatcher extends EventEmitter {
  public id: string = ''
  private _ai_fences: boolean //圍籬
  private _ai_retentate: boolean //滯留物
  private _sos: boolean //SOS
  public ai_fences_alarm_time: number = 0
  public ai_retentate_alarm_time: number = 0
  public sos_time: number = 0
  constructor(id: string) {
    super()
    this.id = id
    this._ai_fences = false
    this._ai_retentate = false
    this._sos = false
  }
  set ai_fences(off: boolean) {
    this._ai_fences !== off && this.emit(AlarmType.FENCES, off)
    this._ai_fences = off
  }
  set ai_retentate(off: boolean) {
    this._ai_retentate !== off && this.emit(AlarmType.RETENTETE, off)
    this._ai_retentate = off
  }
  set sos(off: boolean) {
    this._sos !== off && this.emit(AlarmType.SOS, off)
    this._sos = off
  }
  setStatus(state: I_CameraAlertState) {
    this.ai_fences_alarm_time = state.ai_fences_alarm_time
    this.ai_retentate_alarm_time = state.ai_retentate_alarm_time
    this.sos_time = state.sos_time || 0
    this.ai_fences = state.ai_fences
    this.ai_retentate = state.ai_retentate
    this.sos = state.sos || false
  }
}

class IoAlertWatcher extends EventEmitter {
  public id: string
  private _off: any
  public alarm_time: number = 0
  constructor(id: string) {
    super()
    this.id = id
    this._off = false
  }
  set trigger(_off: boolean) {
    this._off !== _off && this.emit('io', _off)
    this._off = _off
  }
  get trigger() {
    return this._off
  }
  setStatus(state: I_IoAlertState) {
    this.alarm_time = state.alarm_time
    this.trigger = state.alarm[0]
  }
}
