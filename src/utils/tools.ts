/**
 * json深複製
 * @param obj
 * @returns
 */
export const jsonClone = (obj: unknown) => JSON.parse(JSON.stringify(obj))

/**
 * 下載文件
 * @param {string} url - 您要下載的文件的 URL。
 * @param {string} fileName - 您要下載的文件的名稱。
 */
export const downloadUrl = (url: string, fileName: string) => {
  const a = document.createElement('a')
  a.href = url
  a.download = fileName
  a.click()
}

/**
 * ai微秒改時間
 */
export function convertBigNumToM(num: string) {
  return num === '0' ? 0 : Number(num.slice(0, -3))
}

/**
 * 小數點取至第__位
 */
export const roundTo = (num: number, decimal: number) => {
  return Math.round((num + Number.EPSILON) * Math.pow(10, decimal)) / Math.pow(10, decimal)
}

export const createInitialSchedule = () => {
  const blankString = new Array(24).fill(0).toString()
  const initialSchedule = {
    mon: blankString,
    tue: blankString,
    wed: blankString,
    thu: blankString,
    fri: blankString,
    sat: blankString,
    sun: blankString,
  }
  return initialSchedule
}

/* 取得ip最後一欄 ex=> 192.168.1.23 取得 23 */
const getIpEnd = (ip: string) => {
  const _arr = ip.split('.')
  return _arr[3] || '不是IP喔!'
}

const { hostname, protocol } = window.location
/* 取得ptz位置 */
export const generatePtzUrl = (ip: string) => {
  // return `${protocol}//${hostname}:90${getIpEnd(ip)}`
  return `http://${hostname}:90${getIpEnd(ip)}`
}
