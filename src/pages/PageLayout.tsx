import styled from 'styled-components';
import { colorValue } from '../styles';

export default styled.div`
  position: relative;
  width: 1920px;
  height: 1080px;
  color: ${colorValue.main};
  background-color: ${colorValue.background};
`;
