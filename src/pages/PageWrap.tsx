import { Fragment } from 'react';
import styled, { css } from 'styled-components';
import { colorValue } from '../styles';

interface PageWrapProps {
  title?: string;
  children: React.ReactNode;
  disableBg?: boolean;
}

export const PageBackground = styled.div`
  width: 100%;
  height: calc(100% - 40px);
  max-height: 100%;
  display: flex;
  justify-content: center;
  position: absolute;
  &:before {
    content: ' ';
    display: block;
    position: absolute;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    opacity: 0.35;
    background-image: url(/images/scenes_bg.jpg);
    background-repeat: no-repeat;
  }
`;
const PageContainer = styled.div`
  width: calc(100% - 60px);
  max-height: calc(100% - 40px);
  display: flex;
  flex-direction: column;
  border: 1px solid ${colorValue.border};
  margin: 30px 30px;
  position: relative;
  background-color: #0c1a2a80;
`;

const Header = styled.div`
  width: calc(100% - 6px);
  height: 35px;
  display: flex;
  align-items: flex-end;
  border-top: 1px solid ${colorValue.border};
  border-bottom: 1px solid ${colorValue.border};
  margin: 5px 3px 0 3px;
  font-size: 24px;
  font-weight: 600;
  line-height: 35px;
  letter-spacing: 2.4px;
  color: ${colorValue.bkTex};
  padding: 0 10px;
  box-sizing: border-box;
  position: relative;
`;
const HeaderText = styled.div`
  color: ${colorValue.title};
  font-size: 22px;
  font-weight: bold;
  letter-spacing: 2.2px;
  white-space: nowrap;
`;
const SlashBg = styled.div`
  width: 100%;
  height: 10px;
  background-image: url(/images/slash_navy.svg);
  margin: 0 -6px 5px 0;
`;

const DecoCube = styled.div`
  width: 60px;
  height: 20px;
  background-image: url(/images/deco_tri_cube.svg);
  background-repeat: no-repeat;
  position: absolute;
  top: 0px;
  right: 4px;
`;
const PanelWrap = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  padding: 20px 20px;
  box-sizing: border-box;
`;

export default (props: PageWrapProps) => {
  const { disableBg } = props;
  return (
    <Fragment>
      <PageBackground
        style={{ display: disableBg ? 'none' : 'flex' }}
      ></PageBackground>
      <PageContainer>
        <Header>
          <HeaderText>{props.title}</HeaderText>
          <DecoCube></DecoCube>
          <SlashBg></SlashBg>
        </Header>
        <PanelWrap>{props.children}</PanelWrap>
      </PageContainer>
    </Fragment>
  );
};

const warmStyle = css`
  animation: warmbgmov 2s infinite;
`;

export const PagePageBackgroundFull = styled(PageBackground)`
  height: 100%;
  position: relative;
  justify-content: left;
  ${({ warm }: { warm?: boolean }) => (warm ? warmStyle : '')}
`;
