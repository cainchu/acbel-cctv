import { useParams } from 'react-router-dom'
import FloorBgImg from '../components/FloorBgImg'
import VodMapGrid from '../components/VodMapGrid'
import useReadyPagePath from '../hooks/useReadyPagePath'
import useSetPageType from '../hooks/useSetPageType'
import { VDO_PAGE_TYPE } from '../store/vdos'
import { VIRW_ROUER } from './MainRouter'
import { PagePageBackgroundFull } from './PageWrap'

export default () => {
  const { ind } = useParams()

  useSetPageType(VDO_PAGE_TYPE.EQUIPMENT) /* 定義頁面類型 */
  useReadyPagePath() /* Page Ready 事件 */
  return (
    <PagePageBackgroundFull>
      <h1>3F</h1>
      <FloorBgImg
        src="/images/production/scene_a3_production.png"
        x="306"
        y="1"
        meta={[]}
        actOb={() => {}}
      ></FloorBgImg>
      <VodMapGrid path={VIRW_ROUER.CARRIAGE} ind={ind || '0'} />
    </PagePageBackgroundFull>
  )
}
