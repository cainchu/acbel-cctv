import { useReducer, useEffect, useMemo } from 'react'
import { useParams } from 'react-router-dom'
import Lazy from 'lazy.js'
import FloorBgImg, {
  bgTagInit,
  BKmeta,
  bkTagReducer,
  SMTPCBA_0,
  SMTPCBA_1,
} from '../components/FloorBgImg'
import { WorkValueKanban } from '../components/ValueKanban'
import VodMapGrid from '../components/VodMapGrid'
import useReadyPagePath from '../hooks/useReadyPagePath'
import { VIRW_ROUER } from './MainRouter'
import { PagePageBackgroundFull } from './PageWrap'
import useSetPageType from '../hooks/useSetPageType'
import { VDO_PAGE_TYPE } from '../store/vdos'
import { useSmtApi } from '../hooks/callSmtApi'
import { useAppDispatch } from '../store'
import { movePtzAction } from '../store/saga'

const MapBg = ({
  ind,
  meta,
  onAct,
}: {
  ind: string
  meta: BKmeta[]
  onAct: (id: string) => void
}) => {
  switch (ind) {
    case '0':
      return (
        <FloorBgImg
          src="/images/production/scene_a5_production.png"
          x="523"
          y="0"
          meta={meta}
          actOb={onAct}
        ></FloorBgImg>
      )
    default:
      return (
        <FloorBgImg
          src="/images/production/scene_a6_production.png"
          x="532"
          y="0"
          meta={meta}
          actOb={onAct}
        ></FloorBgImg>
      )
  }
}

export default () => {
  const { ind } = useParams()

  const [state, dispatch] = useReducer(bkTagReducer, bgTagInit([]))
  const { setSelectOb, pointLinght } = useSmtApi()
  const dispatchApp = useAppDispatch()

  useEffect(() => {
    dispatch({
      type: 'init',
      payload: ind === '0' ? SMTPCBA_0 : SMTPCBA_1,
    })
  }, [ind])

  const onAct = (id: string) => {
    dispatch({ type: 'act', payload: id })
    dispatchApp(movePtzAction(id)) //呼叫PTZ移動
  }

  const selectObj = useMemo(() => {
    return Lazy(state.meta).find((meta) => meta.act)
  }, [state])

  useEffect(() => {
    setSelectOb(selectObj)
  }, [selectObj])

  //因為設備選取，所以高亮攝影機的 ind
  const selectCameraInd = useMemo(() => {
    return selectObj ? selectObj.cameraInd : -1
  }, [selectObj])

  useSetPageType(VDO_PAGE_TYPE.EQUIPMENT) /* 定義頁面類型 */
  useReadyPagePath() /* Page Ready 事件 */
  return (
    <PagePageBackgroundFull>
      <h1>{ind === `0` ? '5' : '6'}F</h1>
      <MapBg ind={ind || '0'} meta={state.meta} onAct={(id: string) => onAct(id)}></MapBg>
      {selectObj && (
        <WorkValueKanban title={selectObj.title} pointLinght={pointLinght}></WorkValueKanban>
      )}
      <VodMapGrid path={VIRW_ROUER.SMTOCBA} ind={ind || '0'} specialInd={selectCameraInd} />
    </PagePageBackgroundFull>
  )
}
