import Lazy from 'lazy.js'
import { useParams } from 'react-router-dom'
import styled from 'styled-components'
import HeadUpMod from '../components/HeadUpMod'
import AlertPromptMod from '../components/AlertPromptMod'
import { PagePageBackgroundFull } from './PageWrap'
import { VDO_PAGE_TYPE } from '../store/vdos'
import useReadyPagePath from '../hooks/useReadyPagePath'
import useSetPageType from '../hooks/useSetPageType'
import VodMapGrid from '../components/VodMapGrid'
import { VIRW_ROUER } from './MainRouter'
import FloorBgImg from '../components/FloorBgImg'
import MiniMap from '../components/MiniMap'
import { useEffect, useMemo, useState } from 'react'
import { useNotificationSelector, useRootSelector } from '../hooks/useSelect'
import { AlarmType } from '../utils/AlarmSystem'
import CtvAlert from '../components/Ctv/CtvAlert'
import { camerasGetterById } from '../store/root'
import SoundBtn from '../components/Ctv/SoundBtn'

const SoundBtnPositionStyle = styled.div`
  position: absolute;
  right: 330px;
  bottom: 55px;
`

export default () => {
  const { ind } = useParams()

  useSetPageType(VDO_PAGE_TYPE.HASS) /* 定義頁面類型 */
  useReadyPagePath() /* Page Ready 事件 */

  //檢查是否有Hass告警===========================================================
  const [warm, setWarm] = useState(false)
  const [hasSomeone, setHasSomeone] = useState(false)
  const { validNotifications } = useNotificationSelector()
  const { cameras, speakerMap } = useRootSelector()

  useEffect(() => {
    const item = Lazy<I_NotificationMeta>(validNotifications).find(({ type }) => {
      return type === AlarmType.HASS
    })
    setWarm(!!item)
  }, [validNotifications])

  useEffect(() => {
    if (warm) {
      //找出 hass room 中是否有人員闖入 (_manNum告警數量>0)
      const _manNum = Lazy<I_NotificationMeta>(validNotifications)
        .filter(({ type }) => {
          return type === AlarmType.FENCES
        })
        .map(({ id }) => {
          return camerasGetterById(cameras, id)
        })
        .filter(({ cameraKey }) => {
          switch (cameraKey) {
            case 'a1-1':
            case 'a1-2':
            case 'a1-3':
            case 'a1-4':
              return true
            default:
              return false
          }
        })
        .size()
      setHasSomeone(_manNum > 0)
    }
  }, [warm, validNotifications, cameras])

  //畫面上的顯示告警
  const types = useMemo(() => {
    if (hasSomeone) return [AlarmType.HASS, AlarmType.FENCES]
    if (warm) return [AlarmType.HASS]
    return []
  }, [warm, hasSomeone])

  //============================================================================

  return (
    <PagePageBackgroundFull warm={warm}>
      <FloorBgImg
        src="/images/scene_a1_hass.png"
        x="394"
        y="58"
        meta={[]}
        actOb={() => {}}
      ></FloorBgImg>
      <HeadUpMod title="康舒強仕廠1F HASS事件" up={false} down={false} />
      <MiniMap></MiniMap>
      {/* <AlertPromptMod floorKeys={warm ? ['a1'] : []}>含氧量不足事件</AlertPromptMod> */}
      {warm && <CtvAlert types={types}></CtvAlert>}
      <VodMapGrid path={VIRW_ROUER.HASS_ROOM} ind={ind || '0'} />
      <SoundBtnPositionStyle>
        <SoundBtn macId={speakerMap['hass-speaker'] /** TODO:喇叭ID */} big />
      </SoundBtnPositionStyle>
    </PagePageBackgroundFull>
  )
}
