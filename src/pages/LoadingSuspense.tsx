import { cssFlex } from '../styles'

export default () => {
  return (
    <div
      css={`
        ${cssFlex.center}
        height:100%;
      `}
    >
      <h2>網路狀態不佳，請稍等...問題如持續，按F5請重新整理或檢查網路狀態</h2>
    </div>
  )
}
