import { useNavigate, useParams } from 'react-router-dom'
import { AlarmType } from '../utils/AlarmSystem'
import styled from 'styled-components'
import Lazy from 'lazy.js'
import HeadUpMod from '../components/HeadUpMod'
import AlertPromptMod from '../components/AlertPromptMod'
import { PagePageBackgroundFull } from './PageWrap'
import FireFloorImg from '../components/FireFloorImg'
import { VIRW_ROUER } from './MainRouter'
import { useAppDispatch } from '../store'
import { useEffect, useMemo, useState } from 'react'
import { fullAnVdoAction, monitorFloorAction } from '../store/saga'
import { useNotificationSelector, useVdosSelect } from '../hooks/useSelect'
import Ctv from '../components/Ctv'
import { VDO_PAGE_TYPE } from '../store/vdos'
import useReadyPagePath from '../hooks/useReadyPagePath'
import useSetPageType from '../hooks/useSetPageType'
import MiniMap from '../components/MiniMap'
import CtvAlert from '../components/Ctv/CtvAlert'

const FIRE_META = {
  building: {
    a: {
      floors: [
        { title: '康舒強仕廠A棟B2F火警逃生路徑' },
        { title: '康舒強仕廠A棟B1F火警逃生路徑' },
        { title: '康舒強仕廠A棟1F火警逃生路徑' },
        { title: '康舒強仕廠A棟2F火警逃生路徑' },
        { title: '康舒強仕廠A棟3F火警逃生路徑' },
        { title: '康舒強仕廠A棟4F火警逃生路徑' },
        { title: '康舒強仕廠A棟5F火警逃生路徑' },
        { title: '康舒強仕廠A棟6F火警逃生路徑' },
        { title: '康舒強仕廠A棟7F火警逃生路徑' },
        { title: '康舒強仕廠A棟8F火警逃生路徑' },
        { title: '康舒強仕廠A棟9F火警逃生路徑' },
      ],
    },
    b: {
      floors: [
        { title: '康舒強仕廠B棟1F火警逃生路徑' },
        { title: '康舒強仕廠B棟2F火警逃生路徑' },
        { title: '康舒強仕廠B棟3F火警逃生路徑' },
      ],
    },
  },
}

const getFloors = (building: string): I_FloorMeta[] => {
  const build = Lazy(FIRE_META.building).get(building || '')
  if (build) {
    return Lazy(build).get('floors')
  }
  return []
}

/* 取得樓層資訊 */
const getFloorMate = (building: string | undefined, floor: string | undefined) => {
  const floors = getFloors(building || '')
  if (floors) {
    const _meta = Lazy(floors).get(
      building === 'a' ? (floor === 'c' ? 0 : Number(floor) + 1) : Number(floor) - 1,
    )
    if (_meta) {
      return _meta
    }
  }
  return null
}

const GridLauout = styled.div`
  position: absolute;
  right: 0px;
  left: 0px;
  top: 0px;
  bottom: 0px;
  display: grid;
  grid-template-columns: 1fr 320px 320px;
  grid-template-rows: repeat(4, 215px);
  grid-gap: 10px;
  grid-template-areas: ${({ fullKey }: { fullKey: number }) => {
    const vdx = `vd${fullKey + 1}`
    return fullKey === -1
      ? `'a vd5 vd1'
    'a b vd2'
    'a b vd3'
    'a vd6 vd4'`
      : `'${vdx} ${vdx} ${vdx}'
    '${vdx} ${vdx} ${vdx}'
    '${vdx} ${vdx} ${vdx}'
    '${vdx} ${vdx} ${vdx}'`
  }};
  pointer-events: none;
`

type VodBoxProps = {
  area: string
  hidden?: boolean
}
const VodBox = styled.div`
  width: 100%;
  height: 100%;
  pointer-events: auto;
  grid-area: ${({ area }: VodBoxProps) => area};
  background: #00000087;
  visibility: ${({ hidden }: VodBoxProps) => (hidden ? 'hidden' : 'visible')};
`

export default () => {
  const navigat = useNavigate()
  const dispatch = useAppDispatch()
  const { building, floor } = useParams()
  const { vdoMetas, fullKey, realVdoNum } = useVdosSelect()
  const isFull = useMemo(() => fullKey !== -1, [fullKey])

  useEffect(() => {
    dispatch(
      monitorFloorAction({
        floor: `${building || ''}${floor || ''}`,
        pageType: VDO_PAGE_TYPE.FIRE,
      }),
    )
  }, [building, floor])

  const upFloor = () => {
    navigat(`${VIRW_ROUER.Fire}/${building}/${upFloorKey(building, floor)}`)
  }
  const downFloor = () => {
    navigat(`${VIRW_ROUER.Fire}/${building}/${downFloorKey(building, floor)}`)
  }

  const onVodFull = (ind: number) => {
    if (fullKey === -1) {
      dispatch(fullAnVdoAction(ind))
    } else {
      dispatch(fullAnVdoAction(fullKey))
    }
  }

  useSetPageType(VDO_PAGE_TYPE.FIRE) /* 定義頁面類型 */
  useReadyPagePath() /* Page Ready 事件 */

  //檢查是否有火警告警===========================================================
  const [warm, setWarm] = useState(false)
  const { validNotifications } = useNotificationSelector()
  useEffect(() => {
    const item = Lazy<I_NotificationMeta>(validNotifications).find(({ id, type }) => {
      return type === AlarmType.FIRE && id === (building || '') + floor
    })
    setWarm(!!item)
  }, [validNotifications, building, floor])

  //火警樓層
  const floorKeys = useMemo(() => {
    return (
      Lazy(validNotifications)
        .filter(({ type }) => type === AlarmType.FIRE)
        .map(({ id }) => id)
        .toArray() || []
    )
  }, [validNotifications])

  return (
    <PagePageBackgroundFull warm={warm}>
      <FireFloorImg floorId={`${building}${floor}`} />
      <HeadUpMod
        title={getFloorMate(building, floor)?.title || ''}
        onUp={upFloor}
        onDown={downFloor}
        up={!(`${building}${floor}` === 'a9' || `${building}${floor}` === 'b3')}
        down={!(`${building}${floor}` === 'ac' || `${building}${floor}` === 'b1')}
      />
      <MiniMap floor={`${building}${floor}`}></MiniMap>
      <AlertPromptMod floorKeys={floorKeys}> 發 生 火 警</AlertPromptMod>
      <GridLauout fullKey={isFull ? vdoMetas.length - 1 : -1}>
        {vdoMetas.map((camera, ind) => (
          <VodBox
            area={`vd${ind + 1}`}
            hidden={
              isFull
                ? ind < vdoMetas.length - 1
                : ind >= realVdoNum || camera.cameraId === undefined
            }
            key={camera.uuid}
          >
            <Ctv
              meta={camera}
              onFull={() => onVodFull(ind)}
              isFull={ind === vdoMetas.length - 1}
              onClose={isFull ? () => onVodFull(-1) : undefined}
              noDragIcon
            ></Ctv>
          </VodBox>
        ))}
      </GridLauout>
      {warm && <CtvAlert types={[AlarmType.FIRE]}></CtvAlert>}
    </PagePageBackgroundFull>
  )
}

/**
 * a棟最大6樓 b棟最大3樓
 * @param {string} building - string - 你所在的建筑物。
 * @param {string} floor - 当前楼层
 * @returns 楼层号。
 */
function upFloorKey(building: string | undefined, floor: string | undefined) {
  const _floor = floor === 'c' ? -1 : Number(floor || 0)
  return building === 'a' ? (_floor >= 9 ? 9 : _floor + 1) : _floor >= 3 ? 3 : _floor + 1
}

function downFloorKey(building: string | undefined, floor: string | undefined) {
  if (floor === 'c') return 'c'
  const _floor = Number(floor || 0)
  return building === 'a' ? (_floor == 0 ? 'c' : _floor - 1) : _floor <= 1 ? 1 : _floor - 1
}
