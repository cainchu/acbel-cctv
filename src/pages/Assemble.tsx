import { useParams } from 'react-router-dom'
import { useReducer, useMemo } from 'react'
import Lazy from 'lazy.js'
import FloorBgImg, { bgTagInit, bkTagReducer, ASSEMBLE_SETTING } from '../components/FloorBgImg'
import { TemperatureKenban } from '../components/ValueKanban'
import VodMapGrid from '../components/VodMapGrid'
import useReadyPagePath from '../hooks/useReadyPagePath'
import { VIRW_ROUER } from './MainRouter'
import { PagePageBackgroundFull } from './PageWrap'
import useSetPageType from '../hooks/useSetPageType'
import { VDO_PAGE_TYPE } from '../store/vdos'
import { movePtzAction } from '../store/saga'
import { useAppDispatch } from '../store'

export default () => {
  const { ind } = useParams()
  const [state, dispatch] = useReducer(bkTagReducer, bgTagInit(ASSEMBLE_SETTING))
  const dispatchApp = useAppDispatch()
  const onAct = (id: string) => {
    dispatch({ type: 'act', payload: id })
    dispatchApp(movePtzAction(id)) //呼叫PTZ移動
  }

  const selectObj = useMemo(() => {
    return Lazy(state.meta).find((meta) => meta.act)
  }, [state])

  //因為設備選取，所以高亮攝影機的 ind
  const selectCameraInd = useMemo(() => (selectObj ? selectObj.cameraInd : -1), [selectObj])

  useSetPageType(VDO_PAGE_TYPE.EQUIPMENT) /* 定義頁面類型 */
  useReadyPagePath() /* Page Ready 事件 */
  return (
    <PagePageBackgroundFull>
      <h1>4F</h1>
      {selectObj && <TemperatureKenban title={selectObj.title}></TemperatureKenban>}
      <FloorBgImg
        src="/images/production/scene_a4_production.png"
        x="522"
        y="0"
        meta={state.meta}
        actOb={onAct}
      ></FloorBgImg>
      <VodMapGrid path={VIRW_ROUER.ASSEMBLE} ind={ind || '0'} specialInd={selectCameraInd} />
    </PagePageBackgroundFull>
  )
}
