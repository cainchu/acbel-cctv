import LoginMod from '../components/LoginMod';
import { useAppDispatch } from '../store';
import { loginAction } from '../store/saga';
import { pageSize, transformCenter } from '../styles';
import { useAppSelector } from './../store/index';
import PageLayout from './PageLayout';

const loginImg: React.CSSProperties = {
  ...pageSize,
  backgroundImage: 'url(/images/login_bg.png)',
};
const logo: React.CSSProperties = {
  ...transformCenter,
  top: '70px',
};
const title: React.CSSProperties = {
  ...transformCenter,
  top: '195px',
};
const loginBd: React.CSSProperties = {
  ...transformCenter,
  bottom: '166px',
};

export default () => {
  const loginMessage = useAppSelector((store) => store.user.loginMessage);
  const dispatch = useAppDispatch();
  const onLogin = (name: string, pass: string) => {
    dispatch(
      loginAction({
        name,
        pass,
      }),
    );
  };

  return (
    <PageLayout>
      <div style={loginImg}>
        <div style={logo}>
          <img width="392" height="80" src="/images/logo.svg" alt="" />
        </div>
        <div style={title}>
          <img width="1220" height="91" src="/images/title.png" alt="" />
        </div>
        <LoginMod
          style={loginBd}
          onLogin={onLogin}
          errorMessage={loginMessage}
        ></LoginMod>
      </div>
    </PageLayout>
  );
};
