import VdoGrup from '../../components/VdoGrup'
import { useAppSelector } from '../../store'

export default () => {
  const vdoMetas = useAppSelector((store) => store.vdos.vdoMetas)
  return <VdoGrup vdoMetas={vdoMetas}></VdoGrup>
}
