import { useEffect } from 'react';
import DetectList from '../../components/Setting/DetectList';
import styled from 'styled-components';
import PageWrap from '../PageWrap';

const PickArea = styled.div`
  width: 100%;
  height: 100%;
  box-sizing: border-box;
  padding: 0 35px;
`;

export default () => {
  return (
    <PageWrap title="OBJECT DETECTION SETTING">
      <PickArea 
      style={{padding:'0 10px'}}
      >
        <DetectList></DetectList>
      </PickArea>
    </PageWrap>
  );
};
