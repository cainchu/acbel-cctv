import {} from 'react';
import EventSwitch from '../../components/Setting/EventSwitch';
import PageWrap from '../PageWrap';

export default () => {
  return (
    <PageWrap title="ALARM EVENT SWITCH SETTINGS">
      <EventSwitch></EventSwitch>;
    </PageWrap>
  );
};