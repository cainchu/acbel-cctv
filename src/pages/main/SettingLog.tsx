import { useEffect, useState } from 'react';
import EventList from './../../components/Setting/EventList';
import PageWrap from '../PageWrap';

export default () => {
  return (
    <PageWrap title="ALARM EVENT SEARCH AND DOWNLOAD">
      <EventList></EventList>
    </PageWrap>
  );
};
