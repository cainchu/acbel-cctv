import {} from 'react';
import Schedule from '../../components/Setting/Schedule';
import styled from 'styled-components';
import PageWrap from '../PageWrap';

const PickArea = styled.div`
  width: 100%;
  height: 100%;
  box-sizing: border-box;
  padding: 0 35px;
`;

export default () => {
  return (
    <PageWrap title="SCHEDULE CONTROL SETTINGS">
      <PickArea>
        <Schedule></Schedule>
      </PickArea>
    </PageWrap>
  );
};
