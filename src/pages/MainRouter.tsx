import { lazy, Suspense } from 'react'
import { useRoutes } from 'react-router-dom'
import usePermission from '../hooks/usePermission'
import SettingLog from './main/SettingLog'
import { settingPages, equipmentPages, productionPages } from './MenuPages'
import { cssFlex } from '../styles'

const PiarPage = lazy(() => import('./PiarPage')) //碼頭監視
const Admin = lazy(() => import('./Admin')) //戰情

import NoPermission from './NoPermission'
import LoadingSuspense from './LoadingSuspense'

export const MAIN_ROUER = {
  SettingAi: `/setting-ai`,
  SettingAlert: '/setting-alert',
  SettingLog: '/setting-log',
  SettingPush: '/setting-push',
  SettingSchedule: '/setting-schedule',
  ADMIN: '/admin',
}

export const VIRW_ROUER = {
  HANGING_DOOR: '/hanging-door', //吊掛門安全監視
  HASS_ROOM: '/hass-room', //HASS ROOM
  UNLOADING_ZONE: '/unloading-zone', //貨車卸貨區
  BURNIN_TEST: '/burnIn-test', //燒機測試區
  PERSONNEL: '/personnel', //重要區域人員入侵
  PIAR: '/piar', //碼頭系統X
  Fire: '/fire', //火警
  //================================================================
  CARRIAGE: '/carriage', //馬車組裝區
  ASSEMBLE: '/assemble', //組裝燒機區
  SMTOCBA: '/smtpcba', //SMT/PCBA
  WAVE_SOLDERING: '/wave-soldering', //波鋒迴焊爐
  ADMIN: '',
}

export default () => {
  /* 取得各種權限 */
  const { isPermissionAB, isPermissionEquipment, isPermissionProduction, isPermissionPier } =
    usePermission()

  return useRoutes([
    ...(isPermissionAB
      ? [
          ...settingPages,
          {
            path: MAIN_ROUER.ADMIN,
            element: (
              <Suspense fallback={<LoadingSuspense />}>
                <Admin />
              </Suspense>
            ),
          },
        ]
      : []), //權限A
    ...(isPermissionEquipment ? equipmentPages : []), //設備監視權限
    ...(isPermissionProduction ? productionPages : []), //生產安全權限
    ...(isPermissionPier
      ? [
          {
            path: VIRW_ROUER.PIAR,
            element: (
              <Suspense fallback={<LoadingSuspense />}>
                <PiarPage />
              </Suspense>
            ),
          },
          {
            path: MAIN_ROUER.SettingLog, //碼頭權限特例可以查詢碼頭影片log
            element: (
              <Suspense fallback={<LoadingSuspense />}>
                <SettingLog />
              </Suspense>
            ),
          },
        ]
      : []), //碼頭權限
    {
      path: '*',
      element: <NoPermission />,
    },
  ])
}
