/** 燒機測試區監視頁面 */
import { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'
import Lazy from 'lazy.js'
import useReadyPagePath from '../hooks/useReadyPagePath'
import { useCameraIdsByMenu, useNotificationSelector } from '../hooks/useSelect'
import useSetPageType from '../hooks/useSetPageType'
import { useAppDispatch } from '../store'
import { monitorCustomAction } from '../store/saga'
import { VDO_PAGE_TYPE } from '../store/vdos'
import { AlarmType } from '../utils/AlarmSystem'
import FloorVdo from './main/FloorVdo'
import { VIRW_ROUER } from './MainRouter'

export default () => {
  const { ind } = useParams()
  const cameraIds = useCameraIdsByMenu(VIRW_ROUER.BURNIN_TEST, ind)
  const dispatch = useAppDispatch()
  useEffect(() => {
    dispatch(monitorCustomAction(cameraIds))
  }, [cameraIds])

  useSetPageType(VDO_PAGE_TYPE.WINDOW) /* 定義頁面類型 */
  useReadyPagePath() /* Page Ready 事件 */

  return <FloorVdo />
}
