/** 碼頭監視頁面 */
import { useEffect } from 'react'
import styled from 'styled-components'
import SoundBtn from '../components/Ctv/SoundBtn'
import useReadyPagePath from '../hooks/useReadyPagePath'
import { useCameraIdsByMenu, useRootSelector } from '../hooks/useSelect'
import useSetPageType from '../hooks/useSetPageType'
import { useAppDispatch, useAppSelector } from '../store'
import { monitorCustomAction } from '../store/saga'
import { VDO_PAGE_TYPE } from '../store/vdos'
import FloorVdo from './main/FloorVdo'
import { VIRW_ROUER } from './MainRouter'

const SoundBtnPositionStyle = styled.div`
  position: absolute;
  right: 30px;
  bottom: 60px;
`

export default () => {
  const cameraIds = useCameraIdsByMenu(VIRW_ROUER.PIAR)
  const fullKey = useAppSelector((store) => store.vdos.fullKey)
  const dispatch = useAppDispatch()
  useEffect(() => {
    dispatch(monitorCustomAction(cameraIds))
  }, [cameraIds])

  const { speakerMap } = useRootSelector()

  useSetPageType(VDO_PAGE_TYPE.WINDOW) /* 定義頁面類型 */
  useReadyPagePath() /* Page Ready 事件 */

  return (
    <>
      <FloorVdo />
      {fullKey === -1 && (
        <SoundBtnPositionStyle>
          <SoundBtn macId={speakerMap['pair-speaker'] /** TODO:喇叭ID */} big />
        </SoundBtnPositionStyle>
      )}
    </>
  )
}
