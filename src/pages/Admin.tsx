/** Admin */
import { useEffect } from 'react';
import useReadyPagePath from '../hooks/useReadyPagePath';
import useSetPageType from '../hooks/useSetPageType';
import { useAppDispatch } from '../store';
import { monitorFloorAction } from '../store/saga';
import { VDO_PAGE_TYPE } from '../store/vdos';
import FloorVdo from './main/FloorVdo';

export default () => {
  const dispatch = useAppDispatch();
  useEffect(() => {
    dispatch(monitorFloorAction('a1')); //戰情室 一進去先到 a1
  }, []);

  useSetPageType(VDO_PAGE_TYPE.WINDOW); /* 定義頁面類型 */
  useReadyPagePath(); /* Page Ready 事件 */

  return <FloorVdo />;
};
