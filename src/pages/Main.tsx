import styled from 'styled-components'
import PageLayout from './PageLayout'
import AdminBar from '../components/AdminBar'
import MenuBar from '../components/MenuBar'
import { colorValue } from '../styles'
import { useAppDispatch, useAppSelector } from '../store'
import CenterBtn from '../components/CenterBtn'
import FloorMenu from '../components/FloorMenu'
import { logoutAction, monitorFloorAction, viewNotificationAction } from '../store/saga'
import MainRouter, { MAIN_ROUER, VIRW_ROUER } from './MainRouter'
import NotificationMod from '../components/NotificationMod'
import { useLocation, useNavigate } from 'react-router-dom'
import { useCallback, useEffect, useMemo } from 'react'
import { useNotificationSelector, useRootSelector } from '../hooks/useSelect'
import usePermission from '../hooks/usePermission'
import useSettingMenuPermission from '../hooks/useSettingMenuPermission'
import useAlarmSound from '../hooks/useAlarmSound'
import { setAlarmSoundOff } from '../store/notification'

const MianContent = styled.div`
  width: 100%;
  height: 990px;
  display: flex;
  align-items: stretch;
`
const LeftMod = styled.div`
  width: 360px;
  box-sizing: content-box;
  border-right: solid 2px ${colorValue.border};
`
const MainMod = styled.div`
  width: 1560px;
  padding: 20px;
  position: relative;
`

const MenuBoxLayout = styled.div`
  width: 100%;
  height: 760px;
  padding: 20px;
`

export default () => {
  const location = useLocation()
  const { permission, isPermissionAB } = usePermission()

  //選單的權限
  const menuPermission = useMemo(() => {
    switch (permission) {
      case 'A':
        return '111' //0關閉1開啟
      case 'B':
        return '101' //0關閉1開啟
      case 'C':
        return '100'
      case 'D':
        return '010'
      case 'E':
        return '001'
      default:
        return '000'
    }
  }, [permission])

  const adminSettingMenu = useSettingMenuPermission()

  const floors = useAppSelector((store) => store.vdos.treeMap?.floors)
  const { menuTree, initDataReady } = useRootSelector() //選單樹
  const { notifications, alarmSoundOff } = useNotificationSelector()

  const dispatch = useAppDispatch()
  const logout = () => {
    dispatch(logoutAction())
  }
  const monitorFloor = (floorUUid: string) => {
    if (isPermissionAB) {
      navigat(MAIN_ROUER.ADMIN)
      setTimeout(() => {
        //換頁後延遲
        dispatch(monitorFloorAction(floorUUid))
      }, 0)
    }
  }

  /* 觸發監看報警畫面 */
  const toNotification = (meta: I_NotificationMeta) => {
    dispatch(viewNotificationAction(meta))
  }

  const navigat = useNavigate()
  const goAdmin = () => {
    isPermissionAB && navigat(MAIN_ROUER.ADMIN) //回首頁
  }
  const goPair = () => {
    navigat(VIRW_ROUER.PIAR) //回首頁
  }

  //根據權限判斷路由
  useEffect(() => {
    switch (permission) {
      case 'A':
      case 'B':
        goAdmin()
        break
      case 'C':
        navigat(`${VIRW_ROUER.HANGING_DOOR}/0`) //設備安全初始頁面
        break
      case 'D':
        navigat(`${VIRW_ROUER.CARRIAGE}/0`) //生產安全初始頁面
        break
      case 'E':
        goPair()
        break
      default:
    }
  }, [permission])

  /* 按下選單後依據menuTree導頁至對應位置 */
  const onMenuItemClick = useCallback(
    (system: string, systemInd: number, itemInd: number) => {
      const menuTreeModule = getModule(system) as menuTreeModuleType
      const _system = menuTreeModule.system[systemInd]
      const _itemPath = _system.items[itemInd]?.path //如果有這格欄位就直接使用
      navigat(`/${_system.path}${_itemPath ? _itemPath : '/' + itemInd}`)
      function getModule(module: string) {
        switch (module) {
          case 'equipment':
            return menuTree?.equipment || {}
          case 'production':
            return menuTree?.production || {}
          default:
            return {}
        }
      }
    },
    [menuTree],
  )

  /* 報警音效開關 */
  const { soundAwitch } = useAlarmSound('/sound/3104.mp3')
  useEffect(() => {
    soundAwitch(alarmSoundOff)
  }, [alarmSoundOff])
  useEffect(() => {
    dispatch(setAlarmSoundOff(false))
  }, [location])

  return (
    <PageLayout>
      {initDataReady ? (
        <>
          <AdminBar logout={logout} adminMenu={adminSettingMenu} />
          <MenuBar
            menuTree={menuTree}
            onAdmin={() => goAdmin()}
            onPiar={() => goPair()}
            onMenuItemClick={onMenuItemClick}
            permission={menuPermission}
          />
          <MianContent>
            <LeftMod>
              <MenuBoxLayout>
                <CenterBtn onClick={() => goAdmin()}></CenterBtn>
                <FloorMenu
                  floors={floors || []}
                  monitorFloor={monitorFloor}
                  draggable={isPermissionAB}
                ></FloorMenu>
              </MenuBoxLayout>
              <NotificationMod
                notifications={notifications}
                toNotification={(meta: I_NotificationMeta) => toNotification(meta)}
              />
            </LeftMod>
            <MainMod>
              <MainRouter />
            </MainMod>
          </MianContent>
        </>
      ) : (
        <div>Loading...</div>
      )}
    </PageLayout>
  )
}
