import PageWrap from './PageWrap';

export default () => {
  return (
    <PageWrap title="NO Permission">
      <h1>權限不足</h1>
    </PageWrap>
  );
};
