import { useParams } from 'react-router-dom'
import FloorBgImg from '../components/FloorBgImg'
import VodMapGrid from '../components/VodMapGrid'
import useReadyPagePath from '../hooks/useReadyPagePath'
import useSetPageType from '../hooks/useSetPageType'
import { VDO_PAGE_TYPE } from '../store/vdos'
import { VIRW_ROUER } from './MainRouter'
import { PagePageBackgroundFull } from './PageWrap'

const MapBg = ({ ind }: { ind: string }) => {
  switch (ind) {
    case '0':
      return (
        <FloorBgImg
          src="/images/production/scene_a5_productwave.png"
          x="307"
          y="0"
          meta={[]}
          actOb={() => {}}
        ></FloorBgImg>
      )
    default:
      return (
        <FloorBgImg
          src="/images/production/scene_a6_productwave.png"
          x="307"
          y="0"
          meta={[]}
          actOb={() => {}}
        ></FloorBgImg>
      )
  }
}

export default () => {
  const { ind } = useParams()

  useSetPageType(VDO_PAGE_TYPE.EQUIPMENT) /* 定義頁面類型 */
  useReadyPagePath() /* Page Ready 事件 */
  return (
    <PagePageBackgroundFull>
      <h1>{ind === `0` ? '5' : '6'}F</h1>
      <MapBg ind={ind || '0'}></MapBg>
      <VodMapGrid path={VIRW_ROUER.WAVE_SOLDERING} ind={ind || '0'} />
    </PagePageBackgroundFull>
  )
}
