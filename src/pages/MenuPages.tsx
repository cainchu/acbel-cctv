import { lazy, Suspense } from 'react'
import LoadingSuspense from './LoadingSuspense'

//TODO:暫時先這樣，後面要想辦法，重複code，希望由MainRouter引入
enum MAIN_ROUER {
  SettingAi = `/setting-ai`,
  SettingAlert = '/setting-alert',
  SettingLog = '/setting-log',
  SettingPush = '/setting-push',
  SettingSchedule = '/setting-schedule',
  ADMIN = '/admin',
}

enum VIRW_ROUER {
  HANGING_DOOR = '/hanging-door', //吊掛門安全監視
  HASS_ROOM = '/hass-room', //HASS ROOM
  UNLOADING_ZONE = '/unloading-zone', //貨車卸貨區
  BURNIN_TEST = '/burnIn-test', //燒機測試區
  PERSONNEL = '/personnel', //重要區域人員入侵
  PIAR = '/piar', //碼頭系統X
  Fire = '/fire', //火警
  //================================================================
  CARRIAGE = '/carriage', //馬車組裝區
  ASSEMBLE = '/assemble', //組裝燒機區
  SMTOCBA = '/smtpcba', //SMT/PCBA
  WAVE_SOLDERING = '/wave-soldering', //波鋒迴焊爐
  ADMIN = '',
}

/* admin設定頁 ================================================================= */
const SettingAi = lazy(() => import('./main/SettingAi'))
const SettingAlert = lazy(() => import('./main/SettingAlert'))
const SettingLog = lazy(() => import('./main/SettingLog'))
const SettingPush = lazy(() => import('./main/SettingPush'))
const SettingSchedule = lazy(() => import('./main/SettingSchedule'))
/* 設備監視頁 ================================================================= */
const HangingDoor = lazy(() => import('./HangingDoor')) //吊掛門安全監視
const HassRoom = lazy(() => import('./HassRoom')) //Hass Room
const UnloadingZone = lazy(() => import('./UnloadingZone')) //貨車卸貨區
const Personnel = lazy(() => import('./Personnel')) //重要區域人員入侵
const BurnInTest = lazy(() => import('./BurnInTest')) //燒機測試區
const FireView = lazy(() => import('./FireView'))
/* 生產安全監視頁 ================================================================= */
const Carriage = lazy(() => import('./Carriage')) //馬車組裝區
const Assemble = lazy(() => import('./Assemble')) //組裝燒機區
const Smtpcba = lazy(() => import('./Smtpcba')) //SMT/PCBA
const WaveSoldering = lazy(() => import('./WaveSoldering')) //波鋒迴焊爐

/* 管理設定相關頁面 */
export const settingPages = [
  {
    path: MAIN_ROUER.SettingAi,
    element: (
      <Suspense fallback={<LoadingSuspense />}>
        <SettingAi />
      </Suspense>
    ),
  },
  {
    path: MAIN_ROUER.SettingAlert,
    element: (
      <Suspense fallback={<LoadingSuspense />}>
        <SettingAlert />
      </Suspense>
    ),
  },
  {
    path: MAIN_ROUER.SettingLog,
    element: (
      <Suspense fallback={<LoadingSuspense />}>
        <SettingLog />
      </Suspense>
    ),
  },
  {
    path: MAIN_ROUER.SettingPush,
    element: (
      <Suspense fallback={<LoadingSuspense />}>
        <SettingPush />
      </Suspense>
    ),
  },
  {
    path: MAIN_ROUER.SettingSchedule,
    element: (
      <Suspense fallback={<LoadingSuspense />}>
        <SettingSchedule />
      </Suspense>
    ),
  },
  {
    path: MAIN_ROUER.SettingSchedule,
    element: (
      <Suspense fallback={<LoadingSuspense />}>
        <SettingSchedule />
      </Suspense>
    ),
  },
]

/* 設備監視頁面 */
export const equipmentPages = [
  {
    path: `${VIRW_ROUER.HANGING_DOOR}/:ind`, //吊掛門安全監視
    element: (
      <Suspense fallback={<LoadingSuspense />}>
        <HangingDoor />
      </Suspense>
    ),
  },
  {
    path: `${VIRW_ROUER.HASS_ROOM}/:ind`, //Hass Room
    element: (
      <Suspense fallback={<LoadingSuspense />}>
        <HassRoom />
      </Suspense>
    ),
  },
  {
    path: `${VIRW_ROUER.UNLOADING_ZONE}/:ind`, //貨車卸貨區
    element: (
      <Suspense fallback={<LoadingSuspense />}>
        <UnloadingZone />
      </Suspense>
    ),
  },
  {
    path: `${VIRW_ROUER.BURNIN_TEST}/:ind`, //燒機測試區
    element: (
      <Suspense fallback={<LoadingSuspense />}>
        <BurnInTest />
      </Suspense>
    ),
  },
  {
    path: `${VIRW_ROUER.PERSONNEL}/:ind`, //重要區域人員入侵
    element: (
      <Suspense fallback={<LoadingSuspense />}>
        <Personnel />
      </Suspense>
    ),
  },
  {
    path: `${VIRW_ROUER.Fire}/:building/:floor`, //火災
    element: (
      <Suspense fallback={<LoadingSuspense />}>
        <FireView />
      </Suspense>
    ),
  },
]

//生產安全頁面
export const productionPages = [
  {
    path: `${VIRW_ROUER.CARRIAGE}/:ind`, //馬車組裝區
    element: (
      <Suspense fallback={<LoadingSuspense />}>
        <Carriage />
      </Suspense>
    ),
  },
  {
    path: `${VIRW_ROUER.ASSEMBLE}/:ind`, //組裝燒機區
    element: (
      <Suspense fallback={<LoadingSuspense />}>
        <Assemble />
      </Suspense>
    ),
  },
  {
    path: `${VIRW_ROUER.SMTOCBA}/:ind`, //SMT/PCBA
    element: (
      <Suspense fallback={<LoadingSuspense />}>
        <Smtpcba />
      </Suspense>
    ),
  },
  {
    path: `${VIRW_ROUER.WAVE_SOLDERING}/:ind`, //波鋒迴焊爐
    element: (
      <Suspense fallback={<LoadingSuspense />}>
        <WaveSoldering />
      </Suspense>
    ),
  },
]
