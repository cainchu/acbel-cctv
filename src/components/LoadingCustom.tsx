import { Fragment } from 'react';
import { CSSTransition } from 'react-transition-group';
import styled from 'styled-components';

const loadingStyle: React.CSSProperties = {
  width: '100%',
  left: 0,
  top: 0,
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  color: 'white',
  marginTop:'20px',
};

const FlexWrap = styled.div`
  width:100%;
  min-height:calc(100% + 50px);
  display:flex;
  justify-content:center;
  align-items:center;
  flex-direction:column;
  background-color: #00000088;
`;

const LoadingIcon = styled.div`
  width: 50px;
  height: 50px;
  background-image: url(/images/loading.gif);
  background-repeat: no-repeat;
  background-size: contain;
`;
interface Prop {
  loading: boolean;
  minHeight:string;
}
export default ({ loading, minHeight }: Prop) => {
  return (
    <CSSTransition in={loading} timeout={1000} classNames="fade" unmountOnExit>
      <FlexWrap style={{minHeight:minHeight}}>
        <LoadingIcon></LoadingIcon>
        <div style={loadingStyle}>Loading...</div>
      </FlexWrap>
    </CSSTransition>
  );
};
