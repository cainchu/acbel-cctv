import styled from 'styled-components'
import { cssFlex } from '../../styles'
import FloorTable from './FloorTable'
import { colorValue } from './../../styles/index'

const Layout_B = styled.div`
  width: 100%;
  height: 40px;
  padding-left: 218px;
  overflow: hidden;
  background-image: url('/images/warning_fire_event.svg');
  ${cssFlex.verticalCenter}
`

const MessageSpanLayout = styled.div`
  font-size: 22px;
  font-weight: bold;
  line-height: 1.27;
  letter-spacing: 2.2px;
  color: ${colorValue.warn};
  background: #000;
`

const Layout = styled.div`
  width: 500px;
  position: absolute;
  left: 30px;
  bottom: 30px;
`

type Props = {
  children?: string
  floorKeys: string[]
}

export default ({ children, floorKeys }: Props) => {
  return (
    <Layout>
      {floorKeys.length > 0 && (
        <Layout_B>
          <MessageSpanLayout>{children || ''}</MessageSpanLayout>
        </Layout_B>
      )}
      <FloorTable floorKeys={floorKeys} />
    </Layout>
  )
}
