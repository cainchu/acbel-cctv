import styled from 'styled-components'
import { colorValue, cssFlex } from '../../styles'
import { useEffect } from 'react'

const Layout = styled.div`
  width: 500px;
  height: 100px;
  margin-top: 10px;
  padding: 8px 0 0 23px;
  background-image: url(/images/table_floor.svg);
  display: grid;
  grid-template-columns: repeat(10, 40px);
  grid-column-gap: 6px;
`

type FloorBoxProps = {
  act?: 0 | 1 | 2
}

const FloorBox = styled.div`
  width: 40px;
  height: 40px;
  border: solid 1px
    ${({ act }: FloorBoxProps) => (act === 2 ? `#f00` : act === 1 ? `#d0ff00` : colorValue.title)};
  font-size: 20px;
  font-weight: 600;
  color: ${({ act }: FloorBoxProps) =>
    act === 2 ? `#f00` : act === 1 ? `#d0ff00` : colorValue.title};
  ${cssFlex.center}
`

type Props = {
  floorKeys: string[]
}

const filreLvMap = new Map<string, 0 | 1 | 2>()

export default ({ floorKeys }: Props) => {
  useEffect(() => {
    addLvAFloor(floorKeys)
  }, [floorKeys])

  return (
    <Layout>
      <FloorBox>A棟</FloorBox>
      <FloorBox act={filreLvMap.get('ac')}>B2</FloorBox>
      <FloorBox act={filreLvMap.get('a0')}>B1</FloorBox>
      <FloorBox act={filreLvMap.get('a1')}>1F</FloorBox>
      <FloorBox act={filreLvMap.get('a2')}>2F</FloorBox>
      <FloorBox act={filreLvMap.get('a3')}>3F</FloorBox>
      <FloorBox act={filreLvMap.get('a4')}>4F</FloorBox>
      <FloorBox act={filreLvMap.get('a5')}>5F</FloorBox>
      <FloorBox act={filreLvMap.get('a6')}>6F</FloorBox>
      <FloorBox act={filreLvMap.get('a7')}>7F</FloorBox>
      <FloorBox act={filreLvMap.get('a8')}>8F</FloorBox>
      <FloorBox act={filreLvMap.get('a9')}>9F</FloorBox>
      <div></div>
      <div></div>
      <div></div>
      <div></div>
      <FloorBox>B棟</FloorBox>
      <FloorBox act={filreLvMap.get('b1')}>1F</FloorBox>
      <FloorBox act={filreLvMap.get('b2')}>2F</FloorBox>
      <FloorBox act={filreLvMap.get('b3')}>3F</FloorBox>
    </Layout>
  )
}

restMap()
function restMap() {
  filreLvMap.set('ac', 0)
  filreLvMap.set('a0', 0)
  filreLvMap.set('a1', 0)
  filreLvMap.set('a2', 0)
  filreLvMap.set('a3', 0)
  filreLvMap.set('a4', 0)
  filreLvMap.set('a5', 0)
  filreLvMap.set('a6', 0)
  filreLvMap.set('a7', 0)
  filreLvMap.set('a8', 0)
  filreLvMap.set('a9', 0)
  filreLvMap.set('b1', 0)
  filreLvMap.set('b2', 0)
  filreLvMap.set('b3', 0)
}

function addLvAFloor(floorKeys: string[]) {
  restMap()
  floorKeys.forEach((key: string) => {
    filreLvMap.set(key, 2)
    switch (key) {
      case 'ac':
        filreLvMap.get('a0') === 0 && filreLvMap.set('a0', 1)
        filreLvMap.get('a1') === 0 && filreLvMap.set('a1', 1)
        break
      case 'a0':
        filreLvMap.get('ac') === 0 && filreLvMap.set('ac', 1)
        filreLvMap.get('a1') === 0 && filreLvMap.set('a1', 1)
        filreLvMap.get('a2') === 0 && filreLvMap.set('a2', 1)
        break
      case 'a1':
        filreLvMap.get('a0') === 0 && filreLvMap.set('a0', 1)
        filreLvMap.get('a2') === 0 && filreLvMap.set('a2', 1)
        filreLvMap.get('a3') === 0 && filreLvMap.set('a3', 1)
        break
      case 'a2':
        filreLvMap.get('a1') === 0 && filreLvMap.set('a1', 1)
        filreLvMap.get('a3') === 0 && filreLvMap.set('a3', 1)
        filreLvMap.get('a4') === 0 && filreLvMap.set('a4', 1)
        break
      case 'a3':
        filreLvMap.get('a2') === 0 && filreLvMap.set('a2', 1)
        filreLvMap.get('a4') === 0 && filreLvMap.set('a4', 1)
        filreLvMap.get('a5') === 0 && filreLvMap.set('a5', 1)
        break
      case 'a4':
        filreLvMap.get('a3') === 0 && filreLvMap.set('a3', 1)
        filreLvMap.get('a5') === 0 && filreLvMap.set('a5', 1)
        filreLvMap.get('a6') === 0 && filreLvMap.set('a6', 1)
        break
      case 'a5':
        filreLvMap.get('a4') === 0 && filreLvMap.set('a4', 1)
        filreLvMap.get('a6') === 0 && filreLvMap.set('a6', 1)
        filreLvMap.get('a7') === 0 && filreLvMap.set('a7', 1)
        break
      case 'a6':
        filreLvMap.get('a5') === 0 && filreLvMap.set('a5', 1)
        filreLvMap.get('a7') === 0 && filreLvMap.set('a7', 1)
        filreLvMap.get('a8') === 0 && filreLvMap.set('a8', 1)
        break
      case 'a7':
        filreLvMap.get('a6') === 0 && filreLvMap.set('a6', 1)
        filreLvMap.get('a8') === 0 && filreLvMap.set('a8', 1)
        filreLvMap.get('a9') === 0 && filreLvMap.set('a9', 1)
        break
      case 'a8':
        filreLvMap.get('a7') === 0 && filreLvMap.set('a7', 1)
        filreLvMap.get('a9') === 0 && filreLvMap.set('a9', 1)
        break
      case 'a9':
        filreLvMap.get('a8') === 0 && filreLvMap.set('a8', 1)
        break
      case 'b1':
        filreLvMap.get('b2') === 0 && filreLvMap.set('b2', 1)
        filreLvMap.get('b3') === 0 && filreLvMap.set('b3', 1)
        break
      case 'b2':
        filreLvMap.get('b1') === 0 && filreLvMap.set('b1', 1)
        filreLvMap.get('b3') === 0 && filreLvMap.set('b3', 1)
        break
      case 'b3':
        filreLvMap.get('b2') === 0 && filreLvMap.set('b2', 1)
        break
      default:
    }
  })
}
