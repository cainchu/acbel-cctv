import { useState } from 'react'
import { Link } from 'react-router-dom'
import styled from 'styled-components'
import { useAppSelector } from '../store'
import { fontColorSty, colorValue } from '../styles'

interface adminMenuItem {
  itemName: string
  router: string
  enable: boolean
}

const Layout = styled.div`
  width: 100%;
  height: 35px;
  color: ${fontColorSty.second};
  display: flex;
  justify-content: space-between;
`

const MenuWrap = styled.div`
  width: 100%;
  box-sizing: border-box;
  display: flex;
  justify-content: flex-start;
`
const MenuItem = styled.div`
  box-sizing: border-box;
  display: flex;
  justify-content: center;
  align-items: center;
  font-size: 16px;
  font-weight: 600;
  letter-spacing: 0.9px;
  color: ${colorValue.second};
  padding: 0 5px;
  position: relative;
  cursor: pointer;
  &:after {
    width: 1px;
    height: 18px;
    content: '';
    display: block;
    border-right: 1px solid ${colorValue.border};
    padding: 0 5px;
  }
  &:first-child {
    padding-left: 10px;
  }
  &:last-child {
    &:after {
      display: none;
    }
  }
`

const UserButton = styled.div`
  box-sizing: border-box;
  display: flex;
  justify-content: space-between;
  position: relative;
  padding: 0 5px;
  cursor: pointer;
`

const UserIcon = styled.div`
  width: 24px;
  height: 30px;
  background-size: 65%;
  background-image: url(/images/icon_user.svg);
  background-repeat: no-repeat;
  background-position: 0 70%;
`
const IconArea = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  transition: 0.3s;
  cursor: pointer;
  padding: 0 5px;
`
const UserName = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  font-size: 14px;
  white-space: nowrap;
  cursor: pointer;
`
const ArrowDown = styled.div`
  width: 0;
  height: 0;
  border-left: 6px solid transparent;
  border-right: 6px solid transparent;
  border-top: 7px solid #2ab638;
  transition: 0.3s;
  transform: scaleY(-1);
  cursor: pointer;
  &.mouseenter {
    transform: unset;
  }
`

const DropdownPanel = styled.div`
  width: 100%;
  height: 55px;
  display: flex;
  align-items: center;
  justify-content: center;
  position: absolute;
  right: 0;
  bottom: 0;
  transition: 0.3s;
  transform: scaleY(0);
  background-color: #00000080;
  transform-origin: bottom;
  opacity: 0;
  pointer-events: none;
  &.mouseenter {
    transform: scaleY(1) translateY(55px);
    opacity: 1;
    pointer-events: auto;
  }
`
const LogoutIcon = styled.div`
  width: 20px;
  height: 20px;
  background-image: url(/images/icon_logout.svg);
  background-repeat: no-repeat;
  background-size: contain;
  margin: 0 0 0 3px;
  cursor: pointer;
`
const LogoutBtn = styled.div`
  background-color: ${colorValue.second};
  width: 100px;
  height: 30px;
  border-radius: 5px;
  font-size: 13.5px;
  font-weight: bold;
  color: #000;
  display: flex;
  justify-content: flex-start;
  align-items: center;
  cursor: pointer;
  box-sizing: border-box;
  padding: 5px;
  border: 2px solid ${colorValue.second};
`

const linkSty = {
  textDecoration: 'none',
  outline: 'none',
  height: '100%',
  display: 'flex',
  alignItems: 'center',
  color: colorValue.second,
}

export default ({ logout, adminMenu }: { logout: Function; adminMenu: adminMenuItem[] }) => {
  const permission = useAppSelector((state) => state.user.permission)
  const [panelShow, setPanelShow] = useState<Boolean>(false)

  const getUserName = (permission: string) => {
    switch (permission) {
      case 'A':
        return 'Admin'
        break
      case 'B':
        return 'Admin (Remote)'
        break
      case 'C':
        return 'Device Monitor'
        break
      case 'D':
        return 'Assembly Line Monitor'
        break
      case 'E':
        return 'Dock Monitor'
        break
      default:
        return 'User'
        break
    }
  }

  const renderAdminMenu = () => {
    return (
      <MenuWrap>
        {adminMenu.map((item, key) => {
          return (
            <MenuItem
              key={key}
              style={{
                pointerEvents: item.enable ? 'auto' : 'none',
                cursor: item.enable ? 'cursor' : 'initial',
              }}
            >
              <Link to={item.enable ? item.router : ''} style={linkSty}>
                {item.itemName}
              </Link>
            </MenuItem>
          )
        })}
      </MenuWrap>
    )
  }

  return (
    <Layout>
      {/* AdminBar---權限:{permission}---- */}
      {renderAdminMenu()}
      <UserButton onMouseEnter={() => setPanelShow(true)} onMouseLeave={() => setPanelShow(false)}>
        <UserIcon />
        <UserName>{permission ? getUserName(permission) : 'User'}</UserName>
        <IconArea>
          <ArrowDown className={`${panelShow ? 'mouseenter' : ''}`} />
        </IconArea>
        <DropdownPanel className={`${panelShow ? 'mouseenter' : ''}`}>
          <LogoutBtn onClick={() => logout()}>
            <LogoutIcon></LogoutIcon>
            LOGOUT
          </LogoutBtn>
        </DropdownPanel>
      </UserButton>
    </Layout>
  )
}
