import styled from 'styled-components'
import { BKmeta } from '.'
import FloorBgActBk from './FloorBgActBk'

type Props = {
  x: string | number
  y: string | number
  src?: string
  meta?: BKmeta[]
  actOb: (id: string) => void
}
const LayoutProps = styled.div`
  position: absolute;
  left: ${({ x }: Props) => `${x}px`};
  top: ${({ y }: Props) => `${y}px`};
`

export default (props: Props) => {
  return (
    <LayoutProps {...props}>
      <img src={props.src} alt="" />
      {props.meta &&
        props.meta.map(({ title, x, y, w, h, id, act }) => {
          return (
            <FloorBgActBk
              x={x}
              y={y}
              w={w}
              h={h}
              key={id}
              act={act}
              onClick={() => props.actOb(id)}
            >
              {title}
            </FloorBgActBk>
          )
        })}
    </LayoutProps>
  )
}
