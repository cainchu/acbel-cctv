import styled from 'styled-components'
import { colorValue } from './../../styles/index'

interface Props {
  children: string
  x: number
  y: number
  act?: boolean
}

const TagStyle = styled.div`
  position: absolute;
  left: ${({ x }: Props) => `${x}px`};
  top: ${({ y }: Props) => `${y}px`};
  padding: 0 7px;
  box-shadow: 0 1px 2px 0 #000;
  background-color: ${colorValue.special};
  font-size: 18px;
  font-weight: 600;
  letter-spacing: 0.9px;
  color: #000;
  transform: translate(10px, -36px);
  pointer-events: none;
  user-select: none;
  display: ${({ act }: Props) => (act ? `inline-block` : 'none')};
  &:after {
    position: absolute;
    left: 0;
    bottom: -10px;
    content: '';
    width: 0;
    height: 0;
    border-style: solid;
    border-width: 10px 10px 0 0;
    border-color: ${colorValue.special} transparent transparent transparent;
  }
`

export default (props: Props) => {
  return <TagStyle {...props}>{props.children}</TagStyle>
}
