import styled from 'styled-components'
import FloorBgTga from './FloorBgTga'

interface Props {
  children: string
  w: number
  h: number
  x: number
  y: number
  act?: boolean
  onClick: (e: unknown) => void
}

const BkStyle = styled.div.attrs(({ children }: Props) => ({
  title: children,
}))`
  position: absolute;
  left: ${({ x }: Props) => `${x}px`};
  top: ${({ y }: Props) => `${y}px`};
  width: ${({ w }: Props) => `${w}px`};
  height: ${({ h }: Props) => `${h}px`};
  cursor: pointer;
  /* background: #f008; */
  user-select: none;
`

export default (props: Props) => {
  const { x, y, w, h, act } = props
  return (
    <>
      <BkStyle x={x} y={y} w={w} h={h} act={act} children="" onClick={props.onClick}></BkStyle>
      <FloorBgTga {...props} />
    </>
  )
}
