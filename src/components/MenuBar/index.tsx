import styled from 'styled-components'
import useServiceTime from '../../hooks/useServiceTime'
import usePathName from '../../hooks/usePathName'
import { colorValue, fontColorSty } from '../../styles'
import { yyyy_mm_dd_h_m_s } from '../../utils/dateFormat'
import Menu from './Menu'
import './menu.css' /* css收合動態 */

const height = `55px`

const LogoTilLayout = styled.div`
  height: ${height};
  display: flex;
  flex: 0 0 361px;
  align-items: center;
  justify-content: center;
  border-right: solid 2px ${colorValue.border};
`

const LogoTil = () => (
  <LogoTilLayout>
    <img src="/images/logo.png" />
  </LogoTilLayout>
)

const Layout = styled.div`
  width: 100%;
  height: ${height};
  display: flex;
  align-items: center;
  justify-content: space-between;
  color: ${fontColorSty.main};
  border-top: solid 2px ${colorValue.border};
  border-bottom: solid 2px ${colorValue.border};
`

const MenuBoxLayout = styled.div`
  display: flex;
  flex: 1 1 auto;
  height: ${height};
  background-image: url(/images/slash_navy.svg);
`

const AdminBtn = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  padding: 0 9px;
  cursor: pointer;
  font-family: AdobeHeitiStd;
  font-size: 16px;
  color: ${colorValue.title};
  &:before {
    content: '';
    width: 14px;
    height: 14px;
    background-image: url(/images/icon_home.svg);
    background-repeat: no-repeat;
    margin-right: 2px;
  }
`

const TimeView = styled.div`
  width: 280px;
  height: ${height};
  display: flex;
  flex: 0 0 280px;
  align-items: center;
  justify-content: center;
  border-left: solid 2px ${colorValue.border};
  font-family: Verdana;
  font-size: 18px;
`

type Prop = {
  menuTree?: menuTreeType | null
  onAdmin: () => void
  onPiar: () => void
  onMenuItemClick: (system: string, systemInd: number, itemInd: number) => void
  permission: string
}

export default ({ onAdmin, onPiar, menuTree, onMenuItemClick, permission }: Prop) => {
  const { equipment, production, pier } = menuTree || {}
  const { time } = useServiceTime() //取回服務器時間

  const { path } = usePathName()

  return (
    <Layout>
      <LogoTil />
      <MenuBoxLayout>
        <Menu
          system={equipment?.system || []}
          onMenuClick={(systemInd: number, itemInd: number) =>
            onMenuItemClick('equipment', systemInd, itemInd)
          }
          disable={permission.charAt(0) === '0'}
        >
          {equipment?.title}
        </Menu>
        <Menu
          system={production?.system || []}
          onMenuClick={(systemInd: number, itemInd: number) =>
            onMenuItemClick('production', systemInd, itemInd)
          }
          disable={permission.charAt(1) === '0'}
        >
          {production?.title}
        </Menu>
        <Menu onClick={onPiar} disable={permission.charAt(2) === '0'}>
          {pier?.title}
        </Menu>
        {/* <AdminBtn onClick={onAdmin}>戰情中心首頁</AdminBtn> */}
        <AdminBtn>{path}</AdminBtn>
      </MenuBoxLayout>
      <TimeView>{yyyy_mm_dd_h_m_s(time)}</TimeView>
    </Layout>
  )
}
