import styled from 'styled-components'
import { colorValue } from '../../styles'

const MenuLiItem = styled.li`
  position: relative;
  padding-left: 25px;
  white-space: nowrap;
  /* overflow: hidden; */
  text-overflow: ellipsis;
  cursor: pointer;
  &:before {
    content: '';
    display: inline-block;
    width: 10px;
    height: 24px;
    border-left: solid 2px ${colorValue.border};
    border-bottom: solid 2px ${colorValue.border};
    position: absolute;
    left: 7px;
    top: -8px;
  }
  &:hover {
    color: ${colorValue.main};
  }
`

const TitleLayout = styled.div`
  width: 100%;
  font-size: 20px;
  line-height: 27px;
  letter-spacing: 1px;
  display: flex;
  align-items: center;
  &:before {
    content: '';
    display: inline-block;
    width: 24px;
    height: 8px;
    background-image: url(/images/icon_arrow.svg);
    background-repeat: no-repeat;
  }
`

const TilSpanStyle = styled.div`
  width: 100%;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
`

const ListBoxStyle = styled.ul`
  font-size: 18px;
  letter-spacing: 0.9px;
  line-height: 24px;
  color: ${colorValue.second};
  list-style-type: none;
  padding: 0;
`

const Layout = styled.div`
  margin: 13px 25px;
`

type Prop = {
  list: menuTreeItemListType
  onClickItem: (ind: number) => void
}

export default ({ list, onClickItem }: Prop) => {
  return (
    <Layout>
      <TitleLayout>
        <TilSpanStyle>{list.title}</TilSpanStyle>
      </TitleLayout>
      <ListBoxStyle>
        {list.items.map((item, ind) => (
          <MenuLiItem key={ind} onClick={() => onClickItem(ind)}>
            {item.title}
          </MenuLiItem>
        ))}
      </ListBoxStyle>
    </Layout>
  )
}
