import styled from 'styled-components'
import { colorValue } from '../../styles'
import MenuGurp from './MenuGurp'

const Layout = styled.div`
  position: absolute;
  top: 53px;
  left: 0px;
  width: 240px;
  color: ${colorValue.title};
  background-color: #000;
  z-index: 999;
  font-weight: 600;
  max-height: 0px;
  overflow: hidden;
  transition: max-height 0.5s;
`

type Prop = {
  system: menuTreeItemListType[]
  onMenuClick?: (systemInd: number, itemInd: number) => void
}

export default ({ system, onMenuClick }: Prop) => {
  return (
    <Layout className="MenuDrawer">
      {system &&
        system.map((list, ind) => (
          <MenuGurp
            list={list}
            key={ind}
            onClickItem={(itemInd: number) => onMenuClick && onMenuClick(ind, itemInd)}
          ></MenuGurp>
        ))}
    </Layout>
  )
}
