import { MouseEventHandler } from 'react'
import styled from 'styled-components'
import { colorValue } from '../../styles'
import MenuDrawer from './MenuDrawer'

const Layout = styled.div`
  position: relative;
  width: 240px;
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  border-right: solid 2px ${colorValue.border};
  border-bottom: solid 2px ${colorValue.border};
  border-top: solid 2px ${colorValue.border};
  background-color: #000;
  font-family: SegoeUI;
  font-size: 24px;
  font-weight: 600;
  line-height: 0.92;
  letter-spacing: 1.2px;
  cursor: pointer;
  opacity: ${({ disable }: { disable: boolean }) => (disable ? 0.5 : 1)};
  pointer-events: ${({ disable }: { disable: boolean }) => (disable ? 'none' : 'auto')};
`

interface Prop {
  children: JSX.Element | string | undefined
  disable: boolean
  onMenuClick?: (system: number, items: number) => void
  system?: menuTreeItemListType[]
  onClick?: MouseEventHandler<HTMLDivElement> | undefined
}
export default (props: Prop) => {
  return (
    <Layout className="Menu" onClick={props.onClick} disable={props.disable}>
      {props.children}
      <MenuDrawer system={props.system || []} onMenuClick={props?.onMenuClick}></MenuDrawer>
    </Layout>
  )
}
