import { CSSTransition } from 'react-transition-group';
import { pageSize } from '../styles';

const loadingStyle: React.CSSProperties = {
  ...pageSize,
  position: 'absolute',
  left: 0,
  top: 0,
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  color: 'white',
  backgroundColor: '#00000088',
};
interface Prop {
  loading: boolean;
}
export default ({ loading }: Prop) => {
  return (
    <CSSTransition in={loading} timeout={1000} classNames="fade" unmountOnExit>
      <div style={loadingStyle}>Loading...</div>
    </CSSTransition>
  );
};
