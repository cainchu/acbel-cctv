import { useRef } from 'react';
import styled from 'styled-components';
import { useEventListener } from 'usehooks-ts';

const Vd = styled.div`
  grid-area: ${({ num }: { num: number }) => `vd${num}`};
`;

export default ({
  num,
  children,
  onSwitchVdo,
}: {
  num: number;
  children?: React.ReactNode;
  onSwitchVdo: Function;
}) => {
  const vdRef = useRef(null);
  useEventListener(
    'drop',
    (e) => {
      cancelDefault(e);
      const jsonData = e.dataTransfer?.getData('text/plain');
      const vdoMeta = jsonData && JSON.parse(jsonData);
      onSwitchVdo({
        uuid: vdoMeta.uuid,
        key: num,
      });
    },
    vdRef,
  );
  useEventListener('dragover', cancelDefault, vdRef);
  return (
    <Vd num={num} ref={vdRef}>
      {children}
    </Vd>
  );
};

function cancelDefault(e: Event) {
  e.preventDefault();
  e.stopPropagation();
  return false;
}
