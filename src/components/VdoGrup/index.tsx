import styled from 'styled-components'
import Lazy from 'lazy.js'
import Ctv from '../Ctv'
import VdoFrame from './VdoFrame'
import { useAppDispatch, useAppSelector } from '../../store'
import { fullAnVdoAction, removeAlertNotificationAction, switchVdoAction } from '../../store/saga'
import { claerVdoGrupByKey, VDO_LAYOUT } from '../../store/vdos'
import { AlarmType } from '../../utils/AlarmSystem'

interface I_GrupLayout {
  fullKey: number
}

const Main6F = styled.div<I_GrupLayout>`
  display: grid;
  grid-template-columns: 500px 500px 500px;
  grid-template-rows: 310px 310px 310px;
  grid-gap: 10px;
  height: 100%;
  width: 100%;
  grid-template-areas: ${(p) =>
    p.fullKey < 0
      ? `'vd0 vd0 vd1'
    'vd0  vd0 vd2'
    'vd3 vd4 vd5'`
      : `'vd6 vd6 vd6'
    'vd6  vd6 vd6'
    'vd6 vd6 vd6'`};
`

const Main4F = styled.div<I_GrupLayout>`
  display: grid;
  grid-template-columns: 750px 750px;
  grid-template-rows: 465px 465px;
  grid-gap: 20px;
  height: 100%;
  width: 100%;
  grid-template-areas: ${(p) =>
    p.fullKey < 0
      ? `'vd0 vd1'
    'vd2  vd3'`
      : `'vd4 vd4'
    'vd4  vd4'`};
`

interface Prop {
  vdoMetas: I_CtvMeta[]
}

export default ({ vdoMetas }: Prop) => {
  const fullKey = useAppSelector((store) => store.vdos.fullKey) //決定是否滿版的key -1 以外代表滿版
  const vodLayoutType = useAppSelector((store) => store.vdos.vodLayoutType)
  const dispatch = useAppDispatch()
  const onSwitchVdo = ({ uuid, key }: { uuid: string; key: number }) => {
    dispatch(switchVdoAction({ uuid, key }))
  }

  const onVodClose = (ind: number) => {
    if (fullKey === -1) {
      dispatch(claerVdoGrupByKey(ind))
    } else {
      //全螢幕情況下按下關閉 則退回
      dispatch(fullAnVdoAction(fullKey))
    }
  }

  const onVodFull = (ind: number) => {
    if (fullKey === -1) {
      dispatch(fullAnVdoAction(ind))
    } else {
      dispatch(fullAnVdoAction(fullKey))
    }
  }

  const onCtvTouch = (meta: I_CtvMeta) => {
    /* 點掉 SOS 對話報警 */
    const hasSos =
      Lazy(meta.warnTypes).find((warn) => warn === 'sos') === 'sos' /* 這個視窗有SOS對話要求 */
    hasSos &&
      dispatch(
        removeAlertNotificationAction({
          id: meta?.cameraId || '',
          time: new Date().getTime(),
          type: AlarmType.SOS,
        }),
      )
  }

  const mapVdoFrames = (obs: I_CtvMeta[]) => {
    return obs.map((meta, ind) => (
      <VdoFrame num={ind} key={`${meta.uuid}`} onSwitchVdo={onSwitchVdo}>
        <Ctv
          meta={meta}
          onClose={() => onVodClose(ind)}
          onFull={() => onVodFull(ind)}
          isFull={ind === obs.length - 1}
          onTouch={(meta) => onCtvTouch(meta)}
        ></Ctv>
      </VdoFrame>
    ))
  }

  switch (vodLayoutType) {
    case VDO_LAYOUT.V5add1:
      return <Main6F fullKey={fullKey}>{mapVdoFrames(vdoMetas)}</Main6F>
    case VDO_LAYOUT.V4:
      return <Main4F fullKey={fullKey}>{mapVdoFrames(vdoMetas)}</Main4F>
    default:
      return <h1>有什麼搞錯了嗎?</h1>
  }
}
