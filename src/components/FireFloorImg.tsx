import { useMemo } from 'react'
import styled from 'styled-components'

type LayoutProps = {
  left: number
  top: number
}
const Layout = styled.div`
  position: absolute;
  left: ${({ left }: LayoutProps) => left + 'px'};
  top: ${({ top }: LayoutProps) => top + 'px'};
`

/* 圖面對應的座標 */
const imagePositionMap = new Map<string, number[]>()
imagePositionMap.set('ac', [452, 36])
imagePositionMap.set('a0', [465, 36])
imagePositionMap.set('a1', [298, 186])
imagePositionMap.set('a2', [283, 165])
imagePositionMap.set('a3', [362, 159])
imagePositionMap.set('a4', [290, 185])
imagePositionMap.set('a5', [333, 150])
imagePositionMap.set('a6', [263, 186])
imagePositionMap.set('a7', [438, 25])
imagePositionMap.set('a8', [412, 36])
imagePositionMap.set('a9', [411, 11])
imagePositionMap.set('b1', [208, 182])
imagePositionMap.set('b2', [198, 171])
imagePositionMap.set('b3', [207, 172])

type Props = {
  floorId: string
}

export default ({ floorId }: Props) => {
  const { left, top } = useMemo(() => {
    const position = imagePositionMap.get(floorId)
    return position ? { left: position[0], top: position[1] } : { left: 0, top: 0 }
  }, [floorId])
  return (
    <Layout left={left} top={top}>
      <img src={`/images/fire/scene_${floorId}_fire.png`} alt="" />
    </Layout>
  )
}
