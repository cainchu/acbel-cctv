import FloorItem from './FloorItem'

interface Prop {
  floors: I_Floor[]
  monitorFloor: (uuid: string) => void
  draggable?: boolean
}

export default ({ floors, monitorFloor, draggable }: Prop) => {
  return (
    <div
      css={`
        height: 480px;
        overflow: auto;
      `}
    >
      {floors.map((floor) => (
        <FloorItem
          floor={floor}
          key={floor.uuid}
          monitorFloor={monitorFloor}
          draggable={draggable}
        ></FloorItem>
      ))}
    </div>
  )
}
