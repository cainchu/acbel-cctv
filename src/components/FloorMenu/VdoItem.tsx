import { useRef } from 'react'
import { useEventListener } from 'usehooks-ts'
import { colorValue } from '../../styles'

interface I_ActTag {
  select?: boolean
  onSelect?: (select: boolean) => void
}

const ActTag = ({ select, onSelect }: I_ActTag) => {
  const onClicActTag = () => {
    onSelect && onSelect(!!select) //TODO:點擊位置得再調整
  }
  return (
    <div
      onClick={() => onClicActTag()}
      css={`
        flex: 0 0 10px;
        width: 10px;
        height: 10px;
        border-radius: 50%;
        border: 1px solid ${colorValue.second};
        background-color: ${select ? colorValue.second : 'none'};
        cursor: pointer;
      `}
    ></div>
  )
}

interface Prop extends I_ActTag {
  children?: JSX.Element | string
  vdo: I_CtvMeta
  draggable?: boolean
}

export default ({ select, children, vdo, onSelect, draggable }: Prop) => {
  const elRef = useRef(null)
  useEventListener(
    'dragstart',
    (e) => {
      e.dataTransfer?.setData('text/plain', JSON.stringify(vdo))
    },
    elRef,
  )
  return (
    <div
      ref={elRef}
      draggable={draggable}
      css={`
        width: 100%;
        height: 25px;
        margin: 0 10px;
        padding: 0 14px;
        display: flex;
        align-items: center;
        opacity: 0.75;
        background-color: #000;
        cursor: grab;
        &:hover {
          background-color: #313131;
        }
      `}
    >
      <ActTag select={select} onSelect={onSelect}></ActTag>
      <div
        title="拖曳到撥放視窗"
        css={`
          flex: 1 1 auto;
          margin: 0 5px;
          overflow: hidden;
          text-overflow: ellipsis;
          cursor: grab;
          user-select: none;
        `}
      >
        {children}
      </div>
    </div>
  )
}
