import { MouseEvent, useState } from 'react'
import { colorValue } from '../../styles'
import VdoItem from './VdoItem'

interface Prop {
  floor: I_Floor
  monitorFloor: (uuid: string) => void
  draggable?: boolean
}

export default ({ floor, monitorFloor, draggable }: Prop) => {
  const { vdos } = floor
  const [off, setOff] = useState(false)
  const onClick = (e: MouseEvent) => {
    e.stopPropagation()
    monitorFloor(floor.uuid)
  }
  return (
    <div>
      <div
        title={`切換影像 ${floor.title}`}
        onClick={(e) => onClick(e)}
        css={`
          position: relative;
          width: 100%;
          height: 25px;
          display: flex;
          background-color: #000000;
          font-size: 16px;
          line-height: 1.19;
          letter-spacing: 1.6px;
          color: ${colorValue.main};
          border-top: 1px solid ${colorValue.title};
          cursor: pointer;
          &:before {
            content: '';
            display: block;
            margin-right: 9px;
            width: 5px;
            height: 25px;
            background-color: ${colorValue.title};
          }
        `}
      >
        {floor.title}
        <div
          title={`${off ? '關閉' : '展開'}`}
          onClick={(e) => {
            e.stopPropagation()
            setOff(!off)
          }}
          css={`
            border-color: ${colorValue.title};
            border-style: solid;
            border-width: 0 8px 8px 8px;
            border-color: transparent transparent #007bff transparent;
            position: absolute;
            right: 8px;
            top: 8px;
            transform: ${off ? `rotate(0deg)` : 'rotate(180deg)'};
            cursor: pointer;
          `}
        ></div>
      </div>
      <div
        css={`
          max-height: ${off ? '260px' : 0};
          overflow: hidden;
          transition: max-height 200ms;
          transition-timing-function: ease-out;
        `}
      >
        {vdos.map((vdo) => (
          <VdoItem key={vdo.uuid} vdo={vdo} select={vdo.isPlaying} draggable={draggable}>
            {vdo.title}
          </VdoItem>
        ))}
      </div>
    </div>
  )
}
