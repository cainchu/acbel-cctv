import { Fragment, useEffect, useRef, useState } from 'react'
import styled from 'styled-components'
import DropdownAi from './DropdownAi'
import RoiList from './RoiList'
import AiToggle from './AiToggle'
import { colorValue } from '../../styles'
import { useAppDispatch } from './../../store'
import { getAiStatusAction } from './../../store/saga'
import { useAiRoiSelector } from './../../hooks/useSelectAiRoi'
import { setAiRetentateUrl } from './../../store/aiRetentate'
const DetectListContainer = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
`
export const Wrap = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
  max-height: 730px;
  overflow: hidden;
  &.short {
    max-height: 440px;
  }
`
export const WrapInner = styled.div`
  width: 100%;
  display: flex;
  justify-content: flex-start;
  align-items: center;
  flex-direction: column;
  overflow-x: hidden;
  overflow-y: scroll;
  box-sizing: border-box;
  padding: 0 0 30px 0;
`
export const EachRow = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
  border: 1px solid ${colorValue.border};
  position: relative;
  box-sizing: border-box;
  margin: 30px 0 0 0;
  padding: 30px 20px 20px 20px;
`
export const WrapTitle = styled.div`
  display: flex;
  padding: 0 25px;
  height: 36px;
  position: absolute;
  top: -20px;
  box-sizing: border-box;
  justify-content: center;
  color: ${colorValue.main};
  font-size: 24px;
  font-weight: 600;
  background-color: #0b1827;
`

const SearchBtn = styled.div`
  width: 140px;
  height: 30px;
  color: #000;
  font-size: 22px;
  font-weight: bold;
  font-family: SegoeUI;
  border: 1px solid #2a8ab6;
  background-color: #42c6fc;
  display: flex;
  justify-content: center;
  margin: 0 25px;
  line-height: 30px;
  transition: 0.3s;
  cursor: pointer;
  z-index: 1;
  &:hover {
    background-color: #2a8ab6;
    color: #0c1a2a;
  }
  &.disable {
    background-color: #76a6c4;
    pointer-events: none;
    opacity: 0.5;
  }
`

const NavyLine = styled.div`
  height: 5px;
  width: 100%;
  background-image: url(/images/slash_10px.svg);
  margin: 20px 0 20px 0;
`

const TypeSelectRow = styled.div.attrs((props: { showPolygonData: Boolean }) => props)`
  width: 100%;
  box-sizing: border-box;
  display: flex;
  box-sizing: border-box;
`

const TabContainer = styled.div`
  width: 100%;
`

const TabHeader = styled.div`
  display: flex;

  align-items: flex-end;
  margin: 0 0 3px 0;
  border-bottom: 3px solid ${colorValue.border};
  position: relative;

  &:after {
    content: '';
    width: 10px;
    height: 5px;
    background-color: ${colorValue.title};
    position: absolute;
    right: 0;
    bottom: -4px;
  }
`
const Tab = styled.div`
  // background-color:${colorValue.border};
  font-size: 20px;
  box-sizing: border-box;
  padding: 2px 10px;
  margin: 0 0 -3px 0;
  cursor: pointer;
  &.current {
    background-color: ${colorValue.brightBackground};
    border-bottom: 3px solid ${colorValue.title};
  }
`
const TabBody = styled.div.attrs((props: { showContent: Boolean }) => props)`
  background-color: ${colorValue.brightBackground};
  display: flex;
  width: 100%;
  max-height: ${(props) => (props.showContent ? '55px' : 'calc(100% - 35px)')};
  // height:calc(100% - 35px);
  box-sizing: border-box;
  padding: 10px 20px;
`
const decoDashSty = {
  width: '5px',
  height: '10px',
  backgroundColor: colorValue.title,
  margin: '0 0 -7px 0',
}

const intervalSty = {
  width: '1px',
  height: '22px',
  margin: '6px 20px 6px 20px',
  backgroundColor: colorValue.border,
}

const tipTextSty = {
  fontSize: '20px',
  color: '#42c6fc',
  margin: '0 20px 0 0',
  fontWeight: '500',
}

export interface PolygonData {
  deviceId: string
  analysisType: string
  roiType: string
  pos: number[][]
  sensitivity: number
  notation: string
  showRoi: Boolean
  roiColor: string
  uuid: string
}

export default () => {
  const [showPolygonData, setShowPolygonData] = useState<Boolean>(false)
  const [showAiSettingToggle, setShowAiSettingToggle] = useState<Boolean>(false)
  const [selectedDevice, setSelectedDevice] = useState<aiDeviceMenuType>()
  const [selectedAI, setSelectedAI] = useState<analysisType>()
  const [targetAiStatus, setTargetAiStatus] = useState<aiStatusParam>()

  const [currentTab, setCurrentTab] = useState<string>('AI')
  const [aiTypeTree, setAiTypeTree] = useState<analysisType[]>([])
  const { aiMenuList } = useAiRoiSelector()
  // const [aiMenuListWithoutDock, setAiMenuListWithoutDock] = useState<aiCamera[]>([])

  // useEffect(() => {
  //   const tempListWithoutDock = aiMenuList.filter((item) => {
  //     return(
  //       item.id !== 'ccca63a9-11c2-81f2-7aad-2fe90cb69427' &&
  //       item.id !== 'e5206e67-09ea-f717-9752-aedd76fb18b6'
  //     )
  //   })
  //   setAiMenuListWithoutDock(tempListWithoutDock);
  // }, [aiMenuList])

  useEffect(() => {
    getAiStatus()
    setSelectedDevice(undefined)
    setSelectedAI(undefined)
    setShowPolygonData(false)
    setShowAiSettingToggle(false)
    dispatch(setAiRetentateUrl(''))
  }, [currentTab])

  const dispatch = useAppDispatch()

  const getAiStatus = () => {
    dispatch(getAiStatusAction())
  }

  const confirmDeviceForRoi = () => {
    setShowPolygonData(true)
  }
  const resetDeviceForRoi = () => {
    setShowPolygonData(false)
    setSelectedDevice(undefined)
    setSelectedAI(undefined)
  }

  const confirmDeviceForAi = () => {
    setShowAiSettingToggle(true)
  }
  const resetDeviceForAi = () => {
    setShowAiSettingToggle(false)
    setSelectedDevice(undefined)
    setTargetAiStatus(undefined)
    dispatch(setAiRetentateUrl(''))
  }

  useEffect(() => {
    if (selectedDevice && selectedDevice.ai) {
      let tempTypeList = [
        { title: '智能電子圍籬', sysName: 'ai_fences', enable: true },
        { title: '智能大型遺留物', sysName: 'ai_retentate', enable: true },
      ]
      switch (selectedDevice.ai) {
        case '10':
          tempTypeList[0].enable = true
          tempTypeList[1].enable = false
          break
        case '01':
          tempTypeList[0].enable = false
          tempTypeList[1].enable = true
          break
        case '11':
          tempTypeList[0].enable = true
          tempTypeList[1].enable = true
          break

        default:
          break
      }
      setAiTypeTree(tempTypeList)
      setSelectedAI(undefined)
    }
  }, [selectedDevice])

  const renderRoiSettingSelector = () => {
    return (
      <Fragment>
        <TabBody showContent={showPolygonData}>
          <div style={tipTextSty}>請選擇攝影機與偵測的項目 - </div>
          <DropdownAi
            defaultText="SELECT CAMERA ID"
            itemList={aiMenuList}
            defaultUnFold={true}
            readonly={showPolygonData ? true : false}
            menuWidth="300px"
            menuHeight="30px"
            manuMargin="0"
            selectedItem={selectedDevice}
            setSelectedItem={setSelectedDevice}
            dataType={1}
            showPolygonData={showPolygonData}
            showAiSettingToggle={showAiSettingToggle}
            currentTab={currentTab}
          ></DropdownAi>
          <div style={intervalSty}></div>
          <DropdownAi
            defaultText="SELECT AI TYPE"
            itemList={aiTypeTree}
            defaultUnFold={true}
            readonly={showPolygonData ? true : false}
            menuWidth="300px"
            menuHeight="30px"
            manuMargin="0"
            selectedItem={selectedAI}
            setSelectedItem={setSelectedAI}
            dataType={2}
            analysisType={selectedDevice ? selectedDevice.ai : ''}
            showPolygonData={showPolygonData}
            showAiSettingToggle={showAiSettingToggle}
            currentTab={currentTab}
          ></DropdownAi>
          {showPolygonData ? (
            <SearchBtn onClick={resetDeviceForRoi}>重選</SearchBtn>
          ) : selectedDevice && selectedAI ? (
            <SearchBtn onClick={() => confirmDeviceForRoi()}>選擇</SearchBtn>
          ) : (
            <SearchBtn
              style={{
                backgroundColor: '#76a6c4',
                pointerEvents: 'none',
                opacity: '0.5',
              }}
              onClick={() => {}}
            >
              選擇
            </SearchBtn>
          )}
        </TabBody>
        <NavyLine style={{ margin: '0' }} />
      </Fragment>
    )
  }

  const renderAiSettingSelector = () => {
    return (
      <Fragment>
        <TabBody showContent={showAiSettingToggle}>
          <div style={tipTextSty}>您正在設定的攝影機 - </div>
          <DropdownAi
            defaultText="SELECT CAMERA ID"
            itemList={aiMenuList}
            // itemList={aiMenuListWithoutDock}
            defaultUnFold={true}
            readonly={showAiSettingToggle ? true : false}
            menuWidth="300px"
            menuHeight="30px"
            manuMargin="0"
            selectedItem={selectedDevice}
            setSelectedItem={setSelectedDevice}
            dataType={1}
            showPolygonData={showPolygonData}
            showAiSettingToggle={showAiSettingToggle}
            currentTab={currentTab}
          ></DropdownAi>
          <div style={intervalSty}></div>
          {showAiSettingToggle ? (
            <SearchBtn onClick={resetDeviceForAi}>重選</SearchBtn>
          ) : selectedDevice ? (
            <SearchBtn onClick={() => confirmDeviceForAi()}>選擇</SearchBtn>
          ) : (
            <SearchBtn
              style={{
                backgroundColor: '#76a6c4',
                pointerEvents: 'none',
                opacity: '0.5',
              }}
              onClick={() => {}}
            >
              選擇
            </SearchBtn>
          )}
        </TabBody>
        <NavyLine style={{ margin: '0' }} />
      </Fragment>
    )
  }

  return (
    <DetectListContainer>
      <TypeSelectRow showPolygonData={showPolygonData}>
        <TabContainer>
          <TabHeader>
            <div style={decoDashSty}></div>
            <Tab
              className={`${currentTab === 'AI' ? 'current' : ''}`}
              onClick={() => setCurrentTab('AI')}
            >
              智能功能開關
            </Tab>
            <div style={decoDashSty}></div>
            <Tab
              className={`${currentTab === 'ROI' ? 'current' : ''}`}
              onClick={() => setCurrentTab('ROI')}
            >
              ROI功能設定
            </Tab>
            <div style={decoDashSty}></div>
          </TabHeader>
          {currentTab === 'ROI' ? renderRoiSettingSelector() : null}
          {currentTab === 'AI' ? renderAiSettingSelector() : null}
        </TabContainer>
      </TypeSelectRow>

      {currentTab === 'ROI' && showPolygonData && selectedDevice && selectedAI ? (
        <RoiList
          showPolygonData={showPolygonData}
          selectedDevice={selectedDevice}
          selectedAI={selectedAI}
          currentTab={currentTab}
        ></RoiList>
      ) : null}
      {currentTab === 'AI' && showAiSettingToggle && selectedDevice ? (
        <AiToggle
          selectedDevice={selectedDevice}
          currentTab={currentTab}
          targetAiStatus={targetAiStatus}
          setTargetAiStatus={setTargetAiStatus}
        />
      ) : null}
    </DetectListContainer>
  )
}
