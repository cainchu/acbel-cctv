import { Fragment } from 'react'
import styled from 'styled-components'
import { colorValue } from '../../styles'
import { nxTimeFormat } from './../../utils/nxTimeFormat'
import { convertBigNumToM } from './../../utils/tools'
import { yyyy_mm_dd_h_m_s } from '../../utils/dateFormat'
import useIoKey from './../../hooks/useIoKey'
interface EventListTableProps {
  tempHistory: historyResult[]
  selectedDevice: (aiCamera | ioItemResult)[]
  selectedAI: analysisType[]
  playThisVideo: Function
  getDeviceName: Function
  getNxRouting: Function
  getNxDuration: Function
  getIdForNxPlayback: Function
  setDownloadingModalShow: Function
}
const EachRow = styled.div`
  width: 100%;
  display: flex;
  justify-content: flex-start;
  align-items: center;
  border-bottom: 1px dashed ${colorValue.dashedBorder};
  position: relative;
  box-sizing: border-box;
  padding: 10px 20px 10px 20px;
  color: ${colorValue.second};

  &:last-child {
    border-bottom: none;
  }
`

const EachColumn = styled.div`
  display: flex;
  justify-content: flex-start;
  align-items: center;
  box-sizing: border-box;
  padding: 0 20px;
  overflow: hidden;
  white-space: nowrap;
  text-overflow: ellipsis;
  font-size: 18px;
  font-weight: 500;
`

const EventThumbnail = styled.div`
  width: 60px;
  max-width: 60px;
  height: 60px;
  border-radius: 5px;
  background-size: cover;
  background-repeat: no-repeat;
  background-position: center;
`

const PlayBtn = styled.div`
  width: 100px;
  height: 40px;
  background-image: url('./images/btn_play.svg');
  background-repeat: no-repeat;
  background-size: contain;
  cursor: pointer;
  border-radius: 5px;
  margin: 0 30px;
`

const DownloadBtn = styled.div`
  width: 120px;
  height: 40px;
  background-image: url('./images/btn_download.svg');
  background-repeat: no-repeat;
  background-size: contain;
  cursor: pointer;
  border-radius: 5px;
`

const linkSty = {
  textDecoration: 'none',
  outline: 'none',
}

export default (props: EventListTableProps) => {
  const {
    tempHistory,
    playThisVideo,
    getDeviceName,
    getNxRouting,
    getNxDuration,
    getIdForNxPlayback,
    setDownloadingModalShow,
  } = props
  const { getCameraIdByIoKey } = useIoKey()
  const getAnalysisName = (analysis_type: string) => {
    switch (analysis_type) {
      case 'ai_fences':
        return '智能電子圍籬'
        break
      case 'ai_retentate':
        return '智能大型遺留物'
        break
      default:
        return analysis_type
        break
    }
  }

  const downloadStream = (eventTarget: EventTarget, eventItem: historyResult) => {
    setDownloadingModalShow(true)
    const durationSeconds = getNxDuration(
      eventItem.start_alarm_timestamp,
      eventItem.end_alarm_timestamp,
    ) //單位 秒
    const urlFromPlayer = (eventTarget as HTMLElement).dataset.url
    const matchedCameraId = getIdForNxPlayback(eventItem.device, eventItem.id)
    const streamUrl = urlFromPlayer
      ? urlFromPlayer
      : `${getNxRouting(matchedCameraId)}/media/${matchedCameraId}.mp4?pos=${nxTimeFormat(
          eventItem.start_alarm_timestamp,
        )}&duration=${durationSeconds}`

    fetch(streamUrl, { method: 'get', mode: 'no-cors', referrerPolicy: 'no-referrer' })
      .then((res) => res.blob())
      .then((res) => {
        const aElement = document.createElement('a')
        aElement.setAttribute('download', `${eventItem.event_id}.mp4`)
        // console.log('video_res', res)
        const href = URL.createObjectURL(res)
        aElement.href = href
        aElement.setAttribute('target', '_blank')
        aElement.click()
        URL.revokeObjectURL(href)
        setTimeout(() => {
          setDownloadingModalShow(false)
        }, 1000)
      })
      .catch((error) => {
        console.log('error', error)
      })
  }

  return (
    <Fragment>
      {tempHistory.map((eventItem, eventIndex) => {
        return (
          <EachRow key={eventIndex}>
            <EventThumbnail
              style={{
                backgroundImage: `url(${
                  eventItem.photo_path ? eventItem.photo_path : '/images/no_image.jpg'
                })`,
              }}
            ></EventThumbnail>
            <EachColumn style={{ width: '260px', margin: '0 0 0 40px' }}>
              {yyyy_mm_dd_h_m_s(convertBigNumToM(eventItem.start_alarm_timestamp.toString()))}
            </EachColumn>
            <EachColumn style={{ width: '100px' }}>{eventItem.notation}</EachColumn>
            <EachColumn style={{ width: '160px' }}>
              {eventItem.device === 'camera' ? 'CCTV' : 'I/O'}
            </EachColumn>
            <EachColumn style={{ width: '360px', margin: '0 40px 0 0' }}>
              {getDeviceName(eventItem.id) ? getDeviceName(eventItem.id) : eventItem.id}
              {/* {eventItem.id} */}
            </EachColumn>
            <EachColumn style={{ width: '180px' }}>
              {getAnalysisName(eventItem.analysis_type)}
            </EachColumn>
            <PlayBtn
              onClick={(event) =>
                playThisVideo({
                  device: eventItem.device,
                  id: getIdForNxPlayback(eventItem.device, eventItem.id),
                  startTime: eventItem.start_alarm_timestamp,
                  endTime: eventItem.end_alarm_timestamp,
                  ele: event.target,
                })
              }
            ></PlayBtn>
            <DownloadBtn
              className="download_stream"
              onClick={(e) => downloadStream(e.target as HTMLDivElement, eventItem)}
            ></DownloadBtn>
          </EachRow>
        )
      })}
    </Fragment>
  )
}
