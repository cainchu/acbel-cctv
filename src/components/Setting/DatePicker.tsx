import { useState, ChangeEventHandler, useEffect } from 'react'
import styled from 'styled-components'
import { DayPicker as ReactDayPicker, DateFormatter } from 'react-day-picker'
import '../../styles/day-picker-custom.css'
import { format, isValid, parse } from 'date-fns'
import tw from 'date-fns/locale/zh-TW'
import '../../utils/addDays'
import moment from 'moment'
import ClockPicker from './ClockPicker'
import { ClockType } from './EventList'

interface DayPickerProps {
  showPicker: Boolean
  showEventList: Boolean
  from: Date
  to: Date
  setFrom: Function
  setTo: Function
  fromClock: ClockType
  setFromClock: Function
  toClock: ClockType
  setToClock: Function
  getNow: Function
  toMidNight: Function
}

const DayPickerContainer = styled.div`
  display: flex;
`

const DayPickerInputWrap = styled.div`
  width: 100%;
  box-sizing: border-box;
  display: flex;
  justify-content: center;
  position: relative;
`

const DayPickerInput = styled.input`
  max-width: 100%;
  width: 250px;
  height: 30px;
  border: 1px solid #2a8ab6;
  font-weight: 500;
  font-size: 18px;
  padding: 3px 10px;
  outline: none;
  box-sizing: border-box;
  background-color: rgba(0, 0, 0, 0.8);
  line-height: 30px;
  &::placeholder {
    color: #2a8ab6;
    opacity: 1;
  }
`

const DayTimePickerWrap = styled.div`
  position: relative;
  width: 250px;
`

const DateIcon = styled.div.attrs((props: { showPicker: Boolean }) => props)`
  width: 26px;
  height: 30px;
  background-image: url(/images/icon_date.svg);
  background-repeat: no-repeat;
  background-position: center;
  position: absolute;
  right: 5px;
  cursor: ${(props) => (props.showPicker ? 'initial' : 'pointer')};
`

const intervalSty = {
  width: '40px',
  display: 'flex',
  justifyContent: 'center',
  paddingTop: '5px',
  fontWeight: '600',
  fontSize: '18px',
}

export default (props: DayPickerProps) => {
  const {
    from,
    setFrom,
    to,
    setTo,
    showPicker,
    showEventList,
    fromClock,
    setFromClock,
    toClock,
    setToClock,
    getNow,
    toMidNight,
  } = props

  const [startPickerShow, setStartPickerShow] = useState<Boolean>(showPicker)
  const [endPickerShow, setEndPickerShow] = useState<Boolean>(showPicker)

  useEffect(() => {
    let result = toMidNight(from).getTime() + fromClock.millisecond
    setFrom(new Date(result))
  }, [fromClock])

  useEffect(() => {
    let result = toMidNight(to).getTime() + toClock.millisecond
    // console.log('new Date(result)', new Date(result));
    setTo(new Date(result))
  }, [toClock])

  useEffect(() => {
    if (showPicker) {
      if (showEventList) {
        setStartPickerShow(false)
        setEndPickerShow(false)
      } else {
        setStartPickerShow(true)
        setEndPickerShow(true)
      }
    }
  }, [showEventList])

  const formatCaption: DateFormatter = (date, options) => {
    return <>{format(date, 'yyyy年MM月')}</>
  }

  const selectStartDate = (e: Date) => {
    let result = toMidNight(e).getTime() + fromClock.millisecond
    setFrom(new Date(result))
    showPicker ? null : setStartPickerShow(false)
  }

  const selectEndDate = (e: Date) => {
    const millisecond = e.getTime()

    const timeToEndTime = () => {
      let dateFormat = new Date(millisecond + 86399 * 1000)
      return dateFormat
    }

    function getEndTime() {
      let millisecond = 86399 * 1000
      return { name: '23:59', millisecond: millisecond, disable: false }
    }

    let isToday = verifyIsToday(e)
    if (isToday) {
      let result = toMidNight(e).getTime() + toClock.millisecond
      setTo(new Date(result))
      setToClock(getNow())
    } else {
      if (toClock.millisecond === getNow().millisecond) {
        setToClock(getEndTime())
      }
      setTo(timeToEndTime())
    }
    showPicker ? null : setEndPickerShow(false)
  }
  const verifyIsToday = (someDate: Date) => {
    const today = new Date()
    return (
      someDate.getDate() == today.getDate() &&
      someDate.getMonth() == today.getMonth() &&
      someDate.getFullYear() == today.getFullYear()
    )
  }
  const clockList = [
    { name: '00:00', millisecond: 0, disable: false },
    { name: '01:00', millisecond: 3600 * 1 * 1000, disable: false },
    { name: '02:00', millisecond: 3600 * 2 * 1000, disable: false },
    { name: '03:00', millisecond: 3600 * 3 * 1000, disable: false },
    { name: '04:00', millisecond: 3600 * 4 * 1000, disable: false },
    { name: '05:00', millisecond: 3600 * 5 * 1000, disable: false },
    { name: '06:00', millisecond: 3600 * 6 * 1000, disable: false },
    { name: '07:00', millisecond: 3600 * 7 * 1000, disable: false },
    { name: '08:00', millisecond: 3600 * 8 * 1000, disable: false },
    { name: '09:00', millisecond: 3600 * 9 * 1000, disable: false },
    { name: '10:00', millisecond: 3600 * 10 * 1000, disable: false },
    { name: '11:00', millisecond: 3600 * 11 * 1000, disable: false },
    { name: '12:00', millisecond: 3600 * 12 * 1000, disable: false },
    { name: '13:00', millisecond: 3600 * 13 * 1000, disable: false },
    { name: '14:00', millisecond: 3600 * 14 * 1000, disable: false },
    { name: '15:00', millisecond: 3600 * 15 * 1000, disable: false },
    { name: '16:00', millisecond: 3600 * 16 * 1000, disable: false },
    { name: '17:00', millisecond: 3600 * 17 * 1000, disable: false },
    { name: '18:00', millisecond: 3600 * 18 * 1000, disable: false },
    { name: '19:00', millisecond: 3600 * 19 * 1000, disable: false },
    { name: '20:00', millisecond: 3600 * 20 * 1000, disable: false },
    { name: '21:00', millisecond: 3600 * 21 * 1000, disable: false },
    { name: '22:00', millisecond: 3600 * 22 * 1000, disable: false },
    { name: '23:00', millisecond: 3600 * 23 * 1000, disable: false },
  ]

  return (
    <DayPickerContainer>
      <DayTimePickerWrap>
        <DayPickerInputWrap>
          <DayPickerInput
            type="text"
            placeholder={'START DATE'}
            value={from ? format(from, 'y-MM-dd') : ''}
            onChange={() => {}}
          />
          <DateIcon
            showPicker={showPicker}
            onClick={() => (showPicker ? null : setStartPickerShow(!startPickerShow))}
          />
        </DayPickerInputWrap>
        {startPickerShow ? (
          <ReactDayPicker
            mode="single"
            className="event_from"
            defaultMonth={from}
            selected={from}
            onSelect={(e) => (e ? selectStartDate(e) : new Date())}
            numberOfMonths={1}
            pagedNavigation={true}
            locale={tw}
            weekStartsOn={0}
            formatters={{ formatCaption }}
            fixedWeeks={true}
            showOutsideDays={true}
            fromMonth={new Date().addDays(-45)}
            toMonth={to ? to : new Date()}
            disabled={[{ before: new Date().addDays(-45) }, { after: new Date() }]}
          ></ReactDayPicker>
        ) : null}

        <ClockPicker
          defaultText={'sss'}
          itemList={clockList}
          defaultUnFold={false}
          readonly={false}
          menuWidth={'70px'}
          menuHeight={'30px'}
          manuMargin={'0px 5px'}
          selectedItem={fromClock}
          setSelectedItem={setFromClock}
          behavior="pickStartTime"
          getNow={getNow}
          isToday={verifyIsToday(from)}
          showEventList={showEventList}
        ></ClockPicker>
      </DayTimePickerWrap>

      <div style={intervalSty}>TO</div>
      <DayTimePickerWrap>
        <DayPickerInputWrap>
          <DayPickerInput
            type="text"
            placeholder={'END DATE'}
            value={to ? format(to, 'y-MM-dd') : ''}
            onChange={() => {}}
          />
          <DateIcon
            showPicker={showPicker}
            onClick={() => (showPicker ? null : setEndPickerShow(!endPickerShow))}
          />
        </DayPickerInputWrap>
        {endPickerShow ? (
          <ReactDayPicker
            mode="single"
            className="event_to"
            defaultMonth={to}
            selected={to}
            onSelect={(e) => (e ? selectEndDate(e) : new Date().addDays(1))}
            numberOfMonths={1}
            pagedNavigation={true}
            locale={tw}
            weekStartsOn={0}
            formatters={{ formatCaption }}
            showOutsideDays={true}
            fixedWeeks={true}
            fromMonth={new Date().addDays(-45)}
            disabled={[{ before: new Date().addDays(-45) }, { after: new Date() }]}
          ></ReactDayPicker>
        ) : null}
        <ClockPicker
          defaultText={'sss'}
          itemList={clockList}
          defaultUnFold={false}
          readonly={false}
          menuWidth={'70px'}
          menuHeight={'30px'}
          manuMargin={'0px 5px'}
          selectedItem={toClock}
          setSelectedItem={setToClock}
          behavior="pickEndTime"
          getNow={getNow}
          isToday={verifyIsToday(to)}
          showEventList={showEventList}
        ></ClockPicker>
      </DayTimePickerWrap>
    </DayPickerContainer>
  )
}
