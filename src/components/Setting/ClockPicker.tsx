import { useState, useEffect } from 'react'
import styled from 'styled-components'
import { colorValue } from '../../styles'
import { ClockType } from './EventList'

interface DropDownProps {
  defaultText: string
  itemList: ClockType[]
  defaultUnFold?: Boolean
  readonly?: Boolean
  menuWidth: string
  menuHeight: string
  manuMargin?: string
  selectedItem: ClockType
  setSelectedItem: Function
  behavior: string
  getNow?: Function
  isToday: Boolean
  showEventList: Boolean
}

const DropDownContainer = styled.div.attrs(
  (props: { menuWidth: string; manuMargin: string }) => props,
)`
  width: ${(props) => props.menuWidth};
  margin: ${(props) => (props.manuMargin ? props.manuMargin : '5px')};
  position: absolute;
  left: 130px;
  top: 0;
`
const DefaultArea = styled.div.attrs((props: { menuHeight: string }) => props)`
  width: 100%;
  height: ${(props) => props.menuHeight};
  color: ${colorValue.second};
  display: flex;
  align-items: center;
`
const ItemNameArea = styled.div.attrs((props: { menuHeight: string }) => props)`
  width: 100%;
  height: 100%;
  color: #76a6c4;
  // line-height: ${(props) => `calc(${props.menuHeight} - 2px)`};
  font-size: 18px;
  padding: 0 10px;
  overflow: hidden;
  white-space: nowrap;
  text-overflow: ellipsis;
  display: flex;
  justify-content: center;
  align-items: center;
  cursor: pointer;
  transition: 0.3s;
  &:hover {
    background-color: ${colorValue.second};
    color: #0b1216;
  }
`

const UnfoldArea = styled.div.attrs(
  (props: { unfold: Boolean; dynamicWidth: number; dynamicHeight: number }) => props,
)`
  width: 250px;
  height: 210px;
  background-color: #0b1216;
  color: #2a8ab6;
  transition: 0.3s;
  overflow: hidden;
  transform: scaleY(${(props) => (props.unfold ? 1 : 0)});
  transform-origin: top;
  margin-top: 5px;
  border: solid 1px ${colorValue.second};
  box-sizing: border-box;
  max-height: 700px;
  overflow-y: scroll;
  position: absolute;
  left: -135px;
  top: 30px;
  display: flex;
  padding: 10px 15px;
  transition: 0.3s;
  justify-content: flex-start;
  flex-wrap: wrap;
  align-content: center;
  &::-webkit-scrollbar {
    display: none;
  }
`

const EachItem = styled.div`
  font-size: 14px;
  height: 25px;
  width: 48px;
  line-height: 25px;
  font-weight: 500;
  color: ${colorValue.second};
  cursor: pointer;
  display: flex;
  justify-content: center;
  overflow: hidden;
  white-space: nowrap;
  text-overflow: ellipsis;
  background-color: #112133;
  margin: 3px 3px;
  padding: 0 3px;
  box-sizing: border-box;
  transition: 0.3s;
  &:hover {
    background-color: ${colorValue.second};
    color: #0b1216;
  }
  &.disable {
    opacity: 0.5;
    pointer-events: none;
  }
  &.current {
    background-color: ${colorValue.second};
    color: #0b1216;
  }
`

export default (props: DropDownProps) => {
  const {
    defaultText,
    itemList,
    defaultUnFold,
    readonly,
    menuWidth,
    menuHeight,
    manuMargin,
    selectedItem,
    setSelectedItem,
    behavior,
    getNow,
    isToday,
    showEventList,
  } = props

  useEffect(() => {
    if (showEventList) {
      setShowMenu(false)
    }
  }, [showEventList])

  const [showMenu, setShowMenu] = useState<Boolean>(defaultUnFold ? defaultUnFold : false)

  const [nowClock, setNowClock] = useState<ClockType>(getNow ? getNow() : undefined)
  const [modifiedClockList, setModifiedClockList] = useState<ClockType[]>(itemList)

  useEffect(() => {
    if (isToday) {
      let tempList = [...itemList]
      tempList.forEach((item) => {
        if (item.millisecond > nowClock.millisecond) {
          item.disable = true
        } else {
          item.disable = false
        }
      })
      setModifiedClockList(tempList)
    } else {
      setModifiedClockList(itemList)
    }
  }, [nowClock, isToday])

  const selectItem = (item: ClockType) => {
    setSelectedItem(item)
    unFoldMenu()
  }

  const renderItemName = () => {
    let displayName = defaultText
    displayName = selectedItem.name
    return <ItemNameArea menuHeight={menuHeight}>{displayName}</ItemNameArea>
  }

  const unFoldMenu = () => {
    let targetEle
    switch (behavior) {
      case 'pickStartTime':
        targetEle = document.querySelector('.event_from')
        break
      case 'pickEndTime':
        targetEle = document.querySelector('.event_to')
        break
      default:
        break
    }
    if (!targetEle) return
    if (showMenu) {
      targetEle.classList.remove('trans_down')
      setShowMenu(false)
    } else {
      targetEle.classList.add('trans_down')
      setShowMenu(true)
    }
  }

  const renderMunu = () => {
    return (
      <UnfoldArea unfold={showMenu}>
        {modifiedClockList.map((item, key) => {
          if (!item.disable) {
            return (
              <EachItem
                key={key}
                className={`${selectedItem.millisecond === item.millisecond ? 'current' : ''}`}
                onClick={() => selectItem(item)}
              >
                {item.name}
                <div></div>
              </EachItem>
            )
          }
        })}
        {getNow && isToday && behavior === 'pickEndTime' ? (
          <EachItem
            className={`${
              getNow ? (selectedItem.millisecond === getNow().millisecond ? 'current' : '') : null
            }`}
            onClick={() => selectItem(getNow())}
          >
            Now
          </EachItem>
        ) : null}
        {modifiedClockList.map((item, key) => {
          if (item.disable) {
            if (key < modifiedClockList.length - 1) {
              return (
                <EachItem
                  key={key}
                  className={`${item.disable ? 'disable' : ''}`}
                  onClick={() => (item.disable ? null : selectItem(item))}
                >
                  {item.name}
                  <div></div>
                </EachItem>
              )
            }
          }
        })}
      </UnfoldArea>
    )
  }

  return (
    <DropDownContainer menuWidth={menuWidth} manuMargin={manuMargin}>
      <DefaultArea menuHeight={menuHeight} onClick={() => (readonly ? null : unFoldMenu())}>
        {renderItemName()}
      </DefaultArea>

      {renderMunu()}
    </DropDownContainer>
  )
}
