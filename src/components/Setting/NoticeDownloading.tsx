import styled from 'styled-components'
import { colorValue } from '../../styles'
interface Props {
  closeModal: Function
}

const Container = styled.div`
  width:100%;
  height:100%;
  display:flex;
  flex-direction:column;
  justify-content:space-between;
  align-items;center;
  box-sizing:border-box;
  padding:30px;
`
const InnerWrap = styled.div`
  width:100%;
  height:100%;
  display:flex;
  flex-direction:column;
  justify-content:space-between;
  align-items;center;
  border: solid 1px ${colorValue.border};
`
const Header = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
`
const HeaderText = styled.div`
  font-size: 22px;
  font-weight: 600;
  color: #ff5163;
`
const IconNotice = styled.div`
  width: 22px;
  height: 22px;
  background-repeat: no-repeat;
  background-image: url('./images/icon_notice.svg');
  background-size: contain;
  background-position: center;
  cursor: pointer;
  margin: 0 10px;
`

const Content = styled.div`
  width: 100%;
  display: flex;
  height: 100%;
  justify-content: space-evenly;
  align-items: center;
  box-sizing: border-box;
  font-size: 22px;
  font-weight: bold;
`

const LoadingIcon = styled.div`
  width: 50px;
  height: 50px;
  min-height: 30px;
  background-image: url(/images/loading.gif);
  background-repeat: no-repeat;
  background-size: contain;
  margin: 0;
`

export default (props: Props) => {
  const { closeModal } = props

  return (
    <Container>
      <InnerWrap>
        <Header>
          <IconNotice></IconNotice>
          <HeaderText>Downloading...</HeaderText>
        </Header>
        <Content>
          <LoadingIcon></LoadingIcon>
        </Content>
      </InnerWrap>
    </Container>
  )
}
