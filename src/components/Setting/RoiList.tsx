import { useRef, useEffect, useState } from 'react'
import styled from 'styled-components'
import { colorValue } from '../../styles'
import { PolygonData, Wrap, WrapInner, EachRow, WrapTitle } from './DetectList'
import Polygon from './Polygon'
import PolygonCanva from './PolygonCanva'
import ModalTool from './ModalTool'
import Notice from './Notice'
import { useAppDispatch } from './../../store'
import {
  switchDeviceAiRoiAction,
  getCameraThumbnailAction,
  deleteTargetAiRoiAction,
  getTargetAiRoiAction,
} from './../../store/saga'
import { useThumbnailSelector } from './../../hooks/useSelectThumbnail'
import { useAiRoiSelector } from './../../hooks/useSelectAiRoi'

interface Props {
  showPolygonData: Boolean
  selectedDevice: aiDeviceMenuType
  selectedAI: analysisType
  currentTab: string
}

const DetectDeviceName = styled.div`
  color: ${colorValue.border};
  font-size: 24px;
  font-weight: 600;
  display: flex;
  flex-direction: column;
  align-items: flex-end;
  min-width: 360px;
  max-width: 360px;
  position: relative;
`
const InnerText = styled.div`
  position: absolute;
  right: 0;
  overflow: hidden;
  white-space: nowrap;
  text-overflow: ellipsis;
`
const DetectThumbnail = styled.div`
  width: 360px;
  max-width: 360px;
  aspect-ratio: 16 / 9;
  height: auto;
  border: 1px solid ${colorValue.border};
  background-color: rgba(0, 0, 0, 0.6);
  margin: 0 20px 10px 20px;
  background-size: cover;
  background-repeat: no-repeat;
  font-size: 24px;
  font-weight: 600;
  display: flex;
  justify-content: center;
  align-items: center;
  transition: 0.3s;
  color: ${colorValue.border};
  cursor: pointer;
`
const PolygonShowSetting = styled.div`
  color: ${colorValue.border};
  width: 360px;
  font-size: 24px;
  font-weight: 600;
  height: 200px;
  display: flex;
  justify-content: flex-start;
  align-items: flex-start;
  position: relative;
  line-height: 24px;

  &.checked {
    color: ${colorValue.main};
  }
`
const PolygonShowCheck = styled.div`
  width: 24px;
  max-width: 24px;
  height: 24px;
  margin: 0 15px 0 0;
  cursor: pointer;
  border: 1px solid ${colorValue.border};
  &.checked {
    width: 24px;
    height: 24px;
    background-image: url('/images/check.svg');
    background-repeat: no-repeat;
    background-size: 100%;
    border: 1px solid ${colorValue.main};
  }
  &.disable {
    pointer-events: none;
    cursor: initial;
  }
`
const DeleteIcon = styled.div`
  width: 24px;
  height: 24px;
  position: absolute;
  bottom: 0;
  left: 0;
  background-image: url('/images/icon_delete.svg');
  background-repeat: no-repeat;
  background-size: contain;
  cursor: pointer;
`
const CreateBtn = styled.div`
  width: 160px;
  min-height: 40px;
  color: #000;
  font-size: 24px;
  font-weight: bold;
  font-family: SegoeUI;
  border: 1px solid ${colorValue.second};
  background-color: #42c6fc;
  display: flex;
  justify-content: center;
  margin: 25px 0 0 0;
  box-sizing: border-box;
  line-height: 40px;
  transition: 0.3s;
  cursor: pointer;
  &:hover {
    background-color: ${colorValue.second};
    color: #0c1a2a;
  }
  &.disable {
    background-color: #76a6c4;
    pointer-events: none;
    opacity: 0.5;
  }
`

export default (props: Props) => {
  const { showPolygonData, selectedDevice, selectedAI, currentTab } = props
  const wrapInnerRef = useRef<HTMLDivElement>(null)
  const [deleteModalShow, setDeleteModalShow] = useState<Boolean>(false)
  const [drawingModalShow, setDrawingModalShow] = useState<Boolean>(false)
  const [targetData, setTargetData] = useState<PolygonData>()
  const [hasChanged, setHasChanged] = useState<Boolean>(false)
  const [tempPolygonData, setTempPolygonData] = useState<PolygonData[]>([])
  const [deviceThumbnail, setDeviceThumbnail] = useState<string>('')
  const { aiRoiList } = useAiRoiSelector()
  // console.log('aiMenuList', aiMenuList)
  const dispatch = useAppDispatch()
  const { imageUrl } = useThumbnailSelector()
  useEffect(() => {
    setDeviceThumbnail(imageUrl)
  }, [imageUrl])
  useEffect(() => {
    getDetectList()
  }, [selectedDevice, selectedAI])

  const getDetectList = () => {
    if (selectedDevice && selectedAI && currentTab === 'ROI') {
      getThumbnail(selectedDevice.id)
      getAiRoiPos(selectedDevice.id, selectedAI.sysName)
    }
  }

  const getAiRoiPos = (cameraID: string, ai_type: string) => {
    dispatch(getTargetAiRoiAction({ cameraID, ai_type }))
  }

  useEffect(() => {
    let tempList = aiRoiList.map((item) => {
      const { ai_type, id, uuid, notation, pos, roi_color, roi_type, sensitivity, show_roi } = item
      return {
        deviceId: id,
        analysisType: ai_type,
        roiType: roi_type,
        pos: JSON.parse(pos),
        sensitivity: sensitivity,
        notation: notation,
        showRoi: show_roi ? true : false,
        roiColor: roi_color,
        uuid: uuid ? uuid : '',
      }
    })
    setTempPolygonData(tempList)
    setHasChanged(true)
    setTimeout(() => {
      setHasChanged(false)
    }, 200)
  }, [aiRoiList])

  const getThumbnail = (cameraID: string) => {
    dispatch(getCameraThumbnailAction({ cameraID }))
  }

  const createBlankPolygonData = () => {
    if (!selectedDevice) return
    if (!selectedAI) return
    var polygonObj = {
      deviceId: '',
      analysisType: selectedAI.sysName,
      roiType: 'polygon',
      pos: [],
      sensitivity: 0,
      notation: '',
      showRoi: false,
      roiColor: '',
      uuid: selectedDevice.id,
    }
    let newObj = Object.assign({}, polygonObj)
    let tempList = [...tempPolygonData, newObj]
    setTempPolygonData(tempList)
    setTimeout(() => {
      if (wrapInnerRef.current) {
        wrapInnerRef.current.scrollTo({
          top: Number.MAX_SAFE_INTEGER,
          behavior: 'smooth',
        })
      }
    }, 0)
  }

  const deleteAiRoiPos = () => {
    if (!targetData) return

    const data = {
      id: targetData.deviceId,
      ai_type: targetData.analysisType,
      roi_type: targetData.roiType,
      pos: JSON.stringify(targetData.pos),
      sensitivity: targetData.sensitivity,
      notation: targetData.notation,
      show_roi: targetData.showRoi ? 1 : 0,
      roi_color: targetData.roiColor,
      uuid: targetData.uuid,
    }
    // console.log('delete_data', data);
    dispatch(deleteTargetAiRoiAction(data))
    getDetectList()
  }

  const displayPolygon = (polygonItem: PolygonData) => {
    if (!polygonItem) return

    let show_roi
    if (polygonItem.showRoi) {
      show_roi = 0
    } else {
      show_roi = 1
    }
    const data = {
      id: polygonItem.deviceId,
      ai_type: polygonItem.analysisType,
      roi_type: polygonItem.roiType,
      pos: JSON.stringify(polygonItem.pos),
      sensitivity: polygonItem.sensitivity,
      notation: polygonItem.notation,
      show_roi: show_roi,
      roi_color: polygonItem.roiColor,
      uuid: polygonItem.uuid,
    }
    // console.log('switch_data', data);
    dispatch(switchDeviceAiRoiAction(data))
    getDetectList()
  }

  const showDrawingModal = (polygonItem: PolygonData) => {
    setTargetData(polygonItem)
    setDrawingModalShow(true)
  }
  const closeDrawingModal = () => {
    setDrawingModalShow(false)
  }
  const closeDeleteModal = () => {
    setDeleteModalShow(false)
  }

  const renderDrawingModal = () => {
    if (!targetData) return
    if (!selectedDevice) return
    return (
      <ModalTool
        modalShow={drawingModalShow}
        modalCloseFunction={() => closeDrawingModal()}
        modalWidth={1000}
        modalHeight={'auto'}
        backgroundOpacity={0.7}
        background={'#13253A'}
        modalInnerBackground={'#000'}
        zIndex={12}
      >
        <PolygonCanva
          deviceThumbnail={deviceThumbnail}
          selectedDevice={selectedDevice}
          closeModal={() => closeDrawingModal()}
          polygonData={targetData}
          tempPolygonData={tempPolygonData}
          setTempPolygonData={setTempPolygonData}
          hasChanged={hasChanged}
          setHasChanged={setHasChanged}
          selectedAI={selectedAI}
          getDetectList={getDetectList}
        />
      </ModalTool>
    )
  }
  const showDeleteNotice = (polygonItem: PolygonData) => {
    setTargetData(polygonItem)
    setDeleteModalShow(true)
  }

  const renderDeleteModal = () => {
    return (
      <ModalTool
        modalShow={deleteModalShow}
        modalCloseFunction={() => closeDeleteModal()}
        modalWidth={420}
        modalHeight={240}
        backgroundOpacity={0.7}
        background={'#13253A'}
        modalInnerBackground={'#00000075'}
        zIndex={12}
      >
        <Notice
          closeModal={() => closeDeleteModal()}
          action={deleteAiRoiPos}
          noticeText="您是否要刪除此智能電子圍籬設定?"
        />
      </ModalTool>
    )
  }

  return (
    <Wrap className={`${showPolygonData && selectedDevice && selectedAI ? '' : 'short'}`}>
      <WrapInner ref={wrapInnerRef}>
        {tempPolygonData
          .filter((item) => {
            return item.uuid === selectedDevice.id && item.analysisType === selectedAI.sysName
          })
          .map((polygonItem, index) => {
            return (
              <EachRow key={index}>
                <WrapTitle>Invasion-Polygons</WrapTitle>
                <DetectDeviceName>
                  <InnerText>{selectedDevice.title}</InnerText>
                  <InnerText style={{ top: '40px' }}>{polygonItem.analysisType}</InnerText>
                </DetectDeviceName>
                <DetectThumbnail
                  style={{
                    backgroundImage: `url(${deviceThumbnail})`,
                  }}
                  onClick={() => showDrawingModal(polygonItem)}
                >
                  {hasChanged ? null : (
                    <Polygon
                      polygonData={polygonItem}
                      editable={false}
                      setCurrentCoordinate={() => {}}
                    ></Polygon>
                  )}
                </DetectThumbnail>
                <PolygonShowSetting className={`${polygonItem.showRoi ? 'checked' : ''}`}>
                  {/* <PolygonShowCheck
                    className={`${polygonItem.showRoi ? 'checked' : ''} ${
                      polygonItem.pos.length ? '' : 'disable'
                    }`}
                    onClick={() => displayPolygon(polygonItem)}
                  ></PolygonShowCheck>
                  顯示於影片上*/}
                  {/* {polygonItem.pos.length ? ( */}
                  {true ? (
                    <DeleteIcon onClick={() => showDeleteNotice(polygonItem)}></DeleteIcon>
                  ) : null}
                </PolygonShowSetting>
              </EachRow>
            )
          })}
        <CreateBtn
          className={showPolygonData && selectedDevice && showPolygonData ? '' : 'disable'}
          onClick={() => createBlankPolygonData()}
        >
          +新增
        </CreateBtn>
      </WrapInner>
      {deleteModalShow ? renderDeleteModal() : null}
      {drawingModalShow && targetData ? renderDrawingModal() : null}
    </Wrap>
  )
}
