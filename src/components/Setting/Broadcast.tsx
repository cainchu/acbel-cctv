import { Fragment, useEffect, useState } from 'react'
import styled from 'styled-components'
import PageWrap from './../../pages/PageWrap'
import { colorValue } from '../../styles'
import BroadcastInput from './BroadcastInput'
import { getBroadcastAction } from './../../store/saga'
import { useAppDispatch } from './../../store'
import { useBroadcastSelector } from './../../hooks/useSelectBroadcast'
export interface BroadcastItem {
  itemName: string
  apiUrl: string
  broadcastId: string
  type: string
}
interface BroadcastProps {}

const PanelColumn = styled.div`
  width: 100%;
  height: 100%;
  border-right: 1px solid ${colorValue.border};
  display: flex;
  flex-direction: column;
  box-sizing: border-box;
  padding: 0 60px 0 0;
  &:last-child {
    border-right: none;
    padding: 0 20px 0 0;
  }
`

const PanelRow = styled.div`
  width: 100%;
  display: flex;
  flex-grow: 1;
  box-sizing: border-box;
  padding: 0 0 0 20px;
`
const PanelRowInner = styled.div`
  width: 100%;
  border-bottom: 1px dashed ${colorValue.dashedBorder};
  display: flex;
  justify-content: space-between;
  box-sizing: border-box;
  padding: 8px 0 8px 0;
`

const SubTitleDecoCube = styled.div`
  width: 10px;
  height: 10px;
  background-color: #163e52;
  margin: 20px 15px 0 0;
`

const SubTitle = styled.div`
  color: ${colorValue.second};
  font-size: 22px;
  font-weight: bold;
  letter-spacing: 2.2px;
  white-space: nowrap;
  display: flex;
  justify-content: space-between;
  align-items: center;
`

export default (props: BroadcastProps) => {
  const {} = props
  const [tempBroadcastList, setTempBroadcastList] = useState<BroadcastItem[]>()
  const { broadcast } = useBroadcastSelector()
  const dispatch = useAppDispatch()
  useEffect(() => {
    dispatch(getBroadcastAction())
  }, [])

  useEffect(() => {
    let broadcastList = [
      {
        itemName: '吊掛平台開啓事件推播群組',
        apiUrl: broadcast.teams_hook_hanging_platform,
        broadcastId: 'teams_hook_hanging_platform',
        type: 'ms',
      },
      {
        itemName: '貨車靠站告警與事件推播群組',
        apiUrl: broadcast.teams_hook_truck_40m,
        broadcastId: 'teams_hook_truck_40m',
        type: 'ms',
      },
      {
        itemName: '重要區域人員入侵事件推播群組',
        apiUrl: broadcast.teams_hook_person_invasion,
        broadcastId: 'teams_hook_person_invasion',
        type: 'ms',
      },
      {
        itemName: '含氧量不足觸發事件推播群組',
        apiUrl: broadcast.teams_hook_lack_oxygen,
        broadcastId: 'teams_hook_lack_oxygen',
        type: 'ms',
      },
      {
        itemName: '消防火警觸發事件推播群組',
        apiUrl: broadcast.teams_hook_fire_alarm,
        broadcastId: 'teams_hook_fire_alarm',
        type: 'ms',
      },
      {
        itemName: '溫度過高事件推播群組',
        apiUrl: broadcast.teams_hook_gas_fire,
        broadcastId: 'teams_hook_gas_fire',
        type: 'ms',
      },
      {
        itemName: '區域淨空異常事件通報推播群組',
        apiUrl: broadcast.teams_hook_clearance_error,
        broadcastId: 'teams_hook_clearance_error',
        type: 'ms',
      },
      {
        itemName: '吊掛平台開啓事件推播',
        apiUrl: broadcast.mail_address_hanging_platform,
        broadcastId: 'mail_address_hanging_platform',
        type: 'ma',
      },
      {
        itemName: '貨車靠站告警與事件推播',
        apiUrl: broadcast.mail_address_truck_40m,
        broadcastId: 'mail_address_truck_40m',
        type: 'ma',
      },
      {
        itemName: '重要區域人員入侵事件推播',
        apiUrl: broadcast.mail_address_person_invasion,
        broadcastId: 'mail_address_person_invasion',
        type: 'ma',
      },
      {
        itemName: '含氧量不足觸發事件推播',
        apiUrl: broadcast.mail_address_lack_oxygen,
        broadcastId: 'mail_address_lack_oxygen',
        type: 'ma',
      },
      {
        itemName: '消防火警觸發事件推播',
        apiUrl: broadcast.mail_address_fire_alarm,
        broadcastId: 'mail_address_fire_alarm',
        type: 'ma',
      },
      {
        itemName: '溫度過高事件觸發事件推播',
        apiUrl: broadcast.mail_address_gas_fire,
        broadcastId: 'mail_address_gas_fire',
        type: 'ma',
      },
      {
        itemName: '區域淨空異常事件通報推播',
        apiUrl: broadcast.mail_address_clearance_error,
        broadcastId: 'mail_address_clearance_error',
        type: 'ma',
      },
    ]

    setTempBroadcastList(broadcastList)
  }, [broadcast])
  const renderColumn = (tempBroadcastList: BroadcastItem[], type: string) => {
    return tempBroadcastList.map((broadcastItem, key) => {
      if (broadcastItem.type === type) {
        return (
          <PanelRow key={key}>
            <SubTitleDecoCube></SubTitleDecoCube>
            <PanelRowInner style={{}}>
              <SubTitle>{broadcastItem.itemName}</SubTitle>
              <BroadcastInput
                broadcastItem={broadcastItem}
                tempBroadcastList={tempBroadcastList}
                setTempBroadcastList={setTempBroadcastList}
                broadcastTargetType={broadcastItem.type}
              />
            </PanelRowInner>
          </PanelRow>
        )
      }
    })
  }
  return (
    <Fragment>
      <PageWrap title="MS TEAMS BROADCAST SETTING">
        <PanelColumn>
          {tempBroadcastList ? renderColumn(tempBroadcastList, 'ms') : null}
        </PanelColumn>
      </PageWrap>
      <PageWrap title="E-MAIL BROADCAST SETTING" disableBg={true}>
        <PanelColumn>
          {tempBroadcastList ? renderColumn(tempBroadcastList, 'ma') : null}
        </PanelColumn>
      </PageWrap>
    </Fragment>
  )
}
