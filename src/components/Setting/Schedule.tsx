import { DragEvent, Fragment, useEffect, useRef, useState } from 'react'
import * as R from 'ramda'
import styled from 'styled-components'
import DropdownSchedule from './DropdownSchedule'
import { colorValue } from '../../styles'
import { useRootSelector } from './../../hooks/useSelect'
import { getAiScheduleAction, updateAiScheduleAction } from './../../store/saga'
import { useAppDispatch } from './../../store'
import { useAiScheduleSelector } from './../../hooks/useSelectAiSchedule'
import { setSelectAiSchedule, setUpdateAiSchedule } from './../../store/aiSchedule'
import ModalTool from './ModalTool'
import NoticeCheck from './NoticeCheck'
import NoticeCheckChanged from './NoticeCheckChanged'
import LoadingCustom from './../LoadingCustom'
import { createInitialSchedule } from './../../utils/tools'
import { useIoListSelector } from './../../hooks/useSelectIoList'
import { useAiRoiSelector } from './../../hooks/useSelectAiRoi'

interface ScheduleProp {}

type DragPoint = {
  x: number
  y: number
}

interface ClickEventTarget extends EventTarget {
  id: string
}
export interface ScheduleSubMenu {
  menuHeader: string
  menuContent: aiCamera[] | IoItemWithTitle[]
  menuType?: string
}
export interface IoItemWithTitle extends ioItemResult {
  title: string
}

const ScheduleContainer = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
`
const ScheduleHeader = styled.div`
  display: flex;
  justify-content: flex-start;
  flex-direction: column;
`
const HoursCubeWrap = styled.div`
  display: flex;
  justify-content: center;
`
const HoursCube = styled.div`
  min-width: 50px;
  height: 30px;
  color: ${colorValue.second};
  margin: 1.5px 1.5px 1.5px 1.5px;
  display: flex;
  justify-content: center;
  align-items: center;
  font-size: 20px;
  font-weight: 500;
`
const ScheduleBody = styled.div`
  display: flex;
  justify-content: center;
`
const DaysCubeWrap = styled.div`
  display: flex;
  justify-content: center;
  flex-direction: column;
  margin: 0 6px 0 0;
`
const DaysCube = styled.div`
  min-width: 50px;
  height: 60px;
  color: ${colorValue.second};
  margin: 1.5px 1.5px 1.5px 1.5px;
  display: flex;
  justify-content: center;
  align-items: center;
  font-size: 18px;
  font-weight: bold;
  &:nth-child(6) {
    color: #ff5656;
  }
  &:nth-child(7) {
    color: #ff5656;
  }
`
const Wrap = styled.div`
  width: 100%;
  display: flex;
  justify-content: space-between;
  flex-direction: column;
  // border: 1px solid #2a8ab6;
  position: relative;
  padding: 3px;
  box-sizing: border-box;
  transition: 0.3s;
`
const Cube = styled.div`
  min-width: 50px;
  height: 60px;
  background-color: ${colorValue.second};
  margin: 1.5px 1.5px 3px 1.5px;
  transition: 0.1s;
  border-radius: 5px;
  &.selected {
    background-color: rgb(0, 255, 136);
  }
`
const ModeSwitchPanel = styled.div`
  width: 205px;
  height: 105px;
  background-color: rgba(22, 62, 82, 0.3);
  border: 1px solid rgba(52, 86, 118, 0.6);
  display: flex;
  flex-direction: column;
  align-items: center;
  z-index: 1;
`
const ModeSwitchButton = styled.div`
  width: 50px;
  height: 63px;
  border-radius: 5px;
  margin: 3px 8px 0 8px;
  cursor: pointer;
  &.enable {
    border: solid 1px #000;
  }
  &.disable {
    border: solid 1px #42c6fc;
  }
`

const SettingBtn = styled.div`
  width: 160px;
  height: 40px;
  color: #000;
  font-size: 24px;
  font-weight: bold;
  font-family: SegoeUI;
  border: 1px solid ${colorValue.second};
  background-color: #42c6fc;
  display: flex;
  justify-content: center;
  margin: 0 25px;
  line-height: 40px;
  transition: 0.3s;
  cursor: pointer;
  &:hover {
    background-color: ${colorValue.second};
    color: #0c1a2a;
  }
  &.disable {
    background-color: #76a6c4;
    pointer-events: none;
    opacity: 0.5;
  }
`

const SearchBtn = styled.div`
  width: 140px;
  height: 30px;
  color: #000;
  font-size: 22px;
  font-weight: bold;
  font-family: SegoeUI;
  border: 1px solid #2a8ab6;
  background-color: #42c6fc;
  display: flex;
  justify-content: center;
  margin: 0 25px;
  line-height: 30px;
  transition: 0.3s;
  cursor: pointer;
  z-index: 1;
  &:hover {
    background-color: #2a8ab6;
    color: #0c1a2a;
  }
  &.disable {
    background-color: #76a6c4;
    pointer-events: none;
    opacity: 0.5;
  }
`

const NavyLine = styled.div`
  height: 5px;
  width: 100%;
  background-image: url(/images/slash_10px.svg);
  margin: 20px 0 20px 0;
`

const HoursColumn = styled.div`
  display: flex;
`

const TypeSelectRow = styled.div.attrs((props: { showSchedulePicker: Boolean }) => props)`
  width: 100%;
  height: ${(props) => (props.showSchedulePicker ? '30px' : 'unset')};
  box-sizing: border-box;
  display: flex;
`

const panelTitle = {
  fontSize: '18px',
  color: '#2a8ab6',
  display: 'flex',
  justifyContent: 'center',
}
const buttonText = {
  fontSize: '20px',
  color: colorValue.second,
  height: '60px',
  width: '30px',
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
}

const pannelInner = {
  display: 'flex',
  justifyContent: 'center',
  height: '100%',
  minWidth: '100%',
}

const bottomArea = {
  width: '100%',
  display: 'flex',
  justifyContent: 'space-between',
  marginTop: '20px',
  padding: '0 5px 0 10px',
}

const tipText = {
  fontSize: '18px',
  color: colorValue.second,
  height: '105px',
}

const settingBtnArea = {
  display: 'flex',
  justifiContent: 'center',
}

const intervalSty = {
  width: '1px',
  height: '22px',
  margin: '6px 20px 6px 20px',
  backgroundColor: colorValue.border,
}

const tipTextSty = {
  fontSize: '20px',
  color: '#42c6fc',
  margin: '0 20px 0 0',
  fontWeight: '500',
}

export default ({}: ScheduleProp) => {
  const wrapRef = useRef<HTMLDivElement>(null)
  const [lastPoint, setLastPoint] = useState<DragPoint>()
  const [selectMode, setSelectMode] = useState<Boolean>(true)
  const [list, setList] = useState<string[]>([])
  const [showSchedulePicker, setShowSchedulePicker] = useState<Boolean>(false)
  const [selectedDevice, setSelectedDevice] = useState<aiCamera | IoItemWithTitle>()
  const [deviceType, setDeviceType] = useState<string>('')
  const [selectedAI, setSelectedAI] = useState<analysisType>()
  const [tempAiSchedule, setTempAiSchedule] = useState<scheduleTable | undefined>()
  const [updateModalShow, setUpdateModalShow] = useState<Boolean>(false)
  const [changedModalShow, setChangedModalShow] = useState<Boolean>(false)
  const [noticeModalShow, setNoticeModalShow] = useState<Boolean>(false)
  const [loadingShow, setLoadingShow] = useState<Boolean>(false)
  const [requestNotice, setRequestNotice] = useState<string>('')
  const { cameras } = useRootSelector()
  // console.log('cameras', cameras)
  const { aiScheduleSelect, aiScheduleUpdate } = useAiScheduleSelector()
  const { aiMenuList } = useAiRoiSelector()
  const [aiTypeTree, setAiTypeTree] = useState<analysisType[]>([])

  // const [aiMenuListWithoutDock, setAiMenuListWithoutDock] = useState<aiCamera[]>([])

  // useEffect(() => {
  //   const tempListWithoutDock = aiMenuList.filter((item) => {
  //     return(
  //       item.id !== 'ccca63a9-11c2-81f2-7aad-2fe90cb69427' &&
  //       item.id !== 'e5206e67-09ea-f717-9752-aedd76fb18b6'
  //     )
  //   })
  //   setAiMenuListWithoutDock(tempListWithoutDock);
  // }, [aiMenuList])

  useEffect(() => {
    dispatch(setSelectAiSchedule({ data: undefined, status: 0 }))
    dispatch(setUpdateAiSchedule({ data: '', status: 0 }))
    resetScheduleItem()
  }, [])

  useEffect(() => {
    if (aiScheduleSelect.status !== 0) {
      if (aiScheduleSelect.status === 200) {
        setShowSchedulePicker(true)
        setTimeout(() => {
          setLoadingShow(false)
        }, 1000)
      } else {
        setNoticeModalShow(true)
        setRequestNotice('取得排程異常')
      }
    } else {
      setShowSchedulePicker(false)
    }
  }, [aiScheduleSelect])

  useEffect(() => {
    // console.log('aiScheduleUpdate', aiScheduleUpdate)
    if (aiScheduleUpdate.status !== 0) {
      if (aiScheduleUpdate.status === 200) {
        dispatch(setSelectAiSchedule({ data: undefined, status: 0 }))
        setUpdateModalShow(true)
        setList([])
      } else {
        setNoticeModalShow(true)
        setRequestNotice('更新排程異常')
      }
    }
  }, [aiScheduleUpdate])

  const [ioDeviceList, setIoDeviceList] = useState<IoItemWithTitle[]>([])

  const { ioList } = useIoListSelector()

  useEffect(() => {
    let temp = ioList.map((item, index) => {
      return { ...item, title: item.keyword }
    })
    setIoDeviceList(temp)
  }, [ioList])

  const dispatch = useAppDispatch()

  let menuList = [
    {
      menuHeader: 'CCTV LIST',
      menuContent: aiMenuList,
      // menuContent: aiMenuListWithoutDock,
      menuType: 'CCTV',
    },
  ]

  useEffect(() => {
    if (selectedDevice && isCameraDevice(selectedDevice)) {
      if (selectedDevice && selectedDevice.ai) {
        // console.log('selectedDevice', selectedDevice)
        let tempTypeList = [
          { title: '智能電子圍籬', sysName: 'ai_fences', enable: true },
          { title: '智能大型遺留物', sysName: 'ai_retentate', enable: true },
        ]
        switch (selectedDevice.ai) {
          case '10':
            tempTypeList[0].enable = true
            tempTypeList[1].enable = false
            break
          case '01':
            tempTypeList[0].enable = false
            tempTypeList[1].enable = true
            break
          case '11':
            tempTypeList[0].enable = true
            tempTypeList[1].enable = true
            break

          default:
            break
        }
        setAiTypeTree(tempTypeList)
        setSelectedAI(undefined)
      }
    }
  }, [selectedDevice])

  useEffect(() => {
    if (!aiScheduleSelect || !aiScheduleSelect.data) return
    const modifiedSchedule = Object.entries(aiScheduleSelect.data).map((item) => {
      let name = item[0]
      let value = item[1].split(',').map((strItem) => {
        return Number(strItem)
      })
      return { [name]: value }
    })
    let tempListFromDB: string[] = []
    // console.log('modifiedSchedule', modifiedSchedule)
    modifiedSchedule.forEach((item) => {
      let name = Object.keys(item)[0]
      let valueArray = Object.values(item)[0]
      // console.log('valueArray', valueArray)
      valueArray.forEach((innerItem, innerIndex) => {
        if (innerItem === 1) {
          tempListFromDB.push(`${name}-${innerIndex}`)
        }
      })
    })
    // console.log('tempListFromDB', tempListFromDB)
    setList(tempListFromDB)
  }, [aiScheduleSelect])

  const createBlankArray = () => {
    return new Array(24).fill(0)
  }

  const scheduleTableFormat = (list: string[]) => {
    let scheduleStringTable: scheduleTable = createInitialSchedule()

    const modified = list.map((item, index) => {
      return { days: item.split('-')[0], hours: Number(item.split('-')[1]) }
    })

    const byDays = R.groupBy(function (modifiedItem: { days: string; hours: number }) {
      const days = modifiedItem.days
      return days === 'mon'
        ? 'mon'
        : days === 'tue'
        ? 'tue'
        : days === 'wed'
        ? 'wed'
        : days === 'thu'
        ? 'thu'
        : days === 'fri'
        ? 'fri'
        : days === 'sat'
        ? 'sat'
        : days === 'sun'
        ? 'sun'
        : ''
    })
    let groupResult = byDays(modified)
    const groupResultArray = Object.entries(groupResult)
    const finalData = groupResultArray.map((daysItem) => {
      let day = daysItem[0]
      let blankArray = createBlankArray()
      daysItem[1].forEach((hoursItem) => {
        blankArray[hoursItem.hours] = 1
      })
      return { [day]: blankArray }
    })
    finalData.forEach((item) => {
      let name = Object.keys(item)[0]
      let value = Object.values(item)[0]
      scheduleStringTable[name] = value.toString()
    })
    setTempAiSchedule(scheduleStringTable)
  }

  useEffect(() => {
    // if (list.length) {
    scheduleTableFormat(list)
    // }
  }, [list])

  function isCameraDevice(item: aiCamera | IoItemWithTitle): item is aiCamera {
    return item && (item as aiCamera).id !== undefined
  }
  function isIoDevice(item: aiCamera | IoItemWithTitle): item is IoItemWithTitle {
    return item && (item as IoItemWithTitle).title !== undefined
  }

  const handleUpdateSchedule = () => {
    if (!selectedDevice) return
    dispatch(setUpdateAiSchedule({ data: '', status: 0 }))
    setRequestNotice('')
    let analysisTypeName = ''
    if (deviceType === 'CCTV' && isCameraDevice(selectedDevice)) {
      if (!selectedAI) return
      analysisTypeName = selectedAI.sysName

      try {
        dispatch(
          updateAiScheduleAction({
            id: selectedDevice.id,
            ai_type: analysisTypeName,
            schedule_table: tempAiSchedule,
          }),
        )
      } catch (error) {
        console.log('error', error)
      }
    } else if (deviceType === 'I/O' && isIoDevice(selectedDevice)) {
      // if (!selectedIO) return
      analysisTypeName = selectedDevice?.io_type
      try {
        dispatch(
          updateAiScheduleAction({
            id: selectedDevice.keyword,
            ai_type: analysisTypeName,
            schedule_table: tempAiSchedule,
          }),
        )
      } catch (error) {
        console.log('error', error)
      }
    }
  }

  const selectScheduleItem = () => {
    if (!selectedDevice) return
    dispatch(setSelectAiSchedule({ data: undefined, status: 0 }))
    setRequestNotice('')
    setLoadingShow(true)
    let analysisTypeName = ''

    if (deviceType === 'CCTV' && isCameraDevice(selectedDevice)) {
      if (!selectedAI) return
      analysisTypeName = selectedAI.sysName

      if (!selectedDevice.id || !analysisTypeName) {
        console.log('查無裝置')
        resetScheduleItem()
        return
      }
      dispatch(
        getAiScheduleAction({
          id: selectedDevice.id,
          ai_type: analysisTypeName,
        }),
      )
    } else if (deviceType === 'I/O' && isIoDevice(selectedDevice)) {
      // if (!selectedIO) return
      analysisTypeName = selectedDevice.io_type

      if (!selectedDevice.keyword || !analysisTypeName) {
        console.log('查無裝置')
        resetScheduleItem()
        return
      }
      dispatch(
        getAiScheduleAction({
          id: selectedDevice.keyword,
          ai_type: analysisTypeName,
        }),
      )
    }
  }
  const handleResetSelectedDevice = () => {
    if (!R.equals(tempAiSchedule, aiScheduleSelect.data)) {
      setChangedModalShow(true)
    } else {
      resetScheduleItem()
    }
  }
  const resetScheduleItem = () => {
    setShowSchedulePicker(false)
    setSelectedDevice(undefined)
    setSelectedAI(undefined)
    setList([])
    setSelectMode(true)
    dispatch(setSelectAiSchedule({ data: undefined, status: 0 }))
    dispatch(setUpdateAiSchedule({ data: '', status: 0 }))
    setTempAiSchedule(undefined)
  }

  const pickAll = () => {
    let allCubeId: string[] = []
    allList.forEach((hourItem, hourKey: number) => {
      hourItem.forEach((daysItem, daysKey: number) => {
        allCubeId.push(`cube_${hourKey + 1}-${daysKey + 1}`)
      })
    })
    if (selectMode) {
      setList(allCubeId)
    } else {
      setList([])
    }
  }

  const cancelDefault = (e: DragEvent) => {
    e.preventDefault()
    e.stopPropagation()
    return false
  }

  const dragover = (e: DragEvent) => {
    if (!e.dataTransfer) return
    e.dataTransfer.dropEffect = 'move'
    cancelDefault(e)
  }

  const daysName = ['mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun']
  const allList = new Array(7).fill(0).map((item, key) => {
    return new Array(24).fill(daysName[key]).map((item, index) => {
      return item + '-' + index
    })
  })
  const hoursList = [
    '00',
    '01',
    '02',
    '03',
    '04',
    '05',
    '06',
    '07',
    '09',
    '09',
    '10',
    '11',
    '12',
    '13',
    '14',
    '15',
    '16',
    '17',
    '18',
    '19',
    '20',
    '21',
    '22',
    '23',
  ]
  const daysList = ['週一', '週二', '週三', '週四', '週五', '週六', '週日']

  //點擊選取
  const pickCube = (eventTarget: ClickEventTarget) => {
    if (selectMode) {
      tempList.push(eventTarget.id)
      setList(tempList)
    } else {
      tempList = R.without([eventTarget.id], list)
      setList(tempList)
    }
  }
  let tempList = [...list]

  //框選選取
  const moveStart = (e: DragEvent, accordingX: number, accordingY: number, eventType: string) => {
    e.stopPropagation()
    if (eventType === 'dragstart') {
      e.dataTransfer.effectAllowed = 'move'
      var img = new Image()
      img.src = '/images/transparent.png'
      e.dataTransfer.setDragImage(img, 0, 0)
    }
    if (!wrapRef.current) return
    const parentRect = wrapRef.current.getBoundingClientRect()
    let rectStartX = accordingX - parentRect.left
    let rectStartY = accordingY - parentRect.top

    let selectAreaEle = document.createElement('div')
    selectAreaEle.id = 'selectArea'
    selectAreaEle.style.position = 'absolute'
    selectAreaEle.style.backgroundColor = 'rgba(0, 0, 0, 0.5)'
    selectAreaEle.style.left = rectStartX + 'px'
    selectAreaEle.style.top = rectStartY + 'px'

    wrapRef.current.appendChild(selectAreaEle)
    selectAreaEle.dataset.startX = rectStartX.toString()
    selectAreaEle.dataset.startY = rectStartY.toString()
  }

  const move = (e: DragEvent, accordingX: number, accordingY: number) => {
    if (!accordingX && !accordingY) return
    if (!wrapRef.current) return
    const parentRect = wrapRef.current.getBoundingClientRect()

    let targetEle = document.getElementById('selectArea')

    let dragPositionX = accordingX - parentRect.left
    if (dragPositionX < 0) {
      dragPositionX = 0
    } else {
      if (dragPositionX > parentRect.width) {
        dragPositionX = parentRect.width
      }
    }

    let dragPositionY = accordingY - parentRect.top
    if (dragPositionY < 0) {
      dragPositionY = 0
    } else if (dragPositionY > parentRect.height) {
      dragPositionY = parentRect.height
    }
    if (targetEle) {
      const startX = targetEle.dataset.startX
      const startY = targetEle.dataset.startY
      let moveX = dragPositionX - Number(startX)
      let moveY = dragPositionY - Number(startY)

      if (moveX < 0) {
        targetEle.style.left = 'unset'
        targetEle.style.right = parentRect.width - Number(startX) + 'px'
      } else {
        targetEle.style.left = Number(startX) + 'px'
        targetEle.style.right = 'unset'
        if (moveX >= parentRect.width) {
          moveX = parentRect.right - Number(startX)
        }
      }

      if (moveY < 0) {
        targetEle.style.top = 'unset'
        targetEle.style.bottom = parentRect.height - Number(startY) + 'px'
      } else {
        targetEle.style.top = Number(startY) + 'px'
        targetEle.style.bottom = 'unset'
        if (moveY >= parentRect.height) {
          moveY = parentRect.bottom - Number(startY)
        }
      }

      targetEle.style.width = Math.abs(moveX) + 'px'
      targetEle.style.height = Math.abs(moveY) + 'px'
    }

    setLastPoint({ x: dragPositionX, y: dragPositionY })
  }

  const moveEnd = (e: DragEvent) => {
    cancelDefault(e)
    let targetEle = document.getElementById('selectArea')
    if (targetEle) {
      if (wrapRef.current && lastPoint) {
        const parentRect = wrapRef.current.getBoundingClientRect()
        const startX = targetEle.dataset.startX
        const startY = targetEle.dataset.startY
        let moveX = lastPoint.x - Number(startX)
        let moveY = lastPoint.y - Number(startY)
        let direction = ''
        if (moveX > 0 && moveY > 0) {
          direction = 'x+y+'
        } else if (moveX > 0 && moveY < 0) {
          direction = 'x+y-'
        } else if (moveX < 0 && moveY > 0) {
          direction = 'x-y+'
        } else if (moveX < 0 && moveY < 0) {
          direction = 'x-y-'
        }
        detectBeSelect(
          { x: Number(startX), y: Number(startY) },
          { x: lastPoint.x, y: lastPoint.y },
          parentRect,
          direction,
        )
        if (targetEle) {
          targetEle.remove()
          targetEle.dataset.startX = ''
          targetEle.dataset.startY = ''
        }
      }
    }
  }

  //判斷被選取的時刻方塊
  const detectBeSelect = (
    startPoint: DragPoint,
    lastPoint: DragPoint,
    parentRect: DOMRect,
    direction: string,
  ) => {
    let cubeList = [...document.querySelectorAll('.cube')]
    cubeList.forEach((cubeItem, cubeIndex) => {
      let cubeRect = cubeItem.getBoundingClientRect()

      let cubeLeft = cubeRect.left - parentRect.left
      let cubeRight = cubeRect.right - parentRect.left
      let cubeTop = cubeRect.top - parentRect.top
      let cubeBottom = cubeRect.bottom - parentRect.top

      switch (direction) {
        case 'x+y+':
          if (
            startPoint.x <= cubeRight &&
            cubeLeft <= lastPoint.x &&
            startPoint.y <= cubeBottom &&
            cubeTop <= lastPoint.y
          ) {
            if (selectMode) {
              tempList.push(cubeItem.id)
              setList(R.uniq(tempList))
            } else {
              let targetIndex = tempList.indexOf(cubeItem.id)
              if (targetIndex >= 0) {
                tempList.splice(targetIndex, 1)
                setList(R.uniq(tempList))
              } else {
                // console.log('cubeItem.id', cubeItem.id)
              }
            }
          }
          break
        case 'x+y-':
          if (
            startPoint.x <= cubeRight &&
            cubeLeft <= lastPoint.x &&
            startPoint.y >= cubeTop &&
            cubeBottom >= lastPoint.y
          ) {
            if (selectMode) {
              tempList.push(cubeItem.id)
            } else {
              let targetIndex = tempList.indexOf(cubeItem.id)
              if (targetIndex >= 0) {
                tempList.splice(targetIndex, 1)
              }
            }
            setList(R.uniq(tempList))
          }
          break
        case 'x-y+':
          if (
            startPoint.x >= cubeLeft &&
            cubeRight >= lastPoint.x &&
            startPoint.y <= cubeBottom &&
            cubeTop <= lastPoint.y
          ) {
            if (selectMode) {
              tempList.push(cubeItem.id)
            } else {
              let targetIndex = tempList.indexOf(cubeItem.id)
              if (targetIndex >= 0) {
                tempList.splice(targetIndex, 1)
              }
            }
            setList(R.uniq(tempList))
          }
          break
        case 'x-y-':
          if (
            startPoint.x >= cubeLeft &&
            cubeRight >= lastPoint.x &&
            startPoint.y >= cubeTop &&
            cubeBottom >= lastPoint.y
          ) {
            if (selectMode) {
              tempList.push(cubeItem.id)
            } else {
              let targetIndex = tempList.indexOf(cubeItem.id)
              if (targetIndex >= 0) {
                tempList.splice(targetIndex, 1)
              }
            }
            setList(R.uniq(tempList))
          }
          break

        default:
          break
      }
    })
  }

  //結構渲染
  const renderHoursName = () => {
    return (
      <HoursCubeWrap>
        <HoursCube
          style={{
            margin: '3px 7px 3px 3px',
            justifyContent: 'start',
            cursor: 'pointer',
          }}
          onClick={pickAll}
        >
          全部
        </HoursCube>
        {hoursList.map((hoursItem, key) => {
          return <HoursCube key={key}>{key}</HoursCube>
        })}
      </HoursCubeWrap>
    )
  }
  const renderDaysName = () => {
    return (
      <DaysCubeWrap>
        {daysList.map((item, key) => {
          return <DaysCube key={key}>{item}</DaysCube>
        })}
      </DaysCubeWrap>
    )
  }

  const renderScheduleCube = () => {
    return allList.map((dayItem, key) => {
      return (
        <HoursColumn key={key} className="hour">
          {dayItem.map((cubeItem, cubeKey) => {
            return (
              <Cube
                className={`cube ${R.includes(cubeItem, list) ? 'selected' : ''}`}
                id={cubeItem}
                key={cubeKey}
                onClick={(e) =>
                  pickCube({
                    ...e.target,
                    id: cubeItem,
                  })
                }
              ></Cube>
            )
          })}
        </HoursColumn>
      )
    })
  }
  const renderBottomArea = () => {
    return (
      <Fragment>
        <div style={bottomArea}>
          <div style={tipText}>備註:排程時間為伺服器時間</div>
          <ModeSwitchPanel>
            <div style={panelTitle}>排程設定</div>
            <div style={pannelInner}>
              <div style={buttonText}>開</div>
              <ModeSwitchButton
                className={`${selectMode ? 'enable' : ''}`}
                style={{ backgroundColor: 'rgb(0,255,136)' }}
                onClick={() => setSelectMode(true)}
              ></ModeSwitchButton>
              <ModeSwitchButton
                className={`${!selectMode ? 'disable' : ''}`}
                style={{ backgroundColor: '#2a8ab6' }}
                onClick={() => setSelectMode(false)}
              ></ModeSwitchButton>
              <div style={buttonText}>關</div>
            </div>
          </ModeSwitchPanel>
        </div>
        <NavyLine />
        <div style={settingBtnArea}>
          <SettingBtn
            className={`${list.length ? '' : 'disable'}
            ${!R.equals(tempAiSchedule, aiScheduleSelect.data) ? '' : 'disable'}`}
            onClick={handleUpdateSchedule}
          >
            確定{R.equals(tempAiSchedule, aiScheduleSelect.data) ? '' : '*'}
          </SettingBtn>
          <SettingBtn
            className={`${list.length ? '' : 'disable'}
            ${!R.equals(tempAiSchedule, aiScheduleSelect.data) ? '' : 'disable'}`}
            onClick={handleResetSelectedDevice}
          >
            取消
          </SettingBtn>
        </div>
      </Fragment>
    )
  }

  const closeUpdateModalShow = () => {
    setUpdateModalShow(false)
  }

  const actionAfterUpdate = () => {
    closeUpdateModalShow()
    setTempAiSchedule(undefined)
    selectScheduleItem()
    dispatch(setUpdateAiSchedule({ data: '', status: 0 }))
  }
  const actionAfterRequestError = () => {
    setNoticeModalShow(false)
    setTempAiSchedule(undefined)
    resetScheduleItem()
  }

  const renderNoticeModal = () => {
    return (
      <ModalTool
        modalShow={noticeModalShow}
        modalCloseFunction={() => setNoticeModalShow(false)}
        modalWidth={420}
        modalHeight={240}
        backgroundOpacity={0.7}
        background={'#13253A'}
        modalInnerBackground={'#00000075'}
        zIndex={12}
      >
        <NoticeCheck
          closeModal={() => setNoticeModalShow(false)}
          action={actionAfterRequestError}
          noticeText={requestNotice}
        />
      </ModalTool>
    )
  }
  const renderUpdateModal = () => {
    return (
      <ModalTool
        modalShow={updateModalShow}
        modalCloseFunction={() => closeUpdateModalShow()}
        modalWidth={420}
        modalHeight={240}
        backgroundOpacity={0.7}
        background={'#13253A'}
        modalInnerBackground={'#00000075'}
        zIndex={12}
      >
        <NoticeCheck
          closeModal={() => closeUpdateModalShow()}
          action={actionAfterUpdate}
          noticeText="排程設定更新成功"
        />
      </ModalTool>
    )
  }
  const renderChangedModal = () => {
    return (
      <ModalTool
        modalShow={changedModalShow}
        modalCloseFunction={() => setChangedModalShow(false)}
        modalWidth={420}
        modalHeight={240}
        backgroundOpacity={0.7}
        background={'#13253A'}
        modalInnerBackground={'#00000075'}
        zIndex={12}
      >
        <NoticeCheckChanged
          closeModal={() => setChangedModalShow(false)}
          positiveAction={handleUpdateSchedule}
          negativeAction={resetScheduleItem}
          noticeText="變更尚未儲存"
        />
      </ModalTool>
    )
  }

  const renderSearchBtn = () => {
    switch (deviceType) {
      case 'CCTV':
        if (selectedDevice && selectedAI) {
          return <SearchBtn onClick={() => selectScheduleItem()}>查詢</SearchBtn>
        } else {
          return (
            <SearchBtn className="disable" onClick={() => selectScheduleItem()}>
              查詢
            </SearchBtn>
          )
        }
        break
      case 'I/O':
        if (selectedDevice) {
          return <SearchBtn onClick={() => selectScheduleItem()}>查詢</SearchBtn>
        } else {
          return (
            <SearchBtn className="disable" onClick={() => selectScheduleItem()}>
              查詢
            </SearchBtn>
          )
        }
        break

      default:
        return (
          <SearchBtn
            style={{
              backgroundColor: '#76a6c4',
              pointerEvents: 'none',
              opacity: '0.5',
            }}
          >
            查詢
          </SearchBtn>
        )
        break
    }
  }

  return (
    <ScheduleContainer>
      <TypeSelectRow showSchedulePicker={showSchedulePicker}>
        <div style={tipTextSty}>您現在在設定排程的項目 - </div>
        <DropdownSchedule
          defaultText="SELECT CAMERA ID"
          itemList={menuList}
          defaultUnFold={true}
          showSchedulePicker={showSchedulePicker}
          readonly={showSchedulePicker ? true : false}
          menuWidth="300px"
          menuHeight="30px"
          manuMargin="0"
          selectedItem={selectedDevice}
          setSelectedItem={setSelectedDevice}
          selectedType={deviceType}
          setSelectedType={setDeviceType}
          dataType={1}
        ></DropdownSchedule>
        <div style={intervalSty}></div>
        <div>
          {deviceType === 'CCTV' ? (
            <DropdownSchedule
              defaultText="SELECT AI TYPE"
              itemList={aiTypeTree}
              defaultUnFold={true}
              showSchedulePicker={showSchedulePicker}
              readonly={showSchedulePicker ? true : false}
              menuWidth="300px"
              menuHeight="30px"
              manuMargin="0 0 -30px 0"
              selectedItem={selectedAI}
              setSelectedItem={setSelectedAI}
              dataType={2}
              id="aiSelector"
            ></DropdownSchedule>
          ) : null}
        </div>
        {showSchedulePicker ? (
          <SearchBtn onClick={handleResetSelectedDevice}>重選</SearchBtn>
        ) : (
          renderSearchBtn()
        )}
      </TypeSelectRow>

      <NavyLine />
      {showSchedulePicker && selectedDevice ? (
        (deviceType === 'CCTV' && selectedAI) || deviceType === 'I/O' ? (
          loadingShow ? (
            <LoadingCustom loading={true} minHeight={'700px'} />
          ) : (
            <Fragment>
              <ScheduleHeader>{renderHoursName()}</ScheduleHeader>
              <ScheduleBody>
                {renderDaysName()}
                <Wrap
                  onDragEnter={(e) => cancelDefault(e)}
                  onDragOver={(e) => dragover(e)}
                  onDragStart={(e) => moveStart(e, e.clientX, e.clientY, e.type)}
                  onDrag={(e) => move(e, e.clientX, e.clientY)}
                  onDragEnd={(e) => moveEnd(e)}
                  draggable={true}
                  ref={wrapRef}
                >
                  {renderScheduleCube()}
                </Wrap>
              </ScheduleBody>
              {renderBottomArea()}
            </Fragment>
          )
        ) : null
      ) : null}
      {updateModalShow ? renderUpdateModal() : null}
      {changedModalShow ? renderChangedModal() : null}
      {noticeModalShow ? renderNoticeModal() : null}
    </ScheduleContainer>
  )
}
