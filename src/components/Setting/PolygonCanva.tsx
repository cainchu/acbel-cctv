import { Fragment, useRef, useEffect, useState, ChangeEvent } from 'react'
import styled from 'styled-components'
import { colorValue } from '../../styles'
import Polygon from './Polygon'
import * as R from 'ramda'
import { PolygonData } from './DetectList'
import { useAppDispatch } from './../../store'
import { createTargetAiRoiAction } from './../../store/saga'

interface ColorPickTarget extends EventTarget {
  value: string
}

interface PolygonCanvaProps {
  deviceThumbnail: string
  selectedDevice: aiDeviceMenuType
  closeModal: Function
  polygonData: PolygonData
  tempPolygonData: PolygonData[]
  setTempPolygonData: Function
  hasChanged: Boolean
  setHasChanged: Function
  selectedAI?: analysisType
  getDetectList: Function
}

const PolygonCanvaContainer = styled.div`
  width:100%;
  height:100%;
  display:flex;
  flex-direction:column;
  justify-content:space-between;
  align-items;center;
  border: solid 3px ${colorValue.border}
`
const InnerWrap = styled.div`
  width:100%;
  height:100%;
  display:flex;
  flex-direction:column;
  justify-content:space-between;
  align-items;center;
  padding:3px;
`
const Header = styled.div`
  width: 100%;
  color: ${colorValue.second};
  background-color: ${colorValue.background};
  height: 30px;
  line-height: 30px;
  display: flex;
  justify-content: space-between;
  align-items: center;
  box-sizing: border-box;
  padding: 0 4px 0 10px;
`
const DetectName = styled.div`
  font-size: 14px;
  font-weight: 500;
`
const IconClose = styled.div`
  width: 22px;
  height: 22px;
  background-repeat: no-repeat;
  background-image: url('/images/icon_win_close_b.svg');
  background-size: contain;
  background-position: center;
  cursor: pointer;
`
const DetectThumbnail = styled.div`
  width: 100%;
  aspect-ratio: 16 / 9;
  height: auto;
  background-size: cover;
  background-repeat: no-repeat;
  display: flex;
  justify-content: center;
  align-items: center;
`
const Bottom = styled.div`
  width: 100%;
  height: 60px;
  display: flex;
  justify-content: space-between;
  align-items: center;
  box-sizing: border-box;
  padding: 0 10px;
`

const Check = styled.div`
  width: 40px;
  height: 40px;
  margin: 0 10px 0 0;
  cursor: pointer;
  border-radius: 5px;
  border: solid 2px #fff;
  &.changed {
    width: 40px;
    height: 40px;
    background-image: url('/images/check_black.svg');
    background-repeat: no-repeat;
    background-size: 100%;
    background-color: red;
  }
  &.disable {
    pointer-events: none;
    cursor: initial;
  }
`
const IconClear = styled.div`
  width: 20px;
  height: 20px;
  background-repeat: no-repeat;
  background-image: url('/images/icon_clear.svg');
  background-size: contain;
  background-position: center;
  cursor: pointer;
`

const Btn = styled.div`
  width: 160px;
  min-height: 40px;
  color: #000;
  font-size: 24px;
  font-weight: bold;
  font-family: SegoeUI;
  border: 1px solid ${colorValue.second};
  background-color: #42c6fc;
  display: flex;
  justify-content: center;
  box-sizing: border-box;
  line-height: 40px;
  transition: 0.3s;
  cursor: pointer;
  margin: 0 0 0 10px;
  &:hover {
    background-color: ${colorValue.second};
    color: #0c1a2a;
  }
  &.disable {
    background-color: #76a6c4;
    pointer-events: none;
    opacity: 0.5;
  }
`
const flexSty = {
  display: 'flex',
  alignItems: 'center',
}
const textSty = {
  fontSize: '24px',
  fontWeight: '600',
  color: '#555',
  marginLeft: '5px',
  cursor: 'pointer',
}

export default (props: PolygonCanvaProps) => {
  const {
    selectedDevice,
    deviceThumbnail,
    closeModal,
    polygonData,
    tempPolygonData,
    setTempPolygonData,
    setHasChanged,
    selectedAI,
    getDetectList,
  } = props

  const { showRoi, roiColor, pos } = polygonData
  const [currentCoordinate, setCurrentCoordinate] = useState<number[][]>([])
  const [currentColor, setCurrentColor] = useState<string>(roiColor ? roiColor : '#ff0000')
  const changeColor = (eventTarget: ColorPickTarget) => {
    setCurrentColor(eventTarget.value)
  }

  const modifyPolygon = (polygonData: PolygonData) => {
    let modifiedItem = { ...polygonData }

    modifiedItem.pos = currentCoordinate
    modifiedItem.roiColor = currentColor

    let tempList = [...tempPolygonData]
    tempList[R.indexOf(polygonData, tempPolygonData)] = modifiedItem
    setTempPolygonData(tempList)
    createAiRoiPos()
    closeModal()
  }

  const clearColorChoose = () => {
    if (roiColor) {
      setCurrentColor(roiColor)
    } else {
      setCurrentColor('#ff0000')
    }
  }
  const dispatch = useAppDispatch()

  const createAiRoiPos = () => {
    if (!selectedDevice) return
    if (!selectedAI) return
    const data = {
      id: selectedDevice.id,
      ai_type: selectedAI.sysName,
      roi_type: 'polygon',
      pos: JSON.stringify(currentCoordinate),
      sensitivity: 0,
      notation: '',
      show_roi: 0,
      roi_color: currentColor,
    }
    // console.log('createAiRoiPos_data', data);
    dispatch(createTargetAiRoiAction(data))
    getDetectList(selectedDevice.id, selectedAI.sysName)
  }

  return (
    <PolygonCanvaContainer>
      <InnerWrap>
        <Header>
          <DetectName>{selectedDevice.title}</DetectName>
          <IconClose onClick={() => closeModal()}></IconClose>
        </Header>
        <DetectThumbnail
          style={{
            backgroundImage: `url(${deviceThumbnail})`,
            cursor: 'pointer',
          }}
        >
          <Polygon
            polygonData={polygonData}
            editable={polygonData.pos.length ? false : true}
            setCurrentCoordinate={setCurrentCoordinate}
            currentColor={currentColor}
          ></Polygon>
        </DetectThumbnail>
        {polygonData.pos.length ? null : (
          <Bottom>
            <div style={flexSty}>
              {' '}
              <Check
                className={`${
                  roiColor && currentColor && currentColor !== roiColor ? 'changed' : ''
                }
              ${polygonData.pos.length ? 'disable' : ''}`}
                style={{ backgroundColor: currentColor }}
              ></Check>
              {polygonData.pos.length ? null : (
                <Fragment>
                  <input
                    type="color"
                    id="head"
                    name="head"
                    value="#e66465"
                    style={{
                      opacity: '0',
                      width: '40px',
                      height: '40px',
                      position: 'absolute',
                      cursor: 'pointer',
                    }}
                    onChange={(e) => changeColor(e.target)}
                  />
                  <IconClear onClick={clearColorChoose}></IconClear>
                  <div style={textSty} onClick={clearColorChoose}>
                    清除
                  </div>
                </Fragment>
              )}
            </div>

            <div style={flexSty}>
              <Btn onClick={() => modifyPolygon(polygonData)}>確定</Btn>
              <Btn onClick={() => closeModal()}>取消</Btn>
            </div>
          </Bottom>
        )}
      </InnerWrap>
    </PolygonCanvaContainer>
  )
}
