import { useState, useEffect } from 'react'
import styled from 'styled-components'
import { colorValue } from '../../styles'
import { SubMenu } from './EventList'

interface DropDownProps {
  defaultText: string
  itemList: (SubMenu | aiCamera | analysisType | ioItemResult)[]
  defaultUnFold?: Boolean
  readonly?: Boolean
  menuWidth: string
  menuHeight: string
  manuMargin?: string
  selectedItem?:
    | aiCamera[]
    | aiCamera
    | analysisType
    | analysisType[]
    | ioItemResult
    | ioItemResult[]
  setSelectedItem: Function
  selectedType?: string
  setSelectedType?: Function
  dataType: number
  id?: string
  showEventList: Boolean
}

const DropDownContainer = styled.div.attrs(
  (props: { menuWidth: string; manuMargin: string }) => props,
)`
  width: ${(props) => props.menuWidth};
  margin: ${(props) => (props.manuMargin ? props.manuMargin : '5px')};
`
const DefaultArea = styled.div.attrs((props: { menuHeight: string }) => props)`
  width: 100%;
  height: ${(props) => props.menuHeight};
  background-color: rgba(0, 0, 0, 0.8);
  color: #2a8ab6;
  display: flex;
  align-items: center;
  border: solid 1px #2a8ab6;
`
const ItemNameArea = styled.div.attrs((props: { menuHeight: string }) => props)`
  width: 100%;
  height: 100%;
  color: #2a8ab6;
  line-height: ${(props) => `calc(${props.menuHeight} - 2px)`};
  font-size: 14px;
  padding: 0 10px;
  overflow: hidden;
  white-space: nowrap;
  text-overflow: ellipsis;
`
const IconArea = styled.div.attrs((props: { menuHeight: string; readonly: Boolean }) => props)`
  width: ${(props) => props.menuHeight};
  height: ${(props) => `calc(${props.menuHeight} - 6px)`};
  display: flex;
  justify-content: center;
  align-items: center;
  border-left: 1px solid #2a8ab6;
  transition: 0.3s;
  cursor: ${(props) => (props.readonly ? 'initial' : 'pointer')};
`
const ArrowDown = styled.div.attrs((props: { unfold: Boolean; readonly: Boolean }) => props)`
  width: 0;
  height: 0;
  border-left: 5px solid transparent;
  border-right: 5px solid transparent;
  border-top: 5px solid #2a8ab6;
  transition: 0.3s;
  transform: ${(props) => (props.unfold ? 'scaleY(-1)' : 'unset')};
  cursor: ${(props) => (props.readonly ? 'initial' : 'pointer')};
`
const UnfoldArea = styled.div.attrs((props: { unfold: Boolean }) => props)`
  width: 100%;
  background-color: #0b1216;
  color: #2a8ab6;
  transition: 0.3s;
  overflow: hidden;
  transform: scaleY(${(props) => (props.unfold ? 1 : 0)});
  transform-origin: top;
  margin-top: 6px;
  border: solid 1px #2a8ab6;
  box-sizing: border-box;
  padding: 5px;
  max-height: 700px;
  overflow-y: scroll;
`
const EachItem = styled.div.attrs((props: { menuHeight: string }) => props)`
  width: 100%;
  height: ${(props) => props.menuHeight};
  color: #2a8ab6;
  transition: 0.3s;
  line-height: ${(props) => props.menuHeight};
  cursor: pointer;
  padding: 0 10px;
  overflow: hidden;
  white-space: nowrap;
  text-overflow: ellipsis;
  font-size: 14px;
  &:hover {
    background-color: #2a8ab6;
    color: #0b1216;
  }
  &.disable {
    color: #2a8ab650;
    cursor: initial;
    &:hover {
      background-color: unset;
      color: #2a8ab650;
    }
  }
`

const DefaultItem = styled.div`
  color: #42c6fc;
  padding: 0 10px;
  box-sizing: border-box;
  font-size: 16px;
  height: 30px;
  line-height: 30px;
  cursor: pointer;

  &:after {
    padding: 0 10px;
    width: 100%;
    height: 1px;
    content: '';
    display: block;
    border-bottom: 1px solid ${colorValue.border};
  }
  &:hover {
    background-color: #2a8ab6;
    color: #0b1216;
  }
  &.disable {
    color: #2a8ab650;
    cursor: initial;
    &:hover {
      background-color: unset;
      color: #2a8ab650;
    }
  }
`
const MenuHeader = styled.div`
  color: #42c6fc;
  fontsize: 16px;
  height: 30px;
  padding: 5px 10px 0 10px;
  lineheight: 30px;
  fontweight: 600;
  cursor: pointer;
  &:hover {
    background-color: #2a8ab6;
    color: #0b1216;
  }
`
const SubMenuWrap = styled.div`
  margin: 0 0 20px 0;
  &:last-child {
    margin: 0;
  }
`

export default (props: DropDownProps) => {
  const {
    defaultText,
    itemList,
    defaultUnFold,
    readonly,
    menuWidth,
    menuHeight,
    manuMargin,
    selectedItem,
    setSelectedItem,
    selectedType,
    setSelectedType,
    dataType,
    id,
    showEventList
  } = props

  const [showMenu, setShowMenu] = useState<Boolean>(defaultUnFold ? defaultUnFold : false)


  useEffect(() => {
    if (showMenu) {
      if (showEventList) {
        setShowMenu(false)
      } else {
        setShowMenu(true)
      }
    }
  }, [showEventList])

  useEffect(() => {
    switch (dataType) {
      case 1:
        if (defaultUnFold && Array.isArray(selectedItem) && !selectedItem.length) {
          setShowMenu(true)
        }
        break
      case 2:
        if (defaultUnFold && !selectedItem) {
          setShowMenu(true)
        }
        break
      default:
        break
    }
  }, [selectedItem])

  const selectItem = (
    item: aiCamera[] | aiCamera | analysisType | analysisType[] | ioItemResult | ioItemResult[],
    menuType?: string,
  ) => {
    setSelectedItem(item)
    setShowMenu(false)
    setSelectedType ? setSelectedType(menuType) : null
  }

  function isSubMenu(item: SubMenu | aiCamera | analysisType | ioItemResult): item is SubMenu {
    return (item as SubMenu).menuContent !== undefined
  }

  function isCameraType(item: SubMenu | aiCamera | analysisType | ioItemResult): item is aiCamera {
    return item && (item as aiCamera).title !== undefined
  }

  function isIoType(item: SubMenu | aiCamera | analysisType | ioItemResult): item is ioItemResult {
    return item && (item as ioItemResult).keyword !== undefined
  }

  function isAnalysisType(
    item: SubMenu | aiCamera | analysisType | ioItemResult,
  ): item is analysisType {
    return item && (item as analysisType).title !== undefined
  }

  const selectMultiAnalysisiType = () => {
    let tempList: analysisType[] = []
    itemList.forEach((item) => {
      if (isAnalysisType(item)) {
        tempList.push(item)
      }
    })
    setSelectedItem(tempList)
    setShowMenu(false)
    setSelectedType ? setSelectedType('ALL') : null
  }

  const renderItemName = () => {
    let displayName = defaultText
    if (Array.isArray(selectedItem)) {
      if (selectedItem.length === 1) {
        if (isCameraType(selectedItem[0])) {
          displayName = selectedItem[0].title
        } else if (isIoType(selectedItem[0])) {
          displayName = selectedItem[0].keyword
        } else {
          displayName = selectedItem[0].title
        }
      } else {
        switch (dataType) {
          case 1:
            if (selectedType) {
              displayName = selectedType === 'ALL' ? 'ALL' : selectedType
            } else {
              if (isIoType(selectedItem[0])) {
                displayName = selectedItem.length === 1 ? selectedItem[0].keyword : defaultText
              } else {
                displayName = selectedItem.length === 1 ? selectedItem[0].title : defaultText
              }
            }
            break
          case 2:
            if (selectedItem.length === 2) {
              displayName = 'ALL'
            }
          default:
            // displayName = defaultText;
            break
        }
      }
    }
    return <ItemNameArea menuHeight={menuHeight}>{displayName}</ItemNameArea>
  }

  const renderMunu = () => {
    switch (dataType) {
      case 1:
        return (
          <UnfoldArea unfold={showMenu}>
            {itemList.map((item, key) => {
              if (isSubMenu(item)) {
                return (
                  <SubMenuWrap key={key}>
                    {item.menuHeader ? (
                      <MenuHeader onClick={() => selectItem(item.menuContent, item.menuType)}>
                        {item.menuHeader} -
                      </MenuHeader>
                    ) : null}
                    <div>
                      {item.menuContent.map((subItem, subItemKey) => {
                        if (isCameraType(subItem)) {
                          return (
                            <EachItem
                              key={subItemKey}
                              menuHeight={menuHeight}
                              onClick={() => selectItem([subItem], item.menuType)}
                            >
                              {subItem.title}
                            </EachItem>
                          )
                        }
                        if (isIoType(subItem)) {
                          return (
                            <EachItem
                              key={subItemKey}
                              menuHeight={menuHeight}
                              onClick={() => selectItem([subItem], item.menuType)}
                            >
                              {subItem.keyword}
                            </EachItem>
                          )
                        }
                      })}
                    </div>
                  </SubMenuWrap>
                )
              }
            })}
          </UnfoldArea>
        )
        break
      case 2:
        if (isAnalysisType(itemList[0]) && isAnalysisType(itemList[1])) {
          const multiEnable =
            (!itemList[0].enable && !itemList[1].enable) ||
            (itemList[0].enable && itemList[1].enable)
          return (
            <UnfoldArea unfold={showMenu}>
              <DefaultItem
                className={`${multiEnable ? '' : 'disable'}`}
                onClick={() => {
                  multiEnable ? selectMultiAnalysisiType() : null
                }}
              >
                DEFAULT
              </DefaultItem>
              {itemList.map((item, key) => {
                if (isAnalysisType(item)) {
                  return (
                    <EachItem
                      key={key}
                      menuHeight={menuHeight}
                      onClick={() => (item.enable ? selectItem([item]) : null)}
                      className={`${item.enable ? '' : 'disable'}`}
                    >
                      {item.title}
                    </EachItem>
                  )
                }
              })}
            </UnfoldArea>
          )
        }
        break
      default:
        break
    }
  }

  return (
    <DropDownContainer menuWidth={menuWidth} manuMargin={manuMargin} id={id}>
      <DefaultArea
        menuHeight={menuHeight}
        onClick={() => (readonly ? null : setShowMenu(!showMenu))}
      >
        {renderItemName()}
        <IconArea menuHeight={menuHeight} readonly={readonly}>
          <ArrowDown unfold={showMenu} readonly={readonly}></ArrowDown>
        </IconArea>
      </DefaultArea>

      {renderMunu()}
    </DropDownContainer>
  )
}
