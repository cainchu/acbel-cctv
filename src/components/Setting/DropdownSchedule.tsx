import { useState, useEffect } from 'react'
import styled from 'styled-components'
import { colorValue } from '../../styles'
import { ScheduleSubMenu, IoItemWithTitle } from './Schedule'

interface DropDownProps {
  defaultText: string
  itemList: (ScheduleSubMenu | aiCamera | analysisType | IoItemWithTitle)[]
  defaultUnFold?: Boolean
  showSchedulePicker: Boolean
  readonly?: Boolean
  menuWidth: string
  menuHeight: string
  manuMargin?: string
  selectedItem?: aiCamera | analysisType | IoItemWithTitle
  setSelectedItem: Function
  selectedType?: string
  setSelectedType?: Function
  dataType: number
  id?: string
}

const DropDownContainer = styled.div.attrs(
  (props: { menuWidth: string; manuMargin: string }) => props,
)`
  width: ${(props) => props.menuWidth};
  margin: ${(props) => (props.manuMargin ? props.manuMargin : '5px')};
`
const DefaultArea = styled.div.attrs((props: { menuHeight: string }) => props)`
  width: 100%;
  height: ${(props) => props.menuHeight};
  background-color: rgba(0, 0, 0, 0.8);
  color: #2a8ab6;
  display: flex;
  align-items: center;
  border: solid 1px #2a8ab6;
`
const ItemNameArea = styled.div.attrs((props: { menuHeight: string }) => props)`
  width: 100%;
  height: 100%;
  color: #2a8ab6;
  line-height: ${(props) => `calc(${props.menuHeight} - 2px)`};
  font-size: 14px;
  padding: 0 10px;
  overflow: hidden;
  white-space: nowrap;
  text-overflow: ellipsis;
`
const IconArea = styled.div.attrs((props: { menuHeight: string; readonly: Boolean }) => props)`
  width: ${(props) => props.menuHeight};
  height: ${(props) => `calc(${props.menuHeight} - 6px)`};
  display: flex;
  justify-content: center;
  align-items: center;
  border-left: 1px solid #2a8ab6;
  transition: 0.3s;
  cursor: ${(props) => (props.readonly ? 'initial' : 'pointer')};
`
const ArrowDown = styled.div.attrs((props: { unfold: Boolean; readonly: Boolean }) => props)`
  width: 0;
  height: 0;
  border-left: 5px solid transparent;
  border-right: 5px solid transparent;
  border-top: 5px solid #2a8ab6;
  transition: 0.3s;
  transform: ${(props) => (props.unfold ? 'scaleY(-1)' : 'unset')};
  cursor: ${(props) => (props.readonly ? 'initial' : 'pointer')};
`
const UnfoldArea = styled.div.attrs((props: { unfold: Boolean }) => props)`
  width: 100%;
  background-color: #0b1216;
  color: #2a8ab6;
  transition: 0.3s;
  overflow: hidden;
  transform: scaleY(${(props) => (props.unfold ? 1 : 0)});
  transform-origin: top;
  margin-top: 6px;
  border: solid 1px #2a8ab6;
  box-sizing: border-box;
  padding: 5px;
  max-height: 700px;
  overflow-y: scroll;
`
const EachItem = styled.div.attrs((props: { menuHeight: string }) => props)`
  width: 100%;
  height: ${(props) => props.menuHeight};
  color: #2a8ab6;
  transition: 0.3s;
  line-height: ${(props) => props.menuHeight};
  cursor: pointer;
  padding: 0 10px;
  overflow: hidden;
  white-space: nowrap;
  text-overflow: ellipsis;
  font-size: 14px;
  &:hover {
    background-color: #2a8ab6;
    color: #0b1216;
  }
  &.disable {
    color: #2a8ab650;
    cursor: initial;
    &:hover {
      background-color: unset;
      color: #2a8ab650;
    }
  }
`

const MenuHeader = styled.div`
  color: #42c6fc;
  fontsize: 16px;
  height: 30px;
  padding: 5px 10px 0 10px;
  lineheight: 30px;
  fontweight: 600;
`
const SubMenuWrap = styled.div`
  margin: 0 0 20px 0;
  &:last-child {
    margin: 0;
  }
`

export default (props: DropDownProps) => {
  const {
    defaultText,
    itemList,
    defaultUnFold,
    showSchedulePicker,
    readonly,
    menuWidth,
    menuHeight,
    manuMargin,
    selectedItem,
    setSelectedItem,
    selectedType,
    setSelectedType,
    dataType,
    id,
  } = props

  const [showMenu, setShowMenu] = useState<Boolean>(defaultUnFold ? defaultUnFold : false)

  useEffect(() => {
    if (showMenu) {
      if (showSchedulePicker) {
        setShowMenu(false)
      } else {
        setShowMenu(true)
      }
    }
  }, [showSchedulePicker])

  useEffect(() => {
    if (defaultUnFold && !selectedItem) {
      setShowMenu(true)
    }
  }, [selectedItem])

  const selectItem = (item: aiCamera | analysisType | IoItemWithTitle, menuType?: string) => {
    setSelectedItem(item)
    setShowMenu(false)
    setSelectedType ? setSelectedType(menuType) : null
  }

  function isSubMenu(
    item: ScheduleSubMenu | aiCamera | analysisType | IoItemWithTitle,
  ): item is ScheduleSubMenu {
    return (item as ScheduleSubMenu).menuContent !== undefined
  }

  const renderItemName = () => {
    let displayName = defaultText
    if (selectedItem && selectedItem.title) {
      displayName = selectedItem.title
    } else {
      displayName = defaultText
    }

    return <ItemNameArea menuHeight={menuHeight}>{displayName}</ItemNameArea>
  }

  function isAnalysisType(
    item: ScheduleSubMenu | aiCamera | analysisType | ioItemResult,
  ): item is analysisType {
    return item && (item as analysisType).title !== undefined
  }

  const renderMunu = () => {
    switch (dataType) {
      case 1:
        return (
          <UnfoldArea unfold={showMenu}>
            {itemList.map((item, key) => {
              if (isSubMenu(item)) {
                return (
                  <SubMenuWrap key={key}>
                    {item.menuHeader ? <MenuHeader>{item.menuHeader} -</MenuHeader> : null}
                    <div>
                      {item.menuContent.map((subItem, subItemKey) => {
                        return (
                          <EachItem
                            key={subItemKey}
                            menuHeight={menuHeight}
                            onClick={() => selectItem(subItem, item.menuType)}
                          >
                            {subItem.title}
                          </EachItem>
                        )
                      })}
                    </div>
                  </SubMenuWrap>
                )
              }
            })}
          </UnfoldArea>
        )
        break
      case 2:
        return (
          <UnfoldArea unfold={showMenu}>
            {itemList.map((item, key) => {
              if (!isSubMenu(item) && isAnalysisType(item)) {
                return (
                  <EachItem
                    key={key}
                    menuHeight={menuHeight}
                    onClick={() => (item.enable ? selectItem(item) : null)}
                    className={`${item.enable ? '' : 'disable'}`}
                  >
                    {item.title}
                  </EachItem>
                )
              }
            })}
          </UnfoldArea>
        )
        break
      default:
        break
    }
  }

  return (
    <DropDownContainer menuWidth={menuWidth} manuMargin={manuMargin} id={id}>
      <DefaultArea
        menuHeight={menuHeight}
        onClick={() => (readonly ? null : setShowMenu(!showMenu))}
      >
        {renderItemName()}
        <IconArea menuHeight={menuHeight} readonly={readonly}>
          <ArrowDown unfold={showMenu} readonly={readonly}></ArrowDown>
        </IconArea>
      </DefaultArea>

      {renderMunu()}
    </DropDownContainer>
  )
}
