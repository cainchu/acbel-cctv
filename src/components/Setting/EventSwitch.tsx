import { Fragment, useEffect, useState } from 'react'
import styled from 'styled-components'
import { colorValue } from '../../styles'
import SwitchToggle from './../Setting/SwitchToggle'
import { getIoAlarmListAction, updateIoAlarmAction } from './../../store/saga'
import { useAppDispatch } from './../../store'
import { useIoAlarmListSelector } from './../../hooks/useSelectIoAlarmList'

const PanelColumn = styled.div`
  width: 50%;
  height: 100%;
  border-right: 1px solid ${colorValue.border};
  display: flex;
  flex-direction: column;
  box-sizing: border-box;
  padding: 0 60px 0 0;
  &:last-child {
    border-right: none;
    padding: 0 20px 0 0;
  }
`

const PanelRow = styled.div`
  width: 100%;
  display: flex;
  flex-grow: 1;
  box-sizing: border-box;
`
const PanelRowInner = styled.div`
  // width: calc(100% - 80px);
  width: calc(100%);
  border-bottom: 1px dashed #163e52;
  display: flex;
  flex-direction: column;
  margin-bottom: 20px;
  padding: 10px 0;
  box-sizing: border-box;
`

const SubTitleDecoCube = styled.div`
  width: 10px;
  height: 10px;
  background-color: #163e52;
  margin: 20px 15px 0 0;
`

const SubTitle = styled.div`
  color: ${colorValue.second};
  font-size: 22px;
  font-weight: bold;
  letter-spacing: 2.2px;
  white-space: nowrap;
  display: flex;
  justify-content: space-between;
  align-items: center;
`

const EachRowWrap = styled.div`
  display: flex;
  flex-direction: column;
  padding: 5px 0;
  box-sizing: border-box;
`
const EachItem = styled.div`
  display: flex;
  justify-content: space-between;
  box-sizing: border-box;
  padding: 5px 5px;
  color: ${colorValue.title};
  font-size: 18px;
  font-weight: 600;
`

interface settingItem {
  itemName: string
  id: string
  status: Boolean
  group: string
}

export default () => {
  const [ioAlarmListMulti, setIoAlarmListMulti] = useState<settingItem[][]>()
  const [ioAlarmListSingle, setIoAlarmListSingle] = useState<settingItem[]>()
  const { ioAlarmList } = useIoAlarmListSelector()
  // console.log('ioAlarmList', ioAlarmList)

  const dispatch = useAppDispatch()
  useEffect(() => {
    dispatch(getIoAlarmListAction())
  }, [])

  const getStatus = (id: string) => {
    if (!ioAlarmList || !ioAlarmList.length) return false
    try {
      const statusNumber = ioAlarmList.filter((item) => {
        return item.id === id
      })[0].status
      return statusNumber === 1 ? true : false
    } catch (err) {
      console.log('err', err)
      return false
    }
  }

  const getMainItemName = (itemId: string) => {
    switch (itemId) {
      case 'hanging_platform':
        return '吊掛平台開啟事件通報'
        break
      case 'truck_stop':
        return '貨車靠站告警與事件通報'
        break
      case 'important_area':
        return '重要區域人員入侵事件通報'
        break
      default:
        return ''
        break
    }
  }

  useEffect(() => {
    if (!ioAlarmList || !ioAlarmList.length) return
    const switchSettingListMulti = [
      [
        {
          itemName: 'A棟3樓吊掛安全監視通報開關',
          id: 'hanging_platform_A3',
          status: getStatus('hanging_platform_A3'),
          group: 'hanging_platform',
        },
        {
          itemName: 'A棟4樓吊掛安全監視通報開關',
          id: 'hanging_platform_A4',
          status: getStatus('hanging_platform_A4'),
          group: 'hanging_platform',
        },
        {
          itemName: 'A棟5樓吊掛安全監視通報開關',
          id: 'hanging_platform_A5',
          status: getStatus('hanging_platform_A5'),
          group: 'hanging_platform',
        },
        {
          itemName: 'A棟6樓吊掛安全監視通報開關',
          id: 'hanging_platform_A6',
          status: getStatus('hanging_platform_A6'),
          group: 'hanging_platform',
        },
        {
          itemName: 'B棟2樓吊掛安全監視通報開關',
          id: 'hanging_platform_B2',
          status: getStatus('hanging_platform_B2'),
          group: 'hanging_platform',
        },
      ],
      [
        {
          itemName: 'A棟2樓40尺貨車進出通報開關',
          id: 'truck_40m_A2',
          status: getStatus('truck_40m_A2'),
          group: 'truck_stop',
        },
      ],
      [
        {
          itemName: 'B棟1樓人員入侵事件通報開關',
          id: 'person_invasion_B1',
          status: getStatus('person_invasion_B1'),
          group: 'important_area',
        },
        {
          itemName: 'B棟2樓人員入侵事件通報開關',
          id: 'person_invasion_B2',
          status: getStatus('person_invasion_B2'),
          group: 'important_area',
        },
        {
          itemName: 'B棟3樓人員入侵事件通報開關',
          id: 'person_invasion_B3',
          status: getStatus('person_invasion_B3'),
          group: 'important_area',
        },
      ],
    ]
    const switchSettingListSingle = [
      {
        itemName: '消防火警觸發事件通報開關',
        id: 'fire_alarm',
        status: getStatus('fire_alarm'),
        group: '',
      },
      {
        itemName: '含氧量不足觸發事件通報開關',
        id: 'lack_oxygen',
        status: getStatus('lack_oxygen'),
        group: '',
      },
      {
        itemName: '溫度過高事件觸發事件通報開關',
        id: 'gas_fire',
        status: getStatus('gas_fire'),
        group: '',
      },
      {
        itemName: '區域淨空異常事件通報開關',
        id: 'clearance_error',
        status: getStatus('clearance_error'),
        group: '',
      },
    ]
    setIoAlarmListMulti(switchSettingListMulti)
    setIoAlarmListSingle(switchSettingListSingle)
  }, [ioAlarmList])

  const updateStatus = (id: string, status: Boolean) => {
    let targetStatus = !status
    dispatch(updateIoAlarmAction({ id: id, status: { alarm_status: targetStatus ? 1 : 0 } }))
    dispatch(getIoAlarmListAction())
  }
  const renderLeftColumn = () => {
    if (!ioAlarmListMulti) return
    return ioAlarmListMulti.map((mainItem, key) => {
      return (
        <PanelRow key={key}>
          <SubTitleDecoCube></SubTitleDecoCube>
          <PanelRowInner>
            <SubTitle>{getMainItemName(mainItem[0].group)}</SubTitle>
            <EachRowWrap>
              {mainItem.map((rowItem, rowKey) => {
                return (
                  <EachItem key={rowKey}>
                    {rowItem.itemName}
                    <SwitchToggle
                      defaultStatus={rowItem.status}
                      switchAction={() => updateStatus(rowItem.id, rowItem.status)}
                    ></SwitchToggle>
                  </EachItem>
                )
              })}
            </EachRowWrap>
          </PanelRowInner>
        </PanelRow>
      )
    })
  }

  const renderRightColumn = () => {
    if (!ioAlarmListSingle) return
    return ioAlarmListSingle.map((mainItem, key) => {
      return (
        <PanelRow key={key} style={{ padding: '0 0 0 40px', flexGrow: 'unset' }}>
          <SubTitleDecoCube></SubTitleDecoCube>
          <PanelRowInner style={{ padding: '10px 0 10px 0' }}>
            <SubTitle>
              {mainItem.itemName}
              <SwitchToggle
                defaultStatus={mainItem.status}
                switchAction={() => updateStatus(mainItem.id, mainItem.status)}
              ></SwitchToggle>
            </SubTitle>
          </PanelRowInner>
        </PanelRow>
      )
    })
  }
  return (
    <Fragment>
      <PanelColumn>{renderLeftColumn()}</PanelColumn>
      <PanelColumn>{renderRightColumn()}</PanelColumn>
    </Fragment>
  )
}
