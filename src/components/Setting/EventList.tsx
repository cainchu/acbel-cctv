import { Fragment, useEffect, useRef, useState } from 'react'
import styled from 'styled-components'
import { colorValue } from '../../styles'
import * as R from 'ramda'
import Dropdown from './Dropdown'
import DatePicker from '../../components/Setting/DatePicker'
import InnerModalTool from './InnerModalTool'
import ModalTool from './ModalTool'
import Notice from './Notice'
import StreamPlayer from './StreamPlayer'
import { useRootSelector, useUserSelector } from './../../hooks/useSelect'
import { getHistoryAction } from './../../store/saga'
import { useHistorySelector, useHistoryCameraSelector } from './../../hooks/useSelectHistory'
import { useIoListSelector } from './../../hooks/useSelectIoList'
import { useNxServiceSelect } from './../../hooks/useNxServiceSelect'
import { useAppDispatch } from './../../store'
import LoadingCustom from './../LoadingCustom'
import EventListTable from './EventListTable'
import { setHistory } from './../../store/history'
import { convertBigNumToM } from './../../utils/tools'
import { yyyy_mm_dd_h_m_s } from '../../utils/dateFormat'
import moment from 'moment'
import { json2csv } from 'json-2-csv'
import ExportCheck from './ExportCheck'
import NoticeDownloading from './NoticeDownloading'
import useIoKey from './../../hooks/useIoKey'
import { useAiRoiSelector } from './../../hooks/useSelectAiRoi'
import { nxTimeFormat } from './../../utils/nxTimeFormat'
export interface SubMenu {
  menuHeader: string
  menuContent: aiCamera[] | ioItemResult[]
  menuType?: string
}

interface TargetEventData {
  id: string
  cameraKey: string
  device: string
  startTime: number
  endTime: number
  ele: HTMLDivElement
}

interface ScrollEventTarget extends EventTarget {
  scrollTop?: number
  clientHeight?: number
  scrollHeight?: number
}

export interface ClockType {
  name: string
  millisecond: number
  disable: Boolean
}

const EventListContainer = styled.div`
  width: 100%;
  height: 810px;
  display: flex;
  justify-content: flex-start;
  align-items: center;
  flex-direction: column;
  position: relative;
`
const Wrap = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
  max-height: 700px;
  // height: 640px;
  overflow: hidden;
  position: relative;
  height: 700px;
`
const WrapInner = styled.div`
  width: 100%;
  display: flex;
  justify-content: flex-start;
  align-items: center;
  flex-direction: column;
  overflow-x: hidden;
  overflow-y: scroll;
  box-sizing: border-box;
  &::-webkit-scrollbar {
    width: 7px;
    background-color: #152637;
  }
  &::-webkit-scrollbar-button {
    display: none;
  }

  &::-webkit-scrollbar-track-piece {
    background: transparent;
  }

  &::-webkit-scrollbar-thumb {
    border-radius: 4px;
    background-color: ${colorValue.border};
  }

  &::-webkit-scrollbar-track {
    box-shadow: transparent;
  }
`

const Btn = styled.div`
  width: 140px;
  height: 30px;
  color: #000;
  font-size: 22px;
  font-weight: bold;
  font-family: SegoeUI;
  border: 1px solid #2a8ab6;
  background-color: #42c6fc;
  display: flex;
  justify-content: center;
  margin: 0 25px;
  line-height: 30px;
  transition: 0.3s;
  cursor: pointer;
  z-index: 1;
  &:hover {
    background-color: #2a8ab6;
    color: #0c1a2a;
  }
  &.disable {
    background-color: #76a6c4;
    pointer-events: none;
    opacity: 0.5;
  }
`

const TypeSelectRow = styled.div.attrs(
  (props: { showEventList: Boolean; deviceType: string }) => props,
)`
  width: 100%;
  height: ${(props) =>
    props.showEventList ? (props.deviceType === 'ALL' ? '70px' : '30px') : 'unset'};
  box-sizing: border-box;
  display: flex;
`
const NavyLine = styled.div`
  height: 5px;
  width: 100%;
  background-image: url(./images/slash_10px.svg);
  margin: 20px 0 0 0;
`

const Bottom = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: space-between;
  margin: 20px 0 0 0;
`

const dataResultTip = {
  fontSize: '20px',
  fontWeight: '300',
  display: 'flex',
}

const LoadingIcon = styled.div`
  width: 30px;
  height: 30px;
  min-height: 30px;
  background-image: url(/images/loading.gif);
  background-repeat: no-repeat;
  background-size: contain;
  margin: 20px 0;
`
const NoData = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  font-size: 20px;
  font-weight: 600;
`

export default () => {
  const [showEventList, setShowEventList] = useState<Boolean>(false)
  const [selectedDevice, setSelectedDevice] = useState<aiCamera[] | ioItemResult[]>([])
  // console.log('selectedDevice', selectedDevice)
  const [selectedAI, setSelectedAI] = useState<analysisType[]>([])
  const [deviceType, setDeviceType] = useState<string>('')
  const [from, setFrom] = useState<Date>(toMidNight(new Date()))
  const [to, setTo] = useState<Date>(new Date())
  const [playerShow, setPlayerShow] = useState<Boolean>(false)
  const [targetEvent, setTargetEvent] = useState<TargetEventData>()
  const [tempHistory, setTempHistory] = useState<historyResult[]>([])
  const [scrollEvent, setScrollEvent] = useState<Boolean>(true)
  const [exportFormat, setExportFormat] = useState<string>('')
  const [exportModalShow, setExportModalShow] = useState<Boolean>(false)
  const [prevScrollTop, setPrevScrollTop] = useState<number>(0)
  const [dateConflictModalShow, setDateConflictModalShow] = useState<Boolean>(false)
  const [loadingShow, setLoadingShow] = useState(false)
  const [ioDeviceList, setIoDeviceList] = useState<ioItemResult[]>([])
  const [cameraDeviceList, setCameraDeviceList] = useState<aiCamera[]>([])
  const [aiTypeTree, setAiTypeTree] = useState<analysisType[]>([])
  const [eventOriginDuration, setEventOriginDuration] = useState(10) //是否要延長監視串流擷取時間的判斷條件 預設為小於10秒則延長 (單位:秒)
  const [eventExtendTime, setEventExtendTime] = useState(60) //欲延長擷取時間的設定 預設為60秒 (單位:秒)
  const [downloadingModalShow, setDownloadingModalShow] = useState<Boolean>(false)

  const { getNxRouting } = useNxServiceSelect()

  const { aiMenuList } = useAiRoiSelector()

  const { getCameraIdByIoKey } = useIoKey()

  const [fromClock, setFromClock] = useState<ClockType>({
    name: '00:00',
    millisecond: 0,
    disable: false,
  })

  const [toClock, setToClock] = useState<ClockType>(getNow())

  function getNow() {
    let now = new Date()
    let name = moment(now).format('HH:mm')
    let millisecond =
      (Number(name.split(':')[0]) * 60 * 60 + Number(name.split(':')[1]) * 60) * 1000
    return { name: name, millisecond: millisecond, disable: false }
  }

  const listWrapRef = useRef<HTMLDivElement>(null)
  const { subscriptionAlarm } = useRootSelector()
  const { historyList, historyTotalLength, historyStatus } = useHistorySelector()
  const { ioList } = useIoListSelector()

  useEffect(() => {
    let modifiedIoList = ioList.filter((item) => {
      // console.log('item', item)
      return R.includes(item.keyword, subscriptionAlarm.io)
    })
    // console.log('modifiedIoList', modifiedIoList)
    setIoDeviceList(modifiedIoList)
  }, [ioList])

  useEffect(() => {
    let modifiedCameraList = aiMenuList.filter((item) => {
      return R.includes(item.id, subscriptionAlarm.camera)
    })
    setCameraDeviceList(modifiedCameraList)
  }, [aiMenuList])

  const dispatch = useAppDispatch()

  function toMidNight(date: Date) {
    let thisMoment = moment(date).format('YYYY-MM-DD 00:00:00')
    return new Date(thisMoment)
  }

  useEffect(() => {
    setTempHistory(historyList)
    setScrollEvent(true)
    setLoadingShow(false)
  }, [historyList])

  let menuList = [
    {
      menuHeader: 'CCTV LIST',
      menuContent: cameraDeviceList,
      menuType: 'CCTV',
    },
    {
      menuHeader: 'I/O LIST',
      menuContent: ioDeviceList,
      menuType: 'I/O',
    },
  ]

  useEffect(() => {
    if (selectedDevice && selectedDevice.length) {
      let tempTypeList = [
        { title: '智能電子圍籬', sysName: 'ai_fences', enable: true },
        { title: '智能大型遺留物', sysName: 'ai_retentate', enable: true },
      ]
      if (isCameraDevice(selectedDevice[0]) && selectedDevice.length === 1) {
        switch (selectedDevice[0].ai) {
          case '10':
            tempTypeList[0].enable = true
            tempTypeList[1].enable = false
            break
          case '01':
            tempTypeList[0].enable = false
            tempTypeList[1].enable = true
            break
          case '11':
            tempTypeList[0].enable = true
            tempTypeList[1].enable = true
            break

          default:
            break
        }
      } else if (isCameraDevice(selectedDevice[0]) && selectedDevice.length > 1) {
        tempTypeList[0].enable = false
        tempTypeList[1].enable = false
      }
      setAiTypeTree(tempTypeList)
      setSelectedAI([])
    }
  }, [selectedDevice])

  useEffect(() => {
    setSelectedAI([])
    dispatch(setHistory({ historyList: [], historyTotalLength: 0, historyStatus: 0 }))
    setTempHistory([])
  }, [selectedDevice])

  useEffect(() => {
    const ioEle = document.getElementById('ioSelector')
    if (!listWrapRef.current) return
    if (!ioEle) return
    if (deviceType === 'ALL') {
      if (showEventList) {
        ioEle.style.margin = '-100px 0 0 0'
        listWrapRef.current.style.height = '650px'
      } else {
        ioEle.style.margin = '60px 0 0 0'
        listWrapRef.current.style.height = '700px'
      }
    } else {
      ioEle.style.margin = '0'
      listWrapRef.current.style.height = '700px'
    }
  }, [deviceType, selectedAI, showEventList])

  const intervalSty = {
    width: '1px',
    height: '22px',
    margin: '6px 20px 6px 20px',
    backgroundColor: colorValue.border,
  }

  const getDeviceName = (id: string) => {
    const firstEvent = cameraDeviceList.filter((item: aiCamera) => {
      return item.id === id
    })[0]
    if (firstEvent) {
      return firstEvent.title
    } else {
      return ''
    }
  }

  const getIdForNxPlayback = (device: string, id: string) => {
    if (device === 'io') {
      const matchedCamera = getCameraIdByIoKey(id)
      if (!matchedCamera) return id
      return matchedCamera.id
    } else {
      return id
    }
  }

  const resetSearch = () => {
    setShowEventList(false)
    setSelectedDevice([])
    setSelectedAI([])
    setDeviceType('')
    setPrevScrollTop(0)
    setFrom(toMidNight(new Date()))
    setTo(new Date())
    setFromClock({
      name: '00:00',
      millisecond: 0,
      disable: false,
    })
    setToClock(getNow())
  }

  const handleScrollDown = (eventTarget: ScrollEventTarget) => {
    if (!eventTarget.scrollTop) return

    if (eventTarget.scrollTop > prevScrollTop) {
      setPrevScrollTop(eventTarget.scrollTop)
    } else {
      // console.log('scroll up')
      return
    }
    if (totalCount === dataLengthCounter()) {
      // console.log('資料筆數上限');
      return
    }
    if (!eventTarget.scrollTop || !eventTarget.clientHeight || !eventTarget.scrollHeight) return
    if (eventTarget.scrollTop + eventTarget.clientHeight + 20 >= eventTarget.scrollHeight) {
      setScrollEvent(false)
      const analysisTypeName = getAnalysisTypeName(deviceType)
      dispatch(
        getHistoryAction({
          offset: tempHistory.length,
          data_length_per_page: 30,
          id: getSelectDeviceId(),
          analysis_type: analysisTypeName,
          start_alarm_timestamp: from.getTime() * 1000,
          end_alarm_timestamp: to.getTime() * 1000,
          round: 1,
          device_type: deviceType === 'CCTV' ? 'camera' : 'io',
          matched_camera: getIdForNxPlayback(
            deviceType === 'CCTV' ? 'camera' : 'io',
            getSelectDeviceId(),
          ),
        }),
      )
    }
  }

  function isCameraDevice(item: aiCamera | ioItemResult): item is aiCamera {
    return item && (item as aiCamera).id !== undefined
  }
  function isIoDevice(item: aiCamera | ioItemResult): item is ioItemResult {
    return item && (item as ioItemResult).keyword !== undefined
  }

  const getAnalysisTypeName = (deviceType: string) => {
    let analysisTypeName = ''
    switch (deviceType) {
      case 'ALL':
        analysisTypeName = ''
        break
      case 'CCTV':
        analysisTypeName = selectedAI.length > 1 ? '' : selectedAI[0].sysName
        break
      case 'I/O':
        if (isIoDevice(selectedDevice[0])) {
          analysisTypeName = selectedDevice.length > 1 ? '' : selectedDevice[0].io_type
        } else {
          analysisTypeName = ''
        }
        break
      default:
        analysisTypeName = ''
        break
    }
    return analysisTypeName
  }

  const handleSearch = (deviceType: string) => {
    if (from.getTime() > to.getTime()) {
      setDateConflictModalShow(true)
      return
    }

    const analysisTypeName = getAnalysisTypeName(deviceType)
    setLoadingShow(true)
    dispatch(
      getHistoryAction({
        offset: tempHistory.length,
        data_length_per_page: 30,
        id: getSelectDeviceId(),
        analysis_type: analysisTypeName,
        start_alarm_timestamp: from.getTime() * 1000,
        end_alarm_timestamp: to.getTime() * 1000,
        round: 1,
        device_type: deviceType === 'CCTV' ? 'camera' : 'io',
        matched_camera: getIdForNxPlayback(
          deviceType === 'CCTV' ? 'camera' : 'io',
          getSelectDeviceId(),
        ),
      }),
    )
    setShowEventList(true)
  }

  const playThisVideo = (targetEvent: TargetEventData) => {
    setTargetEvent(targetEvent)
    setPlayerShow(true)
  }

  const closePlayerModal = () => {
    setTargetEvent(undefined)
    setPlayerShow(false)
  }

  /*針對所有裝置判斷小於10 秒鐘 (eventOriginDuration) 的片段，皆增加為60秒 (eventExtendTime) */
  const getNxDuration = (startTime: number, endTime: number) => {
    const initialDuretion = Number(convertBigNumToM((endTime - startTime).toString().slice(0, -3))) //秒
    let durationSeconds = eventExtendTime

    if (initialDuretion <= eventOriginDuration) {
      durationSeconds = eventExtendTime
    } else {
      durationSeconds = initialDuretion
    }

    return durationSeconds
  }

  const renderPlayerModal = (targetEvent: TargetEventData) => {
    if (!targetEvent) return
    const durationSeconds = getNxDuration(targetEvent.startTime, targetEvent.endTime) //單位 秒

    const streamUrl = `${getNxRouting(targetEvent.id)}/media/${
      targetEvent.id
    }.mp4?pos=${nxTimeFormat(targetEvent.startTime)}&duration=${durationSeconds}`
    if (targetEvent.ele.parentNode) {
      let downloadEle = targetEvent.ele.parentNode.querySelector('.download_stream') as HTMLElement
      if (downloadEle) {
        downloadEle.dataset.url = streamUrl
      }
    }
    return (
      <ModalTool
        modalShow={playerShow}
        modalCloseFunction={() => closePlayerModal()}
        modalWidth={1520}
        modalHeight={'unset'}
        backgroundOpacity={0.7}
        background={'#13253A'}
        modalInnerBackground={'#000'}
        zIndex={12}
      >
        <StreamPlayer
          closeModal={() => closePlayerModal()}
          // streamUrl={`${getNxRouting(targetEvent.id)}/media/${targetEvent.id}.mp4?pos=${
          //   targetEvent.startTime
          // }&endPos=${targetEvent.endTime}`}
          streamUrl={streamUrl}
          deviceName={getDeviceName(targetEvent.id)}
        />
      </ModalTool>
    )
  }

  const renderSearchBtn = () => {
    switch (deviceType) {
      case 'CCTV':
        if (selectedDevice.length && selectedAI.length) {
          return <Btn onClick={() => handleSearch(deviceType)}>查詢</Btn>
        } else {
          return (
            <Btn className="disable" onClick={() => handleSearch(deviceType)}>
              查詢
            </Btn>
          )
        }
        break
      case 'I/O':
        if (selectedDevice.length) {
          return <Btn onClick={() => handleSearch(deviceType)}>查詢</Btn>
        } else {
          return (
            <Btn className="disable" onClick={() => handleSearch(deviceType)}>
              查詢
            </Btn>
          )
        }
        break

      default:
        return (
          <Btn
            style={{
              backgroundColor: '#76a6c4',
              pointerEvents: 'none',
              opacity: '0.5',
            }}
          >
            查詢
          </Btn>
        )
        break
    }
  }

  const dataLengthCounter = () => {
    return tempHistory.length
  }

  const exportLogData = () => {
    const modifiedHistory = tempHistory.map((item, index) => {
      return {
        deviceId: item.id,
        deviceName: getDeviceName(item.id) ? getDeviceName(item.id) : item.id,
        deviceType: item.device,
        analysisType: item.analysis_type,
        startTime: yyyy_mm_dd_h_m_s(Math.floor(Number(item.start_alarm_timestamp) / 1000)),
        endTime: yyyy_mm_dd_h_m_s(Math.floor(Number(item.end_alarm_timestamp) / 1000)),
        notation: item.notation,
      }
    })

    const searchTimeFileName = moment(new Date()).locale('zh-tw').format('YYYY-MM-DD hh mm ss')
    switch (exportFormat) {
      case 'json':
        const content = JSON.stringify(modifiedHistory)
        let a = document.createElement('a')
        let file = new Blob([content], { type: 'text/json' })
        a.href = URL.createObjectURL(file)
        a.download = `eventLog_${searchTimeFileName}.json`
        a.click()

        break
      case 'csv':
        const options = {
          delimiter: {
            wrap: '"',
            field: ',',
            eol: '\n',
          },
          prependHeader: true,
          sortHeader: false,
          excelBOM: true,
          trimHeaderValues: true,
          trimFieldValues: true,
          keys: [
            'deviceId',
            'deviceName',
            'deviceType',
            'analysisType',
            'startTime',
            'endTime',
            'notation',
          ],
        }
        const responseCSV = (err: any, csv: any) => {
          if (err) throw err
          const content = csv
          let a = document.createElement('a')
          let file = new Blob([content], { type: 'xml/csv' })
          a.href = URL.createObjectURL(file)
          a.download = `eventLog_${searchTimeFileName}.csv`
          a.click()
        }
        json2csv(modifiedHistory, responseCSV, options)
        break

      default:
        break
    }
  }

  const totalCount = historyTotalLength

  const getSelectDeviceId = () => {
    if (permission === 'E') {
      //碼頭權限
      if (isCameraDevice(selectedDevice[0])) {
        if (selectedDevice.length === 1) {
          return selectedDevice[0].id
        } else {
          const tempDeviceIdArray = selectedDevice.map((item) => {
            if (isCameraDevice(item)) {
              return item.id
            }
          })
          return tempDeviceIdArray.toString()
        }
      } else if (isIoDevice(selectedDevice[0])) {
        if (selectedDevice.length === 1) {
          return selectedDevice[0].keyword
        } else {
          const tempDeviceIdArray = selectedDevice.map((item) => {
            if (isIoDevice(item)) {
              return item.keyword
            }
          })
          return tempDeviceIdArray.toString()
        }
      } else {
        return ''
      }
    } else {
      //其他權限
      if (isCameraDevice(selectedDevice[0])) {
        if (selectedDevice.length === 1) {
          return selectedDevice[0].id
        } else {
          return ''
        }
      } else if (isIoDevice(selectedDevice[0])) {
        if (selectedDevice.length === 1) {
          return selectedDevice[0].keyword
        } else {
          return ''
        }
      } else {
        return ''
      }
    }
  }
  const { permission } = useUserSelector()
  // console.log('permission', permission)
  const handleExport = () => {
    let remainderCount = totalCount - tempHistory.length
    // console.log('remainderCount', remainderCount);
    const round = Math.round(remainderCount / 30) + 1
    // console.log('round', round);
    if (tempHistory.length <= totalCount) {
      dispatch(
        getHistoryAction({
          offset: tempHistory.length,
          data_length_per_page: remainderCount,
          id: getSelectDeviceId(),
          analysis_type: getAnalysisTypeName(deviceType),
          start_alarm_timestamp: from.getTime() * 1000,
          end_alarm_timestamp: to.getTime() * 1000,
          round: round,
          device_type: deviceType === 'CCTV' ? 'camera' : 'io',
          matched_camera: getIdForNxPlayback(
            deviceType === 'CCTV' ? 'camera' : 'io',
            getSelectDeviceId(),
          ),
        }),
      )
    }

    setExportModalShow(true)
  }

  const renderExportModal = () => {
    return (
      <InnerModalTool
        modalShow={exportModalShow}
        modalCloseFunction={() => {}}
        modalWidth={420}
        modalHeight={240}
        backgroundOpacity={0.7}
        background={'rgba(0, 0, 0, 0.5)'}
        modalInnerBackground={'#00000075'}
        zIndex={12}
      >
        <ExportCheck
          closeModal={() => setExportModalShow(false)}
          modifyFunc={() => exportLogData()}
          exportFormat={exportFormat}
          setExportFormat={setExportFormat}
          historyList={tempHistory}
          totalCount={totalCount}
        />
      </InnerModalTool>
    )
  }

  const renderDownloadingModal = () => {
    return (
      <InnerModalTool
        modalShow={downloadingModalShow}
        modalCloseFunction={() => {}}
        modalWidth={420}
        modalHeight={240}
        backgroundOpacity={0.7}
        background={'rgba(0, 0, 0, 0.5)'}
        modalInnerBackground={'#00000075'}
        zIndex={12}
      >
        <NoticeDownloading closeModal={() => setDownloadingModalShow(false)} />
      </InnerModalTool>
    )
  }
  const closeDateConflictModal = () => {
    setDateConflictModalShow(false)
  }
  const renderDateConflictModal = () => {
    return (
      <ModalTool
        modalShow={dateConflictModalShow}
        modalCloseFunction={() => closeDateConflictModal()}
        modalWidth={420}
        modalHeight={240}
        backgroundOpacity={0.7}
        background={'#13253A'}
        modalInnerBackground={'#00000075'}
        zIndex={12}
      >
        <Notice
          closeModal={() => closeDateConflictModal()}
          action={resetSearch}
          noticeText="請選擇正確的時間區間"
        />
      </ModalTool>
    )
  }

  function isHistoryType(item: historyResult): item is historyResult {
    return item && (item as historyResult).id !== undefined
  }

  return (
    <EventListContainer>
      <TypeSelectRow showEventList={showEventList} deviceType={deviceType}>
        <DatePicker
          showPicker={true}
          showEventList={showEventList}
          from={from}
          to={to}
          setFrom={setFrom}
          setTo={setTo}
          fromClock={fromClock}
          setFromClock={setFromClock}
          toClock={toClock}
          setToClock={setToClock}
          getNow={getNow}
          toMidNight={toMidNight}
        ></DatePicker>
        <div style={intervalSty}></div>
        <Dropdown
          defaultText="SELECT DEVICE"
          itemList={menuList}
          defaultUnFold={false}
          readonly={showEventList ? true : false}
          menuWidth="300px"
          menuHeight="30px"
          manuMargin="0"
          selectedItem={selectedDevice}
          setSelectedItem={setSelectedDevice}
          selectedType={deviceType}
          setSelectedType={setDeviceType}
          dataType={1}
          showEventList={showEventList}
        ></Dropdown>
        <div style={intervalSty}></div>
        <div>
          {deviceType === 'CCTV' ? (
            <Dropdown
              defaultText="SELECT AI TYPE"
              itemList={aiTypeTree}
              defaultUnFold={true}
              readonly={showEventList ? true : false}
              menuWidth="300px"
              menuHeight="30px"
              manuMargin="0 0 -30px 0"
              selectedItem={selectedAI}
              setSelectedItem={setSelectedAI}
              dataType={2}
              id="aiSelector"
              showEventList={showEventList}
            ></Dropdown>
          ) : null}
        </div>

        {showEventList ? <Btn onClick={resetSearch}>重選</Btn> : renderSearchBtn()}
      </TypeSelectRow>

      {/* {console.log('tempHistory', tempHistory)} */}
      {showEventList ? (
        <Fragment>
          <NavyLine />
          <Wrap ref={listWrapRef}>
            <WrapInner onScroll={(event) => (scrollEvent ? handleScrollDown(event.target) : null)}>
              {tempHistory.length && isHistoryType(tempHistory[0]) ? (
                <Fragment>
                  <EventListTable
                    tempHistory={tempHistory}
                    selectedDevice={selectedDevice}
                    selectedAI={selectedAI}
                    playThisVideo={playThisVideo}
                    getDeviceName={getDeviceName}
                    getNxRouting={getNxRouting}
                    getNxDuration={getNxDuration}
                    getIdForNxPlayback={getIdForNxPlayback}
                    setDownloadingModalShow={setDownloadingModalShow}
                  />
                  {scrollEvent ? null : <LoadingIcon></LoadingIcon>}
                </Fragment>
              ) : loadingShow ? (
                <LoadingCustom loading={true} minHeight={`calc(100% + 50px)`} />
              ) : (
                <NoData>查無資料</NoData>
              )}
            </WrapInner>
          </Wrap>
          <NavyLine style={{ margin: '0 0 0 0' }} />
          <Bottom>
            <div style={dataResultTip}>
              {/* 總計有 {dataLengthCounter()} 筆查詢結果，目前顯示{' '} */}
              總計有 {totalCount} 筆查詢結果，目前顯示 {dataLengthCounter()} 筆{' '}
              {/* <div style={{ color: colorValue.warning, margin: '0 0 0 15px' }}>
                [上限為 300 筆]
              </div> */}
            </div>
            <Btn
              className={`${dataLengthCounter() ? '' : 'disable'}`}
              // onClick={()=>exportLogData()}
              onClick={() => handleExport()}
            >
              匯出
            </Btn>
          </Bottom>
        </Fragment>
      ) : null}
      {playerShow && targetEvent ? renderPlayerModal(targetEvent) : null}
      {dateConflictModalShow ? renderDateConflictModal() : null}
      {exportModalShow ? renderExportModal() : null}
      {downloadingModalShow ? renderDownloadingModal() : null}
    </EventListContainer>
  )
}
