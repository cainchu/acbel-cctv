import { useEffect, useState } from 'react'
import styled from 'styled-components'
import { colorValue } from '../../styles'
import { WrapTitle } from './DetectList'
import ModalTool from './ModalTool'
import Notice from './Notice'
import SwitchToggle from './SwitchToggle'
import { useAiRetentateSelector } from './../../hooks/useSelectAiRetentate'
import { useAiStstusSelector } from './../../hooks/useSelectAiStatus'
import {
  getAiStatusAction,
  updateCameraAiStatusAction,
  getAiRetentateAction,
  updateAiRetentateAction,
} from './../../store/saga'
import { useAppDispatch } from './../../store'
interface Props {
  selectedDevice: aiDeviceMenuType
  currentTab: string
  targetAiStatus?: aiStatusParam
  setTargetAiStatus: Function
}

const AiSwitchRow = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  border: 1px solid ${colorValue.border};
  position: relative;
  box-sizing: border-box;
  margin: 30px 0 0 0;
  padding: 30px 20px 20px 20px;
`

const PanelColumn = styled.div`
  width: 100%;
  height: 100%;
  border-right: 1px solid ${colorValue.border};
  display: flex;
  flex-direction: column;
  box-sizing: border-box;
  padding: 0 60px 20px 0;
  &:last-child {
    border-right: none;
    padding: 0 20px 0 0;
  }
`

const PanelRow = styled.div`
  width: 100%;
  display: flex;
  flex-grow: 1;
  box-sizing: border-box;
  padding: 0 0 0 20px;
`
const PanelRowInner = styled.div`
  width: 100%;
  border-bottom: 1px dashed ${colorValue.dashedBorder};
  display: flex;
  justify-content: space-between;
  box-sizing: border-box;
  padding: 10px 0 10px 0;
`
const SubTitleDecoCube = styled.div`
  width: 10px;
  height: 10px;
  background-color: #163e52;
  margin: 20px 15px 0 0;
`
const SubTitle = styled.div`
  color: ${colorValue.second};
  font-size: 22px;
  font-weight: bold;
  letter-spacing: 2.2px;
  white-space: nowrap;
  display: flex;
  justify-content: space-between;
  align-items: center;
`
const AiSwitchSnapShot = styled.div`
  width: 720px;
  aspect-ratio: 16 / 9;
  height: auto;
  border: 1px solid ${colorValue.border};
  background-color: rgba(0, 0, 0, 0.6);
  margin: 5px 0 0 0;
  background-size: cover;
  background-repeat: no-repeat;
  transition: 0.3s;
`
const SnapshotIcon = styled.div`
  width: 24px;
  height: 24px;
  background-image: url('/images/icon_snapshot_ai.svg');
  background-repeat: no-repeat;
  background-size: contain;
  cursor: pointer;
`

export default (props: Props) => {
  const { selectedDevice, currentTab, targetAiStatus, setTargetAiStatus } = props

  const { aiRetentateUrl } = useAiRetentateSelector()
  const { aiStatusList } = useAiStstusSelector()
  const dispatch = useAppDispatch()
  // console.log('aiRetentateUrl', aiRetentateUrl);
  const [snapshotModalShow, setSnapshotModalShow] = useState<Boolean>(false)
  const [aiRetentateSource, setAiRetentateSource] = useState<string>('')
  // console.log('targetAiStatus', targetAiStatus)

  useEffect(() => {
    if (aiStatusList.length && selectedDevice) {
      let tempList = aiStatusList.filter((item) => {
        return item.id === selectedDevice.id
      })
      setTargetAiStatus(tempList[0])
    }
  }, [aiStatusList, selectedDevice])

  useEffect(() => {
    if (selectedDevice && currentTab === 'AI') {
      // console.log('targetAiStatus', targetAiStatus)
      if (targetAiStatus?.ai_retentate) {
        getAiRetentateBackground(selectedDevice.id)
      }
    }
  }, [selectedDevice, targetAiStatus])

  useEffect(() => {
    // console.log('aiRetentateUrl', aiRetentateUrl)
    setAiRetentateSource(aiRetentateUrl)
  }, [aiRetentateUrl])

  const getAiRetentateBackground = (cameraID: string) => {
    dispatch(getAiRetentateAction({ cameraID }))
  }

  const updateAiRetentateBackground = (cameraID: string) => {
    dispatch(updateAiRetentateAction({ cameraID }))
    dispatch(getAiRetentateAction({ cameraID }))
  }

  const updateAiStatus = (behavior: string) => {
    if (!targetAiStatus) return
    let tempObj = { ...targetAiStatus }
    switch (behavior) {
      case 'switchAiFences':
        if (targetAiStatus.ai_fences) {
          tempObj.ai_fences = 0
        } else {
          tempObj.ai_fences = 1
        }

        break
      case 'switchAiRetentate':
        if (targetAiStatus.ai_retentate) {
          tempObj.ai_retentate = 0
        } else {
          tempObj.ai_retentate = 1
        }
        break
      default:
        break
    }
    // console.log('updateAiStatusData', tempObj);
    dispatch(updateCameraAiStatusAction(tempObj))
    dispatch(getAiStatusAction())
  }

  const renderSnapshotModal = () => {
    if (!selectedDevice) return
    return (
      <ModalTool
        modalShow={snapshotModalShow}
        modalCloseFunction={() => setSnapshotModalShow(false)}
        modalWidth={420}
        modalHeight={240}
        backgroundOpacity={0.7}
        background={'#13253A'}
        modalInnerBackground={'#00000075'}
        zIndex={12}
      >
        <Notice
          closeModal={() => setSnapshotModalShow(false)}
          action={() => updateAiRetentateBackground(selectedDevice.id)}
          noticeText="您是否要拍攝遺留物偵測背景?"
        />
      </ModalTool>
    )
  }
  return (
    <AiSwitchRow>
      <WrapTitle>AI - Switch</WrapTitle>
      <PanelColumn>
        {selectedDevice.ai && selectedDevice.ai[0] === '1' ? (
          <PanelRow>
            <SubTitleDecoCube></SubTitleDecoCube>
            <PanelRowInner style={{}}>
              <SubTitle>智能電子圍籬開關</SubTitle>
              {targetAiStatus ? (
                <SwitchToggle
                  defaultStatus={targetAiStatus.ai_fences === 1 ? true : false}
                  toggleDisable={false}
                  switchAction={() => updateAiStatus('switchAiFences')}
                ></SwitchToggle>
              ) : null}
            </PanelRowInner>
          </PanelRow>
        ) : null}
        {selectedDevice.ai && selectedDevice.ai[1] === '1' ? (
          <PanelRow>
            <SubTitleDecoCube></SubTitleDecoCube>
            <PanelRowInner style={{}}>
              <SubTitle>智能大型遺留物開關</SubTitle>
              {targetAiStatus ? (
                <SwitchToggle
                  defaultStatus={targetAiStatus.ai_retentate === 1 ? true : false}
                  toggleDisable={false}
                  switchAction={() => updateAiStatus('switchAiRetentate')}
                ></SwitchToggle>
              ) : null}
            </PanelRowInner>
          </PanelRow>
        ) : null}
      </PanelColumn>
      {selectedDevice.ai && selectedDevice.ai[1] === '1' ? (
        <div style={{ display: 'flex', flexDirection: 'column' }}>
          <div style={{ display: 'flex', justifyContent: 'space-between' }}>
            <div style={{ fontSize: '22px', color: colorValue.border }}>遺留物偵測背景設定</div>
            <SnapshotIcon onClick={() => setSnapshotModalShow(true)}></SnapshotIcon>
          </div>

          <AiSwitchSnapShot
            style={{
              backgroundImage: `url(${aiRetentateSource})`,
            }}
          ></AiSwitchSnapShot>
        </div>
      ) : null}
      {snapshotModalShow ? renderSnapshotModal() : null}
    </AiSwitchRow>
  )
}
