import { Fragment } from 'react';
import styled from 'styled-components';
import { colorValue } from '../../styles';
import SwitchToggle from './../Setting/SwitchToggle';

interface InnerModalToolProps {
  modalShow: Boolean;
  modalCloseFunction: Function;
  modalWidth: number;
  modalHeight: number | string;
  backgroundOpacity: number;
  background: string;
  modalInnerBackground: string;
  zIndex: number;
  children: React.ReactNode;
}

const InnerModalContainer = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  z-index: 10;
  position: absolute;
  top: 0;
  left: 0;
  &.container_show {
    opacity: 1;
    pointer-events: initial;
    transition: 0.3s;
  }
  &.container_hide {
    opacity: 0;
    pointer-events: none;
    transition: 0.3s;
  }
`;

const ModalInner = styled.div`
  display: flex;
  justify-content: center;
  align-content: center;
  z-index: 11;
  border-radius: 5px;
  &.modal_show {
    opacity: 1;
    transition: 0.3s;
  }
  &.modal_hide {
    opacity: 0;
    transition: 0.3s;
  }
`;
const Background = styled.div`
  position: absolute;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  background-color: rgba(0, 0, 0, 0.5);
  z-index: 10;
  cursor: initial;
`;

export default (props: InnerModalToolProps) => {
  const {
    modalShow,
    modalCloseFunction,
    modalWidth,
    modalHeight,
    backgroundOpacity,
    background,
    modalInnerBackground,
    zIndex,
    children,
  } = props;
  return (
    <InnerModalContainer
      className={`${modalShow ? 'component_show' : 'component_hide'}`}
      style={{ zIndex: zIndex ? zIndex : 10 }}
    >
      <ModalInner
        className={`modal_inner ${modalShow ? 'modal_show' : 'modal_hide'}`}
        style={{width:modalWidth, height:modalHeight, background:modalInnerBackground}}
      >
        {children}
      </ModalInner>
      <Background
        style={{ opacity: backgroundOpacity, background: background }}
        onClick={()=>modalCloseFunction()}
      ></Background>
    </InnerModalContainer>
  );
};
