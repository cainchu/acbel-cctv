import { Fragment, useRef, useEffect, useState } from 'react';
import styled from 'styled-components';
import { colorValue } from '../../styles';

interface NoticeProps {
  closeModal: Function;
  positiveAction: Function;
  negativeAction: Function;
  noticeText:string;
}

const NoticeContainer = styled.div`
  width:100%;
  height:100%;
  display:flex;
  flex-direction:column;
  justify-content:space-between;
  align-items;center;
  box-sizing:border-box;
  padding:30px;
`;
const InnerWrap = styled.div`
  width:100%;
  height:100%;
  display:flex;
  flex-direction:column;
  justify-content:space-between;
  align-items;center;
  border: solid 1px ${colorValue.border};
`;
const Header = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
`;
const HeaderText = styled.div`
  font-size: 22px;
  font-weight: 600;
  color: #ff5163;
`;
const IconNotice = styled.div`
  width: 22px;
  height: 22px;
  background-repeat: no-repeat;
  background-image: url('/images/icon_notice.svg');
  background-size: contain;
  background-position: center;
  cursor: pointer;
  margin: 0 10px;
`;

const Content = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  box-sizing: border-box;
  font-size: 22px;
  font-weight: bold;
`;
const Bottom = styled.div`
  width: 100%;
  height: 60px;
  display: flex;
  justify-content: center;
  align-items: center;
  box-sizing: border-box;
  // padding: 0 10px;
`;

const Btn = styled.div`
  width: 160px;
  min-height: 40px;
  color: #000;
  font-size: 24px;
  font-weight: bold;
  font-family: SegoeUI;
  border: 1px solid ${colorValue.second};
  background-color: #42c6fc;
  display: flex;
  justify-content: center;
  box-sizing: border-box;
  line-height: 40px;
  transition: 0.3s;
  cursor: pointer;
  margin: 0 10px 0 0;
  &:last-child {
    margin: 0;
  }
  &:hover {
    background-color: ${colorValue.second};
    color: #0c1a2a;
  }
  &.disable {
    background-color: #76a6c4;
    pointer-events: none;
    opacity: 0.5;
  }
`;
const flexSty = {
  display: 'flex',
  alignItems: 'center',
};

export default (props: NoticeProps) => {
  const { closeModal, positiveAction, negativeAction, noticeText } = props;

  const confirm = () => {
    closeModal();
    positiveAction();
  };

  const cancel = () => {
    closeModal();
    negativeAction();
  };

  return (
    <NoticeContainer>
      <InnerWrap>
        <Header>
          <IconNotice onClick={() => closeModal()}></IconNotice>
          <HeaderText>NOTICE</HeaderText>
        </Header>
        <Content>{noticeText}</Content>
        <Bottom>
          <div style={flexSty}>
            <Btn onClick={() => confirm()}>儲存</Btn>
            <Btn onClick={() => cancel()}>不儲存</Btn>
          </div>
        </Bottom>
      </InnerWrap>
    </NoticeContainer>
  );
};
