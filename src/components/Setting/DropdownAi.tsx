import { useState, Fragment, useEffect } from 'react'
import styled from 'styled-components'
import { colorValue } from '../../styles'
import { SubMenu } from './EventList'

interface DropDownProps {
  defaultText: string //預設input顯示提示字元
  itemList: (analysisType | aiDeviceMenuType)[] //傳入下拉選單之列表
  defaultUnFold?: Boolean
  readonly?: Boolean
  menuWidth: string
  menuHeight: string
  manuMargin?: string
  selectedItem?: analysisType | aiDeviceMenuType
  setSelectedItem: Function
  selectedType?: string
  setSelectedType?: Function
  dataType: number
  id?: string
  analysisType?: string
  showPolygonData:Boolean
  showAiSettingToggle:Boolean
  currentTab:string
}
const DropDownContainer = styled.div.attrs(
  (props: { menuWidth: string; manuMargin: string }) => props,
)`
  width: ${(props) => props.menuWidth};
  margin: ${(props) => (props.manuMargin ? props.manuMargin : '5px')};
`
const DefaultArea = styled.div.attrs((props: { menuHeight: string }) => props)`
  width: 100%;
  height: ${(props) => props.menuHeight};
  background-color: rgba(0, 0, 0, 0.8);
  color: #2a8ab6;
  display: flex;
  align-items: center;
  border: solid 1px #2a8ab6;
`
const ItemNameArea = styled.div.attrs((props: { menuHeight: string }) => props)`
  width: 100%;
  height: 100%;
  color: #2a8ab6;
  line-height: ${(props) => `calc(${props.menuHeight} - 2px)`};
  font-size: 14px;
  padding: 0 10px;
  overflow: hidden;
  white-space: nowrap;
  text-overflow: ellipsis;
`
const IconArea = styled.div.attrs((props: { menuHeight: string; readonly: Boolean }) => props)`
  width: ${(props) => props.menuHeight};
  height: ${(props) => `calc(${props.menuHeight} - 6px)`};
  display: flex;
  justify-content: center;
  align-items: center;
  border-left: 1px solid #2a8ab6;
  transition: 0.3s;
  cursor: ${(props) => (props.readonly ? 'initial' : 'pointer')};
`
const ArrowDown = styled.div.attrs((props: { unfold: Boolean; readonly: Boolean }) => props)`
  width: 0;
  height: 0;
  border-left: 5px solid transparent;
  border-right: 5px solid transparent;
  border-top: 5px solid #2a8ab6;
  transition: 0.3s;
  transform: ${(props) => (props.unfold ? 'scaleY(-1)' : 'unset')};
  cursor: ${(props) => (props.readonly ? 'initial' : 'pointer')};
`
const UnfoldArea = styled.div.attrs((props: { unfold: Boolean }) => props)`
  width: 100%;
  background-color: #0b1216;
  color: #2a8ab6;
  transition: 0.3s;
  overflow: hidden;
  transform: scaleY(${(props) => (props.unfold ? 1 : 0)});
  transform-origin: top;
  margin-top: 6px;
  border: solid 1px #2a8ab6;
  box-sizing: border-box;
  padding: 5px;
  max-height: 700px;
  overflow-y: scroll;
`
const EachItem = styled.div.attrs((props: { menuHeight: string }) => props)`
  width: 100%;
  height: ${(props) => props.menuHeight};
  color: #2a8ab6;
  transition: 0.3s;
  line-height: ${(props) => props.menuHeight};
  cursor: pointer;
  padding: 0 10px;
  overflow: hidden;
  white-space: nowrap;
  text-overflow: ellipsis;
  font-size: 14px;
  &:hover {
    background-color: #2a8ab6;
    color: #0b1216;
  }
  &.disable {
    color: #2a8ab650;
    cursor: initial;
    &:hover {
      background-color: unset;
      color: #2a8ab650;
    }
  }
`

export default (props: DropDownProps) => {
  const {
    defaultText,
    itemList,
    defaultUnFold,
    readonly,
    menuWidth,
    menuHeight,
    manuMargin,
    selectedItem,
    setSelectedItem,
    setSelectedType,
    dataType,
    id,
    showPolygonData,
    showAiSettingToggle,
    currentTab
  } = props

  const [showMenu, setShowMenu] = useState<Boolean>(defaultUnFold ? defaultUnFold : false)

  useEffect(() => {
    if (showMenu) {
      if(currentTab === 'ROI') {
        if (showPolygonData) {
          setShowMenu(false)
        } else {
          setShowMenu(true)
        }

      } else if(currentTab === 'AI') {
        if (showAiSettingToggle) {
          setShowMenu(false)
        } else {
          setShowMenu(true)
        }
      }
    }
  }, [showPolygonData, showAiSettingToggle])


  useEffect(() => {
    if (defaultUnFold && !selectedItem) {
      setShowMenu(true)
    }
  }, [selectedItem])

  const selectItem = (item: analysisType | aiDeviceMenuType, menuType?: string) => {
    setSelectedItem(item)
    setShowMenu(false)
    setSelectedType ? setSelectedType(menuType) : null
  }

  function isAiDevice(item: analysisType | aiDeviceMenuType): item is aiDeviceMenuType {
    return (item as aiDeviceMenuType).title !== undefined
  }

  function isAnalysisType(item: analysisType | aiDeviceMenuType): item is analysisType {
    return (item as analysisType).title !== undefined
  }

  const renderItemName = () => {
    let displayName = ''
    switch (dataType) {
      case 1:
        if (selectedItem) {
          if (isAiDevice(selectedItem)) {
            displayName = selectedItem.title
          } else {
            displayName = defaultText
          }
        } else {
          displayName = defaultText
        }

        break
      case 2:
        if (selectedItem) {
          if (isAnalysisType(selectedItem)) {
            if (selectedItem.enable) {
              displayName = selectedItem.title
            } else {
              displayName = defaultText
            }
          } else {
            displayName = defaultText
          }
        } else {
          displayName = defaultText
        }
        break
      default:
        displayName = defaultText
        break
    }
    return <ItemNameArea menuHeight={menuHeight}>{displayName}</ItemNameArea>
  }

  const renderMunu = () => {
    switch (dataType) {
      case 1:
        return (
          <UnfoldArea unfold={showMenu}>
            {itemList.map((item, key) => {
              if (isAiDevice(item)) {
                return (
                  <EachItem key={key} menuHeight={menuHeight} onClick={() => selectItem(item)}>
                    {item.title}
                  </EachItem>
                )
              }
            })}
          </UnfoldArea>
        )
        break
      case 2:
        if (itemList.length) {
          return (
            <UnfoldArea unfold={showMenu}>
              {itemList.map((item, key) => {
                if (isAnalysisType(item)) {
                  return (
                    <EachItem
                      key={key}
                      menuHeight={menuHeight}
                      onClick={() => (item.enable ? selectItem(item) : null)}
                      className={`${item.enable ? '' : 'disable'}`}
                    >
                      {item.title}
                    </EachItem>
                  )
                }
              })}
            </UnfoldArea>
          )
        }
        break
      default:
        break
    }
  }

  return (
    <DropDownContainer menuWidth={menuWidth} manuMargin={manuMargin} id={id}>
      <DefaultArea
        menuHeight={menuHeight}
        onClick={() => (readonly ? null : setShowMenu(!showMenu))}
      >
        {renderItemName()}
        <IconArea menuHeight={menuHeight} readonly={readonly}>
          <ArrowDown unfold={showMenu} readonly={readonly}></ArrowDown>
        </IconArea>
      </DefaultArea>
      {renderMunu()}
    </DropDownContainer>
  )
}
