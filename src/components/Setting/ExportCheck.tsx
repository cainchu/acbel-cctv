import { Fragment, useRef, useEffect, useState } from 'react';
import styled, { keyframes } from 'styled-components';
import { colorValue } from '../../styles';

interface Props {
  closeModal: Function;
  modifyFunc: Function;
  exportFormat: string;
  setExportFormat: Function;
  historyList: historyResult[];
  totalCount: number;
}

const Container = styled.div`
  width:100%;
  height:100%;
  display:flex;
  flex-direction:column;
  justify-content:space-between;
  align-items;center;
  box-sizing:border-box;
  padding:30px;
`;
const InnerWrap = styled.div`
  width:100%;
  height:100%;
  display:flex;
  flex-direction:column;
  justify-content:space-between;
  align-items;center;
  border: solid 1px ${colorValue.border};
`;
const Header = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
`;
const HeaderText = styled.div`
  font-size: 22px;
  font-weight: 600;
  color: #ff5163;
`;
const IconNotice = styled.div`
  width: 22px;
  height: 22px;
  background-repeat: no-repeat;
  background-image: url('./images/icon_notice.svg');
  background-size: contain;
  background-position: center;
  cursor: pointer;
  margin: 0 10px;
`;

const Content = styled.div`
  width: 100%;
  display: flex;
  justify-content: space-evenly;
  align-items: center;
  box-sizing: border-box;
  font-size: 22px;
  font-weight: bold;
`;
const Bottom = styled.div`
  width: 100%;
  height: 60px;
  display: flex;
  justify-content: center;
  align-items: center;
  box-sizing: border-box;
`;
const RadioBtn = styled.input`
  width: 20px;
  height: 20px;
  cursor: pointer;
`;
const RadioLabel = styled.div`
  color: ${colorValue.second};
  font-size: 20px;
  height: 30px;
  margin: 0 10px 0 0;
`;

const Btn = styled.div`
  width: 160px;
  min-height: 40px;
  max-height: 40px;
  color: #000;
  font-size: 24px;
  font-weight: bold;
  font-family: SegoeUI;
  border: 1px solid ${colorValue.second};
  background-color: #42c6fc;
  display: flex;
  justify-content: center;
  box-sizing: border-box;
  line-height: 40px;
  transition: 0.3s;
  cursor: pointer;
  margin: 0 10px 0 0;
  position: relative;
  &:last-child {
    margin: 0;
  }
  &:hover {
    background-color: ${colorValue.second};
    color: #0c1a2a;
  }
  &.disable {
    background-color: #76a6c4;
    pointer-events: none;
    opacity: 0.5;
  }
`;
const RadioArea = styled.div`
  height: 30px;
  display: flex;
  align-items: center;
  justify-content: center;
  margin: 0 10px;
  cursor: pointer;
`;
const flexSty = {
  display: 'flex',
  alignItems: 'center',
};

const bounce = keyframes`
  0% {
    transform: translateY(0);
  }
  50% {
    transform: translateY(5px);
  }
  100% {
    transform: translateY(0);
  }
`;
const Dot = styled.div`
  width: 5px;
  height: 5px;
  // border-radius: 50%;
  background-color: #333;
  margin: 3px;
  animation: ${bounce} 1.2s linear infinite;
  &:nth-child(1) {
    animation-delay: 0s;
  }
  &:nth-child(2) {
    animation-delay: 0.3s;
  }
  &:nth-child(3) {
    animation-delay: 0.6s;
  }
`;

export default (props: Props) => {
  const {
    closeModal,
    modifyFunc,
    exportFormat,
    setExportFormat,
    historyList,
    totalCount,
  } = props;

  const modifyConfirm = () => {
    closeModal();
    modifyFunc();
  };

  const cancel = () => {
    closeModal();
    setExportFormat('');
  };

  return (
    <Container>
      <InnerWrap>
        <Header>
          <IconNotice onClick={() => closeModal()}></IconNotice>
          <HeaderText>Export Format</HeaderText>
        </Header>
        <Content>
          <RadioArea>
            <RadioLabel>JSON</RadioLabel>
            <RadioBtn
              type="radio"
              name="format"
              value="json"
              onChange={(e) => setExportFormat(e.target.value)}
            ></RadioBtn>
          </RadioArea>
          <RadioArea>
            <RadioLabel>CSV</RadioLabel>
            <RadioBtn
              type="radio"
              name="format"
              value="csv"
              onChange={(e) => setExportFormat(e.target.value)}
            ></RadioBtn>
          </RadioArea>
        </Content>

        <Bottom>
          <div style={flexSty}>
            <Btn
              className={`${
                exportFormat && historyList.length >= totalCount
                  ? ''
                  : 'disable'
              }`}
              onClick={() => modifyConfirm()}
            >
              {historyList.length >= totalCount ? (
                '下載'
              ) : (
                <div style={{ display: 'flex', alignItems: 'center' }}>
                  <div style={{ marginRight: '5px' }}>載入</div>
                  <Dot></Dot>
                  <Dot></Dot>
                  <Dot></Dot>
                </div>
              )}
            </Btn>
            <Btn
              className={`${historyList.length >= totalCount ? '' : 'disable'}`}
              onClick={() => cancel()}
            >
              取消
            </Btn>
          </div>
        </Bottom>
      </InnerWrap>
    </Container>
  );
};
