import { useState, useRef, useEffect } from 'react'
import styled from 'styled-components'
import { colorValue } from '../../styles'
import { BroadcastItem } from './Broadcast'
import ModalTool from './ModalTool'
import UpdateApiNotice from './UpdateApiNotice'
import { updateBroadcastAction, getBroadcastAction } from './../../store/saga'
import { useAppDispatch } from './../../store'
interface BroadcastInputProps {
  broadcastItem: BroadcastItem
  tempBroadcastList: BroadcastItem[]
  setTempBroadcastList: Function
  broadcastTargetType: string
}

const InputContainer = styled.div`
  // width: calc(100% - 0px);
  height: 100%;
  display: flex;
  justify-content: flex-end;
`
const InputArea = styled.input`
  width: 900px;
  height: 30px;
  background-color: rgba(0, 0, 0, 0.8);
  color: #2a8ab6;
  display: flex;
  align-items: center;
  border: solid 1px ${colorValue.dashedBorder};
  outline: none;
`
const Btn = styled.div`
  width: 80px;
  height: 30px;
  color: #000;
  font-size: 22px;
  font-weight: 600;
  border: 1px solid #2a8ab6;
  background-color: #42c6fc;
  display: flex;
  justify-content: center;
  margin: 0 0 0 25px;
  line-height: 28px;
  transition: 0.3s;
  cursor: pointer;
  user-select: none;
  &:hover {
    background-color: #2a8ab6;
    color: #0c1a2a;
  }
  &.disable {
    background-color: #76a6c4;
    pointer-events: none;
    opacity: 0.5;
  }
`

const DeleteIcon = styled.div`
  width: 30px;
  height: 30px;
  background-image: url('./images/icon_delete_active.svg');
  background-repeat: no-repeat;
  background-size: 70%;
  background-position: center;
  cursor: pointer;
  transition: 0.3s;
  pointer-events: auto;
  &.disable {
    pointer-events: none;
    cursor: initial;
    background-image: url('./images/icon_delete_default.svg');
  }
`
const SaveIcon = styled.div`
  width: 28px;
  height: 30px;
  background-image: url('./images/icon_save_default.svg');
  background-repeat: no-repeat;
  background-size: 90%;
  background-position: center;
  cursor: initial;
  margin: 0 10px;
  transition: 0.3s;
  pointer-events: none;
  &.changed {
    background-image: url('./images/icon_save.svg');
    cursor: pointer;
    pointer-events: auto;
  }
`

export default (props: BroadcastInputProps) => {
  const { broadcastItem, tempBroadcastList, setTempBroadcastList, broadcastTargetType } = props
  const [tempItem, setTempItem] = useState<BroadcastItem>(broadcastItem)
  const [hasChanged, setHasChanged] = useState<boolean>(false)
  const [noticeModalShow, setNoticeModalShow] = useState<boolean>(false)
  const [behavior, setBehavior] = useState<string>('')
  const updateIconRef = useRef<HTMLInputElement>(null)
  const dispatch = useAppDispatch()

    

  useEffect(() => {
    setTempItem(broadcastItem)
  }, [broadcastItem])

  interface ChangeEventTarget extends EventTarget {
    value: string
  }
  const handleChange = (eventTarget: ChangeEventTarget) => {
    let modifiedItem = { ...tempItem }
    modifiedItem.apiUrl = eventTarget.value
    if(modifiedItem.apiUrl !== broadcastItem.apiUrl) {
      setHasChanged(true)
      setTempItem(modifiedItem)
    } else {
      setHasChanged(false)
      setTempItem(modifiedItem)
    }
  }

  const checkUpdate = () => {
    setBehavior('update')
    setNoticeModalShow(true)
  }

  const updateUrl = () => {
    dispatch(
      updateBroadcastAction({ eventId: tempItem.broadcastId, targetUrl: { url: tempItem.apiUrl } }),
    )
    setHasChanged(false)
    dispatch(getBroadcastAction())
  }

  const checkClear = () => {
    setBehavior('clear')
    setNoticeModalShow(true)
  }

  const clearUrl = () => {
    let modifiedItem = { ...tempItem }
    modifiedItem.apiUrl = ''
    setTempItem(modifiedItem)
    dispatch(
      updateBroadcastAction({
        eventId: modifiedItem.broadcastId,
        targetUrl: { url: modifiedItem.apiUrl },
      }),
    )
    dispatch(getBroadcastAction())
  }

  const closeNoticeModal = () => {
    setNoticeModalShow(false)
  }

  const renderNoticeModal = () => {
    return (
      <ModalTool
        modalShow={noticeModalShow}
        modalCloseFunction={() => closeNoticeModal()}
        modalWidth={420}
        modalHeight={240}
        backgroundOpacity={0.7}
        background={'#13253A'}
        modalInnerBackground={'#00000075'}
        zIndex={12}
      >
        <UpdateApiNotice
          closeModal={() => closeNoticeModal()}
          modifyFunc={() => (behavior === 'clear' ? clearUrl() : updateUrl())}
          behavior={behavior}
        />
      </ModalTool>
    )
  }

  return (
    <InputContainer>
      <InputArea
        placeholder={broadcastTargetType === 'ms' ? '請輸入API位址' : '請輸入E-mail'}
        value={tempItem.apiUrl ? tempItem.apiUrl : ''}
        onChange={(e) => handleChange(e.target)}
        spellCheck={false}
      ></InputArea>
      <SaveIcon
        className={`${hasChanged ? 'changed' : ''}`}
        ref={updateIconRef}
        onClick={() => (hasChanged ? checkUpdate() : null)}
      ></SaveIcon>
      <DeleteIcon
        className={`${broadcastItem.apiUrl ? '' : 'disable'}`}
        onClick={() => (broadcastItem.apiUrl ? checkClear() : null)}
      ></DeleteIcon>
      {noticeModalShow ? renderNoticeModal() : null}
    </InputContainer>
  )
}
