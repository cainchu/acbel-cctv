import { Fragment, useRef, useEffect, useState } from 'react';
import styled from 'styled-components';
import { colorValue } from '../../styles';
import AreaDrawer from '../../utils/AreaDrawer';
import { PolygonData } from './DetectList';

interface PolygonProps {
  polygonData: PolygonData;
  editable: boolean;
  setCurrentCoordinate: Function;
  currentColor?: string;
}

const PolygonContainer = styled.div`
  width: 100%;
  aspect-ratio: 16 / 9;
  height: auto;
  background-size: cover;
  background-repeat: no-repeat;
  display: flex;
  justify-content: center;
  align-items: center;
  cursor: pointer;
`;

export default (props: PolygonProps) => {
  const {
    polygonData,
    editable,
    setCurrentCoordinate,
    currentColor,
  } = props;
  const { roiColor, pos } = polygonData;
  const drawRef = useRef<HTMLDivElement>(null);
  const polygonRef = useRef<HTMLDivElement>(null);
  const [tempPos, setTempPos] = useState<number[][]>([]);

  const roundTo = ( num:number, decimal:number ) => { return Math.round( ( num + Number.EPSILON ) * Math.pow( 10, decimal ) ) / Math.pow( 10, decimal ); }

  useEffect(() => {
    const container = drawRef.current;
    const polygonRect = polygonRef.current?.getBoundingClientRect();
    if (!container || !polygonRect) throw new Error('no viewer');
    const { width, height } = polygonRect;
    AreaDrawer.selectColor = '#f00'; //預設選取色 選取後按鍵盤del可刪除
    const calcCoordinate = (coordinate: number[][]) => {
      let modified = coordinate.map((coorItem) => {
        return { x: roundTo(coorItem[0] * width, 6), y: roundTo(coorItem[1] * height, 6) };
      });
      return modified;
    };
    const areaDrawer = new AreaDrawer(
      {
        container, //容器 區域可繪製
        width: width, //size
        height: height,
      },
      {
        fill: (currentColor ? currentColor : roiColor) + '80', //區域色
        stroke: currentColor ? currentColor : roiColor, //線條色
        max: 10,
        strokeWidth: width * 0.01,
      },
      editable, //可編輯
      (points) => {
        // console.log('🖨️', points); //變動觸發輸出
        let pointsRatio = points.map((pointItem) => {
          return [roundTo(pointItem.x / width, 6), roundTo(pointItem.y / height, 6)];
        });
        setCurrentCoordinate(pointsRatio);
        setTempPos(pointsRatio);
      },
    );
    /* 填入建立區域 給空陣列可清除 */
    if (tempPos.length) {
      areaDrawer.drawPath(calcCoordinate(tempPos));
    } else {
      areaDrawer.drawPath(calcCoordinate(pos));
    }

    return () => {
      areaDrawer.unload(); //退出記得銷毀事件
    };
  }, [currentColor]);
  return (
    <PolygonContainer ref={polygonRef}>
      <div
        css={`
          position: relative;
          width: 100%;
          height: 100%;
        `}
      >
        <div
          ref={drawRef}
          css={`
            position: absolute;
            top: 0;
            left: 0;
          `}
        ></div>
      </div>
    </PolygonContainer>
  );
};
