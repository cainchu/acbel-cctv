import { Fragment } from 'react';
import styled from 'styled-components';
import { colorValue } from '../../styles';
import SwitchToggle from './../Setting/SwitchToggle';

interface ModalToolProps {
  modalShow: Boolean;
  modalCloseFunction: Function;
  modalWidth: number;
  modalHeight: number | string;
  backgroundOpacity: number;
  background: string;
  modalInnerBackground: string;
  zIndex: number;
  children: React.ReactNode;
}

const ModalContainer = styled.div`
  width: 100vw;
  height: 100vh;
  display: flex;
  justify-content: center;
  align-items: center;
  z-index: 10;
  position: fixed;
  top: 0;
  left: 0;
  &.container_show {
    opacity: 1;
    pointer-events: initial;
    transition: 0.3s;
  }
  &.container_hide {
    opacity: 0;
    pointer-events: none;
    transition: 0.3s;
  }
`;

const ModalInner = styled.div`
  display: flex;
  justify-content: center;
  align-content: center;
  z-index: 11;
  border-radius: 5px;
  &.modal_show {
    opacity: 1;
    transition: 0.3s;
  }
  &.modal_hide {
    opacity: 0;
    transition: 0.3s;
  }
`;
const Background = styled.div`
  position: fixed;
  left: 0;
  top: 0;
  width: 100vw;
  height: 100vh;
  background-color: rgba(0, 0, 0, 0.95);
  z-index: 10;
  cursor: initial;
`;

export default (props: ModalToolProps) => {
  const {
    modalShow,
    modalCloseFunction,
    modalWidth,
    modalHeight,
    backgroundOpacity,
    background,
    modalInnerBackground,
    zIndex,
    children,
  } = props;
  return (
    <ModalContainer
      className={`${modalShow ? 'component_show' : 'component_hide'}`}
      style={{ zIndex: zIndex ? zIndex : 10 }}
    >
      <ModalInner
        className={`modal_inner ${modalShow ? 'modal_show' : 'modal_hide'}`}
        style={{width:modalWidth, height:modalHeight, background:modalInnerBackground}}
      >
        {children}
      </ModalInner>
      <Background
        style={{ opacity: backgroundOpacity, background: background }}
        onClick={()=>modalCloseFunction()}
      ></Background>
    </ModalContainer>
  );
};
