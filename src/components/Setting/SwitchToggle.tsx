import { useState, ChangeEventHandler, useEffect, Fragment } from 'react';
import styled from 'styled-components';
import axios from 'axios';
import { colorValue } from '../../styles';
interface SwitchToggleProps {
  switchAction: Function; //API
  defaultStatus: Boolean; //開關狀態預設是 ON 或 OFF
  toggleDisable?: Boolean; //是否有權限控制該項目，若有則true
}

const SwitchToggleContainer = styled.div.attrs(
  (props: { toggleDisable: Boolean }) => props,
)`
  display: flex;
  align-items: center;
  justify-content: flex-start;
  opacity:${(props)=>props.toggleDisable ? '0.5':'unset'}
`;

const ToggleWrap = styled.div.attrs(
  (props: { toggleOn: Boolean; toggleDisable: Boolean }) => props,
)`
  width: 42px;
  height: 20px;
  display: flex;
  align-items: center;
  justify-content:flex-end;
  border-radius: 20px;
  background-color: ${(props) =>
    !props.toggleOn || props.toggleDisable ? '#bbbbbb' : colorValue.title};
  border: ${(props) =>
    !props.toggleOn || props.toggleDisable
      ? '4px solid #d2d2d2'
      : '4px solid #0088ff'};
  cursor: ${(props)=> props.toggleDisable ? 'not-allowed':'pointer'};
`;

const ToggleButton = styled.div.attrs((props: { toggleOn: Boolean }) => props)`
  width: 15px;
  height: 15px;
  border-radius: 50%;
  background-color: #fff;
  transition: 0.3s;
  transform: ${(props) =>
    props.toggleOn ? 'translateX(1px)' : 'translateX(-20px)'};
  pointer-events: none;
`;

const StatusText = styled.div.attrs((props: { toggleOn: Boolean, toggleDisable: Boolean }) => props)`
  width: 30px;
  display: flex;
  justify-content: center;
  transition: 0.3s;
  color: ${(props) => (!props.toggleOn || props.toggleDisable ? '#163e52' : '#2f99dd')};
  font-size: 14px;
  margin-left: 10px;
`;

export default (props: SwitchToggleProps) => {
  const { switchAction, defaultStatus, toggleDisable } = props;
  const [toggleOn, setToggleOn] = useState<Boolean>(defaultStatus);

  const handleSwitch = () => {
    switchAction();
    setToggleOn(!toggleOn);
  };

  return (
    <SwitchToggleContainer toggleDisable={toggleDisable}>
      <ToggleWrap
        onClick={() => (toggleDisable ? null : handleSwitch())}
        toggleOn={toggleOn}
        toggleDisable={toggleDisable}
      >
        <ToggleButton toggleOn={toggleOn}></ToggleButton>
      </ToggleWrap>
      <StatusText toggleOn={toggleOn}>{toggleOn ? 'ON' : 'OFF'}</StatusText>
    </SwitchToggleContainer>
  );
};
