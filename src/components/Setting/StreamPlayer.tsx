import styled from 'styled-components';
import { colorValue } from '../../styles';

interface StreamPlayerProps {
  closeModal: Function;
  deviceName: string;
  streamUrl: string;
}

const StreamPlayerContainer = styled.div`
  width:100%;
  height:100%;
  display:flex;
  flex-direction:column;
  justify-content:space-between;
  align-items;center;
  border: solid 3px ${colorValue.border}
`;
const InnerWrap = styled.div`
  width:100%;
  height:100%;
  display:flex;
  flex-direction:column;
  justify-content:space-between;
  align-items;center;
  padding:3px;
`;
const Header = styled.div`
  width: 100%;
  color: ${colorValue.second};
  background-color: ${colorValue.background};
  height: 30px;
  line-height: 30px;
  display: flex;
  justify-content: space-between;
  align-items: center;
  box-sizing: border-box;
  padding: 0 3px 0 3px;
`;
const StreamName = styled.div`
  font-size: 14px;
  font-weight: 500;
`;
const IconClose = styled.div`
  width: 26px;
  height: 26px;
  background-repeat: no-repeat;
  background-image: url('./images/icon_win_close_b.svg');
  background-size: contain;
  background-position: center;
  cursor: pointer;
`;
const IconCamera = styled.div`
  width: 30px;
  height: 30px;
  background-repeat: no-repeat;
  background-image: url('./images/icon_camera_b.svg');
  background-size: contain;
  background-position: center;
  cursor: pointer;
  margin: 0 10px 0 0;
`;
const PlayArea = styled.div`
  width: 100%;
  aspect-ratio: 16 / 9;
  height: auto;
  background-size: cover;
  background-repeat: no-repeat;
  display: flex;
  justify-content: center;
  align-items: center;
  margin: 3px 0 0 0;
`;

export default (props: StreamPlayerProps) => {
  const { closeModal, deviceName, streamUrl } = props;

  return (
    <StreamPlayerContainer>
      <InnerWrap>
        <Header>
          <div style={{ display: 'flex' }}>
            <IconCamera></IconCamera>
            <StreamName>{deviceName}</StreamName>
          </div>
          <IconClose onClick={() => closeModal()}></IconClose>
        </Header>
        <PlayArea>
          <video
            autoPlay
            // muted
            crossOrigin="anonymous"
            loop
            style={{ width: '100%', height: '100%', objectFit: 'cover' }}
            controls={true}
          >
            <source src={streamUrl} type="video/mp4" />
          </video>
        </PlayArea>
      </InnerWrap>
    </StreamPlayerContainer>
  );
};
