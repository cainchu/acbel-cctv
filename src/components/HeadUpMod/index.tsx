import styled from 'styled-components'
import { colorValue } from '../../styles'

const Layout = styled.div`
  position: absolute;
  left: 29px;
  top: 31px;
  width: 502px;
  height: 150px;
  padding: 60px 10px;
  background-image: ${({ bn }: { bn: boolean }) => `url(/images/event_title_noarrow.svg)`};
  font-size: 26px;
  font-weight: bold;
  line-height: 1.27;
  letter-spacing: 5.2px;
  text-align: center;
  color: ${colorValue.light};
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
`

interface AreaBtnProp {
  width: number
  height: number
  left: number
  top: number
}

const UpBtn = styled.div`
  position: absolute;
  background-image: url(/images/event_title_arrowup.svg);
  width: 122px;
  height: 32px;
  left: 175px;
  top: -55px;
  cursor: pointer;
`

const DownBtn = styled.div`
  position: absolute;
  background-image: url(/images/event_title_arrowdown.svg);
  width: 122px;
  height: 32px;
  left: 175px;
  top: 55px;
  cursor: pointer;
`

interface Prop {
  title: string
  onUp?: () => void
  onDown?: () => void
  up: boolean
  down: boolean
}

export default ({ title, onUp, onDown, up, down }: Prop) => {
  return (
    <Layout bn={!!onUp}>
      <div
        css={`
          position: relative;
        `}
      >
        {title}
        {up && onUp && <UpBtn onClick={onUp} />}
        {down && onDown && <DownBtn onClick={onDown} />}
      </div>
    </Layout>
  )
}
