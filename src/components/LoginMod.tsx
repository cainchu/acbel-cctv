import { useRef } from 'react';
import { colorValue } from '../styles';

interface Prop {
  style: React.CSSProperties;
  errorMessage?: string;
  onLogin: (name: string, pass: string) => void;
}

const bdStyle: React.CSSProperties = {
  width: '478px',
  // height: '295px',
  height: '400px',
  padding: '50px 0px',
  textAlign: 'center',
  fontSize: '20px',
  // backgroundColor: 'rgba(255, 255, 255,0.5)',
  backgroundImage: 'url(/images/login_border.svg)',
  backgroundRepeat: 'no-repeat',
};
const label: React.CSSProperties = {
  margin: '4px 6px',
  fontSize: '20px',
  fontWeight: 'bold',
  lineHeight: 1.45,
  letterSpacing: '3.2px',
  color: '#42c6fc',
};
const inpSty = {
  width: '360px',
  height: '36px',
  border: 'solid 1px #2a8ab6',
  color: '#4ed2ee',
};
const btnSty = {
  width: '360px',
  height: '36px',
  padding: '2px 127px',
  marginTop: '16px',
  backgroundColor: '#42c6fc',
  border: 'none',

  fontSize: '26px',
  fontWeight: 'bold',
  lineHeight: 1.19,
  letterSpacing: '2.6px',
  color: '#000',
};
export default ({ style, onLogin, errorMessage }: Prop) => {
  const nameInpRef = useRef<HTMLInputElement>(null);
  const passInpRef = useRef<HTMLInputElement>(null);
  const onSubmit = () => {
    if (!nameInpRef.current || !passInpRef.current)
      throw new Error('no login ui!');
    onLogin(nameInpRef.current?.value, passInpRef.current?.value);
  };
  return (
    <div style={{ ...style, ...bdStyle }}>
      <div>
        <div style={label}>USER NAME</div>
        <input
          style={inpSty}
          ref={nameInpRef}
          type="text"
          autoFocus
          placeholder="請輸入帳號"
        />
      </div>
      <div>
        <div style={{ ...label }}>PASSWORD</div>
        <input
          style={inpSty}
          ref={passInpRef}
          type="password"
          placeholder="請輸入密碼"
        />
      </div>
      <button style={btnSty} onClick={onSubmit}>
        LOGIN
      </button>
      <div
        style={{
          fontFamily: 'SegoeUI',
          fontSize: '18px',
          fontWeight: 'bold',
          lineHeight: 1.67,
          letterSpacing: '1.8px',
          textAlign: 'center',
          color: colorValue.warn,
          marginTop: '40px',
        }}
      >
        {errorMessage}
      </div>
    </div>
  );
};
