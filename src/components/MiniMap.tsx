import { useMemo } from 'react'
import styled from 'styled-components'
const MiniMapStyle = styled.div.attrs((props: { pos: string }) => props)`
  position: absolute;
  top: ${(props) => (props.pos === 'a' ? `215px` : `30px`)};
  left: ${(props) => (props.pos === 'a' ? `30px` : `560px`)};
`

type Props = {
  floor?: string
}

export default ({ floor }: Props) => {
  const pos = useMemo(() => (floor ? floor.charAt(0) : 'a'), [floor])
  const imagePath = useMemo(
    () => (floor ? `floorplan_${floor}_fire` : 'floorplan_a1_hass'),
    [floor],
  )
  return (
    <MiniMapStyle pos={pos}>
      <img src={`/images/mini/${imagePath}.png`} alt="" />
    </MiniMapStyle>
  )
}
