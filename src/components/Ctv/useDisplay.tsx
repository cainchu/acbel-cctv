import { useEffect, useState } from 'react'
import { useAppSelector } from '../../store'

export default (isFull: boolean) => {
  const fullKey = useAppSelector((store) => store.vdos.fullKey) //決定是否滿版的key -1 以外代表滿版
  const [display, setDisplay] = useState('block')
  useEffect(() => {
    if (fullKey === -1) {
      if (isFull) {
        setDisplay('none')
        return
      } else {
        setDisplay('block')
        return
      }
    } else {
      if (isFull) {
        setDisplay('block')
        return
      } else {
        setDisplay('none')
        return
      }
    }
  }, [fullKey, isFull])
  return display
}
