import styled from 'styled-components'
import { colorValue } from '../../styles'

const PtzLayout = styled.div`
  position: absolute;
  right: 8px;
  top: 10px;
  padding: 2px 14px;
  font-size: 20px;
  font-weight: bold;
  line-height: 1.35;
  letter-spacing: 2px;
  color: #fff;
  background: #000;
  border-radius: 15px;
  cursor: pointer;
  &:hover {
    color: ${colorValue.main};
  }
`
export default ({ onClick }: { onClick?: () => void }) => {
  return <PtzLayout onClick={onClick}>PTZ</PtzLayout>
}
