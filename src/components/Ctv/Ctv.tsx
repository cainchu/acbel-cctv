import { useRootSelector } from '../../hooks/useSelect'
import { useEffect, useRef } from 'react'
import styled from 'styled-components'
import { useEventListener } from 'usehooks-ts'
import useVdoLifeCycle from '../../hooks/useVdoLifeCycle'
import { colorValue } from '../../styles'
import CtvAlert from './CtvAlert'
import TilBar from './TilBar'
import Tv from './Tv'
import useDisplay from './useDisplay'
import { camerasGetterById } from '../../store/root'
import ErrorBoundary from '../ErrorBoundary'
import { generatePtzUrl } from '../../utils/tools'

export type WarnType = {
  warn?: boolean
  special?: boolean
}

const Layout = styled.div`
  position: relative;
  width: 100%;
  height: 100%;
  background-color: #000;
  border: solid 2px ${({ warn }: WarnType) => (warn ? colorValue.warn : colorValue.border)};
`

export interface Prop {
  meta: I_CtvMeta
  onClose?: (meta: I_CtvMeta) => void
  onFull?: (meta: I_CtvMeta) => void
  isFull?: boolean
  isSpecial?: boolean
  noDragIcon?: boolean
  onTouch?: (meta: I_CtvMeta) => void
}

export default ({ meta, onClose, onFull, isFull, noDragIcon, onTouch, isSpecial }: Prop) => {
  const elRef = useRef(null)
  useEventListener(
    'dragstart',
    (e) => {
      e.dataTransfer?.setData('text/plain', JSON.stringify(meta))
    },
    elRef,
  )
  useVdoLifeCycle(meta)
  const display = useDisplay(isFull || false) //不顯示就隱藏的處理

  const { cameras } = useRootSelector()

  //打開攝影機的PTZ頁面
  const onPtzClick = (data: I_CtvMeta) => {
    const _camera = camerasGetterById(cameras, data.cameraId || '')
    if (_camera) {
      const { info } = _camera
      info?.ip && window.open(generatePtzUrl(info?.ip || ''), 'ptz')
    }
  }

  return (
    <Layout
      draggable={!isFull}
      ref={elRef}
      style={{ display }}
      warn={meta?.warnTypes.length > 0}
      onMouseDown={() => onTouch && onTouch(meta)}
    >
      <TilBar
        meta={meta}
        onClose={onClose}
        onFull={onFull}
        isFull={isFull}
        isSpecial={isSpecial}
        noDragIcon={noDragIcon}
      ></TilBar>
      <ErrorBoundary>
        <Tv
          suuid={meta.cameraId}
          ptz={meta.ptz}
          onPtzClick={() => onPtzClick && onPtzClick(meta)}
          sos={meta.sos}
          mutedOff={!isFull}
        ></Tv>
      </ErrorBoundary>
      <CtvAlert types={meta?.warnTypes || []} />
    </Layout>
  )
}
