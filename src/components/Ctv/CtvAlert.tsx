import { method, multi } from '@arrows/multimethod'
import styled from 'styled-components'
import { AlarmType } from '../../utils/AlarmSystem'

type Prop = {
  types: string[]
}

const alertMessage = multi(
  (type: string) => type,
  method(AlarmType.FENCES, () => {
    return <img src="/images/alarm_invasion_event.svg" alt="" key={AlarmType.FENCES} />
  }),
  method(AlarmType.NO_CLEAR, () => {
    return <img src="/images/alarm_noclear_event.svg" alt="" key={AlarmType.NO_CLEAR} />
  }),
  method(AlarmType.RETENTETE, () => {
    return <img src="/images/alarm_remnants_event.svg" alt="" key={AlarmType.RETENTETE} />
  }),
  method(AlarmType.FIRE, () => {
    return <img src="/images/alarm_fire_event.svg" alt="" key={AlarmType.FIRE} />
  }),
  method(AlarmType.PAIR, () => {
    return <img src="/images/alarm_dock_event.svg" alt="" key={AlarmType.PAIR} />
  }),
  method(AlarmType.HASS, () => {
    return <img src="/images/alarm_gas_event.svg" alt="" key={AlarmType.HASS} />
  }),
  method(AlarmType.DOOR, () => {
    return <img src="/images/alarm_hanging_event.svg" alt="" key={AlarmType.DOOR} />
  }),
  method(AlarmType.SOS, () => {
    return <img src="/images/alarm_sos_event.svg" alt="" key={AlarmType.SOS} />
  }),
  method(AlarmType.BURN_1, () => {
    return <img src="/images/alarm_gasno1_event.svg" alt="" key={AlarmType.BURN_1} />
  }),
  method(AlarmType.BURN_2, () => {
    return <img src="/images/alarm_gasno2_event.svg" alt="" key={AlarmType.BURN_2} />
  }),
  method(AlarmType.BURN_3, () => {
    return <img src="/images/alarm_gasno3_event.svg" alt="" key={AlarmType.BURN_3} />
  }),
  method(AlarmType.BURN_4, () => {
    return <img src="/images/alarm_gasno4_event.svg" alt="" key={AlarmType.BURN_4} />
  }),
  method(AlarmType.BURN_5, () => {
    return <img src="/images/alarm_gasno5_event.svg" alt="" key={AlarmType.BURN_5} />
  }),
  method(AlarmType.BURN_6, () => {
    return <img src="/images/alarm_gasno6_event.svg" alt="" key={AlarmType.BURN_6} />
  }),
  method(() => {
    return ''
  }),
)

const CtvAlertStyle = styled.div`
  position: absolute;
  left: 50%;
  top: 50%;
  width: 50%;
  transform: translate3D(-50%, -50%, 0);
  pointer-events: none;
`

export default ({ types }: Prop) => {
  return <CtvAlertStyle>{types.map((type) => alertMessage(type))}</CtvAlertStyle>
}
