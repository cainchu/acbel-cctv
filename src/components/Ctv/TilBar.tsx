import { useMemo } from 'react'
import styled from 'styled-components'
import { colorValue } from '../../styles'
import { Prop, WarnType } from './Ctv'

const Layout = styled.div`
  width: 100%;
  height: 26px;
  padding: 0px 2px;
  background-color: ${({ warn, special }: WarnType) =>
    warn ? '#2a0c0c' : special ? '#0d2308' : colorValue.background};
  color: ${({ warn, special }: WarnType) =>
    warn ? colorValue.warn : special ? colorValue.special : colorValue.title};
`
const Titel = styled.div`
  display: flex;
  align-items: stretch;
  justify-content: space-between;
  padding: 0 4px;
  cursor: move;
`
const TitelTxt = styled.div`
  margin-left: 4px;
  font-size: 16px;
  font-weight: 400;
  font-stretch: normal;
  font-style: normal;
  line-height: 26px;
  letter-spacing: normal;
`
const IconBtn = styled.div`
  width: 20px;
  margin-left: 4px;
  cursor: pointer;
  & img {
    cursor: pointer;
  }
`

const StretchLayout = styled.div`
  display: flex;
  align-items: stretch;
`

export default ({ meta, onClose, onFull, isFull, noDragIcon, isSpecial }: Prop) => {
  const { title, warnTypes } = meta
  const warn = useMemo(() => {
    return warnTypes.length > 0
  }, [warnTypes])

  return (
    <Layout warn={warn} special={isSpecial}>
      <Titel>
        <StretchLayout>
          <img width="24" src={`/images/icon_camera_${warn ? 'r' : isSpecial ? 'g' : 'b'}.svg`} />
          <TitelTxt>{title || 'No Signal'}</TitelTxt>
        </StretchLayout>
        <div
          css={`
            display: flex;
            align-items: stretch;
            justify-content: flex-end;
          `}
        >
          {!isFull ? (
            <>
              {noDragIcon || (
                <IconBtn>
                  <img width="18" src={`/images/icon_win_move_${warn ? 'r' : 'b'}.svg`} />
                </IconBtn>
              )}
              <IconBtn onClick={() => onFull && onFull(meta)}>
                <img
                  width="18"
                  src={`/images/icon_win_enlarge_${warn ? 'r' : isSpecial ? 'g' : 'b'}.svg`}
                />
              </IconBtn>
            </>
          ) : (
            ''
          )}
          {onClose && (
            <IconBtn onClick={() => onClose(meta)}>
              <img width="18" src={`/images/icon_win_close_${warn ? 'r' : 'b'}.svg`} />
            </IconBtn>
          )}
        </div>
      </Titel>
    </Layout>
  )
}
