import { useEffect, useRef, useState } from 'react'
import WTCVideo from 'cain-wtc-video'
import styled from 'styled-components'
import { downloadUrl } from '../../utils/tools'
import PtzBtn from './PtzBtn'
import { useBoolean } from 'usehooks-ts'
import SoundBtn from './SoundBtn/SoundBtn'

interface I_IconBtnProps {
  icon: string
  title: string
}

const IconBtn = styled.div.attrs((props: I_IconBtnProps) => ({
  title: props.title,
}))`
  width: 32px;
  height: 32px;
  background-color: #000;
  background-image: url(${(props: I_IconBtnProps) => props.icon});
  background-repeat: no-repeat;
  background-position: center center;
  border-radius: 50%;
  background-size: 30px;
  cursor: pointer;
`

interface I_IconSwatchBtnProps extends I_IconBtnProps {
  iconoff: string
  off?: boolean
  onClick?: () => void
}

const IconSwatchBtn = ({ icon, iconoff, title, off, onClick }: I_IconSwatchBtnProps) => {
  const click = () => {
    onClick && onClick()
  }
  return <IconBtn icon={off ? icon : iconoff} title={title} onClick={() => click()} />
}

const TVLayout = styled.div`
  position: absolute;
  left: 0;
  right: 0;
  bottom: 0;
  top: 26px;
  background-color: #000000;
`

const UiBtnsLayout = styled.div`
  position: absolute;
  right: 10px;
  bottom: 10px;
  display: flex;
`

const VdoBgLayout = styled.div`
  width: 100%;
  height: 100%;
  background-color: #000000;
  background-image: url(/images/no_singal.svg);
  background-repeat: no-repeat;
  background-size: cover;
`

interface I_WTCVideoRefProps {
  screenshot: () => Promise<HTMLCanvasElement>
}

export default ({
  suuid,
  ptz,
  onPtzClick,
  sos,
  mutedOff,
}: {
  suuid: string | undefined
  ptz?: boolean
  onPtzClick?: () => void
  sos?: boolean
  mutedOff?: boolean
}) => {
  const [isPlaying, setIsPlaying] = useState(false)
  const wTCVideoRef = useRef<I_WTCVideoRefProps>(null)
  const { value: muted, toggle: toggleMuted, setValue: setMuted } = useBoolean(true)
  useEffect(() => {
    sos && setMuted(!!mutedOff)
  }, [mutedOff, sos])

  /**
   * 如果视频元素存在，请对其进行截图并下载。
   */
  const onScreenshot = async () => {
    if (!wTCVideoRef.current) return
    const canvas = await wTCVideoRef.current.screenshot()
    downloadUrl(canvas.toDataURL('image/jpeg'), `${new Date()}.jpg`)
  }

  return (
    <TVLayout>
      <VdoBgLayout>
        {suuid && (
          <>
            <WTCVideo
              ref={wTCVideoRef}
              hidden={!isPlaying}
              suuid={suuid}
              onPlay={() => setIsPlaying(true)}
              muted={muted}
            ></WTCVideo>
            {ptz && <PtzBtn onClick={onPtzClick} />}
            {sos && <SoundBtn macId={suuid} />}
            <UiBtnsLayout>
              <IconBtn
                icon="/images/icon_snapshot.svg"
                title="截圖"
                css={`
                  margin-right: 3px;
                `}
                onClick={() => onScreenshot()}
              ></IconBtn>
              {sos && (
                <IconSwatchBtn
                  icon="/images/icon_audio_on.svg"
                  iconoff="/images/icon_audio_off.svg"
                  title="聲音"
                  off={!muted}
                  onClick={() => toggleMuted()}
                ></IconSwatchBtn>
              )}
            </UiBtnsLayout>
          </>
        )}
      </VdoBgLayout>
    </TVLayout>
  )
}
