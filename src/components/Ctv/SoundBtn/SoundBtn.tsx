import styled, { css } from 'styled-components'
import { useBoolean } from 'usehooks-ts'
import useNxTalk from '../../../hooks/useNxTalk'
import { MouseEvent, useEffect } from 'react'

type SoundBtnProps = {
  big?: boolean
  macId?: string
}

const sm = css`
  position: absolute;
  right: 8px;
  bottom: 50px;
`

const SoundBtnStyle = styled.div`
  ${({ big }: SoundBtnProps) => (big ? '' : sm)}
  & > * {
    cursor: pointer;
  }
`

export default ({ big, macId }: SoundBtnProps) => {
  const { host, protocol } = window.location
  const wsService = `${protocol === 'https:' ? 'wss' : 'ws'}://${host}/nx`
  const { value, setTrue, setFalse } = useBoolean(false)
  const { setCameraId, setSpeakerOff } = useNxTalk(wsService)
  useEffect(() => {
    setCameraId(macId || '')
  }, [macId])
  useEffect(() => {
    setSpeakerOff(value)
  }, [value])
  const onMouseDown = (e: MouseEvent) => {
    e.stopPropagation()
    e.preventDefault()
    setTrue()
  }
  return (
    <SoundBtnStyle
      onMouseDown={(e) => onMouseDown(e)}
      onMouseUp={setFalse}
      onMouseLeave={setFalse}
      big={big}
    >
      <img src={`/images/btn_speak_${value ? 'light' : 'dark'}.svg`} width={big ? 320 : 150} />
    </SoundBtnStyle>
  )
}
