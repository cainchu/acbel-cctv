import styled from 'styled-components';
import { colorValue, cssFlex } from '../../styles';

const Layout = styled.div`
  font-size: 18px;
  font-weight: bold;
  line-height: 1.11;
  letter-spacing: ${({ spacing }: { spacing: number }) => `${spacing}px`};
  color: ${colorValue.second};
  display: flex;
  ${cssFlex.verticalCenter}
  &:before {
    content: '';
    display: inline-block;
    width: 12px;
    height: 12px;
    opacity: 0.5;
    background-color: ${colorValue.second};
    margin-right: 13px;
  }
`;

const TilStyle = styled.span`
  display: inline-block;
  width: 140px;
  text-align: justfy;
`;

const TxtStyle = styled.span`
  font-size: 18px;
  font-weight: bold;
  line-height: 2.11;
  letter-spacing: 3.6px;
  color: ${colorValue.main};
`;

type Props = {
  title: string;
  children?: JSX.Element | string;
  spacing?: number;
};

export default ({ children, title, spacing }: Props) => {
  return (
    <Layout spacing={spacing || 0}>
      <TilStyle>{title}</TilStyle>
      <TxtStyle>{children || '__'}</TxtStyle>
    </Layout>
  );
};
