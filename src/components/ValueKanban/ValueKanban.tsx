import styled from 'styled-components'
import { colorValue, cssFlex } from './../../styles/index'
import KanbanHeader from './KanbanHeader'
import PointLinghtStatus from './PointLinghtStatus'
import PoItemLayout from './PoItemLayout'

const ValueKanbanLayout = styled.div`
  position: absolute;
  top: 0px;
  left: 0px;
  /* width: 790px; */
  width: 390px;
  height: 215px;
  border: solid 1px ${colorValue.light};
  background-color: #0000003c;
  ${cssFlex.center}
`

const Content = styled.div`
  width: 784px;
  height: 208px;
  background-color: #0c1a2a50;
`

const Line = styled.div`
  border-top: dashed 2px ${colorValue.dashedBorder};
  height: 0px;
  overflow: hidden;
`

export const PortStyle = styled.div`
  display: grid;
  grid-template-columns: 1fr;
  grid-gap: 23px;
`

export const PartMainStyle = styled.div`
  height: calc(100% - 30px);
  display: grid;
  grid-template-columns: ${({ repeat }: { repeat: number }) => `repeat(${repeat}, 1fr)`};
  grid-gap: 93px;
  padding: 25px 54px 39px 37px;
`

export default () => {
  return (
    <ValueKanbanLayout>
      <Content>
        <KanbanHeader>BURN-IN ROOM1</KanbanHeader>
        <PartMainStyle repeat={2}>
          <PortStyle>
            <PointLinghtStatus />
            <Line />
            <PoItemLayout title="稼動率" spacing={22}>
              5%
            </PoItemLayout>
          </PortStyle>
          <PortStyle>
            <PoItemLayout title="Cycle Time">15</PoItemLayout>
            <Line />
            <PoItemLayout title="Alarm Code">0</PoItemLayout>
          </PortStyle>
        </PartMainStyle>
      </Content>
    </ValueKanbanLayout>
  )
}
