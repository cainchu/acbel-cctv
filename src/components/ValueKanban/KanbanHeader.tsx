import styled from 'styled-components';
import { colorValue, cssFlex } from '../../styles';

const HeaderStyle = styled.div`
  height: 33px;
  border-top: solid 2px ${colorValue.border};
  border-bottom: solid 2px ${colorValue.border};
  padding: 0 6px;
  ${cssFlex.verticalCenter}
  font-size: 16px;
  line-height: 1.25;
  letter-spacing: 1.6px;
  color: ${colorValue.title};
  &:after {
    content: '';
    display: inline-block;
    width: 70px;
    height: 10px;
    background-image: url('/images/mark_right_top.svg');
    position: absolute;
    top: 8px;
    right: 5px;
  }
`;

const TxtStyle = styled.span`
  display: inline-block;
  margin-left: 6px;
  font-weight: bold;
  color: ${colorValue.main};
`;

type Props = {
  children: string;
};

export default ({ children }: Props) => {
  return (
    <HeaderStyle>
      DEVICE NAME :<TxtStyle>{children}</TxtStyle>
    </HeaderStyle>
  );
};
