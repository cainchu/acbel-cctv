import PoItemLayout from './PoItemLayout'

type Props = {
  off?: number
}

export default ({ off }: Props) => {
  return (
    <PoItemLayout title="燈號狀態" spacing={9}>
      {off === undefined ? (
        '__'
      ) : off === 1 ? (
        <img src="/images/icon_light_status_on.svg" width="20px"></img>
      ) : (
        <img src="/images/icon_light_status_off.svg" width="20px"></img>
      )}
    </PoItemLayout>
  )
}
