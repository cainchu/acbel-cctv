import styled from 'styled-components'
import { colorValue, cssFlex } from './../../styles/index'
import KanbanHeader from './KanbanHeader'
import PointLinghtStatus from './PointLinghtStatus'
import PoItemLayout from './PoItemLayout'
import { PartMainStyle, PortStyle } from './ValueKanban'

const ValueKanbanLayout = styled.div`
  position: absolute;
  top: 0px;
  left: 0px;
  /* width: 790px; */
  width: 390px;
  height: 215px;
  border: solid 1px ${colorValue.light};
  background-color: #0000003c;
  ${cssFlex.center}
`

const Content = styled.div`
  width: 784px;
  height: 208px;
  background-color: #0c1a2a50;
`

const Line = styled.div`
  border-top: dashed 2px ${colorValue.dashedBorder};
  height: 0px;
  overflow: hidden;
`

type Props = {
  title: string
  pointLinght?: number
  temperature?: number
}

export default ({ title, pointLinght, temperature }: Props) => {
  return (
    <ValueKanbanLayout>
      <Content>
        <KanbanHeader>{title}</KanbanHeader>
        <PartMainStyle repeat={1}>
          <PortStyle>
            <PointLinghtStatus off={pointLinght} />
            <Line />
            <PoItemLayout title="作業溫度" spacing={9}>
              {temperature ? `${temperature}℃` : '__'}
            </PoItemLayout>
          </PortStyle>
        </PartMainStyle>
      </Content>
    </ValueKanbanLayout>
  )
}
