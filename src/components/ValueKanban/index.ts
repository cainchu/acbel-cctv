import ValueKanban from './ValueKanban';
import WorkValueKanban_ from './WorkValueKanban';
import TemperatureKenban_ from './TemperatureKenban';

export default ValueKanban;

export const WorkValueKanban = WorkValueKanban_;
export const TemperatureKenban = TemperatureKenban_;
