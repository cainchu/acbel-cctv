import styled from 'styled-components'
import { cssFlex } from '../styles'

const FullMessageLayout = styled.div`
  position: absolute;
  left: 0;
  top: 0;
  width: 1920px;
  height: 1080px;
  background-color: #00000094;
  ${cssFlex.center}
`
const FullMessage = styled.div`
  min-width: 100px;
  min-height: 30px;
  padding: 15px;
  background-color: #fff;
  color: #2a2a2a;
  border-radius: 5px;
  cursor: pointer;
`

type Props = {
  children: string
}

export default ({ children }: Props) => {
  return (
    <FullMessageLayout>
      <FullMessage
        onClick={() => {
          location.reload()
        }}
      >
        {children}
      </FullMessage>
    </FullMessageLayout>
  )
}
