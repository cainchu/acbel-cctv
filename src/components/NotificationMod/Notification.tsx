import { useMemo } from 'react'
import styled from 'styled-components'
import { AlarmType } from '../../utils/AlarmSystem'
import { yyyy_mm_dd_h_m_s } from '../../utils/dateFormat'

const NotificationLayout = styled.div`
  width: 100%;
  height: 30px;
  margin: 0 0 3px 0;
  display: flex;
  align-items: stretch;
  cursor: pointer;
  opacity: ${({ disable }: { disable: boolean }) => (disable ? 0.4 : 1)};
  &:before {
    content: '';
    display: block;
    flex: 0 0 5px;
    background: #f00;
  }
  &:after {
    content: '';
    display: block;
    height: 100%;
    flex: 0 0 5px;
    background: #f00;
  }
`

type Props = {
  meta: I_NotificationMeta
  onClick?: (meta: I_NotificationMeta) => void
}

export default ({ meta, onClick }: Props) => {
  const { type, disable, id } = meta
  const message = useMemo(() => {
    switch (type) {
      case AlarmType.FENCES:
        return '重要區域人員入侵'
      case AlarmType.NO_CLEAR:
        return '人員未淨空事件'
      case AlarmType.RETENTETE:
        return '遺留物通報'
      case AlarmType.PAIR:
        return '碼頭靠站'
      case AlarmType.DOOR:
        return '吊掛門開啓事件'
      case AlarmType.FIRE:
        switch (id) {
          case 'a0':
            return 'A-B1火警通報事件'
          case 'ac':
            return 'A-B2火警通報事件'
          default:
            return `${id.toUpperCase()}火警通報事件`
        }
      case AlarmType.HASS: //棄用
        return `含氧量不足事件`
      case AlarmType.BURN_1:
        return `傳統燒機室-NO.1溫度過高事件`
      case AlarmType.BURN_2:
        return `傳統燒機室-NO.2溫度過高事件`
      case AlarmType.BURN_3:
        return `傳統燒機室-NO.3溫度過高事件`
      case AlarmType.BURN_4:
        return `傳統燒機室-NO.4溫度過高事件`
      case AlarmType.BURN_5:
        return `傳統燒機室-NO.5溫度過高事件`
      case AlarmType.BURN_6:
        return `傳統燒機室-NO.6溫度過高事件`
      case AlarmType.SOS:
        return `要求對講通話`
      default:
        return type
    }
  }, [type])

  return (
    <NotificationLayout disable={!!disable} onClick={() => onClick && onClick(meta)}>
      <div
        css={`
          display: flex;
          align-items: center;
          flex: 1 1 auto;
          margin: 0 0 0 10px;
          font-size: 22px;
          font-weight: 600;
          line-height: 1.5;
          color: #f00;
          overflow: hidden;
          text-overflow: ellipsis;
          white-space: nowrap;
          pointer-events: none;
        `}
      >
        {message}
      </div>
      <div
        css={`
          display: flex;
          align-items: center;
          flex: 0 0 135px;
          margin: 0 10px 0 0;
          font-size: 12px;
          line-height: 2;
          color: #f00;
          overflow: hidden;
          text-overflow: ellipsis;
          white-space: nowrap;
          pointer-events: none;
        `}
      >
        {yyyy_mm_dd_h_m_s(meta.time)}
      </div>
    </NotificationLayout>
  )
}
