import styled from 'styled-components'
// import usePocPushNotifcation from '../../hooks/usePocPushNotifcation'
import Notification from './Notification'
import { useRef } from 'react'
import useRollList from '../../hooks/useRollList'
import useRefEvent from '../../hooks/useRefEvent'

const Layout = styled.div`
  width: 100%;
  height: 230px;
  padding: 34px 3px 34px 5px;
  background-image: url(/images/border_event_alarm.svg);
`

type Props = {
  notifications: I_NotificationMeta[]
  toNotification: (meta: I_NotificationMeta) => void
}

/* 監視ref節點的hook */
export default ({ notifications, toNotification }: Props) => {
  // usePocPushNotifcation() // TODO: 告警測試 按下
  const elRef = useRef<HTMLDivElement>(null)
  const { viewlist, next } = useRollList(notifications, 5)
  const onWheel = (e: WheelEvent) => {
    next(e.deltaY > 0 ? 1 : -1)
  }
  useRefEvent<HTMLDivElement, WheelEvent>(elRef, 'wheel', onWheel)

  return (
    <Layout ref={elRef}>
      {viewlist.map((meta) => (
        <Notification meta={meta} key={meta.id + meta.time} onClick={toNotification}></Notification>
      ))}
    </Layout>
  )
}
