import { useEffect, useMemo } from 'react'
import styled from 'styled-components'
import { useCameraIdsByMenu, useVdosSelect } from '../../hooks/useSelect'
import { useAppDispatch } from '../../store'
import { fullAnVdoAction, monitorCustomAction } from '../../store/saga'
import Ctv from '../Ctv'
import GridLauout from './GridLauout'

type VodBoxProps = {
  area: string
  hidden?: boolean
}
const VodBox = styled.div`
  width: 100%;
  height: 100%;
  pointer-events: auto;
  grid-area: ${({ area }: VodBoxProps) => area};
  background: #00000087;
  visibility: ${({ hidden }: VodBoxProps) => (hidden ? 'hidden' : 'visible')};
`

type VodMapGridProps = {
  path: string
  ind: string
  specialInd?: number
}

export default ({ path, ind, specialInd }: VodMapGridProps) => {
  const dispatch = useAppDispatch()
  const cameraIds = useCameraIdsByMenu(path, ind)
  useEffect(() => {
    dispatch(monitorCustomAction(cameraIds))
  }, [cameraIds])

  const { vdoMetas, fullKey, realVdoNum } = useVdosSelect()
  const isFull = useMemo(() => fullKey !== -1, [fullKey])

  const onVodFull = (ind: number) => {
    if (fullKey === -1) {
      dispatch(fullAnVdoAction(ind))
    } else {
      dispatch(fullAnVdoAction(fullKey))
    }
  }

  return (
    <GridLauout fullKey={isFull ? vdoMetas.length - 1 : -1}>
      {vdoMetas.map((camera, ind) => (
        <VodBox
          area={`vd${ind + 1}`}
          hidden={
            isFull ? ind < vdoMetas.length - 1 : ind >= realVdoNum || camera.cameraId === undefined
          }
          key={camera.uuid}
        >
          <Ctv
            meta={camera}
            onFull={() => onVodFull(ind)}
            isFull={ind === vdoMetas.length - 1}
            onClose={isFull ? () => onVodFull(-1) : undefined}
            noDragIcon
            isSpecial={specialInd === ind}
          ></Ctv>
        </VodBox>
      ))}
    </GridLauout>
  )
}
