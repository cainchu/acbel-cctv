import styled from 'styled-components';

export default styled.div`
  position: absolute;
  right: 0px;
  left: 0px;
  top: 0px;
  bottom: 0px;
  display: grid;
  grid-template-columns: 1fr 320px 320px;
  grid-template-rows: repeat(4, 215px);
  grid-gap: 10px;
  grid-template-areas: ${({ fullKey }: { fullKey: number }) => {
    const vdx = `vd${fullKey + 1}`;
    return fullKey === -1
      ? `'a vd5 vd1'
  'a b vd2'
  'a b vd3'
  'a vd6 vd4'`
      : `'${vdx} ${vdx} ${vdx}'
  '${vdx} ${vdx} ${vdx}'
  '${vdx} ${vdx} ${vdx}'
  '${vdx} ${vdx} ${vdx}'`;
  }};
  pointer-events: none;
`;
