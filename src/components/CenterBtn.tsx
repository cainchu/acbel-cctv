import styled from 'styled-components';
import { colorValue } from '../styles';

const TilBoxLayout = styled.div`
  width: 100%;
  height: 35px;
  padding: 0 4px;
  background-image: url(/images/title_center_bg.svg);
  font-size: 24px;
  font-weight: 600;
  line-height: 35px;
  letter-spacing: 2.4px;
  color: ${colorValue.bkTex};
`;
const BuildingImg = styled.div`
  width: 320px;
  height: 220px;
  background-image: url(/images/building_center.png);
  cursor: pointer;
`;
const Layout = styled.div`
  width: 320px;
  height: 255px;
`;

export default ({ onClick }: { onClick?: () => void }) => {
  return (
    <Layout onClick={onClick}>
      <TilBoxLayout>戰情室中控中心</TilBoxLayout>
      <BuildingImg></BuildingImg>
    </Layout>
  );
};
