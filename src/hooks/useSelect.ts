import { useAppSelector } from '../store'
import { useMemo } from 'react'
import { pickCameraIdsByMenu } from './getter'

const rootSelect = () => useAppSelector((store) => store.root)
const userSelect = () => useAppSelector((store) => store.user)
const vdosSelect = () => useAppSelector((store) => store.vdos)
const notificationSelect = () => useAppSelector((store) => store.notification)

/**
 * 此函数从根状态返回建筑物、楼层和摄像机。
 * @returns rootSelector 正在从 rootSelect 返回构建、楼层和摄像机。
 */
export const useRootSelector = () => {
  const pushPath = rootSelect().pushPath
  const builds = rootSelect().builds
  const floors = rootSelect().floors
  const cameras = rootSelect().cameras
  const ios = rootSelect().ios
  const menuTree = rootSelect().menuTree
  const subscriptionAlarm = rootSelect().subscriptionAlarm
  const readyPagePath = rootSelect().readyPagePath
  const initDataReady = rootSelect().initDataReady
  const speakerMap = rootSelect().speakerMap
  const message = rootSelect().message
  return {
    pushPath,
    builds,
    floors,
    cameras,
    ios,
    menuTree,
    subscriptionAlarm,
    readyPagePath,
    initDataReady,
    speakerMap,
    message,
  }
}

/**
 * 根據路由的參數取得攝影機的key列表，除了或火警外都有
 * @param path - VIRW_ROUER 类型
 * @param {string} [ind] - 菜单项的索引，即菜单树中菜单项的索引。
 */
export const useCameraIdsByMenu = (path: string, ind?: string) => {
  const { menuTree } = useRootSelector()
  return useMemo(() => pickCameraIdsByMenu(menuTree, path, ind), [menuTree, path, ind])
}

/**
 * 此函数从 uid、权限和 user相關。
 * @returns 具有 uid、permission 和 loginMessage 属性的对象。
 */
export const useUserSelector = () => {
  const uid = userSelect().uid
  const permission = userSelect().permission
  const loginMessage = userSelect().loginMessage
  return { uid, permission, loginMessage }
}

/**
 * Vdos store
 * @returns
 */
export const useVdosSelect = () => {
  const vdoMetas = vdosSelect().vdoMetas
  const fullKey = vdosSelect().fullKey
  const realVdoNum = vdosSelect().realVdoNum
  const pageType = vdosSelect().pageType
  return { vdoMetas, realVdoNum, pageType, fullKey }
}

/**
 * 報警 store
 */
export const useNotificationSelector = () => {
  const notifications = notificationSelect().notifications
  const validNotifications = notificationSelect().validNotifications
  const alarmSoundOff = notificationSelect().alarmSoundOff
  return { notifications, validNotifications, alarmSoundOff }
}
