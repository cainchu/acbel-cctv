import { RefObject, useEffect } from 'react';

export default <T, E>(
  elRef: RefObject<T extends HTMLElement ? any : any>,
  event: string,
  fn: (e: E) => void,
) => {
  useEffect(() => {
    const el = elRef.current;
    if (el) {
      el.addEventListener(event, fn);
    }
    return () => {
      if (el) {
        el.removeEventListener(event, fn);
      }
    };
  }, [fn]);
};
