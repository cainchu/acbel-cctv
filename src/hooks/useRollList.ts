import { useEffect, useState, useCallback } from 'react';
import Lazy from 'lazy.js';

/* 滾動列表控制 */
export default <T>(list: T[], showNum: number = 1) => {
  const [viewlist, setViewList] = useState([] as T[]);
  const [ind, setInd] = useState(0);

  /* 滾動 */
  const next = useCallback(
    (_add: number) => {
      let _ind = ind + _add;
      const _len = list.length;
      if (_len <= showNum || _ind < 0) {
        //以下不超過條件
        _ind = 0;
      } else if (_ind > _len - showNum) {
        _ind = _len - showNum;
      }
      setInd(_ind);
    },
    [ind, list],
  );

  useEffect(() => {
    /* 取回特定數量的list */
    setViewList(Lazy(list).rest(ind).first(showNum).toArray());
  }, [list, ind, showNum]);

  return {
    viewlist,
    next,
  };
};
