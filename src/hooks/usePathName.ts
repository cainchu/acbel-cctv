import { useMemo } from 'react'
import { useLocation } from 'react-router-dom'
import { useRootSelector } from './useSelect'
import { adminSettingMenu } from './useSettingMenuPermission'

/* 老子怕煩不怕累，超級無敵列舉王，麵包削 */

export default () => {
  const { pathname } = useLocation()
  const { menuTree } = useRootSelector()
  const path = useMemo(() => {
    switch (pathname) {
      case '/admin':
        return '戰情中心首頁'
      case '/hanging-door/0':
        return `${menuTree?.equipment.title} / ${menuTree?.equipment.system[0].title} / ${menuTree?.equipment.system[0].items[0].title}`
      case '/hanging-door/1':
        return `${menuTree?.equipment.title} / ${menuTree?.equipment.system[0].title} / ${menuTree?.equipment.system[0].items[1].title}`
      case '/hanging-door/2':
        return `${menuTree?.equipment.title} / ${menuTree?.equipment.system[0].title} / ${menuTree?.equipment.system[0].items[2].title}`
      case '/hanging-door/3':
        return `${menuTree?.equipment.title} / ${menuTree?.equipment.system[0].title} / ${menuTree?.equipment.system[0].items[3].title}`
      case '/hanging-door/4':
        return `${menuTree?.equipment.title} / ${menuTree?.equipment.system[0].title} / ${menuTree?.equipment.system[0].items[4].title}`
      case '/hanging-door/5':
        return `${menuTree?.equipment.title} / ${menuTree?.equipment.system[0].title} / ${menuTree?.equipment.system[0].items[5].title}`
      case '/hass-room/0':
        return `${menuTree?.equipment.title} / ${menuTree?.equipment.system[1].title} / ${menuTree?.equipment.system[1].items[0].title}`
      case '/unloading-zone/0':
        return `${menuTree?.equipment.title} / ${menuTree?.equipment.system[2].title} / ${menuTree?.equipment.system[2].items[0].title}`
      case '/unloading-zone/1':
        return `${menuTree?.equipment.title} / ${menuTree?.equipment.system[2].title} / ${menuTree?.equipment.system[2].items[1].title}`
      case '/burnIn-test/0':
        return `${menuTree?.equipment.title} / ${menuTree?.equipment.system[3].title} / ${menuTree?.equipment.system[3].items[0].title}`
      case '/personnel/0':
        return `${menuTree?.equipment.title} / ${menuTree?.equipment.system[4].title} / ${menuTree?.equipment.system[4].items[0].title}`
      case '/personnel/1':
        return `${menuTree?.equipment.title} / ${menuTree?.equipment.system[4].title} / ${menuTree?.equipment.system[4].items[1].title}`
      case '/personnel/2':
        return `${menuTree?.equipment.title} / ${menuTree?.equipment.system[4].title} / ${menuTree?.equipment.system[4].items[2].title}`
      case '/fire/a/c':
        return `${menuTree?.equipment.title} / ${menuTree?.equipment.system[5].title} / ${menuTree?.equipment.system[5].items[0].title}`
      case '/fire/a/0':
        return `${menuTree?.equipment.title} / ${menuTree?.equipment.system[5].title} / ${menuTree?.equipment.system[5].items[1].title}`
      case '/fire/a/1':
        return `${menuTree?.equipment.title} / ${menuTree?.equipment.system[5].title} / ${menuTree?.equipment.system[5].items[2].title}`
      case '/fire/a/2':
        return `${menuTree?.equipment.title} / ${menuTree?.equipment.system[5].title} / ${menuTree?.equipment.system[5].items[3].title}`
      case '/fire/a/3':
        return `${menuTree?.equipment.title} / ${menuTree?.equipment.system[5].title} / ${menuTree?.equipment.system[5].items[4].title}`
      case '/fire/a/4':
        return `${menuTree?.equipment.title} / ${menuTree?.equipment.system[5].title} / ${menuTree?.equipment.system[5].items[5].title}`
      case '/fire/a/5':
        return `${menuTree?.equipment.title} / ${menuTree?.equipment.system[5].title} / ${menuTree?.equipment.system[5].items[6].title}`
      case '/fire/a/6':
        return `${menuTree?.equipment.title} / ${menuTree?.equipment.system[5].title} / ${menuTree?.equipment.system[5].items[7].title}`
      case '/fire/a/7':
        return `${menuTree?.equipment.title} / ${menuTree?.equipment.system[5].title} / ${menuTree?.equipment.system[5].items[8].title}`
      case '/fire/a/8':
        return `${menuTree?.equipment.title} / ${menuTree?.equipment.system[5].title} / ${menuTree?.equipment.system[5].items[9].title}`
      case '/fire/a/9':
        return `${menuTree?.equipment.title} / ${menuTree?.equipment.system[5].title} / ${menuTree?.equipment.system[5].items[10].title}`
      case '/fire/b/1':
        return `${menuTree?.equipment.title} / ${menuTree?.equipment.system[5].title} / ${menuTree?.equipment.system[5].items[11].title}`
      case '/fire/b/2':
        return `${menuTree?.equipment.title} / ${menuTree?.equipment.system[5].title} / ${menuTree?.equipment.system[5].items[12].title}`
      case '/fire/b/3':
        return `${menuTree?.equipment.title} / ${menuTree?.equipment.system[5].title} / ${menuTree?.equipment.system[5].items[13].title}`
      case '/carriage/0':
        return `${menuTree?.production.title} / ${menuTree?.production.system[0].title} / ${menuTree?.production.system[0].items[0].title}`
      case '/assemble/0':
        return `${menuTree?.production.title} / ${menuTree?.production.system[1].title} / ${menuTree?.production.system[1].items[0].title}`
      case '/smtpcba/0':
        return `${menuTree?.production.title} / ${menuTree?.production.system[2].title} / ${menuTree?.production.system[2].items[0].title}`
      case '/smtpcba/1':
        return `${menuTree?.production.title} / ${menuTree?.production.system[2].title} / ${menuTree?.production.system[2].items[1].title}`
      case '/wave-soldering/0':
        return `${menuTree?.production.title} / ${menuTree?.production.system[3].title} / ${menuTree?.production.system[3].items[0].title}`
      case '/wave-soldering/1':
        return `${menuTree?.production.title} / ${menuTree?.production.system[3].title} / ${menuTree?.production.system[3].items[1].title}`
      case '/piar':
        return `${menuTree?.pier.title}`
      case '/setting-ai':
        return adminSettingMenu[0].itemName
      case '/setting-alert':
        return adminSettingMenu[1].itemName
      case '/setting-log':
        return adminSettingMenu[2].itemName
      case '/setting-push':
        return adminSettingMenu[3].itemName
      case '/setting-schedule':
        return adminSettingMenu[4].itemName
      default:
        return '戰情中心首頁'
    }
  }, [pathname, menuTree])

  return { path }
}
