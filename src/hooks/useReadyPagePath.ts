import { useEffect } from 'react';
import { useAppDispatch } from '../store';
import { pathReadyAction } from '../store/saga';
export default () => {
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(pathReadyAction(window.location.pathname));
    return () => {
      dispatch(pathReadyAction(''));
    };
  }, [location.pathname]);
};
