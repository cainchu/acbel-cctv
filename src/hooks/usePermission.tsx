//根據權限返回可進入的頁面

import { useMemo } from 'react';
import { hasPermission } from '../utils/permission';
import { useUserSelector } from './useSelect';

export default function () {
  const { permission } = useUserSelector();
  const {
    isPermissionAB,
    isPermissionEquipment,
    isPermissionProduction,
    isPermissionPier,
  } = useMemo(() => {
    return {
      isPermissionAB: hasPermission('AB')(permission), //管理這權限
      isPermissionEquipment: hasPermission('ABC')(permission), //設備監看權限
      isPermissionProduction: hasPermission('AD')(permission), //生產安全權限
      isPermissionPier: hasPermission('ABE')(permission), //碼頭權限
    };
  }, [permission]);

  return {
    permission,
    isPermissionAB,
    isPermissionEquipment,
    isPermissionProduction,
    isPermissionPier,
  };
}
