import { useAppDispatch } from '../store';
import { setReadyPagePath } from '../store/root';

export default () => {
  const dispatch = useAppDispatch();
  const dispatchReadyPagePath = (path: string = '') => {
    dispatch(setReadyPagePath(path));
  };
  return { dispatchReadyPagePath /* 目前ready頁面 */ };
};
