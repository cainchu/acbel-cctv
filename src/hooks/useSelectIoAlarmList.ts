import { useAppSelector } from '../store';

const IoAlarmListSelect = () => useAppSelector((store) => store.ioAlarmList);

export const useIoAlarmListSelector = () => {
  const ioAlarmList = IoAlarmListSelect().ioAlarmList;
  return { ioAlarmList };
};
