/* 頂部選單的權限 */

import { useMemo } from 'react'
import { MAIN_ROUER } from '../pages/MainRouter'
import { useAppSelector } from '../store'
import { hasPermission } from '../utils/permission'

export const adminSettingMenu = [
  {
    itemName: '智能功能開關與設定',
    router: MAIN_ROUER.SettingAi,
    enable: true,
  },
  {
    itemName: '告警事件開關設定',
    router: MAIN_ROUER.SettingAlert,
    enable: true,
  },
  {
    itemName: '告警事件Log查詢與影像下載',
    router: MAIN_ROUER.SettingLog,
    enable: true,
  },
  {
    itemName: '推播設定',
    router: MAIN_ROUER.SettingPush,
    enable: true,
  },
  {
    itemName: '排程控制設定',
    router: MAIN_ROUER.SettingSchedule,
    enable: true,
  },
]

interface I_Menu {
  router: string
}

const isPermissionAB = hasPermission('AB')
const isPermissionPier = hasPermission('E')

export default () => {
  const permission = useAppSelector((state) => state.user.permission)
  const permissionSetting = useMemo(() => {
    return adminSettingMenu.map((menu) => {
      menu.enable = teatMenuEnable(menu, permission)
      return menu
    })
  }, [permission])
  return permissionSetting
}

function teatMenuEnable(menu: I_Menu, permission: string) {
  if (isPermissionAB(permission)) {
    /* AB權限 */
    return true
  } else if (isPermissionPier(permission)) {
    /* C權限 */
    return menu.router === MAIN_ROUER.SettingLog
  }
  return false
}
