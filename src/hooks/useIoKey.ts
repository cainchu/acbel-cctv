/* 使用 io key 取的 應對應的攝影機 */

import { useCallback } from 'react'
import Lazy from 'lazy.js'
import { useRootSelector } from './useSelect'
import { camerasGetterByKey, iosGetterByKey } from '../store/root'

export default () => {
  const { ios, cameras } = useRootSelector()
  const getCameraIdByIoKey = useCallback(
    (ioKey: string) => {
      const io = iosGetterByKey(ios, ioKey)
      return io ? camerasGetterByKey(cameras, io.cameraKey) : null
    },
    [ios, cameras],
  )
  const getIoByIoKey = useCallback(
    (ioKey: string) => {
      return iosGetterByKey(ios, ioKey)
    },
    [ios],
  )

  return {
    getCameraIdByIoKey /* 使用 io key 取得 應對應的攝影機 */,
    getIoByIoKey /* 使用 io key 取得 io */,
  }
}
