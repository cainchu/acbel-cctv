import { useAppSelector } from '../store';

const IoListSelect = () => useAppSelector((store) => store.ioList);

export const useIoListSelector = () => {
  const ioList = IoListSelect().ioList;
  return { ioList };
};
