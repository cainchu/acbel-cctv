import { useAppSelector } from '../store';

const aiStatusSelect = () => useAppSelector((store) => store.aiStatusList);

export const useAiStstusSelector = () => {
  const aiStatusList = aiStatusSelect().aiStatusList;
  return { aiStatusList };
};
