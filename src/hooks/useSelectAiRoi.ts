import { useAppSelector } from '../store';

const aiRoiSelect = () => useAppSelector((store) => store.aiRoiList);

export const useAiRoiSelector = () => {
  const aiRoiList = aiRoiSelect().aiRoiList;
  const aiMenuList = aiRoiSelect().aiMenuList;
  return { aiRoiList, aiMenuList };
};
