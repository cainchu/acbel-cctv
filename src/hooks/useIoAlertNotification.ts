import { useCallback } from 'react'
import { useAppDispatch } from '../store'
import { alertNotificationAction, removeAlertNotificationAction } from '../store/saga'
import { AlarmType } from '../utils/AlarmSystem'
import useIoKey from './useIoKey'

const useDispatchNotificationBase = () => {
  const dispatch = useAppDispatch()
  const dispatchNotification = ({
    id,
    off,
    time,
    type,
  }: {
    id: string
    off: boolean
    time: number
    type: string
  }) => {
    dispatch(
      greaterNotificationAction(off)({
        id,
        time,
        type,
      }),
    )
  }
  return {
    dispatchNotification,
  }
}

export const useDispatchNotification = () => {
  const { dispatchNotification } = useDispatchNotificationBase()
  const { getCameraIdByIoKey } = useIoKey()

  const dispatchNotificationByType = ({
    ioKey,
    off,
    time,
    type,
  }: {
    type: string
    ioKey: string
    off: boolean
    time: number
  }) => {
    dispatchNotification({
      off,
      time,
      id: (getCameraIdByIoKey(ioKey)?.id || '') as string,
      type,
    })
  }

  function fireIoKeyToFloorKey(ioKey: string) {
    switch (ioKey) {
      case 'A-B2-firealarm':
        return 'ac'
      case 'A-B1-firealarm':
        return 'a0'
      default:
        return `${ioKey.charAt(0).toLowerCase()}${ioKey.charAt(2)}`
    }
  }

  const dispatchFire = ({ id, off, time }: { id: string; off: boolean; time: number }) => {
    dispatchNotification({
      off,
      time,
      id: fireIoKeyToFloorKey(id),
      type: AlarmType.FIRE,
    })
  }

  const dispatchHass = ({ off, time }: { off: boolean; time: number }) => {
    dispatchNotification({ off, time, id: AlarmType.HASS, type: AlarmType.HASS })
  }

  /* 舊的ioKey已棄用 */
  // const burnCameraId /* 燒機室攝影機ID */ = useMemo(() => {
  //   return getCameraIdByIoKey('A-4F-co2sensor')?.id || ''
  // }, [getCameraIdByIoKey])

  const getBurnCameraId /* 燒機室攝影機ID */ = useCallback(
    (ioKey: string) => {
      return getCameraIdByIoKey(ioKey)?.id || ''
    },
    [getCameraIdByIoKey],
  )

  const dispatchBurn = ({
    off,
    time,
    id,
    type,
  }: {
    id: string
    off: boolean
    time: number
    type: string
  }) => {
    dispatchNotification({ off, time, id: getBurnCameraId(id), type })
  }

  const dispatchDOOR = ({ id, off, time }: { id: string; off: boolean; time: number }) => {
    dispatchNotificationByType({
      ioKey: id,
      off,
      time,
      type: AlarmType.DOOR,
    })
  }

  const dispatchPAIR = ({ id, off, time }: { id: string; off: boolean; time: number }) => {
    dispatchNotificationByType({
      ioKey: id,
      off,
      time,
      type: AlarmType.PAIR,
    })
  }

  const dispatchSOS = ({ id, off, time }: { id: string; off: boolean; time: number }) => {
    off &&
      dispatchNotificationByType({
        ioKey: id,
        off,
        time,
        type: AlarmType.SOS,
      }) /* SOS不使用false關閉事件 */
  }

  return {
    dispatchFire /* 火警告警Action */,
    dispatchHass /* HassRoom告警Action */,
    dispatchBurn /* 燒機室告警Action */,
    dispatchDOOR /* 門開啟告警Action */,
    dispatchPAIR /* 貨車靠岸告警Action */,
    dispatchSOS /* SOS告警Action */,
  }
}

function greaterNotificationAction(off: boolean) {
  return off ? alertNotificationAction : removeAlertNotificationAction
}
