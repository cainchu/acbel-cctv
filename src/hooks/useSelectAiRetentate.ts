import { useAppSelector } from '../store';

const AiRetentateSelect = () => useAppSelector((store) => store.aiRetentate);

export const useAiRetentateSelector = () => {
  const aiRetentateUrl = AiRetentateSelect().aiRetentateUrl;
  return { aiRetentateUrl };
};
