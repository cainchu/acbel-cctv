import { method, multi } from '@arrows/multimethod'

/**
 * 取的menuTree中的特定功能的攝影機key 列舉
 */
export const pickCameraIdsByMenu = multi(
  (menuTree: menuTreeType, path: string, ind?: string) => path,
  method(
    '/piar' /* TODO:值得探討! 居然無法用 VIRW_ROUER 往後再思考必要性.. */,
    (menuTree: menuTreeType) => {
      return menuTree.pier.cameraKeys
    },
  ),
  method('/hanging-door', (menuTree: menuTreeType, path: string, ind?: string) => {
    return menuTree.equipment.system[0].items[Number(ind)].cameraKeys
  }),
  method('/hass-room', (menuTree: menuTreeType, path: string, ind?: string) => {
    return menuTree.equipment.system[1].items[Number(ind)].cameraKeys
  }),
  method('/unloading-zone', (menuTree: menuTreeType, path: string, ind?: string) => {
    return menuTree.equipment.system[2].items[Number(ind)].cameraKeys
  }),
  method('/burnIn-test', (menuTree: menuTreeType, path: string, ind?: string) => {
    return menuTree.equipment.system[3].items[Number(ind)].cameraKeys
  }),
  method('/personnel', (menuTree: menuTreeType, path: string, ind?: string) => {
    return menuTree.equipment.system[4].items[Number(ind)].cameraKeys
  }),
  method('/carriage', (menuTree: menuTreeType, path: string, ind?: string) => {
    return menuTree.production.system[0].items[Number(ind)].cameraKeys
  }),
  method('/assemble', (menuTree: menuTreeType, path: string, ind?: string) => {
    return menuTree.production.system[1].items[Number(ind)].cameraKeys
  }),
  method('/smtpcba', (menuTree: menuTreeType, path: string, ind?: string) => {
    return menuTree.production.system[2].items[Number(ind)].cameraKeys
  }),
  method('/wave-soldering', (menuTree: menuTreeType, path: string, ind?: string) => {
    return menuTree.production.system[3].items[Number(ind)].cameraKeys
  }),
  method(() => {
    return []
  }),
)
