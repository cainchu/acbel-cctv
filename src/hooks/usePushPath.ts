import { useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import { useRootSelector } from './useSelect';

export default () => {
  const navigat = useNavigate();
  const { pushPath } = useRootSelector();
  useEffect(() => {
    if (pushPath !== null) {
      navigat(pushPath);
    }
  }, [pushPath]);
};
