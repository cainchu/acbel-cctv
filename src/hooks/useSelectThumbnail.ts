import { useAppSelector } from '../store';

const thumbnailSelect = () => useAppSelector((store) => store.thumbnail);

export const useThumbnailSelector = () => {
  const imageUrl = thumbnailSelect().imageUrl;
  return { imageUrl };
};
