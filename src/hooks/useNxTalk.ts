import { useEffect, useRef, useState } from 'react'
import { useBoolean } from 'usehooks-ts'
import { MicrophoneProcessor } from 'microphone-processor'

export default (wsServer: string, delayClose: number = 3000) => {
  const { value: off, setValue: setSpeakerOff } = useBoolean(false)
  const { value: wsOff, setValue: setWsOff } = useBoolean(false)
  const [cameraId, setCameraId] = useState('')
  const [linkState, setLinkState] = useState(false)
  const [microphoneProcessor, setMicrophoneProcessor] = useState<MicrophoneProcessor>()
  const wsRef = useRef<WebSocket | undefined>()
  const clearTimeKeyRef = useRef<NodeJS.Timeout>()

  useEffect(() => {
    clearTimeout(clearTimeKeyRef.current)
    if (off) {
      setWsOff(true)
    } else {
      clearTimeKeyRef.current = setTimeout(() => {
        setWsOff(false)
      }, delayClose)
    }
  }, [off, delayClose])

  useEffect(() => {
    if (cameraId && wsOff) {
      const ws = new WebSocket(
        `${wsServer}/api/http_audio?camera_id=${cameraId}&format=f32le&sample_rate=44100&channels=1`,
      )
      ws.binaryType = 'arraybuffer'
      ws.onopen = function () {
        setLinkState(true)
      }
      ws.onclose = function () {
        setLinkState(false)
      }
      ws.onerror = function (err) {
        setLinkState(false)
        console.error(err)
      }
      wsRef.current = ws
    } else {
      wsRef.current?.close()
    }
    return () => {
      if (wsRef.current) {
        wsRef.current.close()
      }
    }
  }, [cameraId, wsOff])

  useEffect(() => {
    if (linkState) {
      navigator.mediaDevices
        .getUserMedia({
          audio: true,
        })
        .then(function (stream: MediaStream) {
          setMicrophoneProcessor(new MicrophoneProcessor(stream))
        })
        .catch(function (err) {
          console.error(err)
        })
    } else {
      setMicrophoneProcessor(undefined)
    }
  }, [linkState])

  useEffect(() => {
    if (microphoneProcessor) {
      microphoneProcessor.on('buffer', (data: Float32Array) => {
        wsRef.current?.send(data)
      })
    }
    return () => {
      microphoneProcessor?.destroy()
    }
  }, [microphoneProcessor])
  return { cameraId, setCameraId, setSpeakerOff }
}
