/* 取得SMT點位設備相關 */

import { useEffect, useMemo, useState } from 'react'
import Lazy from 'lazy.js'
import { BKmeta } from '../components/FloorBgImg'
import { smpApi } from '../utils/iotApi'

interface I_SMT_ITEM {
  status: 0 | 1
  index: number
}

interface I_SMT_META {
  equipInfo: [I_SMT_ITEM]
}

export const useSmtApi = () => {
  const [selectOb, setSelectOb] = useState<BKmeta | null>(null)
  const line = useMemo(() => (selectOb ? selectOb.title.charAt(0) : null), [selectOb])
  const index = useMemo(() => (selectOb ? selectOb.index : null), [selectOb])
  const [meta, setMeta] = useState<I_SMT_META | null>(null)
  const [pointLinght, setPointLinght] = useState<0 | 1 | undefined>(undefined)
  useEffect(() => {
    line && callSmpApi(line)
    const _timeLoop = setInterval(() => {
      line && callSmpApi(line)
    }, 5000) //5秒call一次
    async function callSmpApi(_line: string) {
      try {
        const { data } = await smpApi(
          _line,
          new Date().getTime() - 1000 * 6,
          new Date().getTime() - 1000,
        )
        setMeta(data)
      } catch (error) {
        setMeta(null)
      }
    }
    return () => {
      clearInterval(_timeLoop)
    }
  }, [line])
  useEffect(() => {
    if (meta && index) {
      const item = Lazy(meta.equipInfo).find((item) => item.index === index)
      item ? setPointLinght(item.status) : setPointLinght(undefined)
    }
    return () => {
      setMeta(null)
    }
  }, [meta, index])
  return {
    setSelectOb,
    pointLinght,
  }
}
