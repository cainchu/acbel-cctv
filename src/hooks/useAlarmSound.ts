import { useEffect } from 'react';
import { useBoolean } from 'usehooks-ts';

const sound = new Audio();
sound.loop = true;

export default (mapPath: string) => {
  const { value, setValue: soundAwitch } = useBoolean(false);
  useEffect(() => {
    sound.src = mapPath;
    value ? sound.play() : sound.pause();
  }, [value, mapPath]);

  return { soundAwitch };
};
