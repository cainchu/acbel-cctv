import { useAppSelector } from '../store';

const BroadcastSelect = () => useAppSelector((store) => store.broadcast);

export const useBroadcastSelector = () => {
  const broadcast = BroadcastSelect().broadcast;
  return { broadcast };
};
