/* 選取NX的路由 */

import { useCallback } from 'react'
import { camerasGetterById } from '../store/root'
import { useRootSelector } from './useSelect'

export const useNxServiceSelect = () => {
  const { cameras } = useRootSelector()
  const getNxRouting = useCallback(
    (uuid: string) => {
      const _camera = camerasGetterById(cameras, uuid)
      // console.log('_camera', _camera)
      try {
        return _camera.info?.source === 'NVR2' ? '/nx2' : '/nx'
      } catch (error) {
        // console.log('error', error)
      }
    },
    [cameras],
  )
  return { getNxRouting }
}
