import { useCallback, useMemo } from 'react'
import Lazy from 'lazy.js'
import { useRootSelector } from './useSelect'
import { camerasGetterById } from '../store/root'

/**
 * 返回位于特定建筑物特定楼层的摄像机列表。
 * @param {string} building - 建物
 * @param {string} floor - 樓層
 * @returns 对象数组。
 */
export const useGetCameraByFloor = (building: string, floor: string) => {
  const { cameras } = useRootSelector()
  const cameraList = useMemo(
    () =>
      Lazy<treeDataCameraType>(cameras)
        .filter((camera) => camera.floor === `${building}${floor}`)
        .toArray(),
    [cameras, building, floor],
  )
  return cameraList
}

export const useGetCameras = () => {
  const { cameras } = useRootSelector()
  const getCameraByUuid = useCallback(
    (uuid: string) => {
      return camerasGetterById(cameras, uuid)
    },
    [cameras],
  )
  return { getCameraByUuid /* 使用 uuid 取得攝影機物件 */ }
}
