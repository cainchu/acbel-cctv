/* TODO:測試推送告警 */
import { useEventListener } from 'usehooks-ts'
import { useAppDispatch } from '../store'
import { alertNotificationAction, removeAlertNotificationAction } from '../store/saga'
import { AlarmType } from '../utils/AlarmSystem'

const testStrings = ['火警事件', '溫度過高事件', '吊掛門開啓事件', '重要區域人員入侵', '貨櫃車靠站']

export default () => {
  const dispatch = useAppDispatch()
  const pushMessage = () => {}
  useEventListener('keyup', ({ code }) => {
    if (code === 'KeyM') {
      pushMessage()
    } else if (code === 'KeyP') {
      //碼頭報警
      dockAlarm()
    } else if (code === 'KeyD') {
      //吊掛門開啟
      doorOpenAlarm()
    } else if (code === 'KeyF') {
      //火災
      fire()
    } else if (code === 'KeyH') {
      //Hass
      hass()
    } else if (code === 'KeyT') {
      //SOS
      talk()
    } else if (code === 'KeyB') {
      //氣體消防
      burn()
    } else if (code === 'KeyG') {
      //人員入侵
      dispatch(
        alertNotificationAction({
          id: '23d91a92-9bbf-fa26-1e28-5cd07fe9d822',
          time: new Date().getTime(),
          type: AlarmType.FENCES,
        }),
      )
    }
  })
  function burn() {
    dispatch(
      alertNotificationAction({
        id: 'dd0f5117-c13d-71ba-975e-22318675f143', //a4-1
        time: new Date().getTime(),
        type: AlarmType.BURN_1,
      }),
    )
    // dispatch(
    //   alertNotificationAction({
    //     id: 'dd0f5117-c13d-71ba-975e-22318675f143', //a4-1
    //     time: new Date().getTime(),
    //     type: AlarmType.BURN_2,
    //   }),
    // )
  }
  //SOS
  function talk() {
    dispatch(
      alertNotificationAction({
        id: '9104ef19-a16c-b3ef-1f4b-e0e14a8bb62e',
        time: new Date().getTime(),
        type: AlarmType.SOS,
      }),
    )
  }
  //Hass
  function hass() {
    dispatch(
      alertNotificationAction({
        id: AlarmType.HASS,
        time: new Date().getTime(),
        type: AlarmType.HASS,
      }),
    )
    setTimeout(() => {
      dispatch(
        alertNotificationAction({
          id: '56e09f8c-8525-033e-ab12-21b66a121b00',
          time: new Date().getTime(),
          type: AlarmType.NO_CLEAR,
        }),
      )
    }, 1000)
  }
  //火災
  function fire() {
    dispatch(
      alertNotificationAction({
        id: 'a1',
        time: new Date().getTime(),
        type: AlarmType.FIRE,
      }),
    )
  }
  //吊掛門開啟
  function doorOpenAlarm() {
    dispatch(
      alertNotificationAction({
        id: '8949b967-98c2-735a-473a-b656f4bb71d0',
        time: new Date().getTime(),
        type: AlarmType.DOOR,
      }),
    )
    setTimeout(() => {
      dispatch(
        removeAlertNotificationAction({
          id: '8949b967-98c2-735a-473a-b656f4bb71d0',
          time: new Date().getTime(),
          type: AlarmType.DOOR,
        }),
      )
    }, 3000)
  }
  //碼頭報警
  function dockAlarm() {
    dispatch(
      alertNotificationAction({
        id: 'cd9a43c5-43a7-db88-8505-95664d09b6e5',
        time: new Date().getTime(),
        type: AlarmType.PAIR,
      }),
    )

    setTimeout(() => {
      dispatch(
        alertNotificationAction({
          id: 'cd9a43c5-43a7-db88-8505-95664d09b6e5',
          time: new Date().getTime(),
          type: AlarmType.FENCES,
        }),
      )
    }, 2000)

    setTimeout(() => {
      dispatch(
        removeAlertNotificationAction({
          id: 'cd9a43c5-43a7-db88-8505-95664d09b6e5',
          time: new Date().getTime(),
          type: AlarmType.FENCES,
        }),
      )
    }, 4000)

    setTimeout(() => {
      dispatch(
        removeAlertNotificationAction({
          id: 'cd9a43c5-43a7-db88-8505-95664d09b6e5',
          time: new Date().getTime(),
          type: AlarmType.PAIR,
        }),
      )
    }, 6000)
  }
}
