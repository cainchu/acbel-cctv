import { useEffect } from 'react';
import { useAppDispatch } from '../store';
import { setPageType, VDO_PAGE_TYPE } from '../store/vdos';

export default (type: VDO_PAGE_TYPE) => {
  const dispatch = useAppDispatch();
  useEffect(() => {
    dispatch(setPageType(type));
  }, []);
};
