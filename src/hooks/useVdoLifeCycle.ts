import { useEffect } from 'react';
import { useAppDispatch } from '../store';
import { setVdoLifeCycle } from '../store/vdos';

/**
 * Vdo物件播放生命週期
 */
export default (meta: I_CtvMeta) => {
  const { uuid } = meta;
  const dispatch = useAppDispatch();
  useEffect(() => {
    dispatch(setVdoLifeCycle({ uuid, isPlaying: true }));
    return () => {
      dispatch(setVdoLifeCycle({ uuid, isPlaying: false }));
    };
  }, []);
};
