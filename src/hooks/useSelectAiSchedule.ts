import { useAppSelector } from '../store';

const AiSchedule = () => useAppSelector((store) => store.aiSchedule);

export const useAiScheduleSelector = () => {
  const aiScheduleSelect = AiSchedule().aiScheduleSelect;
  const aiScheduleUpdate = AiSchedule().aiScheduleUpdate;
  return { aiScheduleSelect, aiScheduleUpdate };
};
