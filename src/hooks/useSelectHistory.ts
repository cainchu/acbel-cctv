import { useMemo, useRef } from 'react';
import Lazy from 'lazy.js';
import { useAppSelector } from '../store';
import { useRootSelector, useUserSelector } from './useSelect';
import { camerasGetterByKey } from '../store/root';
import { hasPermission } from '../utils/permission';

const historySelect = () =>
  useAppSelector((store) => store.history);

export const useHistorySelector = () => {
  const historyList = historySelect().historyList;
  const historyTotalLength = historySelect().historyTotalLength;
  const historyStatus = historySelect().historyStatus;
  return { historyList, historyTotalLength, historyStatus };
};

/**
 * 可以查詢的歷史紀錄列表
 */
export const useHistoryCameraSelector = () => {
  const { permission } = useUserSelector();
  const { cameras, menuTree } = useRootSelector();
  const cameraRef = useRef(cameras);
  const menuTreeRef = useRef(menuTree);
  return useMemo(() => {
    return permission === 'E'
      ? Lazy(menuTreeRef.current?.pier.cameraKeys || [])
          .map((key) => camerasGetterByKey(cameraRef.current, key))
          .filter((camera) => !!camera)
          .toArray()
      : isPermissionAB(permission)
      ? cameras
      : [];
  }, [permission]);
};

/* A B 權限 */
function isPermissionAB(permission: string): boolean {
  return hasPermission('AB')(permission);
}
