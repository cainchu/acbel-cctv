import { useEffect, useRef, useState } from 'react'
import { useAppDispatch } from '../store'
import useIoKey from '../hooks/useIoKey'
import AlarmSystem, { AlarmType } from '../utils/AlarmSystem'
import { alertNotificationAction, removeAlertNotificationAction } from '../store/saga'
import { convertBigNumToM } from '../utils/tools'
import { useDispatchNotification } from './useIoAlertNotification'
import { useGetCameras } from './useGetCameraList'
// import fake from '../../public/fake.json'

enum LINK {
  INIT,
  OPEN,
  CLOSE = -1,
}

interface I_CameraEvent {
  id: string
  off?: boolean
  time: number
}

const alarmSystem = new AlarmSystem([], [])

//TODO: 優化: 登入前不需要監控
export default (
  socketSerice: string = `/ivapControl/analysisResult/alarm`,
  aiChkMeta: I_AlarmMeta,
) => {
  const { host, protocol } = window.location
  const dispatch = useAppDispatch()
  const wsRef = useRef<WebSocket>()
  const [alarmList, setAlarmList] = useState([])
  const [link, setLink] = useState<LINK>(LINK.INIT)

  const createWs = () => {
    // const { cmera, io } = fake
    // setTimeout(() => {
    //   alarmSystem.setStatus(convertCamerasToM(cmera), convertIoToM(io))
    // }, 1000)
    // return

    if (!wsRef.current) {
      wsRef.current = new WebSocket(
        `${protocol === 'https:' ? 'wss' : 'ws'}://${host}${socketSerice}`,
      )
    }
    wsRef.current.onopen = () => {
      setLink(LINK.OPEN)
    }
    wsRef.current.onclose = () => {
      setLink(LINK.CLOSE)
    }
    wsRef.current.onmessage = (e: MessageEvent) => {
      /* 取回告警狀態 */
      const { cmera, io } = JSON.parse(e.data) as AlarmMetaResType
      alarmSystem.setStatus(convertCamerasToM(cmera), convertIoToM(io))
    }
    wsRef.current.onerror = (e) => {
      console.error(e)
    }
  }

  const { getCameraByUuid } = useGetCameras()
  const { getIoByIoKey } = useIoKey()

  const {
    dispatchFire /* 火警告警Action */,
    dispatchHass /* HassRoom告警Action */,
    dispatchBurn /* 燒機室告警Action */,
    dispatchDOOR /* 門開啟告警Action */,
    dispatchPAIR /* 貨車靠岸告警Action */,
    dispatchSOS,
  } = useDispatchNotification()

  useEffect(() => {
    alarmSystem.on(AlarmType.FENCES, ({ id, off, time }: I_CameraEvent) => {
      const { cameraKey } = getCameraByUuid(id) || {}
      off
        ? dispatch(alertNotificationAction({ id, time, type: getFencesTypeBycameraKey(cameraKey) }))
        : dispatch(
            removeAlertNotificationAction({ id, time, type: getFencesTypeBycameraKey(cameraKey) }),
          )
    })
    alarmSystem.on(AlarmType.RETENTETE, ({ id, off, time }: I_CameraEvent) => {
      off
        ? dispatch(alertNotificationAction({ id, time, type: AlarmType.RETENTETE }))
        : dispatch(
            removeAlertNotificationAction({
              id,
              time,
              type: AlarmType.RETENTETE,
            }),
          )
    })
    alarmSystem.on(AlarmType.SOS, ({ id, off, time }: I_CameraEvent) => {
      off
        ? dispatch(alertNotificationAction({ id, time, type: AlarmType.SOS }))
        : '' /* SOS不使用false關閉事件 */
    })
    alarmSystem.on('io', ({ id, off, time }: { id: string; off: boolean; time: number }) => {
      const io = getIoByIoKey(id)
      switch (io.type) {
        case AlarmType.FIRE:
          if (id === 'A-1F-firealarm') {
            //如果A1火災連動影響b1 b2
            dispatchFire({ id: 'A-B2-firealarm', off, time })
            dispatchFire({ id: 'A-B1-firealarm', off, time })
          } else if (id === 'B-1F-firealarm') {
            //如果B棟1F火災連動影響 2F 3f
            dispatchFire({ id: 'B-3F-firealarm', off, time })
            dispatchFire({ id: 'B-2F-firealarm', off, time })
          }
          dispatchFire({ id, off, time })
          break
        case AlarmType.HASS:
          dispatchHass({ off, time })
          break
        case AlarmType.BURN_1:
        case AlarmType.BURN_2:
        case AlarmType.BURN_3:
        case AlarmType.BURN_4:
        case AlarmType.BURN_5:
        case AlarmType.BURN_6:
          dispatchBurn({ off, time, id, type: io.type })
          break
        case AlarmType.DOOR:
          dispatchDOOR({ id, off, time })
          break
        case AlarmType.PAIR:
          dispatchPAIR({ id, off, time })
          break
        case AlarmType.SOS:
          dispatchSOS({ id, off, time })
          break
        default:
          console.warn('🏯未定義的告警類型', io)
      }
    })
    return () => {
      alarmSystem.removeAllListners()
    }
  }, [getIoByIoKey, getCameraByUuid])

  useEffect(() => {
    const { camera, io } = aiChkMeta
    alarmSystem.initCameraAlertWatcher(camera)
    alarmSystem.initIoAlertWatcher(io)
  }, [aiChkMeta])

  useEffect(() => {
    let _tmp: NodeJS.Timer
    switch (link) {
      case LINK.INIT:
        createWs()
        break
      case LINK.CLOSE:
        setTimeout(() => {
          createWs()
        }, 3000)
        break
      case LINK.OPEN:
        _tmp = setInterval(() => {
          wsRef.current?.send(JSON.stringify(aiChkMeta))
        }, 2000)
        break
      default:
    }
    return () => {
      clearInterval(_tmp)
    }
  }, [link, aiChkMeta, alarmList])
}

function convertCamerasToM(cameras: I_CameraAlertState[]) {
  return cameras.map((camera) => ({
    ...camera,
    ai_fences_alarm_time: convertBigNumToM(camera.ai_fences_alarm_time + ''),
    ai_retentate_alarm_time: convertBigNumToM(camera.ai_retentate_alarm_time + ''),
  }))
}

function convertIoToM(ios: I_IoAlertState[]) {
  return ios.map((io) => ({
    ...io,
    alarm_time: convertBigNumToM(io.alarm_time + ''),
  }))
}

function getFencesTypeBycameraKey(cameraKey: string | null): string {
  switch (cameraKey) {
    /* hass */
    case 'a1-1':
    case 'a1-2':
    case 'a1-3':
    case 'a1-4':
    /* 碼頭 */
    case 'a2-1':
    case 'a2-2':
      return AlarmType.NO_CLEAR
    default:
  }
  return AlarmType.FENCES
}
