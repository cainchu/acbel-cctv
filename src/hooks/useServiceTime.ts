/* 取回服務器時間 */

import axios from 'axios';
import { useEffect, useRef, useState } from 'react';
const SEC = 1000;
export default () => {
  const timeRef = useRef(new Date().getTime());
  const [time, setTime] = useState(timeRef.current);
  async function getServiceTime() {
    try {
      const { data } = await axios.get('/api/time');
      timeRef.current = data;
    } catch (err) {
      console.warn('🚒', err);
    }
  }
  useEffect(() => {
    getServiceTime();
    const _timeKey = setInterval(() => {
      timeRef.current += SEC;
      setTime(timeRef.current);
    }, SEC);
    return () => {
      clearInterval(_timeKey);
    };
  }, []);
  return { time };
};
